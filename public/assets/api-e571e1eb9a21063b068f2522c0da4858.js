var startPath = "/missions";

angular.module('MC', ['ionic', 'ngResource', 'ngRoute', 'MC.controllers', 'MC.services', 'MC.directives', 'yaMap', 'checklist-model'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
//        $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');

        $stateProvider
            .state('login', {
                url: "/login",
                templateUrl: "login.html",
                controller: 'LoginCtrl'
            })

            .state('recover', {
                url: "/recover",
                templateUrl: "recover.html",
                controller: 'RecoverCtrl'
            })

            .state('tabs', {
                url: "",
                abstract: true,
                templateUrl: "tabs.html"
            })

            .state('tabs.missions', {
                url: "/missions",
                views: {
                    'missions-tab': {
                        templateUrl: 'missions.html',
                        controller: 'MissionsCtrl'
                    }
                }
            })
            .state('tabs.mission', {
                url: "/missions/:missionId",
                views: {
                    'missions-tab': {
                        templateUrl: 'mission.html',
                        controller: 'MissionCtrl'
                    }
                }
            })

            .state('tabs.tasks', {
                url: "/tasks",
                views: {
                    'tasks-tab': {
                        templateUrl: "tasks.html",
                        controller: 'TasksCtrl'
                    }
                }
            })
            .state('tabs.task', {
                url: "/tasks/:taskId",
                views: {
                    'tasks-tab': {
                        templateUrl: "task.html",
                        controller: 'TaskCtrl'
                    }
                }
            })

            .state('tabs.map', {
                url: "/map",
                views: {
                    'map-tab': {
                        templateUrl: "map.html",
                        controller: 'MapTabCtrl'
                    }
                }
            })
        ;

        $urlRouterProvider.otherwise('/missions');
    })

    .run(function ($location, $rootScope, GeolocationService) {
        // TODO get agent data on login and update
//        $rootScope.currentUser = {
//            location: {
//                geometry: {
//                    type: 'Point',
//                    coordinates: [37.6167,55.7500]
//                }
//            }
//        };

        $rootScope.hideTabs = function() {
            $rootScope.$broadcast('tabs.hide');
        };

        $rootScope.showTabs = function() {
            $rootScope.$broadcast('tabs.show');
        };

        $rootScope.seenTimeout = null;

        $rootScope.badges = {
            missions: 0,
            targets: 0,
            tasks: 0
        };

        $rootScope.updateBadges = function() {
            MissionModel
                .all()
                .filter('mission_id', 'not in (?)', 'select mission_id from tasks')
                .filter('login', '=', $rootScope.currentUser.phone)
                .filter('seen', '=', false)
                .list(function(results) {
                    $rootScope.badges.missions = results.length;
                    $rootScope.$apply();
                }
            );

            TaskModel
                .all()
                .filter('mission_id', 'in (?)', 'select id from missions')
                .filter('login', '=', $rootScope.currentUser.phone)
                .filter('seen', '=', false)
                .filter('removed', '=', false)
                .list(function(results) {
                    $rootScope.badges.tasks = results.length;
                    $rootScope.$apply();
                });

            MissionModel
                .all()
                .filter('mission_id', 'not in (?)', 'select mission_id from tasks')
                .filter('seen', '=', false)
                .filter('login', '=', $rootScope.currentUser.phone)
                .filter('target_id', 'is not', 0)
                .list(function(results) {
                    $rootScope.badges.targets = results.length;
                    $rootScope.$apply();
                }
            );
        };

        $rootScope.locateTimeout = null;

        $rootScope.locationUpdater = function() {
            console.log('UPDATING LOCATION');
            GeolocationService.update();

            $rootScope.locateTimeout = setTimeout(function() {
                $rootScope.locationUpdater();
            }, 1000 * 60 * 5);
        };

        $rootScope.stopLocationUpdater = function() {
            clearTimeout($rootScope.locateTimeout);
        };

//        TODO locate after login
        if ($rootScope.currentUser)
        {
            $rootScope.updateBadges();

            var path = $location.path();
            $rootScope.startPath = path == '/locate' ? startPath : path;
            $location.path('/locate');
        }
        else
        {
            $location.path('/login');
        }
    });
angular.module('MC.controllers', [])
    .controller('RecoverCtrl', function ($http, $scope, $ionicModal, $rootScope, $state, $ionicLoading, $ionicViewService, UserService) {
        //TODO save login

        var isRecovering = false
            ;

        $scope.user = { login: '' };

        $scope.isRecovering = function () {
            console.log('isRecovering');
            return isRecovering;
        };

        $rootScope.hideTabs();

        $scope.recover = function () {
            isRecovering = true;

//            UserService.login($scope.user,
//                function () {
//                    //TODO get token
//                    $rootScope.currentUser = UserService.currentUser();
//                    $http.defaults.headers.common["X-Auth-Token"] = $rootScope.currentUser.auth_token;
//                    $http.defaults.headers.common["X-Auth-Login"] = $rootScope.currentUser.phone;
//
//                    $rootScope.locationUpdater();
//                    $rootScope.updateBadges();
//                    $state.go('tabs.missions');
//
//                    $scope.$broadcast('login.enable');
//                    isLoggingIn = false;
//                },
//                function (message) {
//                    alertify.error(message);
//                    $scope.$broadcast('login.enable');
//                    isLoggingIn = false;
//                }
//            );
        }

        $scope.recover = function (form) {
            if (form.$valid) $scope.recover();
        }
    })

    .controller('LoginCtrl', function ($http, $scope, $ionicModal, $rootScope, $state, $ionicLoading, $ionicViewService, UserService) {
        //TODO save login
        $scope.user = {login: '', password: ''};
        var isLoggingIn = false;

        $scope.isLogingIn = function () {
            return isLoggingIn;
        };

        $rootScope.hideTabs();

        $scope.login = function () {
            isLoggingIn = true;

            UserService.login($scope.user,
                function () {
                    //TODO get token
                    $rootScope.currentUser = UserService.currentUser();
                    $http.defaults.headers.common["X-Auth-Token"] = $rootScope.currentUser.auth_token;
                    $http.defaults.headers.common["X-Auth-Login"] = $rootScope.currentUser.phone;

                    $rootScope.locationUpdater();
                    $rootScope.updateBadges();
                    $state.go('tabs.missions');

                    $scope.$broadcast('login.enable');
                    isLoggingIn = false;
                },
                function (message) {
                    alertify.error(message);
                    $scope.$broadcast('login.enable');
                    isLoggingIn = false;
                }
            );
        }

        $scope.checkAndGo = function (form) {
            if (form.$valid) $scope.login();
        }
    })


    .controller('MissionsCtrl', function ($scope, $timeout, $state, $http, $rootScope, $ionicModal, $ionicLoading, $ionicViewService, MissionsService, UserService) {
        $ionicViewService.clearHistory();
        $rootScope.showTabs();

        profileModal($scope, $rootScope, $state, $ionicModal, UserService);

        var load = function (callback) {
            MissionModel
                .all()
                .filter('login', '=', $rootScope.currentUser.phone)
                .filter('mission_id', 'not in (?)', 'select mission_id from tasks')
                .and(new persistence.PropertyFilter('type', '!=', 'sensus'))
                .or(new persistence.PropertyFilter('type', '=', 'sensus'))
//
                .order('distance')
                .list(function (results) {
                    var missions = [];
                    results.forEach(function (mission) {
                        missions.push(MissionsMapper.data(mission));
                    });

                    $scope.missions = MissionsMapper.categories(missions);

                    if (callback) callback();
                })
            ;
        };

        $scope.onRefresh = function () {
            new SyncAll($http, function () {
                $rootScope.updateBadges();

                load(function () {
                    $scope.$apply();
                    $scope.$broadcast('scroll.refreshComplete');
                });

            });
        };

        $scope.$broadcast('loading.start');
        load(function () {
            setTimeout(function () {
                $scope.$apply();
                $scope.$broadcast('loading.complete');
            })

        });
    })
    .controller('TaskCtrl', function ($scope, $state, $ionicModal, $rootScope, $ionicViewService, $stateParams, $location, $ionicScrollDelegate, $ionicLoading, TasksService, templateLayoutFactory) {
        $rootScope.hideTabs();
        var taskId = $stateParams.taskId;

        taskInfoModal($scope, $ionicModal, templateLayoutFactory);

        $scope.goBack = function () {
            $state.go('tabs.tasks');
        };

        $scope.answers = {};

        var showLoading = function () {
            $scope.loading = $ionicLoading.show({
                content: 'Загрузка задания...',
                maxWidth: 200
            });
        };

        var hideLoading = function (scope) {
            $scope.loading.hide();
        };

        var load = function (callback) {
            TaskModel.load(taskId, function (entity) {
                if (entity) {
                    $scope.task = TasksMapper.data(entity);
                    console.log($scope.task);

                    MissionModel.load(entity.mission_id, function (mission) {
                        $scope.mission = MissionsMapper.data(mission);
                    });

                    if ($scope.task.status == 'current' || $scope.task.status == '') {
                        var builder = new TaskBuilder(entity, $scope);

                        builder.currentStep(function (entity, step_id) {
                            $scope.task = entity;
                            $scope.step_id = step_id;
                            callback();
                        });

                        $scope.lastStep = function () {
                            return builder.lastStep();
                        };

                        $scope.canGoBack = function () {
                            return builder.canGoBack();
                        };

                        $scope.backClicked = function () {
                            showLoading();
                            builder.stepBack(function (entity, step_id) {
                                $scope.task = entity;
                                $scope.step_id = step_id;
                                callback();

                                hideLoading();
                            });
                        };

                        $scope.next = function () {
                            showLoading();
                            builder.nextStep(function (entity, step_id) {
                                $scope.task = entity;
                                $scope.step_id = step_id;
                                callback();
                                hideLoading();
                            });
                        };
                    }
                    else {
                        callback();
                    }
                }
                else {
                    $scope.notFound = true;
                }
            })
        }

        $scope.$broadcast('loading.start');

        load(function () {
            setTimeout(function () {
                $scope.$apply();
                $scope.$broadcast('loading.complete');
                $ionicScrollDelegate.scrollTop(false);
            });
        });

        // ACCEPT CURRENT STEP, MOVE TO NEXT STEP
        $scope.nextClicked = function (form) {
            if (form.$valid) $scope.next();
        };

        // UPLOAD TASK
        $scope.upload = function () {
            //TODO update user data
            TaskSend($scope, $state, TasksService, $ionicLoading);
        };

        // SAVE FIELD VALUE
        $scope.update = function (field) {
            var fieldId = field.id,
                taskId = $scope.task.id,
                stepId = $scope.task.step.id,
                value = $scope.answers[fieldId],
                removeAnswer = false
                ;

            if (!stepId)
            {
                console.log('NO STEP LOADED');
                return;
            }

            if (value == undefined) removeAnswer = true;

            value = field.type == 'checkbox' || field.type == 'location' ? JSON.stringify(value) : value;

            var values = removeAnswer
                    ? {id: [taskId, fieldId].join('@'), _removed: true}
                    : {id: [taskId, fieldId].join('@'), field_id: fieldId, step_id: stepId, task_id: taskId, value: value, type: field.type}
                ;

            new Sync(
                AnswerModel,
                [values],
                AnswersMapper,
                function () {
//                    console.log('answer.saved!');
                },
                false
            );
        };

        // CANCEL TASK
        $scope.cancel = function () {
            //TODO update user data
            var task = $scope.task;

            //TODO confirm
            if (confirm('Вы действительно хотите отменить это задание?')) {
                TaskModel.load($scope.task.id + '', function (row) {
                    TasksService.cancel($scope.task.id, function () {
                        persistence.remove(row);
                        persistence.flush(function () {
                            AnswerModel
                                .all()
                                .filter('task_id', '=', $scope.task.id)
                                .destroyAll();

                            $rootScope.updateBadges();
                            $state.go('tabs.tasks');
                        });
                    });
                });
            }
        };

        $scope.remove = function () {
            var task = $scope.task;
            //TODO update user data

            //TODO confirm
            if (confirm('Вы действительно хотите удалить это задание из списка?')) {
                TaskModel.load($scope.task.id + '', function (row) {
                    row.removed = true;

                    persistence.flush(function () {
                        AnswerModel
                            .all()
                            .filter('task_id', '=', $scope.task.id)
                            .destroyAll()
                        ;

                        $rootScope.updateBadges();
                        $state.go('tabs.tasks');
                    });
                });
            }
        };
    })
    .controller('MissionCtrl', function ($scope, $rootScope, $state, $ionicViewService, $stateParams, $location, $ionicLoading, MissionsService, templateLayoutFactory) {
        $rootScope.hideTabs();
        var missionId = $stateParams.missionId;

        $scope.goBack = function () {
            $state.go('tabs.missions');
        }

        missionMap($scope, templateLayoutFactory);

        var showLoading = function () {
            $scope.loading = $ionicLoading.show({
                content: 'Загрузка задания...',
                maxWidth: 200
            });
        };

        var hideLoading = function (scope) {
            $scope.loading.hide();
        };

        $scope.start = function () {
            showLoading();
            MissionsService.start(
                missionId,
                function (task) {
                    setTimeout(function () {
                        $rootScope.updateBadges();
                        $state.go('tabs.task', {taskId: task.id})
                    });

                    hideLoading();
                },
                function (result) {
                    if (result.remove === true) {
                        MissionModel.load(missionId, function (mission) {
                            persistence.remove(mission);
                            //TODO текст для удаленного задания: типа.. задание более недоступно, удаляем из списка!
                            alertify.error('Задание недоступно!');
                            $state.go('tabs.missions');
                        })
                    }
                    else {
                        alertify.error('Произошла ошибка!');
                    }

                    hideLoading();
                }
            );
        };

        var load = function (callback) {
            MissionModel.load(missionId, function (mission) {
                if (mission)
                    $scope.mission = MissionsMapper.data(mission);
                else
                    $scope.notFound = true;

                if (callback) callback();
            })
        }

        $scope.$broadcast('loading.start');

        load(function () {
            setTimeout(function () {
                $scope.showMap = true;
                $scope.$apply();
                $scope.$broadcast('loading.complete');
            });
        })
    })
    .controller('MapTabCtrl', function ($scope, $state, $ionicModal, $rootScope, $location, templateLayoutFactory, UserService) {
        profileModal($scope, $rootScope, $state, $ionicModal, UserService);

        $rootScope.showTabs();
        $scope.$broadcast('loading.start');

        //TODO from currentUser
        $scope.map = {
            center: $rootScope.currentUser.location.geometry.coordinates,
            zoom: 10,
            location: $rootScope.currentUser.location
        };

        var load = function (callback) {
            MissionModel
                .all()
                .filter('target_id', 'is not', 'null')
                .filter('id', 'not in (?)', 'select mission_id from tasks')
                .filter('login', '=', $rootScope.currentUser.phone)
                .list(function (results) {
                    var targets = [];

                    results.forEach(function (target) {
                        targets.push(MapMapper.data(target));
                    });

                    $scope.map = $.extend($scope.map, {targets: targets});

                    if (callback()) callback();
                })
            ;
        }

        load(function () {
            setTimeout(function () {
                $scope.$apply();
            });
        })

        var setSize = function (container) {
            var mapEl = $('#tasks-map'),
                viewport = mapEl.closest('.scroll-content')
                ;

            mapEl.css({width: viewport.width(), height: viewport.height(), display: 'block'});
        }

        $scope.beforeMapInit = function () {
            setSize();
        };

        $scope.afterMapInit = function (map) {
            YMap(map, $scope, templateLayoutFactory, $rootScope);
            $scope.$broadcast('loading.complete');
        };
    })
    .controller('TasksCtrl', function ($scope, $rootScope, $rootScope, $state, $http, $ionicModal, UserService, $ionicViewService) {
        $ionicViewService.clearHistory();
        profileModal($scope, $rootScope, $state, $ionicModal, UserService);

        $rootScope.$broadcast('tabs.show');
        $scope.$broadcast('loading.start');

        var load = function (callback) {
            TaskModel
                .all()
                .filter('mission_id', 'in (?)', 'select id from missions')
                .filter('removed', '=', false)
                .filter('login', '=', $rootScope.currentUser.phone)
                .list(function (results) {
                    var tasks = [];

                    results.forEach(function (task) {
                        tasks.push(task.toJSON());
                    });

                    $scope.tasks = TasksMapper.categories(tasks);

                    if (callback()) callback();
                })
            ;
        }

        load(function () {
            setTimeout(function () {
                $scope.$apply();
                $scope.$broadcast('loading.complete');
            });
        })

        $scope.onRefresh = function () {
            new SyncAll($http, function () {
                $rootScope.updateBadges();

                load(function () {
                    $scope.$apply();
                    $scope.$broadcast('scroll.refreshComplete');
                });
            });
        };
    })
    .controller('ProfileCtrl', function ($scope) {

    });
;

var profileModal = function ($scope, $rootScope, $state, $ionicModal, UserService) {
    $ionicModal.fromTemplateUrl(
        'profile.html',
        function (modal) {
            $scope.modal = modal;
        },
        {
            scope: $scope,
            animation: 'slide-in-up'
        }
    );

    $scope.logout = function () {
        //TODO logout
        UserService.logout(function () {
            $state.go('login');
        });
    }

    $scope.openModal = function () {
        $scope.profile = $rootScope.currentUser;
        $scope.modal.show();
    };

    $scope.closeModal = function () {
        $scope.modal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });

    if ($scope.rightButtons == undefined)
        $scope.rightButtons = [];

    $scope.rightButtons.push(
        {
            type: 'button-clear',
            content: '<i class="icon ion-ios7-person"></i>',
            tap: function (e) {
                $scope.openModal();
            }
        }
    );
}

var taskInfoModal = function ($scope, $ionicModal, templateLayoutFactory) {
    var target;

    missionMap($scope, templateLayoutFactory);

    $ionicModal.fromTemplateUrl(
        'task_info.html',
        function (modal) {
            $scope.modal = modal;
        },
        {
            scope: $scope,
            animation: 'slide-in-up'
        }
    );

    $scope.openModal = function () {
        $scope.modal.show();
        $scope.$broadcast('map.reload');
    };

    $scope.closeModal = function () {
        $scope.modal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });


    if ($scope.rightButtons == undefined)
        $scope.rightButtons = [];

    $scope.rightButtons.push(
        {
            type: 'button-clear',
            content: '<i class="icon ion-ios7-information"></i>',
            tap: function (e) {
                $scope.openModal();
            }
        }
    );
}

var missionMap = function ($scope, templateLayoutFactory) {
    var $target;

    $scope.$on('map.reload', function () {
        if ($target) {
            $target.container.fitToViewport();
            $scope.afterMapInit($target);
        }
    });

    $scope.afterMapInit = function (map) {
        $target = map;

        var map_el = $(map.container.getElement()).parent(),
            viewport = map_el.closest('.mission-map')
            ;

        YMap(map, $scope, templateLayoutFactory);

        if ($scope.mission.target) {
            var objects = [], center = [];

            if ($scope.mission.target) {
                objects.push($scope.mission.target);
                center.push($scope.mission.target.geometry.coordinates);
            }

            if ($scope.currentUser.location) {
                objects.push($scope.currentUser.location);
                center.push($scope.currentUser.location.geometry.coordinates);
            }

            if (map_el.size()) {

                if (center[0][0] > center[1][0])
                    center = center.reverse();

                var params = ymaps.util.bounds.getCenterAndZoom(
                    center,
                    [map_el.width(), map_el.height()]
                );

                $scope.mission.center = params.center;
                $scope.mission.zoom = params.zoom;
                map.setZoom(params.zoom - 1);
            }
        }
    };
}

var TaskSend = function (scope, state, TaskService, $ionicLoading) {
    AnswerModel
        .all()
        .filter('task_id', '=', scope.task.id)
        .list(function (results) {
            var answers = {},
                files = []
            ;

            results.forEach(function (row) {
                if (row.type == 'image')
                    files.push({field_id: row.field_id, url: row.value});
                else
                    answers[row.field_id] = row.value;
            });

            TaskService.send($ionicLoading, scope, scope.task.id, files, answers, function (update) {
                TaskModel.load(scope.task.id, function (task) {
                    task.status = 'synced';
                    persistence.add(task);

                    persistence.flush(function () {
                        $.extend(scope.currentUser, update.user);
                        scope.loading.hide();
                        state.go('tabs.tasks');
                    });
                });
            }, function (message) {
                alert('Ошибка выгрузки задачи: ' + message);
                scope.loading.hide();
            });
        });
}
;
angular.module('MC.directives', ['ngResource'])
    .directive('missionStatus', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'mission_status.html'
        }
    })
    .directive('missionInfo', function () {
        return {
            restrict: 'E',
            templateUrl: 'mission_info.html'
        };
    })
    .directive('formField', function () {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                scope.$watch('taskForm.field' + scope.field.id + '.$valid', function () {
//                    scope.$broadcast('scroll.refreshComplete');
                });

                if (scope.field.type == 'checkbox' || scope.field.type == 'location') {
                    var val,
                        values = scope.answers[scope.field.id]
                        ;

                    try {
                        val = JSON.parse(values);
                    }
                    catch (e) {
                        val = scope.field.type == 'location' ? null : {};
                    }

                    scope.answers[scope.field.id] = val;
                }

                if (scope.field.type == 'location' && scope.field.required) {
                    ngModel.$setValidity('valid', false);
                    ngModel.$setValidity('required', true);
                }

                scope.$watch('answers.' + scope.field.id, function (newValue, oldValue) {
                    if (scope.field.type == 'checkbox') {
                        var answers = scope.answers[scope.field.id],
                            valid = false
                            ;

                        for (var i in answers) {
                            if (answers[i] === true) {
                                valid = true;
                                break;
                            }
                        }

                        ngModel.$setValidity('required', valid);
                    }

                    else if (scope.field.type == 'location') {
                        var answers = scope.answers[scope.field.id],
                            valid = answers && typeof(answers.latitude) != 'undefined' && typeof(answers.longitude) != 'undefined'
                            ;

                        ngModel.$setValidity('valid', valid);
                    }

                    scope.update(scope.field);
                }, true);
            }
        }
    })
    .directive('formCheck', function () {
        return {
            restrict: 'A',
            link: function (scope) {
                if (scope.field.type == 'checkbox') {
                    scope.isChecked = function (answers, variant) {
                        if (typeof(answers[variant.id]) == 'undefined')
                            answers[variant.id] = variant.default;

                        return answers[variant.id];
                    };
                }
                else if (scope.field.type == 'radio') {
                    scope.isChecked = function (answers, variant) {
                        if (typeof(answers) == 'undefined') {
                            if (variant.default) scope.answers[scope.field.id] = variant.id;
                            return variant.default;
                        }
                        else {
                            return answers == variant.id;
                        }
                    };
                }
            }
        }
    })

    .directive('distance', function () {
        return {
            restrict: 'E',
            templateUrl: 'distance.html',
            link: function (scope, element, attrs) {
                var distance = scope.distance;

                if (distance > 1000) {
                    distance = Math.round(distance / 10) / 100 + 'км';
                }
                else {
                    distance = distance + 'м';
                }

                scope.distance = distance;
            }
        };
    })

    .directive('locateButton', function () {
        return {
            restrict: 'E',
            templateUrl: 'locate_button.html',
            link: function (scope, element, attrs) {
                var answers = scope.answers[scope.field.id];

                scope.locating = false;
                scope.located = answers && typeof(answers.latitude) !== 'undefined' && typeof(answers.longitude) !== 'undefined';
                scope.locationError = false;

                scope.getCurrentLocation = function () {
                    scope.locating = true;
                    scope.located = false;
                    scope.answers[scope.field.id] = null;

                    Geolocator.position(
                        function (coords, timestamp) {
                            scope.locating = false;
                            scope.located = true;

                            var location = {
                                longitude: null,
                                latitude: null,
                                accuracy: null
                            };

                            location.longitude = coords.longitude;
                            location.latitude = coords.latitude;
                            location.accuracy = coords.accuracy;

                            scope.answers[scope.field.id] = location;
                            scope.$apply();
                        },
                        function () {
                            scope.locationError = true;
                            scope.locating = false;
                            scope.located = false;

                            scope.answers[scope.field.id] = null;
                            scope.$apply();
                        }
                    );
                }
            }
        };
    })

    .directive('audioButton', function () {
        return {
            restrict: 'E',
            templateUrl: 'audio_button.html',
            link: function (scope, element, attrs) {

            }
        };
    })

    .directive('imageButton', function () {
        return {
            restrict: 'E',
            templateUrl: 'image_button.html',
            link: function (scope, element, attrs) {
                scope.processing = false;
                scope.url = false;

                var onSuccess = function (imageURI) {
                    scope.answers[scope.field.id] = imageURI;
                    scope.url = imageURI;
                    scope.processing = false;

                    setTimeout(function () {
                        scope.$apply();
                    })
                };

                var onFail = function (message) {
                    scope.url = false;
                    scope.processing = false;

                    setTimeout(function () {
                        scope.$apply();
                    })
                }

                scope.takePicture = function () {
                    scope.processing = true;
                    setTimeout(function() {
                        scope.$apply();
                    });

                    navigator.camera.getPicture(
                        onSuccess,
                        onFail,
                        {
                            quality: 51,
                            allowEdit: true,
                            correctOrientation: true,
                            encodingType: Camera.EncodingType.JPEG,
                            destinationType: Camera.DestinationType.FILE_URI
                        }
                    );
                }

            }
        };
    })

    .directive('videoButton', function () {
        return {
            restrict: 'E',
            templateUrl: 'video_button.html',
            link: function (scope, element, attrs) {

            }
        };
    })

    .directive('loading', function () {
        return {
            restrict: 'E',
            templateUrl: 'loading.html',
            scope: {},
            link: function (scope, element, attrs) {
                var element = angular.element(element);

                scope.$on('loading.start', function () {
                    element.removeClass('fade-out');
                });

                scope.$on('loading.complete', function () {
                    element.addClass('fade-out');
                });
            }
        }
    })
    .directive('taskBlock', function () {
        return {
            restrict: 'E',
            templateUrl: 'task_block.html'
        };
    })
    .directive('taskField', function () {
        return {
            restrict: 'E',
            templateUrl: 'task_field.html'
        };
    })
    .directive('taskCurrent', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'task_current.html'
        };
    })
    .directive('taskNotFound', function () {
        return {
            restrict: 'E',
            templateUrl: 'task_not_found.html'
        };
    })
    .directive('taskDone', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'task_done.html'
        };
    })
    .directive('taskSynced', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'task_synced.html'
        };
    })
    .directive('taskApproved', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'task_approved.html'
        };
    })

    .directive('footerAcceptMission', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_accept_mission.html'
        };
    })

    .directive('footerTask', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_task.html'
        };
    })

    .directive('footerEmpty', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_empty.html'
        };
    })

    .directive('footerTaskInfo', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_task_info.html'
        };
    })


    .directive('footerLogout', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_logout.html'
        };
    })

    .directive('footerCloseModal', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_close_modal.html'
        };
    })

    .directive('missionListItem', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'mission_list_item.html'
        };
    })

    // TODO if element seen
    .directive('missionSeen', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var timeout;
                var interval = element.attr('mission-seen') ? 500 : 5000;

                scope.$on('$destroy', function () {
                    clearTimeout(timeout);
                });

                timeout = setTimeout(function () {
                    if (scope.mission) {
                        Seen(scope.mission.mission_id, MissionModel);
                        scope.updateBadges();
                    }
                }, interval);
            }
        }
    })

    .directive('taskSeen', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var timeout;
                var interval = element.attr('task-seen') ? 500 : 5000;

                scope.$on('$destroy', function () {
                    clearTimeout(timeout);
                });

                timeout = setTimeout(function () {
                    Seen(scope.task.id + '', TaskModel);
                    scope.updateBadges();
                }, interval);
            }
        }
    })

    .directive('taskListItem', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'task_list_item.html',
            link: function (scope, element) {
                if (scope.task && scope.task.status == '') scope.task.status = 'current';
            }

        };
    })
    .directive('autosizeable', ['$document', function ($document) {
        return function (scope, element, attr) {
            element.autosize();
        };
    }])

    .directive('phonify', ['$document', function ($document) {
        return function (scope, element, attr) {
//            TODO angular ui-mask
            $(element).mask("+7 (999) 999-99-99");
        };
    }])

    .directive('timer', function () {
        return {
            restrict: 'E',
            templateUrl: 'timer.html',
            link: function (scope, element, attrs) {
                var task = scope.task || scope.mission;
                new Timer(element, task.time_left);
            }
        };
    })

    .directive('appTabs', function () {
        return {
            restrict: 'A',
            scope: {},
            link: function (scope, element, attrs) {
                element.removeAttr('app-tabs');

                scope.$on('tabs.hide', function () {
                    element.find('.tabs').addClass('tabs-item-hide');
                    $('.has-tabs').removeClass('has-tabs').addClass('_has-tabs');
                });

                scope.$on('tabs.show', function () {
                    element.find('.bar-footer').hide();

                    element.find('.tabs').removeClass('tabs-item-hide');
                    $('._has-tabs').removeClass('_has-tabs').addClass('has-tabs');
                });
            }
        }
    })

    .directive('time', function () {
        return {
            restrict: 'E',
            templateUrl: 'time.html',
            link: function (scope, element, attrs) {
                var task = scope.task || scope.mission;
                new Time(element, task.time_left);
            }
        };
    })

    .directive('noTabs', function ($timeout) {
        return {
            restrict: 'E',
            link: function (scope, element, attrs) {
                element.find('.bar-footer').hide();

                $timeout(function () {
                    element.find('.has-tabs').removeClass('has-tabs').addClass('_has-tabs');
                    var view = element.find('.has-footer').removeClass('has-footer').addClass('_has-footer');

                    element.find('.bar-footer').slideDown(200, function () {
                        view.removeClass('_has-footer').addClass('has-footer');
                    });
                });
            }
        };
    })

    .directive('dynamicName', function ($compile, $parse) {
        return {
            restrict: 'A',
            terminal: true,
            priority: 100000,
            link: function (scope, elem) {
                var name = $parse(elem.attr('dynamic-name'))(scope);

                elem.removeAttr('dynamic-name');
                elem.attr('name', name);
                $compile(elem)(scope);
            }
        };
    })
;
var DOMAIN = 'http://mobilecontroller.ru';

angular.module('MC.services', ['ngResource'])

    .factory('MissionsService', function ($resource, $http) {
        var resource = $resource(DOMAIN + '/api/missions/:id.json', {id: "@id"}, {
            query: { method: 'GET' },
            get: { method: 'GET' },
            start: { method: 'PATCH', isArray: false }
        });

        return {
            start: function (id, onSuccess, onError) {
                MissionModel.load(id, function (mission) {
                    resource.start({id: id, lastSync: Math.random()})
                        .$promise
                        .then(
                        function (result) {
                            var task = TasksMapper.data(result.task);
                            Sync(TaskModel, [result.task], TasksMapper, function () {
                                onSuccess(task);
                            }, false);
                        },
                        function (error) {
                            if (onError) onError(error);
                            else alertify.error('Ошибка запроса');
                        }
                    )
                    ;
                })
            }
        }
    })

    .factory('UserService', function ($resource) {
        var user = null;
        var resource = $resource(DOMAIN + '/api/user.json', {}, {
            login: { method: 'POST' },
            logout: { method: 'DELETE' }
        })

        return {
            login: function (data, onSuccess, onError) {
                resource.login({agent: data, dt: new Date().getTime()})
                    .$promise
                    .then(function (result) {
                        user = UserMapper.data(result);
                        console.log(user);
                        onSuccess();
                    }, function (result) {
                        onError(result.data.message);
                    })
                ;
            },
            logout: function (onSuccess) {
                resource.logout().$promise.then(function (result) {
                    user = null;
                    onSuccess();
                });
            },
            currentUser: function () {
                return user;
            }
        };
    })

    .factory('TasksService', function ($resource, $http, $ionicLoading) {
        return {
            cancel: function (taskId, onSuccess, onError) {
                $http({method: 'DELETE', url: DOMAIN + '/api/tasks/' + taskId, data: {}})
                    .success(function () {
                        if (onSuccess) onSuccess();
                    })
                    .error(function () {
                        alertify.error('Ошибка запроса');
                        if (onError) onError();
                    })
                ;
            },
            send: function ($ionicLoading, scope, taskId, files, answers, onSuccess, onError) {

                if (files.length)
                    scope.loading = $ionicLoading.show({
                        content: 'Отправка выполненного задания...',
                        maxWidth: 200
                    });

                persistence.asyncForEach(files,
                    function(file, next) {
                        SendImage(
                            taskId,
                            file.field_id,
                            file.url,
                            scope.currentUser,
                            function() {
                                next();
                            },
                            function() {
                                onError('Фотография не отправлена!');
                            }
                        );
                    },
                    function() {
                        scope.loading = $ionicLoading.show({
                            content: 'Отправка введенных данных...',
                            maxWidth: 200
                        });

                        $http({method: 'PATCH', url: DOMAIN + '/api/tasks/' + taskId, data: { answers: answers }})
                            .success(function (response) {
                                if (onSuccess) onSuccess(response);
                            })
                            .error(function (response) {
                                if (onError) onError(response);
                            })
                        ;
                    }
                );

                return;

            }
        }
    })
    .factory('GeolocationService', function ($resource, $state, $location, $ionicLoading, $rootScope) {
        var showLoading = function (scope) {
            scope.loading = $ionicLoading.show({
                content: 'Определение вашего местоположения...',
                maxWidth: 200
            });
        };

        var hideLoading = function (scope) {
            scope.loading.hide();
        };

        var resource = $resource(DOMAIN + '/api/geolocation', {}, {
                update: {
                    method: 'PATCH'
                }
            }
        );

        var gotoRoot = function () {
            $state.go('tabs.missions');
        };

        var errorHandler = function (scope, showLoading) {
            alertify.error('Не удалось определить ваше местоположение...');
            if (showLoading) {
                gotoRoot();
                hideLoading(scope);
            }
        }

        var getCoordinates = function (scope) {
            var showLoading = typeof(scope) !== 'undefined' ? true : false;
            if (showLoading) showLoading(scope);

            Geolocator.position(
                function (coords, timestamp) {
                    resource
                        .update({latitude: coords.latitude, longitude: coords.longitude})
                        .$promise
                        .then(
                        function (response) {
                            if ($rootScope.currentUser)
                            {
                                var update = UserMapper.data({latitude: coords.latitude, longitude: coords.longitude})
                                $rootScope.currentUser = $.extend($rootScope.currentUser, update);
                            };

                            if (showLoading) {
                                hideLoading(scope);
                                gotoRoot();
                            }
                        },
                        function () {
                            errorHandler(scope, showLoading);
                        }
                    )
                },
                function () {
                    if (showLoading) hideLoading(scope);
                    errorHandler(scope)
                }
            );
        }

        return {
            update: function (scope, showLoading) {
                if (showLoading === true) getCoordinates(scope, showLoading);
                else getCoordinates();
            }
        }
    })
;

var SendImage = function (taskId, fieldId, fileURL, currentUser, onSuccess, onError) {
    console.log('SEND IMAGE: ', currentUser);

//    var win = function (fileUrl) {
//        alert('uploaded!', fileUrl);
//    }
//
//    var fail = function (error) {
//        alert("An error has occurred: Code = " + error.code);
//    }

    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";

    var params = {};
    params.fieldId = fieldId;
    params.taskId = taskId;
    params.auth_login = currentUser.phone;
    params.auth_token = currentUser.auth_token;
    options.params = params;

    var ft = new FileTransfer();
    ft.upload(fileURL, encodeURI(DOMAIN + "/api/upload"), onSuccess, onError, options);
}

//var FileUploadOptions = function() {
//    return {};
//}
//
//var FileTransfer = function() {
//    this.upload = function(fileUrl, uploadTo, onSuccess, onError, options) {
//        if (fileUrl == 'error') onError({code: 404});
//        else onSuccess(fileUrl);
//    };
//}
;
var TaskBuilder = function (entity, $scope) {
    var task = TasksMapper.data(entity),
        completed = function () {
            return task.steps
        },

        self = this,
        current = false
    awaiting = []
    ;

    this.getSteps = function (callback) {
        StepModel
            .all()
            .filter('task_id', '=', task.task_id)
            .filter('id', 'not in', completed())
            .order('sorting_order')
            .list(function (results) {
                awaiting = results;

                if (results.length === 0) self.done(callback);
                else
                    for (var i in results) {
                        var step = results[i];
                        break;
                    }

                if (step) {
                    step = StepsMapper.data(step);
                    callback(step);
                }
            });
    };

    //TODO ACCEPT CURRENT STEP & GO TO NEXT
    this.currentStep = function (callback) {
        this.getSteps(
            function (step) {
                current = step;
                self.buildStep(step, callback);
            }
        );
    };

    this.canGoBack = function() {
        return (completed().length > 0 && task.status != 'done') ? true : false;
    }

    this.lastStep = function() {
        return awaiting.length === 1 ? true : false;
    }

    this.buildStep = function (step, callback) {
        task.step = step;

        AnswerModel
            .all()
            .filter('task_id', '=', task.id)
            .filter('step_id', '=', step.id)
            .list(function (results) {
                results.forEach(function (row) {
                    $scope.answers[row.field_id / 1] = row.value;
                });

                callback(task);
            })
        ;
    };

    this.done = function (callback) {
        task.status = 'done';
        entity.status = 'done';

//        persistence.add(entity);
        persistence.flush(function () {
            callback(task);
        });
    }

    this.nextStep = function (callback) {
        var _completed = completed();
        _completed.push(current.id);

        entity.steps = _completed.join(',')

//        persistence.add(entity);
        persistence.flush(function () {
            self.currentStep(callback);
        });
    };

    this.stepBack = function (callback) {
        var _completed = completed();
        _completed.pop();

        entity.steps = _completed.join(',')

        persistence.flush(function () {
            self.currentStep(callback);
        });
    };
}
;
var Seen = function(id, entity) {
    if (!id) return;

    entity.load(id, function(item) {
        item.seen = true;
        persistence.add(item);
        persistence.flush();
    });
};
persistence.debug = false;

var DB = {
    name: 'mc',
    version: '1.0',
    description: 'MobileController DB',
    size: 100 * 1024 * 1024,

    init: function (callback) {
//        persistence.store.cordovasql.config(
//            persistence,
//            DB.name,
//            DB.version,
//            DB.description,
//            DB.size,
//            0
//        );
        persistence.store.websql.config(
            persistence,
            DB.name,
            DB.version,
            DB.description,
            DB.size
        );

        // before migrations init
        persistence.migrations.init(function() {
            console.log('MIGRATIONS STARTED');
        });

        // after migrate
        persistence.migrate(function() {
            console.log('MIGRATIONS COMPLETED');
            callback();
        });

//        persistence.store.cordovasql.config(
//            persistence,
//            DB.name,
//            DB.version,
//            DB.description,             // DB version
//            DB.size,
//            0                       // SQLitePlugin Background processing disabled
//        );
    }
}


;
var MissionsMapper = {
    categories: function (missions) {
        var missionCategories =
        {
            important: [],
            targeted: [],
            common: []
        }

        for (var missionId in missions) {
            var mission = missions[missionId],
                noTask = true
                ;

            if (mission.target_id)
                missionCategories.targeted.push(mission);
            else if (mission.important === 'true' || mission.important === true)
                missionCategories.important.push(mission);
            else
                missionCategories.common.push(mission);

        }
        ;

        return missionCategories;
    },

    // to DB
    row: function (data) {
        var data = data || {};

        var attributes = {
            id: data.id,
            mission_id: data.id,
            project_id: data.project_id,
            task_id: data.task_id,

            type: data.type,

            name: data.name,
            description: data.description,
            done: data.done,

            important: data.important,
            login: data.login,

            time_to_complete: data.time_to_complete,
            time_left: data.time_left,

            rating: data.rating,
            price: data.price
        };

        if (data.target_id) {
            attributes.target_id = data.target_id;
            attributes.address = data.address;
            attributes.distance = data.distance;
            attributes.longitude = data.longitude;
            attributes.latitude = data.latitude;
        }

        return attributes;
    },
    data: function (row) {
        var row = row || {};

        var target = null,
            attributes = {
                mission_id: row.mission_id,
                project_id: row.project_id,
                task_id: row.task_id,

                seen: row.seen,

                type: row.type,

                name: row.name,
                description: row.description,
                done: row.done,
                important: row.important,
                login: row.login,


                time_to_complete: row.time_to_complete,
                time_left: row.time_left,

                rating: row.rating,
                price: row.price
            }
            ;

        if (row.target_id / 1) {
            var add = {
                target_id: row.target_id,
                address: row.address,
                distance: row.distance,
                target: {
                    geometry: {
                        type: 'Point',
                        coordinates: [row.longitude / 1, row.latitude / 1]
                    }
                }
            };

            attributes = $.extend(attributes, add);
        }

        return attributes;
    }
};

var MapMapper = {
    data: function (row) {
        var row = row || {};
        return {
            mission_id: row.mission_id,
            properties: {
                mission_id: row.mission_id,
                name: row.name,
                address: row.address,
                rating: row.rating,
                price: row.price,
                description: row.description,
                baloonLayout: 'targetBaloon'
            },

            geometry: {
                type: 'Point',
                coordinates: [row.longitude / 1, row.latitude / 1]
            }
        }
    }
};

var UserMapper = {
    data: function (row) {
        var row = row || {},
            attributes = {}
            ;

        var keys = ['full_name', 'approved', 'done', 'balance', 'rating', 'email', 'phone', 'auth_token']

        for (var key in keys)
        {
            var attribute = keys[key],
                value = row[attribute];

            if (value != undefined) attributes[attribute] = value;
        }


        if (row.latitude && row.longitude)
            attributes.location = {
                geometry: {
                    type: 'Point',
                    coordinates: [row.longitude / 1, row.latitude / 1]
                }
            }

        return attributes;
    }
};

var TasksMapper = {
    categories: function (tasks) {
        var taskCategories = {
            current: [],
            done: [],
            synced: [],
            approved: []
        };

        for (var taskId in tasks) {
            var task = tasks[taskId];

            if (task.status == 'done')
                taskCategories.done.push(task);
            else if (task.status == 'synced')
                taskCategories.synced.push(task);
            else if (task.status == 'approved')
                taskCategories.approved.push(task);
            else
                taskCategories.current.push(task);
        }
        ;

        return taskCategories;

    },
    row: function (data) {
        var data = data || {};

        var properties = {
            id: data.id,
            mission_id: data.mission_id,
            task_id: data.task_id,

            type: data.type,
            name: data.name,

            time_to_complete: data.time_to_complete,
            time_left: data.time_left,
            login: data.login,

            rating: data.rating,
            price: data.price
        };

        if (data.status) properties.status = data.status;
        if (data.status != 'approved' && data.status != 'synced')
            properties.removed = 0;

        if (data.status != '' && data.status != 'current')
            properties.seen = 1;

        return properties;
    },

    data: function (row) {
        var row = row || {};

        var steps = [];
        if (row.steps) steps = row.steps.split(',');

        return {
            id: row.id,
            mission_id: row.mission_id,
            task_id: row.task_id,
            steps: steps,

            seen: row.seen,
            removed: row.removed,

            type: row.type,
            name: row.name,
            status: row.status,
            login: row.login,

            completed_for: row.completed_for,

            time_to_complete: row.time_to_complete,
            time_left: row.time_left,

            rating: row.rating,
            price: row.price
        }
    }
};


var StepsMapper = {
    // to db
    row: function (data) {
        var data = data || {};

        return {
            id: data.id,
            task_id: data.task_id,
            name: data.name,
            description: data.description,
            content: JSON.stringify(data.content),
            sorting_order: data.sorting_order
        }

    },

    // to template
    data: function (row) {
        var row = row || {},
            blocks = row.content
            ;

        try {
            blocks = JSON.parse(row.content)
        }
        catch (e) {

        }

        return {
            id: row.id,
            task_id: row.task_id,
            name: row.name,
            description: row.description,
            blocks: blocks,
            sorting_order: row.sorting_order
        }
    }
};

var AnswersMapper = {
    row: function (data) {
        var data = data || {}
            ;

        return {
            id: data.id,
            task_id: data.task_id,
            step_id: data.step_id,
            type: data.type,
            field_id: data.field_id,
            value: data.value
        }
    },
    data: function (row) {
        var row = row || {}
            ;

        return {
            id: row.id,
            task_id: row.task_id,
            step_id: row.step_id,
            type: row.type,
            field_id: row.field_id,
            value: null
        }
    }
};
persistence.defineMigration(1, {
    up: function () {
        this.createTable('missions', function (table) {
            table.text('mission_id');
            table.integer('project_id');
            table.integer('task_id');
            table.integer('target_id');

            table.integer('seen');
            table.text('login');

            table.text('name');
            table.boolean('important');
            table.text('type');

            table.text('address');
            table.text('description');
            table.text('done');

            table.integer('rating');
            table.integer('price');

            table.integer('time_to_complete');
            table.integer('time_left');

            table.integer('distance');

            table.text('latitude');
            table.text('longitude');
        });
    }
});

persistence.defineMigration(2, {
    up: function () {
        this.createTable('tasks', function (table) {
            table.text('mission_id');
            table.integer('task_id');

            table.integer('seen');
            table.text('login');
            table.integer('removed');

            table.text('name');
            table.text('type');
            table.text('address');

            table.text('status');
            table.text('steps');

            table.integer('rating');
            table.integer('price');

            table.integer('time_to_complete');
            table.integer('time_left');
        });
    }
});

persistence.defineMigration(3, {
    up: function () {
        this.createTable('steps', function (table) {
            table.integer('task_id');

            table.text('name');
            table.text('content');

            table.text('description');
            table.integer('sorting_order');
        });
    }
});

persistence.defineMigration(4, {
    up: function () {
        this.createTable('answers', function (table) {
            table.integer('field_id');
            table.text('type');
            table.text('task_id');
            table.text('step_id');
            table.text('value');
        });
    }
});
var MissionModel = persistence.define('Missions', {
    mission_id: 'TEXT',
    project_id: 'INTEGER',
    task_id: 'INTEGER',
    target_id: 'INTEGER',

    seen: 'BOOL',
    login: 'TEXT',

    name: "TEXT",
    type: "TEXT",
    address: "TEXT",
    description: "TEXT",
    done: "TEXT",

    distance: 'TEXT',
    important: "BOOL",

    rating: "INTEGER",
    price: "INTEGER",

    time_to_complete: "INTEGER",
    time_left: "INTEGER",

    latitude: "TEXT",
    longitude: "TEXT"
});

var TaskModel = persistence.define('Tasks', {
    mission_id: 'TEXT',
    task_id: 'INTEGER',
    steps: 'TEXT',

    seen: 'BOOL',
    login: 'TEXT',

    name: "TEXT",
    type: "TEXT",
    address: "TEXT",

    rating: "INTEGER",
    price: "INTEGER",

    status: "TEXT",

    time_to_complete: "INTEGER",
    time_left: "INTEGER",
    removed: 'BOOL'
});

var StepModel = persistence.define('Steps', {
    task_id: "INTEGER",
    name: "TEXT",
    description: "TEXT",
    sorting_order: "INTEGER",
    content: "TEXT"
});

var AnswerModel = persistence.define('Answers', {
    type: "TEXT",
    field_id: "INTEGER",
    step_id: "TEXT",
    task_id: "TEXT",
    value: "TEXT"
});
var Prefetch = function ($rootScope, onSuccess) {
    var db = {
            steps: null
        },
        self = this
    ;


    this.getSteps = function(results, callback) {
        var steps = {
            items: [],
            byTask: {},
            ids: {}
        }

        results.forEach(function (row) {
            var step = StepsMapper.data(row);
            steps.items.push(step);
            steps.ids[step.id] = step;

            if (undefined === steps.byTask[step.task_id]) steps.byTask[step.task_id] = { items: [], ids: {}};
            steps.byTask[step.task_id].items.push(step);
            steps.byTask[step.task_id].ids[step.id] = step;
        });

        db.steps = steps;
        callback();
    };

    persistence.asyncForEach([StepModel],
        function(entity, next) {
            var name = entity.meta.name;

            entity.all().order('sorting_order').list(function (results) {
                self['get' + name](results, function() {
                    next();
                });
            });
        },
        function() {
            $rootScope.db = db;
            onSuccess();
        }
    );

//    MissionModel.all().list(function (results) {
//        var missions = {},
//            targets = []
//            ;
//
//        results.forEach(function (row) {
//            var mission = MissionsMapper.data(row);
//            missions[mission.mission_id] = mission;
//
//            if (mission.target_id) {
//                var target = MapMapper.data(row);
//                targets.push(target);
//            }
//        });
//
//        db.missions = missions;
//        db.targets = targets;
//        ready.missions = true;
//        callback();
//    });
//
//    TaskModel.all().list(function (results) {
//        var tasks = {};
//
//        results.forEach(function (task) {
//            var task = TasksMapper.data(task);
//            tasks[task.id] = task;
//        });
//
//        db.tasks = tasks;
//        ready.tasks = true;
//        callback();
//    });
}
;
var Sync = function (entity, results, mapper, callback, syncRemote) {
    var toRemove = [],
        toInsert = [],
        ids = [],
        lookup = {},
        syncRemote = syncRemote == undefined ? true : syncRemote,
        meta = entity.meta,
        fieldSpec = meta.fields
        ;

    results.forEach(function (item) {
        if (item._removed)
            toRemove.push(item.id);
        else {
            ids.push(item.id);
            lookup[item.id] = item;
        }
    });

    entity.all().list(function (results) {
        var updatesToPush = []
            ;

        results.forEach(function (row) {
            if (row) {
                var remoteData = mapper.row(lookup[row.id]),
                    updates;

                if (lookup[row.id]) {

                    delete lookup[row.id];

                    for (var p in fieldSpec) {

                        if (remoteData[p] != undefined && remoteData[p] != row[p]) {
                            var update = {
                                property: p,
                                value: remoteData[p]
                            };

                            if (!updates) {
                                updates = {
                                    row: row,
                                    changes: []
                                };
                            }
                            updates.changes.push(update);
                        }
                    }

                    if (updates) updatesToPush.push(updates);
                }
                else if (syncRemote) {
                    toRemove.push(row.id);
                }
            }
        });

        for (var id in lookup)
            if (!lookup[id]._removed)
                toInsert.push(id);


        entity.all().filter('id', 'in', toRemove).destroyAll(function() {
            persistence.asyncForEach(toInsert, function (id, next) {
                var item = new entity(mapper.row(lookup[id]));
                persistence.add(item).flush(function () {
                    next();
                });
            }, function () {
//                console.log('UPDATES: ', updatesToPush);

                updatesToPush.forEach(function (item) {
                    item.changes.forEach(function (change) {
                        var property = change.property,
                            value = change.value
                            ;

                        item.row[property] = value;
                    })

                    persistence.add(item.row);
                });

                persistence.flush(function () {
                    callback();
                })
            });
        });

    })
};
var SyncAll = function ($http, onSyncComplete) {
    var toSync = ['missions', 'tasks', 'steps'];
    var ready = {};

    var complete = function () {
        for (var scope in ready)
            if (ready[scope] === false) return;

        console.log('SYNC COMPLETE');
        onSyncComplete();
    }

    $http({method: 'POST', url: DOMAIN + '/api/sync', data: {lastSync: Math.random()}})
        .success(function (results) {
            var syncs = [];

            for (var key in results)
                if (toSync.indexOf(key) >= 0) {
                    ready[key] = false;
                    syncs.push({key: key, items: results[key]});
                }

            persistence.asyncForEach(syncs, function (item, next) {
                var key = item.key,
                    items = item.items,
                    entity = window[[key.capitalize().replace(/s$/, ''), 'Model'].join('')],
                    mapper = window[[key.capitalize(), 'Mapper'].join('')]
                    ;

                new Sync(
                    entity,
                    items,
                    mapper,
                    function () {
                        ready[key] = true;
                        next();
                    }
                );

            }, function () {
                complete();
            })
        })
        .error(function () {
            alertify.error('Ошибка запроса');
        })
    ;
}
;
function without(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}
;
var Geolocator = {
    position: function(onSuccess, onError) {
        var onError = onError || function() { alertify.error('Служба геолокации недоступна...') }
        var onSuccess = onSuccess || function() { alertify.notice('Положение определено успешно!') };;

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    onSuccess(position.coords, position.timestamp)
                },
                function() {
                    onError();
                }
            );
        }
        else {
            onError();
        }
    }
}
;
// moment.js language configuration
// language : russian (ru)
// author : Viktorminator : https://github.com/Viktorminator
// Author : Menelion Elensúle : https://github.com/Oire

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['moment'], factory); // AMD
    } else if (typeof exports === 'object') {
        module.exports = factory(require('../moment')); // Node
    } else {
        factory(window.moment); // Browser global
    }
}(function (moment) {
    function plural(word, num) {
        var forms = word.split('_');
        return num % 10 === 1 && num % 100 !== 11 ? forms[0] : (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) ? forms[1] : forms[2]);
    }

    function relativeTimeWithPlural(number, withoutSuffix, key) {
        var format = {
            'mm': 'минута_минуты_минут',
            'hh': 'час_часа_часов',
            'dd': 'день_дня_дней',
            'MM': 'месяц_месяца_месяцев',
            'yy': 'год_года_лет'
        };
        if (key === 'm') {
            return withoutSuffix ? 'минута' : 'минуту';
        }
        else {
            return number + ' ' + plural(format[key], +number);
        }
    }

    function monthsCaseReplace(m, format) {
        var months = {
                'nominative': 'январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь'.split('_'),
                'accusative': 'января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря'.split('_')
            },

            nounCase = (/D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/).test(format) ?
                'accusative' :
                'nominative';

        return months[nounCase][m.month()];
    }

    function monthsShortCaseReplace(m, format) {
        var monthsShort = {
                'nominative': 'янв_фев_мар_апр_май_июнь_июль_авг_сен_окт_ноя_дек'.split('_'),
                'accusative': 'янв_фев_мар_апр_мая_июня_июля_авг_сен_окт_ноя_дек'.split('_')
            },

            nounCase = (/D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/).test(format) ?
                'accusative' :
                'nominative';

        return monthsShort[nounCase][m.month()];
    }

    function weekdaysCaseReplace(m, format) {
        var weekdays = {
                'nominative': 'воскресенье_понедельник_вторник_среда_четверг_пятница_суббота'.split('_'),
                'accusative': 'воскресенье_понедельник_вторник_среду_четверг_пятницу_субботу'.split('_')
            },

            nounCase = (/\[ ?[Вв] ?(?:прошлую|следующую)? ?\] ?dddd/).test(format) ?
                'accusative' :
                'nominative';

        return weekdays[nounCase][m.day()];
    }

    return moment.lang('ru', {
        months : monthsCaseReplace,
        monthsShort : monthsShortCaseReplace,
        weekdays : weekdaysCaseReplace,
        weekdaysShort : "вс_пн_вт_ср_чт_пт_сб".split("_"),
        weekdaysMin : "вс_пн_вт_ср_чт_пт_сб".split("_"),
        monthsParse : [/^янв/i, /^фев/i, /^мар/i, /^апр/i, /^ма[й|я]/i, /^июн/i, /^июл/i, /^авг/i, /^сен/i, /^окт/i, /^ноя/i, /^дек/i],
        longDateFormat : {
            LT : "HH:mm",
            L : "DD.MM.YYYY",
            LL : "D MMMM YYYY г.",
            LLL : "D MMMM YYYY г., LT",
            LLLL : "dddd, D MMMM YYYY г., LT"
        },
        calendar : {
            sameDay: '[Сегодня в] LT',
            nextDay: '[Завтра в] LT',
            lastDay: '[Вчера в] LT',
            nextWeek: function () {
                return this.day() === 2 ? '[Во] dddd [в] LT' : '[В] dddd [в] LT';
            },
            lastWeek: function () {
                switch (this.day()) {
                    case 0:
                        return '[В прошлое] dddd [в] LT';
                    case 1:
                    case 2:
                    case 4:
                        return '[В прошлый] dddd [в] LT';
                    case 3:
                    case 5:
                    case 6:
                        return '[В прошлую] dddd [в] LT';
                }
            },
            sameElse: 'L'
        },
        relativeTime : {
            future : "через %s",
            past : "%s назад",
            s : "несколько секунд",
            m : relativeTimeWithPlural,
            mm : relativeTimeWithPlural,
            h : "час",
            hh : relativeTimeWithPlural,
            d : "день",
            dd : relativeTimeWithPlural,
            M : "месяц",
            MM : relativeTimeWithPlural,
            y : "год",
            yy : relativeTimeWithPlural
        },

        // M. E.: those two are virtually unused but a user might want to implement them for his/her website for some reason

        meridiem : function (hour, minute, isLower) {
            if (hour < 4) {
                return "ночи";
            } else if (hour < 12) {
                return "утра";
            } else if (hour < 17) {
                return "дня";
            } else {
                return "вечера";
            }
        },

        ordinal: function (number, period) {
            switch (period) {
                case 'M':
                case 'd':
                case 'DDD':
                    return number + '-й';
                case 'D':
                    return number + '-го';
                case 'w':
                case 'W':
                    return number + '-я';
                default:
                    return number;
            }
        },

        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 7  // The week that contains Jan 1st is the first week of the year.
        }
    });
}));
moment.lang('ru');
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
;
var Timer = function (el, seconds) {

    var now = new Date().getTime() / 1000;

    var countdown = function () {
        var passed = seconds + Math.round(now - new Date().getTime() / 1000);


        el.toggleClass('time-warning', passed < 3600);
        el.toggleClass('time-danger', passed < 60);

        el.html(moment.duration(passed, "seconds").humanize());

        setTimeout(function () {
            if (passed > 0) countdown();
        }, 1000);
    }

    countdown();
}

var toHHMMSS = function (_seconds) {
    var sec_num = parseInt(_seconds, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    var time = [];

    time.push(hours);
    time.push(minutes);
    time.push(seconds);

    return time.join(':');
}
;
var YMap = function(map, $scope, templateLayoutFactory, $rootScope) {
    $scope.overrides={
        build: function () {
            var BalloonContentLayout = templateLayoutFactory.get('targetBaloon');
            BalloonContentLayout.superclass.build.call(this);
            angular.element($('[data-mission]')).bind('click', this.onMissionClick);

            var missionId = $('[data-mission]').first().data('mission');

            if (missionId) {
                Seen(missionId, MissionModel);
                $scope.updateBadges();
            }
        },

        onMissionClick: function() {
            var mission = $('[data-mission]').first().data('mission');
            window.location.href="#/missions/" + mission;
        },

        clear: function () {
            angular.element($('[data-mission]')).unbind('click', this.onMissionClick)
            var BalloonContentLayout = templateLayoutFactory.get('targetBaloon');
            BalloonContentLayout.superclass.clear.call(this);
        }
    };
}
;











