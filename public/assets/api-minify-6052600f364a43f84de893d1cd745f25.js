/* Modernizr 2.7.1 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-touch-teststyles-prefixes
 */

;window.Modernizr=function(a,b,c){function v(a){i.cssText=a}function w(a,b){return v(l.join(a+";")+(b||""))}function x(a,b){return typeof a===b}function y(a,b){return!!~(""+a).indexOf(b)}function z(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:x(f,"function")?f.bind(d||b):f}return!1}var d="2.7.1",e={},f=b.documentElement,g="modernizr",h=b.createElement(g),i=h.style,j,k={}.toString,l=" -webkit- -moz- -o- -ms- ".split(" "),m={},n={},o={},p=[],q=p.slice,r,s=function(a,c,d,e){var h,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:g+(d+1),l.appendChild(j);return h=["&#173;",'<style id="s',g,'">',a,"</style>"].join(""),l.id=g,(m?l:n).innerHTML+=h,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=f.style.overflow,f.style.overflow="hidden",f.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),f.style.overflow=k),!!i},t={}.hasOwnProperty,u;!x(t,"undefined")&&!x(t.call,"undefined")?u=function(a,b){return t.call(a,b)}:u=function(a,b){return b in a&&x(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=q.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(q.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(q.call(arguments)))};return e}),m.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:s(["@media (",l.join("touch-enabled),("),g,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c};for(var A in m)u(m,A)&&(r=A.toLowerCase(),e[r]=m[A](),p.push((e[r]?"":"no-")+r));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)u(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof enableClasses!="undefined"&&enableClasses&&(f.className+=" "+(b?"":"no-")+a),e[a]=b}return e},v(""),h=j=null,e._version=d,e._prefixes=l,e.testStyles=s,e}(this,this.document);
/**
 * alertify
 * An unobtrusive customizable JavaScript notification system
 *
 * @author Fabien Doiron <fabien.doiron@gmail.com>
 * @copyright Fabien Doiron 2012
 * @license MIT <http://opensource.org/licenses/mit-license.php>
 * @link http://fabien-d.github.com/alertify.js/
 * @module alertify
 * @version 0.3.2
 */

/*global define*/

(function (global, undefined) {
  "use strict";

  var document = global.document,
      Alertify;

  Alertify = function () {

    var _alertify = {},
        dialogs   = {},
        isopen    = false,
        keys      = { ENTER: 13, ESC: 27, SPACE: 32 },
        queue     = [],
        $, elCallee, elCover, elDialog, elLog, getTransitionEvent;

    /**
     * Markup pieces
     * @type {Object}
     */
    dialogs = {
      buttons : {
        holder : "<nav class=\"alertify-buttons\">{{buttons}}</nav>",
        submit : "<button type=\"submit\" class=\"alertify-button alertify-button-ok\" id=\"alertify-ok\" />{{ok}}</button>",
        ok     : "<a href=\"#\" class=\"alertify-button alertify-button-ok\" id=\"alertify-ok\">{{ok}}</a>",
        cancel : "<a href=\"#\" class=\"alertify-button alertify-button-cancel\" id=\"alertify-cancel\">{{cancel}}</a>"
      },
      input   : "<div class=\"alertify-text-wrapper\"><input type=\"text\" class=\"alertify-text\" id=\"alertify-text\"></div>",
      message : "<p class=\"alertify-message\">{{message}}</p>",
      log     : "<article class=\"alertify-log{{class}}\">{{message}}</article>"
    };

    /**
     * Return the proper transitionend event
     * @return {String}    Transition type string
     */
    getTransitionEvent = function () {
      var t,
          el = document.createElement("fakeelement"),
          transitions = {
          "transition"       : "transitionend",
          "OTransition"      : "otransitionend",
          "MSTransition"     : "msTransitionEnd",
          "MozTransition"    : "transitionend",
          "WebkitTransition" : "webkitTransitionEnd"
        };

      for (t in transitions) {
        if (el.style[t] !== undefined) return transitions[t];
      }
    };

    /**
     * Shorthand for document.getElementById()
     *
     * @param  {String} id    A specific element ID
     * @return {Object}       HTML element
     */
    $ = function (id) {
      return document.getElementById(id);
    };

    /**
     * Alertify private object
     * @type {Object}
     */
    _alertify = {

      /**
       * Labels object
       * @type {Object}
       */
      labels : {
        ok     : "OK",
        cancel : "Cancel"
      },

      /**
       * Delay number
       * @type {Number}
       */
      delay : 5000,

      /**
       * Whether buttons are reversed (default is secondary/primary)
       * @type {Boolean}
       */
      buttonReverse : false,

      /**
       * Set the transition event on load
       * @type {[type]}
       */
      transition : undefined,

      /**
       * Set the proper button click events
       *
       * @param {Function} fn    [Optional] Callback function
       *
       * @return {undefined}
       */
      addListeners : function (fn) {
        var btnReset  = $("alertify-resetFocus"),
            btnOK     = $("alertify-ok")     || undefined,
            btnCancel = $("alertify-cancel") || undefined,
            input     = $("alertify-text")   || undefined,
            form      = $("alertify-form")   || undefined,
            hasOK     = (typeof btnOK !== "undefined"),
            hasCancel = (typeof btnCancel !== "undefined"),
            hasInput  = (typeof input !== "undefined"),
            val       = "",
            self      = this,
            ok, cancel, common, key, reset;

        // ok event handler
        ok = function (event) {
          if (typeof event.preventDefault !== "undefined") event.preventDefault();
          common(event);
          if (typeof input !== "undefined") val = input.value;
          if (typeof fn === "function") fn(true, val);
        };

        // cancel event handler
        cancel = function (event) {
          if (typeof event.preventDefault !== "undefined") event.preventDefault();
          common(event);
          if (typeof fn === "function") fn(false);
        };

        // common event handler (keyup, ok and cancel)
        common = function (event) {
          self.hide();
          self.unbind(document.body, "keyup", key);
          self.unbind(btnReset, "focus", reset);
          if (hasInput) self.unbind(form, "submit", ok);
          if (hasOK) self.unbind(btnOK, "click", ok);
          if (hasCancel) self.unbind(btnCancel, "click", cancel);
        };

        // keyup handler
        key = function (event) {
          var keyCode = event.keyCode;
          if (keyCode === keys.SPACE && !hasInput) ok(event);
          if (keyCode === keys.ESC && hasCancel) cancel(event);
        };

        // reset focus to first item in the dialog
        reset = function (event) {
          if (hasInput) input.focus();
          else if (hasCancel) btnCancel.focus();
          else btnOK.focus();
        };

        // handle reset focus link
        // this ensures that the keyboard focus does not
        // ever leave the dialog box until an action has
        // been taken
        this.bind(btnReset, "focus", reset);
        // handle OK click
        if (hasOK) this.bind(btnOK, "click", ok);
        // handle Cancel click
        if (hasCancel) this.bind(btnCancel, "click", cancel);
        // listen for keys, Cancel => ESC
        this.bind(document.body, "keyup", key);
        // bind form submit
        if (hasInput) this.bind(form, "submit", ok);
        // set focus on OK button or the input text
        global.setTimeout(function () {
          if (input) {
            input.focus();
            input.select();
          }
          else btnOK.focus();
        }, 50);
      },

      /**
       * Bind events to elements
       *
       * @param  {Object}   el       HTML Object
       * @param  {Event}    event    Event to attach to element
       * @param  {Function} fn       Callback function
       *
       * @return {undefined}
       */
      bind : function (el, event, fn) {
        if (typeof el.addEventListener === "function") {
          el.addEventListener(event, fn, false);
        } else if (el.attachEvent) {
          el.attachEvent("on" + event, fn);
        }
      },

      /**
       * Append button HTML strings
       *
       * @param {String} secondary    The secondary button HTML string
       * @param {String} primary      The primary button HTML string
       *
       * @return {String}             The appended button HTML strings
       */
      appendButtons : function (secondary, primary) {
        return this.buttonReverse ? primary + secondary : secondary + primary;
      },

      /**
       * Build the proper message box
       *
       * @param  {Object} item    Current object in the queue
       *
       * @return {String}         An HTML string of the message box
       */
      build : function (item) {
        var html    = "",
            type    = item.type,
            message = item.message,
            css     = item.cssClass || "";

        html += "<div class=\"alertify-dialog\">";

        if (type === "prompt") html += "<form id=\"alertify-form\">";

        html += "<article class=\"alertify-inner\">";
        html += dialogs.message.replace("{{message}}", message);

        if (type === "prompt") html += dialogs.input;

        html += dialogs.buttons.holder;
        html += "</article>";

        if (type === "prompt") html += "</form>";

        html += "<a id=\"alertify-resetFocus\" class=\"alertify-resetFocus\" href=\"#\">Reset Focus</a>";
        html += "</div>";

        switch (type) {
        case "confirm":
          html = html.replace("{{buttons}}", this.appendButtons(dialogs.buttons.cancel, dialogs.buttons.ok));
          html = html.replace("{{ok}}", this.labels.ok).replace("{{cancel}}", this.labels.cancel);
          break;
        case "prompt":
          html = html.replace("{{buttons}}", this.appendButtons(dialogs.buttons.cancel, dialogs.buttons.submit));
          html = html.replace("{{ok}}", this.labels.ok).replace("{{cancel}}", this.labels.cancel);
          break;
        case "alert":
          html = html.replace("{{buttons}}", dialogs.buttons.ok);
          html = html.replace("{{ok}}", this.labels.ok);
          break;
        default:
          break;
        }

        elDialog.className = "alertify alertify-show alertify-" + type + " " + css;
        elCover.className  = "alertify-cover";
        return html;
      },

      /**
       * Close the log messages
       *
       * @param  {Object} elem    HTML Element of log message to close
       * @param  {Number} wait    [optional] Time (in ms) to wait before automatically hiding the message, if 0 never hide
       *
       * @return {undefined}
       */
      close : function (elem, wait) {
        // Unary Plus: +"2" === 2
        var timer = (wait && !isNaN(wait)) ? +wait : this.delay,
            self  = this,
            removeElement;

        this.bind(elem, "click", function () {
          elLog.removeChild(elem);
        });

        // Remove element after transition is done
        removeElement = function (event) {
          event.stopPropagation();
          // transitionend event gets fired for every property
          // this ensures it only tries to remove the element once
          if (event.propertyName === "opacity") elLog.removeChild(this);
        };

        // never close (until click) if wait is set to 0
        if (wait === 0) return;

        setTimeout(function () {
          // ensure element exists
          if (typeof elem !== "undefined" && elem.parentNode === elLog) {
            // whether CSS transition exists
            if (typeof self.transition !== "undefined") {
              self.bind(elem, self.transition, removeElement);
              elem.className += " alertify-log-hide";
            } else {
              elLog.removeChild(elem);
            }
          }
        }, timer);
      },

      /**
       * Create a dialog box
       *
       * @param  {String}   message        The message passed from the callee
       * @param  {String}   type           Type of dialog to create
       * @param  {Function} fn             [Optional] Callback function
       * @param  {String}   placeholder    [Optional] Default value for prompt input field
       * @param  {String}   cssClass       [Optional] Class(es) to append to dialog box
       *
       * @return {Object}
       */
      dialog : function (message, type, fn, placeholder, cssClass) {
        // set the current active element
        // this allows the keyboard focus to be resetted
        // after the dialog box is closed
        elCallee = document.activeElement;
        // check to ensure the alertify dialog element
        // has been successfully created
        var check = function () {
          if (elDialog && elDialog.scrollTop !== null) return;
          else check();
        };
        // error catching
        if (typeof message !== "string") throw new Error("message must be a string");
        if (typeof type !== "string") throw new Error("type must be a string");
        if (typeof fn !== "undefined" && typeof fn !== "function") throw new Error("fn must be a function");
        // initialize alertify if it hasn't already been done
        if (typeof this.init === "function") {
          this.init();
          check();
        }

        queue.push({ type: type, message: message, callback: fn, placeholder: placeholder, cssClass: cssClass });
        if (!isopen) this.setup();

        return this;
      },

      /**
       * Extend the log method to create custom methods
       *
       * @param  {String} type    Custom method name
       *
       * @return {Function}
       */
      extend : function (type) {
        if (typeof type !== "string") throw new Error("extend method must have exactly one paramter");
        return function (message, wait) {
          this.log(message, type, wait);
          return this;
        };
      },

      /**
       * Hide the dialog and rest to defaults
       *
       * @return {undefined}
       */
      hide : function () {
        // remove reference from queue
        queue.splice(0,1);
        // if items remaining in the queue
        if (queue.length > 0) this.setup();
        else {
          isopen = false;
          elDialog.className = "alertify alertify-hide alertify-hidden";
          elCover.className  = "alertify-cover alertify-hidden";
          // set focus to the last element or body
          // after the dialog is closed
          elCallee.focus();
        }
      },

      /**
       * Initialize Alertify
       * Create the 2 main elements
       *
       * @return {undefined}
       */
      init : function () {
        // ensure legacy browsers support html5 tags
        document.createElement("nav");
        document.createElement("article");
        document.createElement("section");
        // cover
        elCover = document.createElement("div");
        elCover.setAttribute("id", "alertify-cover");
        elCover.className = "alertify-cover alertify-hidden";
        document.body.appendChild(elCover);
        // main element
        elDialog = document.createElement("section");
        elDialog.setAttribute("id", "alertify");
        elDialog.className = "alertify alertify-hidden";
        document.body.appendChild(elDialog);
        // log element
        elLog = document.createElement("section");
        elLog.setAttribute("id", "alertify-logs");
        elLog.className = "alertify-logs";
        document.body.appendChild(elLog);
        // set tabindex attribute on body element
        // this allows script to give it focus
        // after the dialog is closed
        document.body.setAttribute("tabindex", "0");
        // set transition type
        this.transition = getTransitionEvent();
        // clean up init method
        delete this.init;
      },

      /**
       * Show a new log message box
       *
       * @param  {String} message    The message passed from the callee
       * @param  {String} type       [Optional] Optional type of log message
       * @param  {Number} wait       [Optional] Time (in ms) to wait before auto-hiding the log
       *
       * @return {Object}
       */
      log : function (message, type, wait) {
        // check to ensure the alertify dialog element
        // has been successfully created
        var check = function () {
          if (elLog && elLog.scrollTop !== null) return;
          else check();
        };
        // initialize alertify if it hasn't already been done
        if (typeof this.init === "function") {
          this.init();
          check();
        }
        this.notify(message, type, wait);
        return this;
      },

      /**
       * Add new log message
       * If a type is passed, a class name "alertify-log-{type}" will get added.
       * This allows for custom look and feel for various types of notifications.
       *
       * @param  {String} message    The message passed from the callee
       * @param  {String} type       [Optional] Type of log message
       * @param  {Number} wait       [Optional] Time (in ms) to wait before auto-hiding
       *
       * @return {undefined}
       */
      notify : function (message, type, wait) {
        var log = document.createElement("article");
        log.className = "alertify-log" + ((typeof type === "string" && type !== "") ? " alertify-log-" + type : "");
        log.innerHTML = message;
        // prepend child
        elLog.insertBefore(log, elLog.firstChild);
        // triggers the CSS animation
        setTimeout(function() { log.className = log.className + " alertify-log-show"; }, 50);
        this.close(log, wait);
      },

      /**
       * Set properties
       *
       * @param {Object} args     Passing parameters
       *
       * @return {undefined}
       */
      set : function (args) {
        var k;
        // error catching
        if (typeof args !== "object" && args instanceof Array) throw new Error("args must be an object");
        // set parameters
        for (k in args) {
          if (args.hasOwnProperty(k)) {
            this[k] = args[k];
          }
        }
      },

      /**
       * Initiate all the required pieces for the dialog box
       *
       * @return {undefined}
       */
      setup : function () {
        var item = queue[0];

        isopen = true;
        elDialog.innerHTML = this.build(item);
        if (typeof item.placeholder === "string" && item.placeholder !== "") $("alertify-text").value = item.placeholder;
        this.addListeners(item.callback);
      },

      /**
       * Unbind events to elements
       *
       * @param  {Object}   el       HTML Object
       * @param  {Event}    event    Event to detach to element
       * @param  {Function} fn       Callback function
       *
       * @return {undefined}
       */
      unbind : function (el, event, fn) {
        if (typeof el.removeEventListener === "function") {
          el.removeEventListener(event, fn, false);
        } else if (el.detachEvent) {
          el.detachEvent("on" + event, fn);
        }
      }
    };

    return {
      alert   : function (message, fn, cssClass) { _alertify.dialog(message, "alert", fn, "", cssClass); return this; },
      confirm : function (message, fn, cssClass) { _alertify.dialog(message, "confirm", fn, "", cssClass); return this; },
      extend  : _alertify.extend,
      init    : _alertify.init,
      log     : function (message, type, wait) { _alertify.log(message, type, wait); return this; },
      prompt  : function (message, fn, placeholder, cssClass) { _alertify.dialog(message, "prompt", fn, placeholder, cssClass); return this; },
      success : function (message, wait) { _alertify.log(message, "success", wait); return this; },
      error   : function (message, wait) { _alertify.log(message, "error", wait); return this; },
      set     : function (args) { _alertify.set(args); },
      labels  : _alertify.labels
    };
  };

  // AMD and window support
  if (typeof define === "function") {
    define([], function () { return new Alertify(); });
  } else {
    if (typeof global.alertify === "undefined") {
      global.alertify = new Alertify();
    }
  }

}(this));
/*!
 * jQuery JavaScript Library v1.11.0
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-01-23T21:02Z
 */


(function( global, factory ) {

	if ( typeof module === "object" && typeof module.exports === "object" ) {
		// For CommonJS and CommonJS-like environments where a proper window is present,
		// execute the factory and get jQuery
		// For environments that do not inherently posses a window with a document
		// (such as Node.js), expose a jQuery-making factory as module.exports
		// This accentuates the need for the creation of a real window
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Can't do this because several apps including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
// Support: Firefox 18+
//

var deletedIds = [];

var slice = deletedIds.slice;

var concat = deletedIds.concat;

var push = deletedIds.push;

var indexOf = deletedIds.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var trim = "".trim;

var support = {};



var
	version = "1.11.0",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {
		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Make sure we trim BOM and NBSP (here's looking at you, Safari 5.0 and IE)
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([\da-z])/gi,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

jQuery.fn = jQuery.prototype = {
	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// Start with an empty selector
	selector: "",

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num != null ?

			// Return a 'clean' array
			( num < 0 ? this[ num + this.length ] : this[ num ] ) :

			// Return just the object
			slice.call( this );
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;
		ret.context = this.context;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	// (You can seed the arguments with an array of args, but this is
	// only used internally.)
	each: function( callback, args ) {
		return jQuery.each( this, callback, args );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map(this, function( elem, i ) {
			return callback.call( elem, i, elem );
		}));
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[j] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor(null);
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: deletedIds.sort,
	splice: deletedIds.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var src, copyIsArray, copy, name, options, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
		target = {};
	}

	// extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray(src) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject(src) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend({
	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	// See test/unit/core.js for details concerning isFunction.
	// Since version 1.3, DOM methods and functions like alert
	// aren't supported. They return false on IE (#2968).
	isFunction: function( obj ) {
		return jQuery.type(obj) === "function";
	},

	isArray: Array.isArray || function( obj ) {
		return jQuery.type(obj) === "array";
	},

	isWindow: function( obj ) {
		/* jshint eqeqeq: false */
		return obj != null && obj == obj.window;
	},

	isNumeric: function( obj ) {
		// parseFloat NaNs numeric-cast false positives (null|true|false|"")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		return obj - parseFloat( obj ) >= 0;
	},

	isEmptyObject: function( obj ) {
		var name;
		for ( name in obj ) {
			return false;
		}
		return true;
	},

	isPlainObject: function( obj ) {
		var key;

		// Must be an Object.
		// Because of IE, we also have to check the presence of the constructor property.
		// Make sure that DOM nodes and window objects don't pass through, as well
		if ( !obj || jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
			return false;
		}

		try {
			// Not own constructor property must be Object
			if ( obj.constructor &&
				!hasOwn.call(obj, "constructor") &&
				!hasOwn.call(obj.constructor.prototype, "isPrototypeOf") ) {
				return false;
			}
		} catch ( e ) {
			// IE8,9 Will throw exceptions on certain host objects #9897
			return false;
		}

		// Support: IE<9
		// Handle iteration over inherited properties before own properties.
		if ( support.ownLast ) {
			for ( key in obj ) {
				return hasOwn.call( obj, key );
			}
		}

		// Own properties are enumerated firstly, so to speed up,
		// if last one is own, then all properties are own.
		for ( key in obj ) {}

		return key === undefined || hasOwn.call( obj, key );
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}
		return typeof obj === "object" || typeof obj === "function" ?
			class2type[ toString.call(obj) ] || "object" :
			typeof obj;
	},

	// Evaluates a script in a global context
	// Workarounds based on findings by Jim Driscoll
	// http://weblogs.java.net/blog/driscoll/archive/2009/09/08/eval-javascript-global-context
	globalEval: function( data ) {
		if ( data && jQuery.trim( data ) ) {
			// We use execScript on Internet Explorer
			// We use an anonymous function so that context is window
			// rather than jQuery in Firefox
			( window.execScript || function( data ) {
				window[ "eval" ].call( window, data );
			} )( data );
		}
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
	},

	// args is for internal usage only
	each: function( obj, callback, args ) {
		var value,
			i = 0,
			length = obj.length,
			isArray = isArraylike( obj );

		if ( args ) {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			}

		// A special, fast, case for the most common use of each
		} else {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			}
		}

		return obj;
	},

	// Use native String.trim function wherever possible
	trim: trim && !trim.call("\uFEFF\xA0") ?
		function( text ) {
			return text == null ?
				"" :
				trim.call( text );
		} :

		// Otherwise use our own trimming functionality
		function( text ) {
			return text == null ?
				"" :
				( text + "" ).replace( rtrim, "" );
		},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArraylike( Object(arr) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		var len;

		if ( arr ) {
			if ( indexOf ) {
				return indexOf.call( arr, elem, i );
			}

			len = arr.length;
			i = i ? i < 0 ? Math.max( 0, len + i ) : i : 0;

			for ( ; i < len; i++ ) {
				// Skip accessing in sparse arrays
				if ( i in arr && arr[ i ] === elem ) {
					return i;
				}
			}
		}

		return -1;
	},

	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		while ( j < len ) {
			first[ i++ ] = second[ j++ ];
		}

		// Support: IE<9
		// Workaround casting of .length to NaN on otherwise arraylike objects (e.g., NodeLists)
		if ( len !== len ) {
			while ( second[j] !== undefined ) {
				first[ i++ ] = second[ j++ ];
			}
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var value,
			i = 0,
			length = elems.length,
			isArray = isArraylike( elems ),
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArray ) {
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var args, proxy, tmp;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: function() {
		return +( new Date() );
	},

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
});

// Populate the class2type map
jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
});

function isArraylike( obj ) {
	var length = obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	if ( obj.nodeType === 1 && length ) {
		return true;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v1.10.16
 * http://sizzlejs.com/
 *
 * Copyright 2013 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-01-13
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	compile,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + -(new Date()),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// General-purpose constants
	strundefined = typeof undefined,
	MAX_NEGATIVE = 1 << 31,

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf if we can't use a native one
	indexOf = arr.indexOf || function( elem ) {
		var i = 0,
			len = this.length;
		for ( ; i < len; i++ ) {
			if ( this[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",
	// http://www.w3.org/TR/css3-syntax/#characters
	characterEncoding = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

	// Loosely modeled on CSS identifier characters
	// An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors
	// Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = characterEncoding.replace( "w", "w#" ),

	// Acceptable operators http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + characterEncoding + ")" + whitespace +
		"*(?:([*^$|!~]?=)" + whitespace + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + identifier + ")|)|)" + whitespace + "*\\]",

	// Prefer arguments quoted,
	//   then not containing pseudos/brackets,
	//   then attribute selectors/non-parenthetical expressions,
	//   then anything else
	// These preferences are here to reduce the number of selectors
	//   needing tokenize in the PSEUDO preFilter
	pseudos = ":(" + characterEncoding + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + attributes.replace( 3, 8 ) + ")*)|.*)\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + characterEncoding + ")" ),
		"CLASS": new RegExp( "^\\.(" + characterEncoding + ")" ),
		"TAG": new RegExp( "^(" + characterEncoding.replace( "w", "w*" ) + ")" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,
	rescape = /'|\\/g,

	// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	};

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var match, elem, m, nodeType,
		// QSA vars
		i, groups, old, nid, newContext, newSelector;

	if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
		setDocument( context );
	}

	context = context || document;
	results = results || [];

	if ( !selector || typeof selector !== "string" ) {
		return results;
	}

	if ( (nodeType = context.nodeType) !== 1 && nodeType !== 9 ) {
		return [];
	}

	if ( documentIsHTML && !seed ) {

		// Shortcuts
		if ( (match = rquickExpr.exec( selector )) ) {
			// Speed-up: Sizzle("#ID")
			if ( (m = match[1]) ) {
				if ( nodeType === 9 ) {
					elem = context.getElementById( m );
					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document (jQuery #6963)
					if ( elem && elem.parentNode ) {
						// Handle the case where IE, Opera, and Webkit return items
						// by name instead of ID
						if ( elem.id === m ) {
							results.push( elem );
							return results;
						}
					} else {
						return results;
					}
				} else {
					// Context is not a document
					if ( context.ownerDocument && (elem = context.ownerDocument.getElementById( m )) &&
						contains( context, elem ) && elem.id === m ) {
						results.push( elem );
						return results;
					}
				}

			// Speed-up: Sizzle("TAG")
			} else if ( match[2] ) {
				push.apply( results, context.getElementsByTagName( selector ) );
				return results;

			// Speed-up: Sizzle(".CLASS")
			} else if ( (m = match[3]) && support.getElementsByClassName && context.getElementsByClassName ) {
				push.apply( results, context.getElementsByClassName( m ) );
				return results;
			}
		}

		// QSA path
		if ( support.qsa && (!rbuggyQSA || !rbuggyQSA.test( selector )) ) {
			nid = old = expando;
			newContext = context;
			newSelector = nodeType === 9 && selector;

			// qSA works strangely on Element-rooted queries
			// We can work around this by specifying an extra ID on the root
			// and working up from there (Thanks to Andrew Dupont for the technique)
			// IE 8 doesn't work on object elements
			if ( nodeType === 1 && context.nodeName.toLowerCase() !== "object" ) {
				groups = tokenize( selector );

				if ( (old = context.getAttribute("id")) ) {
					nid = old.replace( rescape, "\\$&" );
				} else {
					context.setAttribute( "id", nid );
				}
				nid = "[id='" + nid + "'] ";

				i = groups.length;
				while ( i-- ) {
					groups[i] = nid + toSelector( groups[i] );
				}
				newContext = rsibling.test( selector ) && testContext( context.parentNode ) || context;
				newSelector = groups.join(",");
			}

			if ( newSelector ) {
				try {
					push.apply( results,
						newContext.querySelectorAll( newSelector )
					);
					return results;
				} catch(qsaError) {
				} finally {
					if ( !old ) {
						context.removeAttribute("id");
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {Function(string, Object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
function assert( fn ) {
	var div = document.createElement("div");

	try {
		return !!fn( div );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( div.parentNode ) {
			div.parentNode.removeChild( div );
		}
		// release memory in IE
		div = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = attrs.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			( ~b.sourceIndex || MAX_NEGATIVE ) -
			( ~a.sourceIndex || MAX_NEGATIVE );

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== strundefined && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare,
		doc = node ? node.ownerDocument || node : preferredDoc,
		parent = doc.defaultView;

	// If no document and documentElement is available, return
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Set our document
	document = doc;
	docElem = doc.documentElement;

	// Support tests
	documentIsHTML = !isXML( doc );

	// Support: IE>8
	// If iframe document is assigned to "document" variable and if iframe has been reloaded,
	// IE will throw "permission denied" error when accessing "document" variable, see jQuery #13936
	// IE6-8 do not support the defaultView property so parent will be undefined
	if ( parent && parent !== parent.top ) {
		// IE11 does not have attachEvent, so all must suffer
		if ( parent.addEventListener ) {
			parent.addEventListener( "unload", function() {
				setDocument();
			}, false );
		} else if ( parent.attachEvent ) {
			parent.attachEvent( "onunload", function() {
				setDocument();
			});
		}
	}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties (excepting IE8 booleans)
	support.attributes = assert(function( div ) {
		div.className = "i";
		return !div.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( div ) {
		div.appendChild( doc.createComment("") );
		return !div.getElementsByTagName("*").length;
	});

	// Check if getElementsByClassName can be trusted
	support.getElementsByClassName = rnative.test( doc.getElementsByClassName ) && assert(function( div ) {
		div.innerHTML = "<div class='a'></div><div class='a i'></div>";

		// Support: Safari<4
		// Catch class over-caching
		div.firstChild.className = "i";
		// Support: Opera<10
		// Catch gEBCN failure to find non-leading classes
		return div.getElementsByClassName("i").length === 2;
	});

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( div ) {
		docElem.appendChild( div ).id = expando;
		return !doc.getElementsByName || !doc.getElementsByName( expando ).length;
	});

	// ID find and filter
	if ( support.getById ) {
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== strundefined && documentIsHTML ) {
				var m = context.getElementById( id );
				// Check parentNode to catch when Blackberry 4.6 returns
				// nodes that are no longer in the document #6963
				return m && m.parentNode ? [m] : [];
			}
		};
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
	} else {
		// Support: IE6/7
		// getElementById is not reliable as a find shortcut
		delete Expr.find["ID"];

		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== strundefined && elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== strundefined ) {
				return context.getElementsByTagName( tag );
			}
		} :
		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== strundefined && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See http://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( doc.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( div ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// http://bugs.jquery.com/ticket/12359
			div.innerHTML = "<select t=''><option selected=''></option></select>";

			// Support: IE8, Opera 10-12
			// Nothing should be selected when empty strings follow ^= or $= or *=
			if ( div.querySelectorAll("[t^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !div.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}
		});

		assert(function( div ) {
			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = doc.createElement("input");
			input.setAttribute( "type", "hidden" );
			div.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( div.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":enabled").length ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			div.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( div ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( div, "div" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( div, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully does not implement inclusive descendent
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf.call( sortInput, a ) - indexOf.call( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === doc ? -1 :
				b === doc ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf.call( sortInput, a ) - indexOf.call( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return doc;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch(e) {}
	}

	return Sizzle( expr, document, null, [elem] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[5] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] && match[4] !== undefined ) {
				match[2] = match[4];

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== strundefined && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, outerCache, node, diff, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) {
										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {
							// Seek `elem` from a previously-cached index
							outerCache = parent[ expando ] || (parent[ expando ] = {});
							cache = outerCache[ type ] || [];
							nodeIndex = cache[0] === dirruns && cache[1];
							diff = cache[0] === dirruns && cache[2];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									outerCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						// Use previously-cached element index if available
						} else if ( useCache && (cache = (elem[ expando ] || (elem[ expando ] = {}))[ type ]) && cache[0] === dirruns ) {
							diff = cache[1];

						// xml :nth-child(...) or :nth-last-child(...) or :nth(-last)?-of-type(...)
						} else {
							// Use the same loop as above to seek `elem` from the start
							while ( (node = ++nodeIndex && node && node[ dir ] ||
								(diff = nodeIndex = 0) || start.pop()) ) {

								if ( ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) && ++diff ) {
									// Cache the index of each encountered element
									if ( useCache ) {
										(node[ expando ] || (node[ expando ] = {}))[ type ] = [ dirruns, diff ];
									}

									if ( node === elem ) {
										break;
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf.call( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": function( elem ) {
			return elem.disabled === false;
		},

		"disabled": function( elem ) {
			return elem.disabled === true;
		},

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

function tokenize( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
}

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		checkNonElements = base && dir === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from dir caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});
						if ( (oldCache = outerCache[ dir ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							outerCache[ dir ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf.call( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf.call( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			return ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context !== document && context;
			}

			// Add elements passing elementMatchers directly to results
			// Keep `i` a string if there are no elements so `matchedCount` will be "00" below
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context, xml ) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// Apply set filters to unmatched elements
			matchedCount += i;
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, group /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !group ) {
			group = tokenize( selector );
		}
		i = group.length;
		while ( i-- ) {
			cached = matcherFromTokens( group[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );
	}
	return cached;
};

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function select( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		match = tokenize( selector );

	if ( !seed ) {
		// Try to minimize operations if there is only one group
		if ( match.length === 1 ) {

			// Take a shortcut and set the context if the root selector is an ID
			tokens = match[0] = match[0].slice( 0 );
			if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
					support.getById && context.nodeType === 9 && documentIsHTML &&
					Expr.relative[ tokens[1].type ] ) {

				context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
				if ( !context ) {
					return results;
				}
				selector = selector.slice( tokens.shift().value.length );
			}

			// Fetch a seed set for right-to-left matching
			i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
			while ( i-- ) {
				token = tokens[i];

				// Abort if we hit a combinator
				if ( Expr.relative[ (type = token.type) ] ) {
					break;
				}
				if ( (find = Expr.find[ type ]) ) {
					// Search, expanding context for leading sibling combinators
					if ( (seed = find(
						token.matches[0].replace( runescape, funescape ),
						rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
					)) ) {

						// If seed is empty or no tokens remain, we can return early
						tokens.splice( i, 1 );
						selector = seed.length && toSelector( tokens );
						if ( !selector ) {
							push.apply( results, seed );
							return results;
						}

						break;
					}
				}
			}
		}
	}

	// Compile and execute a filtering function
	// Provide `match` to avoid retokenization if we modified the selector above
	compile( selector, match )(
		seed,
		context,
		!documentIsHTML,
		results,
		rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
}

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome<14
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( div1 ) {
	// Should return 1, but returns 4 (following)
	return div1.compareDocumentPosition( document.createElement("div") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( div ) {
	div.innerHTML = "<a href='#'></a>";
	return div.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( div ) {
	div.innerHTML = "<input/>";
	div.firstChild.setAttribute( "value", "" );
	return div.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( div ) {
	return div.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[":"] = jQuery.expr.pseudos;
jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;



var rneedsContext = jQuery.expr.match.needsContext;

var rsingleTag = (/^<(\w+)\s*\/?>(?:<\/\1>|)$/);



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			/* jshint -W018 */
			return !!qualifier.call( elem, i, elem ) !== not;
		});

	}

	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		});

	}

	if ( typeof qualifier === "string" ) {
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}

		qualifier = jQuery.filter( qualifier, elements );
	}

	return jQuery.grep( elements, function( elem ) {
		return ( jQuery.inArray( elem, qualifier ) >= 0 ) !== not;
	});
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	return elems.length === 1 && elem.nodeType === 1 ?
		jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
		jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
			return elem.nodeType === 1;
		}));
};

jQuery.fn.extend({
	find: function( selector ) {
		var i,
			ret = [],
			self = this,
			len = self.length;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter(function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			}) );
		}

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		// Needed because $( selector, context ) becomes $( context ).find( selector )
		ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
		ret.selector = this.selector ? this.selector + " " + selector : selector;
		return ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow(this, selector || [], false) );
	},
	not: function( selector ) {
		return this.pushStack( winnow(this, selector || [], true) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
});


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// Use the correct document accordingly with window argument (sandbox)
	document = window.document,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,

	init = jQuery.fn.init = function( selector, context ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector.charAt(0) === "<" && selector.charAt( selector.length - 1 ) === ">" && selector.length >= 3 ) {
				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && (match[1] || !context) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[1] ) {
					context = context instanceof jQuery ? context[0] : context;

					// scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[1],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[1] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {
							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[2] );

					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document #6963
					if ( elem && elem.parentNode ) {
						// Handle the case where IE and Opera return items
						// by name instead of ID
						if ( elem.id !== match[2] ) {
							return rootjQuery.find( selector );
						}

						// Otherwise, we inject the element directly into the jQuery object
						this.length = 1;
						this[0] = elem;
					}

					this.context = document;
					this.selector = selector;
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || rootjQuery ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this.context = this[0] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return typeof rootjQuery.ready !== "undefined" ?
				rootjQuery.ready( selector ) :
				// Execute immediately if ready is not present
				selector( jQuery );
		}

		if ( selector.selector !== undefined ) {
			this.selector = selector.selector;
			this.context = selector.context;
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,
	// methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.extend({
	dir: function( elem, dir, until ) {
		var matched = [],
			cur = elem[ dir ];

		while ( cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !jQuery( cur ).is( until )) ) {
			if ( cur.nodeType === 1 ) {
				matched.push( cur );
			}
			cur = cur[dir];
		}
		return matched;
	},

	sibling: function( n, elem ) {
		var r = [];

		for ( ; n; n = n.nextSibling ) {
			if ( n.nodeType === 1 && n !== elem ) {
				r.push( n );
			}
		}

		return r;
	}
});

jQuery.fn.extend({
	has: function( target ) {
		var i,
			targets = jQuery( target, this ),
			len = targets.length;

		return this.filter(function() {
			for ( i = 0; i < len; i++ ) {
				if ( jQuery.contains( this, targets[i] ) ) {
					return true;
				}
			}
		});
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
				jQuery( selectors, context || this.context ) :
				0;

		for ( ; i < l; i++ ) {
			for ( cur = this[i]; cur && cur !== context; cur = cur.parentNode ) {
				// Always skip document fragments
				if ( cur.nodeType < 11 && (pos ?
					pos.index(cur) > -1 :

					// Don't pass non-elements to Sizzle
					cur.nodeType === 1 &&
						jQuery.find.matchesSelector(cur, selectors)) ) {

					matched.push( cur );
					break;
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.unique( matched ) : matched );
	},

	// Determine the position of an element within
	// the matched set of elements
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[0] && this[0].parentNode ) ? this.first().prevAll().length : -1;
		}

		// index in selector
		if ( typeof elem === "string" ) {
			return jQuery.inArray( this[0], jQuery( elem ) );
		}

		// Locate the position of the desired element
		return jQuery.inArray(
			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[0] : elem, this );
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.unique(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter(selector)
		);
	}
});

function sibling( cur, dir ) {
	do {
		cur = cur[ dir ];
	} while ( cur && cur.nodeType !== 1 );

	return cur;
}

jQuery.each({
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return jQuery.dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return jQuery.dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return jQuery.dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return jQuery.sibling( elem.firstChild );
	},
	contents: function( elem ) {
		return jQuery.nodeName( elem, "iframe" ) ?
			elem.contentDocument || elem.contentWindow.document :
			jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var ret = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			ret = jQuery.filter( selector, ret );
		}

		if ( this.length > 1 ) {
			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				ret = jQuery.unique( ret );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				ret = ret.reverse();
			}
		}

		return this.pushStack( ret );
	};
});
var rnotwhite = (/\S+/g);



// String to Object options format cache
var optionsCache = {};

// Convert String-formatted options into Object-formatted ones and store in cache
function createOptions( options ) {
	var object = optionsCache[ options ] = {};
	jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	});
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		( optionsCache[ options ] || createOptions( options ) ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,
		// Last fire value (for non-forgettable lists)
		memory,
		// Flag to know if list was already fired
		fired,
		// End of the loop when firing
		firingLength,
		// Index of currently firing callback (modified by remove if needed)
		firingIndex,
		// First callback to fire (used internally by add and fireWith)
		firingStart,
		// Actual callback list
		list = [],
		// Stack of fire calls for repeatable lists
		stack = !options.once && [],
		// Fire callbacks
		fire = function( data ) {
			memory = options.memory && data;
			fired = true;
			firingIndex = firingStart || 0;
			firingStart = 0;
			firingLength = list.length;
			firing = true;
			for ( ; list && firingIndex < firingLength; firingIndex++ ) {
				if ( list[ firingIndex ].apply( data[ 0 ], data[ 1 ] ) === false && options.stopOnFalse ) {
					memory = false; // To prevent further calls using add
					break;
				}
			}
			firing = false;
			if ( list ) {
				if ( stack ) {
					if ( stack.length ) {
						fire( stack.shift() );
					}
				} else if ( memory ) {
					list = [];
				} else {
					self.disable();
				}
			}
		},
		// Actual Callbacks object
		self = {
			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {
					// First, we save the current length
					var start = list.length;
					(function add( args ) {
						jQuery.each( args, function( _, arg ) {
							var type = jQuery.type( arg );
							if ( type === "function" ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && type !== "string" ) {
								// Inspect recursively
								add( arg );
							}
						});
					})( arguments );
					// Do we need to add the callbacks to the
					// current firing batch?
					if ( firing ) {
						firingLength = list.length;
					// With memory, if we're not firing then
					// we should call right away
					} else if ( memory ) {
						firingStart = start;
						fire( memory );
					}
				}
				return this;
			},
			// Remove a callback from the list
			remove: function() {
				if ( list ) {
					jQuery.each( arguments, function( _, arg ) {
						var index;
						while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
							list.splice( index, 1 );
							// Handle firing indexes
							if ( firing ) {
								if ( index <= firingLength ) {
									firingLength--;
								}
								if ( index <= firingIndex ) {
									firingIndex--;
								}
							}
						}
					});
				}
				return this;
			},
			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ? jQuery.inArray( fn, list ) > -1 : !!( list && list.length );
			},
			// Remove all callbacks from the list
			empty: function() {
				list = [];
				firingLength = 0;
				return this;
			},
			// Have the list do nothing anymore
			disable: function() {
				list = stack = memory = undefined;
				return this;
			},
			// Is it disabled?
			disabled: function() {
				return !list;
			},
			// Lock the list in its current state
			lock: function() {
				stack = undefined;
				if ( !memory ) {
					self.disable();
				}
				return this;
			},
			// Is it locked?
			locked: function() {
				return !stack;
			},
			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( list && ( !fired || stack ) ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					if ( firing ) {
						stack.push( args );
					} else {
						fire( args );
					}
				}
				return this;
			},
			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},
			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


jQuery.extend({

	Deferred: function( func ) {
		var tuples = [
				// action, add listener, listener list, final state
				[ "resolve", "done", jQuery.Callbacks("once memory"), "resolved" ],
				[ "reject", "fail", jQuery.Callbacks("once memory"), "rejected" ],
				[ "notify", "progress", jQuery.Callbacks("memory") ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				then: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;
					return jQuery.Deferred(function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {
							var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];
							// deferred[ done | fail | progress ] for forwarding actions to newDefer
							deferred[ tuple[1] ](function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
										.done( newDefer.resolve )
										.fail( newDefer.reject )
										.progress( newDefer.notify );
								} else {
									newDefer[ tuple[ 0 ] + "With" ]( this === promise ? newDefer.promise() : this, fn ? [ returned ] : arguments );
								}
							});
						});
						fns = null;
					}).promise();
				},
				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Keep pipe for back-compat
		promise.pipe = promise.then;

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 3 ];

			// promise[ done | fail | progress ] = list.add
			promise[ tuple[1] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(function() {
					// state = [ resolved | rejected ]
					state = stateString;

				// [ reject_list | resolve_list ].disable; progress_list.lock
				}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
			}

			// deferred[ resolve | reject | notify ]
			deferred[ tuple[0] ] = function() {
				deferred[ tuple[0] + "With" ]( this === deferred ? promise : this, arguments );
				return this;
			};
			deferred[ tuple[0] + "With" ] = list.fireWith;
		});

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( subordinate /* , ..., subordinateN */ ) {
		var i = 0,
			resolveValues = slice.call( arguments ),
			length = resolveValues.length,

			// the count of uncompleted subordinates
			remaining = length !== 1 || ( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

			// the master Deferred. If resolveValues consist of only a single Deferred, just use that.
			deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

			// Update function for both resolve and progress values
			updateFunc = function( i, contexts, values ) {
				return function( value ) {
					contexts[ i ] = this;
					values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( values === progressValues ) {
						deferred.notifyWith( contexts, values );

					} else if ( !(--remaining) ) {
						deferred.resolveWith( contexts, values );
					}
				};
			},

			progressValues, progressContexts, resolveContexts;

		// add listeners to Deferred subordinates; treat others as resolved
		if ( length > 1 ) {
			progressValues = new Array( length );
			progressContexts = new Array( length );
			resolveContexts = new Array( length );
			for ( ; i < length; i++ ) {
				if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
					resolveValues[ i ].promise()
						.done( updateFunc( i, resolveContexts, resolveValues ) )
						.fail( deferred.reject )
						.progress( updateFunc( i, progressContexts, progressValues ) );
				} else {
					--remaining;
				}
			}
		}

		// if we're not waiting on anything, resolve the master
		if ( !remaining ) {
			deferred.resolveWith( resolveContexts, resolveValues );
		}

		return deferred.promise();
	}
});


// The deferred used on DOM ready
var readyList;

jQuery.fn.ready = function( fn ) {
	// Add the callback
	jQuery.ready.promise().done( fn );

	return this;
};

jQuery.extend({
	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Hold (or release) the ready event
	holdReady: function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	},

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
		if ( !document.body ) {
			return setTimeout( jQuery.ready );
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );

		// Trigger any bound ready events
		if ( jQuery.fn.trigger ) {
			jQuery( document ).trigger("ready").off("ready");
		}
	}
});

/**
 * Clean-up method for dom ready events
 */
function detach() {
	if ( document.addEventListener ) {
		document.removeEventListener( "DOMContentLoaded", completed, false );
		window.removeEventListener( "load", completed, false );

	} else {
		document.detachEvent( "onreadystatechange", completed );
		window.detachEvent( "onload", completed );
	}
}

/**
 * The ready event handler and self cleanup method
 */
function completed() {
	// readyState === "complete" is good enough for us to call the dom ready in oldIE
	if ( document.addEventListener || event.type === "load" || document.readyState === "complete" ) {
		detach();
		jQuery.ready();
	}
}

jQuery.ready.promise = function( obj ) {
	if ( !readyList ) {

		readyList = jQuery.Deferred();

		// Catch cases where $(document).ready() is called after the browser event has already occurred.
		// we once tried to use readyState "interactive" here, but it caused issues like the one
		// discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
		if ( document.readyState === "complete" ) {
			// Handle it asynchronously to allow scripts the opportunity to delay ready
			setTimeout( jQuery.ready );

		// Standards-based browsers support DOMContentLoaded
		} else if ( document.addEventListener ) {
			// Use the handy event callback
			document.addEventListener( "DOMContentLoaded", completed, false );

			// A fallback to window.onload, that will always work
			window.addEventListener( "load", completed, false );

		// If IE event model is used
		} else {
			// Ensure firing before onload, maybe late but safe also for iframes
			document.attachEvent( "onreadystatechange", completed );

			// A fallback to window.onload, that will always work
			window.attachEvent( "onload", completed );

			// If IE and not a frame
			// continually check to see if the document is ready
			var top = false;

			try {
				top = window.frameElement == null && document.documentElement;
			} catch(e) {}

			if ( top && top.doScroll ) {
				(function doScrollCheck() {
					if ( !jQuery.isReady ) {

						try {
							// Use the trick by Diego Perini
							// http://javascript.nwbox.com/IEContentLoaded/
							top.doScroll("left");
						} catch(e) {
							return setTimeout( doScrollCheck, 50 );
						}

						// detach all dom ready events
						detach();

						// and execute any waiting functions
						jQuery.ready();
					}
				})();
			}
		}
	}
	return readyList.promise( obj );
};


var strundefined = typeof undefined;



// Support: IE<9
// Iteration over object's inherited properties before its own
var i;
for ( i in jQuery( support ) ) {
	break;
}
support.ownLast = i !== "0";

// Note: most support tests are defined in their respective modules.
// false until the test is run
support.inlineBlockNeedsLayout = false;

jQuery(function() {
	// We need to execute this one support test ASAP because we need to know
	// if body.style.zoom needs to be set.

	var container, div,
		body = document.getElementsByTagName("body")[0];

	if ( !body ) {
		// Return for frameset docs that don't have a body
		return;
	}

	// Setup
	container = document.createElement( "div" );
	container.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px";

	div = document.createElement( "div" );
	body.appendChild( container ).appendChild( div );

	if ( typeof div.style.zoom !== strundefined ) {
		// Support: IE<8
		// Check if natively block-level elements act like inline-block
		// elements when setting their display to 'inline' and giving
		// them layout
		div.style.cssText = "border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1";

		if ( (support.inlineBlockNeedsLayout = ( div.offsetWidth === 3 )) ) {
			// Prevent IE 6 from affecting layout for positioned elements #11048
			// Prevent IE from shrinking the body in IE 7 mode #12869
			// Support: IE<8
			body.style.zoom = 1;
		}
	}

	body.removeChild( container );

	// Null elements to avoid leaks in IE
	container = div = null;
});




(function() {
	var div = document.createElement( "div" );

	// Execute the test only if not already executed in another module.
	if (support.deleteExpando == null) {
		// Support: IE<9
		support.deleteExpando = true;
		try {
			delete div.test;
		} catch( e ) {
			support.deleteExpando = false;
		}
	}

	// Null elements to avoid leaks in IE.
	div = null;
})();


/**
 * Determines whether an object can have data
 */
jQuery.acceptData = function( elem ) {
	var noData = jQuery.noData[ (elem.nodeName + " ").toLowerCase() ],
		nodeType = +elem.nodeType || 1;

	// Do not set data on non-element DOM nodes because it will not be cleared (#8335).
	return nodeType !== 1 && nodeType !== 9 ?
		false :

		// Nodes accept data unless otherwise specified; rejection can be conditional
		!noData || noData !== true && elem.getAttribute("classid") === noData;
};


var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /([A-Z])/g;

function dataAttr( elem, key, data ) {
	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {

		var name = "data-" + key.replace( rmultiDash, "-$1" ).toLowerCase();

		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = data === "true" ? true :
					data === "false" ? false :
					data === "null" ? null :
					// Only convert to a number if it doesn't change the string
					+data + "" === data ? +data :
					rbrace.test( data ) ? jQuery.parseJSON( data ) :
					data;
			} catch( e ) {}

			// Make sure we set the data so it isn't changed later
			jQuery.data( elem, key, data );

		} else {
			data = undefined;
		}
	}

	return data;
}

// checks a cache object for emptiness
function isEmptyDataObject( obj ) {
	var name;
	for ( name in obj ) {

		// if the public data object is empty, the private is still empty
		if ( name === "data" && jQuery.isEmptyObject( obj[name] ) ) {
			continue;
		}
		if ( name !== "toJSON" ) {
			return false;
		}
	}

	return true;
}

function internalData( elem, name, data, pvt /* Internal Use Only */ ) {
	if ( !jQuery.acceptData( elem ) ) {
		return;
	}

	var ret, thisCache,
		internalKey = jQuery.expando,

		// We have to handle DOM nodes and JS objects differently because IE6-7
		// can't GC object references properly across the DOM-JS boundary
		isNode = elem.nodeType,

		// Only DOM nodes need the global jQuery cache; JS object data is
		// attached directly to the object so GC can occur automatically
		cache = isNode ? jQuery.cache : elem,

		// Only defining an ID for JS objects if its cache already exists allows
		// the code to shortcut on the same path as a DOM node with no cache
		id = isNode ? elem[ internalKey ] : elem[ internalKey ] && internalKey;

	// Avoid doing any more work than we need to when trying to get data on an
	// object that has no data at all
	if ( (!id || !cache[id] || (!pvt && !cache[id].data)) && data === undefined && typeof name === "string" ) {
		return;
	}

	if ( !id ) {
		// Only DOM nodes need a new unique ID for each element since their data
		// ends up in the global cache
		if ( isNode ) {
			id = elem[ internalKey ] = deletedIds.pop() || jQuery.guid++;
		} else {
			id = internalKey;
		}
	}

	if ( !cache[ id ] ) {
		// Avoid exposing jQuery metadata on plain JS objects when the object
		// is serialized using JSON.stringify
		cache[ id ] = isNode ? {} : { toJSON: jQuery.noop };
	}

	// An object can be passed to jQuery.data instead of a key/value pair; this gets
	// shallow copied over onto the existing cache
	if ( typeof name === "object" || typeof name === "function" ) {
		if ( pvt ) {
			cache[ id ] = jQuery.extend( cache[ id ], name );
		} else {
			cache[ id ].data = jQuery.extend( cache[ id ].data, name );
		}
	}

	thisCache = cache[ id ];

	// jQuery data() is stored in a separate object inside the object's internal data
	// cache in order to avoid key collisions between internal data and user-defined
	// data.
	if ( !pvt ) {
		if ( !thisCache.data ) {
			thisCache.data = {};
		}

		thisCache = thisCache.data;
	}

	if ( data !== undefined ) {
		thisCache[ jQuery.camelCase( name ) ] = data;
	}

	// Check for both converted-to-camel and non-converted data property names
	// If a data property was specified
	if ( typeof name === "string" ) {

		// First Try to find as-is property data
		ret = thisCache[ name ];

		// Test for null|undefined property data
		if ( ret == null ) {

			// Try to find the camelCased property
			ret = thisCache[ jQuery.camelCase( name ) ];
		}
	} else {
		ret = thisCache;
	}

	return ret;
}

function internalRemoveData( elem, name, pvt ) {
	if ( !jQuery.acceptData( elem ) ) {
		return;
	}

	var thisCache, i,
		isNode = elem.nodeType,

		// See jQuery.data for more information
		cache = isNode ? jQuery.cache : elem,
		id = isNode ? elem[ jQuery.expando ] : jQuery.expando;

	// If there is already no cache entry for this object, there is no
	// purpose in continuing
	if ( !cache[ id ] ) {
		return;
	}

	if ( name ) {

		thisCache = pvt ? cache[ id ] : cache[ id ].data;

		if ( thisCache ) {

			// Support array or space separated string names for data keys
			if ( !jQuery.isArray( name ) ) {

				// try the string as a key before any manipulation
				if ( name in thisCache ) {
					name = [ name ];
				} else {

					// split the camel cased version by spaces unless a key with the spaces exists
					name = jQuery.camelCase( name );
					if ( name in thisCache ) {
						name = [ name ];
					} else {
						name = name.split(" ");
					}
				}
			} else {
				// If "name" is an array of keys...
				// When data is initially created, via ("key", "val") signature,
				// keys will be converted to camelCase.
				// Since there is no way to tell _how_ a key was added, remove
				// both plain key and camelCase key. #12786
				// This will only penalize the array argument path.
				name = name.concat( jQuery.map( name, jQuery.camelCase ) );
			}

			i = name.length;
			while ( i-- ) {
				delete thisCache[ name[i] ];
			}

			// If there is no data left in the cache, we want to continue
			// and let the cache object itself get destroyed
			if ( pvt ? !isEmptyDataObject(thisCache) : !jQuery.isEmptyObject(thisCache) ) {
				return;
			}
		}
	}

	// See jQuery.data for more information
	if ( !pvt ) {
		delete cache[ id ].data;

		// Don't destroy the parent cache unless the internal data object
		// had been the only thing left in it
		if ( !isEmptyDataObject( cache[ id ] ) ) {
			return;
		}
	}

	// Destroy the cache
	if ( isNode ) {
		jQuery.cleanData( [ elem ], true );

	// Use delete when supported for expandos or `cache` is not a window per isWindow (#10080)
	/* jshint eqeqeq: false */
	} else if ( support.deleteExpando || cache != cache.window ) {
		/* jshint eqeqeq: true */
		delete cache[ id ];

	// When all else fails, null
	} else {
		cache[ id ] = null;
	}
}

jQuery.extend({
	cache: {},

	// The following elements (space-suffixed to avoid Object.prototype collisions)
	// throw uncatchable exceptions if you attempt to set expando properties
	noData: {
		"applet ": true,
		"embed ": true,
		// ...but Flash objects (which have this classid) *can* handle expandos
		"object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
	},

	hasData: function( elem ) {
		elem = elem.nodeType ? jQuery.cache[ elem[jQuery.expando] ] : elem[ jQuery.expando ];
		return !!elem && !isEmptyDataObject( elem );
	},

	data: function( elem, name, data ) {
		return internalData( elem, name, data );
	},

	removeData: function( elem, name ) {
		return internalRemoveData( elem, name );
	},

	// For internal use only.
	_data: function( elem, name, data ) {
		return internalData( elem, name, data, true );
	},

	_removeData: function( elem, name ) {
		return internalRemoveData( elem, name, true );
	}
});

jQuery.fn.extend({
	data: function( key, value ) {
		var i, name, data,
			elem = this[0],
			attrs = elem && elem.attributes;

		// Special expections of .data basically thwart jQuery.access,
		// so implement the relevant behavior ourselves

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = jQuery.data( elem );

				if ( elem.nodeType === 1 && !jQuery._data( elem, "parsedAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {
						name = attrs[i].name;

						if ( name.indexOf("data-") === 0 ) {
							name = jQuery.camelCase( name.slice(5) );

							dataAttr( elem, name, data[ name ] );
						}
					}
					jQuery._data( elem, "parsedAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each(function() {
				jQuery.data( this, key );
			});
		}

		return arguments.length > 1 ?

			// Sets one value
			this.each(function() {
				jQuery.data( this, key, value );
			}) :

			// Gets one value
			// Try to fetch any internally stored data first
			elem ? dataAttr( elem, key, jQuery.data( elem, key ) ) : undefined;
	},

	removeData: function( key ) {
		return this.each(function() {
			jQuery.removeData( this, key );
		});
	}
});


jQuery.extend({
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = jQuery._data( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || jQuery.isArray(data) ) {
					queue = jQuery._data( elem, type, jQuery.makeArray(data) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// not intended for public consumption - generates a queueHooks object, or returns the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return jQuery._data( elem, key ) || jQuery._data( elem, key, {
			empty: jQuery.Callbacks("once memory").add(function() {
				jQuery._removeData( elem, type + "queue" );
				jQuery._removeData( elem, key );
			})
		});
	}
});

jQuery.fn.extend({
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[0], type );
		}

		return data === undefined ?
			this :
			this.each(function() {
				var queue = jQuery.queue( this, type, data );

				// ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[0] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			});
	},
	dequeue: function( type ) {
		return this.each(function() {
			jQuery.dequeue( this, type );
		});
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},
	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = jQuery._data( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
});
var pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;

var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHidden = function( elem, el ) {
		// isHidden might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;
		return jQuery.css( elem, "display" ) === "none" || !jQuery.contains( elem.ownerDocument, elem );
	};



// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = jQuery.access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		length = elems.length,
		bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			jQuery.access( elems, fn, i, key[i], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !jQuery.isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {
			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < length; i++ ) {
				fn( elems[i], key, raw ? value : value.call( elems[i], i, fn( elems[i], key ) ) );
			}
		}
	}

	return chainable ?
		elems :

		// Gets
		bulk ?
			fn.call( elems ) :
			length ? fn( elems[0], key ) : emptyGet;
};
var rcheckableType = (/^(?:checkbox|radio)$/i);



(function() {
	var fragment = document.createDocumentFragment(),
		div = document.createElement("div"),
		input = document.createElement("input");

	// Setup
	div.setAttribute( "className", "t" );
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a>";

	// IE strips leading whitespace when .innerHTML is used
	support.leadingWhitespace = div.firstChild.nodeType === 3;

	// Make sure that tbody elements aren't automatically inserted
	// IE will insert them into empty tables
	support.tbody = !div.getElementsByTagName( "tbody" ).length;

	// Make sure that link elements get serialized correctly by innerHTML
	// This requires a wrapper element in IE
	support.htmlSerialize = !!div.getElementsByTagName( "link" ).length;

	// Makes sure cloning an html5 element does not cause problems
	// Where outerHTML is undefined, this still works
	support.html5Clone =
		document.createElement( "nav" ).cloneNode( true ).outerHTML !== "<:nav></:nav>";

	// Check if a disconnected checkbox will retain its checked
	// value of true after appended to the DOM (IE6/7)
	input.type = "checkbox";
	input.checked = true;
	fragment.appendChild( input );
	support.appendChecked = input.checked;

	// Make sure textarea (and checkbox) defaultValue is properly cloned
	// Support: IE6-IE11+
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;

	// #11217 - WebKit loses check when the name is after the checked attribute
	fragment.appendChild( div );
	div.innerHTML = "<input type='radio' checked='checked' name='t'/>";

	// Support: Safari 5.1, iOS 5.1, Android 4.x, Android 2.3
	// old WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE<9
	// Opera does not clone events (and typeof div.attachEvent === undefined).
	// IE9-10 clones events bound via attachEvent, but they don't trigger with .click()
	support.noCloneEvent = true;
	if ( div.attachEvent ) {
		div.attachEvent( "onclick", function() {
			support.noCloneEvent = false;
		});

		div.cloneNode( true ).click();
	}

	// Execute the test only if not already executed in another module.
	if (support.deleteExpando == null) {
		// Support: IE<9
		support.deleteExpando = true;
		try {
			delete div.test;
		} catch( e ) {
			support.deleteExpando = false;
		}
	}

	// Null elements to avoid leaks in IE.
	fragment = div = input = null;
})();


(function() {
	var i, eventName,
		div = document.createElement( "div" );

	// Support: IE<9 (lack submit/change bubble), Firefox 23+ (lack focusin event)
	for ( i in { submit: true, change: true, focusin: true }) {
		eventName = "on" + i;

		if ( !(support[ i + "Bubbles" ] = eventName in window) ) {
			// Beware of CSP restrictions (https://developer.mozilla.org/en/Security/CSP)
			div.setAttribute( eventName, "t" );
			support[ i + "Bubbles" ] = div.attributes[ eventName ].expando === false;
		}
	}

	// Null elements to avoid leaks in IE.
	div = null;
})();


var rformElems = /^(?:input|select|textarea)$/i,
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|contextmenu)|click/,
	rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {
		var tmp, events, t, handleObjIn,
			special, eventHandle, handleObj,
			handlers, type, namespaces, origType,
			elemData = jQuery._data( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !(events = elemData.events) ) {
			events = elemData.events = {};
		}
		if ( !(eventHandle = elemData.handle) ) {
			eventHandle = elemData.handle = function( e ) {
				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== strundefined && (!e || jQuery.event.triggered !== e.type) ?
					jQuery.event.dispatch.apply( eventHandle.elem, arguments ) :
					undefined;
			};
			// Add elem as a property of the handle fn to prevent a memory leak with IE non-native events
			eventHandle.elem = elem;
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend({
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join(".")
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !(handlers = events[ type ]) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener/attachEvent if the special events handler returns false
				if ( !special.setup || special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
					// Bind the global event handler to the element
					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle, false );

					} else if ( elem.attachEvent ) {
						elem.attachEvent( "on" + type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

		// Nullify elem to prevent memory leaks in IE
		elem = null;
	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {
		var j, handleObj, tmp,
			origCount, t, events,
			special, handlers, type,
			namespaces, origType,
			elemData = jQuery.hasData( elem ) && jQuery._data( elem );

		if ( !elemData || !(events = elemData.events) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[2] && new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector || selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown || special.teardown.call( elem, namespaces, elemData.handle ) === false ) {
					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			delete elemData.handle;

			// removeData also checks for emptiness and clears the expando if empty
			// so use it instead of delete
			jQuery._removeData( elem, "events" );
		}
	},

	trigger: function( event, data, elem, onlyHandlers ) {
		var handle, ontype, cur,
			bubbleType, special, tmp, i,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split(".") : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf(".") >= 0 ) {
			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split(".");
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf(":") < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join(".");
		event.namespace_re = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === (elem.ownerDocument || document) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( (cur = eventPath[i++]) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( jQuery._data( cur, "events" ) || {} )[ event.type ] && jQuery._data( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && jQuery.acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( (!special._default || special._default.apply( eventPath.pop(), data ) === false) &&
				jQuery.acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name name as the event.
				// Can't use an .isFunction() check here because IE6/7 fails that test.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && elem[ type ] && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					try {
						elem[ type ]();
					} catch ( e ) {
						// IE<9 dies on focus/blur to hidden element (#1486,#12518)
						// only reproducible on winXP IE8 native, not IE9 in IE8 mode
					}
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	dispatch: function( event ) {

		// Make a writable jQuery.Event from the native event object
		event = jQuery.event.fix( event );

		var i, ret, handleObj, matched, j,
			handlerQueue = [],
			args = slice.call( arguments ),
			handlers = ( jQuery._data( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[0] = event;
		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( (matched = handlerQueue[ i++ ]) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( (handleObj = matched.handlers[ j++ ]) && !event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or
				// 2) have namespace(s) a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.namespace_re || event.namespace_re.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle || handleObj.handler )
							.apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( (event.result = ret) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var sel, handleObj, matches, i,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		// Black-hole SVG <use> instance trees (#13180)
		// Avoid non-left-click bubbling in Firefox (#3861)
		if ( delegateCount && cur.nodeType && (!event.button || event.type !== "click") ) {

			/* jshint eqeqeq: false */
			for ( ; cur != this; cur = cur.parentNode || this ) {
				/* jshint eqeqeq: true */

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && (cur.disabled !== true || event.type !== "click") ) {
					matches = [];
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matches[ sel ] === undefined ) {
							matches[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) >= 0 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matches[ sel ] ) {
							matches.push( handleObj );
						}
					}
					if ( matches.length ) {
						handlerQueue.push({ elem: cur, handlers: matches });
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		if ( delegateCount < handlers.length ) {
			handlerQueue.push({ elem: this, handlers: handlers.slice( delegateCount ) });
		}

		return handlerQueue;
	},

	fix: function( event ) {
		if ( event[ jQuery.expando ] ) {
			return event;
		}

		// Create a writable copy of the event object and normalize some properties
		var i, prop, copy,
			type = event.type,
			originalEvent = event,
			fixHook = this.fixHooks[ type ];

		if ( !fixHook ) {
			this.fixHooks[ type ] = fixHook =
				rmouseEvent.test( type ) ? this.mouseHooks :
				rkeyEvent.test( type ) ? this.keyHooks :
				{};
		}
		copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

		event = new jQuery.Event( originalEvent );

		i = copy.length;
		while ( i-- ) {
			prop = copy[ i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Support: IE<9
		// Fix target property (#1925)
		if ( !event.target ) {
			event.target = originalEvent.srcElement || document;
		}

		// Support: Chrome 23+, Safari?
		// Target should not be a text node (#504, #13143)
		if ( event.target.nodeType === 3 ) {
			event.target = event.target.parentNode;
		}

		// Support: IE<9
		// For mouse/key events, metaKey==false if it's undefined (#3368, #11328)
		event.metaKey = !!event.metaKey;

		return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
	},

	// Includes some event props shared by KeyEvent and MouseEvent
	props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),

	fixHooks: {},

	keyHooks: {
		props: "char charCode key keyCode".split(" "),
		filter: function( event, original ) {

			// Add which for key events
			if ( event.which == null ) {
				event.which = original.charCode != null ? original.charCode : original.keyCode;
			}

			return event;
		}
	},

	mouseHooks: {
		props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
		filter: function( event, original ) {
			var body, eventDoc, doc,
				button = original.button,
				fromElement = original.fromElement;

			// Calculate pageX/Y if missing and clientX/Y available
			if ( event.pageX == null && original.clientX != null ) {
				eventDoc = event.target.ownerDocument || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;

				event.pageX = original.clientX + ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) - ( doc && doc.clientLeft || body && body.clientLeft || 0 );
				event.pageY = original.clientY + ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) - ( doc && doc.clientTop  || body && body.clientTop  || 0 );
			}

			// Add relatedTarget, if necessary
			if ( !event.relatedTarget && fromElement ) {
				event.relatedTarget = fromElement === event.target ? original.toElement : fromElement;
			}

			// Add which for click: 1 === left; 2 === middle; 3 === right
			// Note: button is not normalized, so don't use it
			if ( !event.which && button !== undefined ) {
				event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
			}

			return event;
		}
	},

	special: {
		load: {
			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {
			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					try {
						this.focus();
						return false;
					} catch ( e ) {
						// Support: IE<9
						// If we error on focus to hidden element (#1486, #12518),
						// let .trigger() run the handlers
					}
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {
			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( jQuery.nodeName( this, "input" ) && this.type === "checkbox" && this.click ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return jQuery.nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Even when returnValue equals to undefined Firefox will still show alert
				if ( event.result !== undefined ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	},

	simulate: function( type, elem, event, bubble ) {
		// Piggyback on a donor event to simulate a different one.
		// Fake originalEvent to avoid donor's stopPropagation, but if the
		// simulated event prevents default then we do the same on the donor.
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true,
				originalEvent: {}
			}
		);
		if ( bubble ) {
			jQuery.event.trigger( e, null, elem );
		} else {
			jQuery.event.dispatch.call( elem, e );
		}
		if ( e.isDefaultPrevented() ) {
			event.preventDefault();
		}
	}
};

jQuery.removeEvent = document.removeEventListener ?
	function( elem, type, handle ) {
		if ( elem.removeEventListener ) {
			elem.removeEventListener( type, handle, false );
		}
	} :
	function( elem, type, handle ) {
		var name = "on" + type;

		if ( elem.detachEvent ) {

			// #8545, #7054, preventing memory leaks for custom events in IE6-8
			// detachEvent needed property on element, by name of that event, to properly expose it to GC
			if ( typeof elem[ name ] === strundefined ) {
				elem[ name ] = null;
			}

			elem.detachEvent( name, handle );
		}
	};

jQuery.Event = function( src, props ) {
	// Allow instantiation without the 'new' keyword
	if ( !(this instanceof jQuery.Event) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined && (
				// Support: IE < 9
				src.returnValue === false ||
				// Support: Android < 4.0
				src.getPreventDefault && src.getPreventDefault() ) ?
			returnTrue :
			returnFalse;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;
		if ( !e ) {
			return;
		}

		// If preventDefault exists, run it on the original event
		if ( e.preventDefault ) {
			e.preventDefault();

		// Support: IE
		// Otherwise set the returnValue property of the original event to false
		} else {
			e.returnValue = false;
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;
		if ( !e ) {
			return;
		}
		// If stopPropagation exists, run it on the original event
		if ( e.stopPropagation ) {
			e.stopPropagation();
		}

		// Support: IE
		// Set the cancelBubble property of the original event to true
		e.cancelBubble = true;
	},
	stopImmediatePropagation: function() {
		this.isImmediatePropagationStopped = returnTrue;
		this.stopPropagation();
	}
};

// Create mouseenter/leave events using mouseover/out and event-time checks
jQuery.each({
	mouseenter: "mouseover",
	mouseleave: "mouseout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mousenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || (related !== target && !jQuery.contains( target, related )) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
});

// IE submit delegation
if ( !support.submitBubbles ) {

	jQuery.event.special.submit = {
		setup: function() {
			// Only need this for delegated form submit events
			if ( jQuery.nodeName( this, "form" ) ) {
				return false;
			}

			// Lazy-add a submit handler when a descendant form may potentially be submitted
			jQuery.event.add( this, "click._submit keypress._submit", function( e ) {
				// Node name check avoids a VML-related crash in IE (#9807)
				var elem = e.target,
					form = jQuery.nodeName( elem, "input" ) || jQuery.nodeName( elem, "button" ) ? elem.form : undefined;
				if ( form && !jQuery._data( form, "submitBubbles" ) ) {
					jQuery.event.add( form, "submit._submit", function( event ) {
						event._submit_bubble = true;
					});
					jQuery._data( form, "submitBubbles", true );
				}
			});
			// return undefined since we don't need an event listener
		},

		postDispatch: function( event ) {
			// If form was submitted by the user, bubble the event up the tree
			if ( event._submit_bubble ) {
				delete event._submit_bubble;
				if ( this.parentNode && !event.isTrigger ) {
					jQuery.event.simulate( "submit", this.parentNode, event, true );
				}
			}
		},

		teardown: function() {
			// Only need this for delegated form submit events
			if ( jQuery.nodeName( this, "form" ) ) {
				return false;
			}

			// Remove delegated handlers; cleanData eventually reaps submit handlers attached above
			jQuery.event.remove( this, "._submit" );
		}
	};
}

// IE change delegation and checkbox/radio fix
if ( !support.changeBubbles ) {

	jQuery.event.special.change = {

		setup: function() {

			if ( rformElems.test( this.nodeName ) ) {
				// IE doesn't fire change on a check/radio until blur; trigger it on click
				// after a propertychange. Eat the blur-change in special.change.handle.
				// This still fires onchange a second time for check/radio after blur.
				if ( this.type === "checkbox" || this.type === "radio" ) {
					jQuery.event.add( this, "propertychange._change", function( event ) {
						if ( event.originalEvent.propertyName === "checked" ) {
							this._just_changed = true;
						}
					});
					jQuery.event.add( this, "click._change", function( event ) {
						if ( this._just_changed && !event.isTrigger ) {
							this._just_changed = false;
						}
						// Allow triggered, simulated change events (#11500)
						jQuery.event.simulate( "change", this, event, true );
					});
				}
				return false;
			}
			// Delegated event; lazy-add a change handler on descendant inputs
			jQuery.event.add( this, "beforeactivate._change", function( e ) {
				var elem = e.target;

				if ( rformElems.test( elem.nodeName ) && !jQuery._data( elem, "changeBubbles" ) ) {
					jQuery.event.add( elem, "change._change", function( event ) {
						if ( this.parentNode && !event.isSimulated && !event.isTrigger ) {
							jQuery.event.simulate( "change", this.parentNode, event, true );
						}
					});
					jQuery._data( elem, "changeBubbles", true );
				}
			});
		},

		handle: function( event ) {
			var elem = event.target;

			// Swallow native change events from checkbox/radio, we already triggered them above
			if ( this !== elem || event.isSimulated || event.isTrigger || (elem.type !== "radio" && elem.type !== "checkbox") ) {
				return event.handleObj.handler.apply( this, arguments );
			}
		},

		teardown: function() {
			jQuery.event.remove( this, "._change" );

			return !rformElems.test( this.nodeName );
		}
	};
}

// Create "bubbling" focus and blur events
if ( !support.focusinBubbles ) {
	jQuery.each({ focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
				jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ), true );
			};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = jQuery._data( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				jQuery._data( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = jQuery._data( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					jQuery._removeData( doc, fix );
				} else {
					jQuery._data( doc, fix, attaches );
				}
			}
		};
	});
}

jQuery.fn.extend({

	on: function( types, selector, data, fn, /*INTERNAL*/ one ) {
		var type, origFn;

		// Types can be a map of types/handlers
		if ( typeof types === "object" ) {
			// ( types-Object, selector, data )
			if ( typeof selector !== "string" ) {
				// ( types-Object, data )
				data = data || selector;
				selector = undefined;
			}
			for ( type in types ) {
				this.on( type, selector, data, types[ type ], one );
			}
			return this;
		}

		if ( data == null && fn == null ) {
			// ( types, fn )
			fn = selector;
			data = selector = undefined;
		} else if ( fn == null ) {
			if ( typeof selector === "string" ) {
				// ( types, selector, fn )
				fn = data;
				data = undefined;
			} else {
				// ( types, data, fn )
				fn = data;
				data = selector;
				selector = undefined;
			}
		}
		if ( fn === false ) {
			fn = returnFalse;
		} else if ( !fn ) {
			return this;
		}

		if ( one === 1 ) {
			origFn = fn;
			fn = function( event ) {
				// Can use an empty set, since event contains the info
				jQuery().off( event );
				return origFn.apply( this, arguments );
			};
			// Use same guid so caller can remove using origFn
			fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
		}
		return this.each( function() {
			jQuery.event.add( this, types, fn, data, selector );
		});
	},
	one: function( types, selector, data, fn ) {
		return this.on( types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {
			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {
			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {
			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each(function() {
			jQuery.event.remove( this, types, fn, selector );
		});
	},

	trigger: function( type, data ) {
		return this.each(function() {
			jQuery.event.trigger( type, data, this );
		});
	},
	triggerHandler: function( type, data ) {
		var elem = this[0];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
});


function createSafeFragment( document ) {
	var list = nodeNames.split( "|" ),
		safeFrag = document.createDocumentFragment();

	if ( safeFrag.createElement ) {
		while ( list.length ) {
			safeFrag.createElement(
				list.pop()
			);
		}
	}
	return safeFrag;
}

var nodeNames = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|" +
		"header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
	rinlinejQuery = / jQuery\d+="(?:null|\d+)"/g,
	rnoshimcache = new RegExp("<(?:" + nodeNames + ")[\\s/>]", "i"),
	rleadingWhitespace = /^\s+/,
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
	rtagName = /<([\w:]+)/,
	rtbody = /<tbody/i,
	rhtml = /<|&#?\w+;/,
	rnoInnerhtml = /<(?:script|style|link)/i,
	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptType = /^$|\/(?:java|ecma)script/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,

	// We have to close these tags to support XHTML (#13200)
	wrapMap = {
		option: [ 1, "<select multiple='multiple'>", "</select>" ],
		legend: [ 1, "<fieldset>", "</fieldset>" ],
		area: [ 1, "<map>", "</map>" ],
		param: [ 1, "<object>", "</object>" ],
		thead: [ 1, "<table>", "</table>" ],
		tr: [ 2, "<table><tbody>", "</tbody></table>" ],
		col: [ 2, "<table><tbody></tbody><colgroup>", "</colgroup></table>" ],
		td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

		// IE6-8 can't serialize link, script, style, or any html5 (NoScope) tags,
		// unless wrapped in a div with non-breaking characters in front of it.
		_default: support.htmlSerialize ? [ 0, "", "" ] : [ 1, "X<div>", "</div>"  ]
	},
	safeFragment = createSafeFragment( document ),
	fragmentDiv = safeFragment.appendChild( document.createElement("div") );

wrapMap.optgroup = wrapMap.option;
wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

function getAll( context, tag ) {
	var elems, elem,
		i = 0,
		found = typeof context.getElementsByTagName !== strundefined ? context.getElementsByTagName( tag || "*" ) :
			typeof context.querySelectorAll !== strundefined ? context.querySelectorAll( tag || "*" ) :
			undefined;

	if ( !found ) {
		for ( found = [], elems = context.childNodes || context; (elem = elems[i]) != null; i++ ) {
			if ( !tag || jQuery.nodeName( elem, tag ) ) {
				found.push( elem );
			} else {
				jQuery.merge( found, getAll( elem, tag ) );
			}
		}
	}

	return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
		jQuery.merge( [ context ], found ) :
		found;
}

// Used in buildFragment, fixes the defaultChecked property
function fixDefaultChecked( elem ) {
	if ( rcheckableType.test( elem.type ) ) {
		elem.defaultChecked = elem.checked;
	}
}

// Support: IE<8
// Manipulating tables requires a tbody
function manipulationTarget( elem, content ) {
	return jQuery.nodeName( elem, "table" ) &&
		jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ?

		elem.getElementsByTagName("tbody")[0] ||
			elem.appendChild( elem.ownerDocument.createElement("tbody") ) :
		elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = (jQuery.find.attr( elem, "type" ) !== null) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );
	if ( match ) {
		elem.type = match[1];
	} else {
		elem.removeAttribute("type");
	}
	return elem;
}

// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var elem,
		i = 0;
	for ( ; (elem = elems[i]) != null; i++ ) {
		jQuery._data( elem, "globalEval", !refElements || jQuery._data( refElements[i], "globalEval" ) );
	}
}

function cloneCopyEvent( src, dest ) {

	if ( dest.nodeType !== 1 || !jQuery.hasData( src ) ) {
		return;
	}

	var type, i, l,
		oldData = jQuery._data( src ),
		curData = jQuery._data( dest, oldData ),
		events = oldData.events;

	if ( events ) {
		delete curData.handle;
		curData.events = {};

		for ( type in events ) {
			for ( i = 0, l = events[ type ].length; i < l; i++ ) {
				jQuery.event.add( dest, type, events[ type ][ i ] );
			}
		}
	}

	// make the cloned public data object a copy from the original
	if ( curData.data ) {
		curData.data = jQuery.extend( {}, curData.data );
	}
}

function fixCloneNodeIssues( src, dest ) {
	var nodeName, e, data;

	// We do not need to do anything for non-Elements
	if ( dest.nodeType !== 1 ) {
		return;
	}

	nodeName = dest.nodeName.toLowerCase();

	// IE6-8 copies events bound via attachEvent when using cloneNode.
	if ( !support.noCloneEvent && dest[ jQuery.expando ] ) {
		data = jQuery._data( dest );

		for ( e in data.events ) {
			jQuery.removeEvent( dest, e, data.handle );
		}

		// Event data gets referenced instead of copied if the expando gets copied too
		dest.removeAttribute( jQuery.expando );
	}

	// IE blanks contents when cloning scripts, and tries to evaluate newly-set text
	if ( nodeName === "script" && dest.text !== src.text ) {
		disableScript( dest ).text = src.text;
		restoreScript( dest );

	// IE6-10 improperly clones children of object elements using classid.
	// IE10 throws NoModificationAllowedError if parent is null, #12132.
	} else if ( nodeName === "object" ) {
		if ( dest.parentNode ) {
			dest.outerHTML = src.outerHTML;
		}

		// This path appears unavoidable for IE9. When cloning an object
		// element in IE9, the outerHTML strategy above is not sufficient.
		// If the src has innerHTML and the destination does not,
		// copy the src.innerHTML into the dest.innerHTML. #10324
		if ( support.html5Clone && ( src.innerHTML && !jQuery.trim(dest.innerHTML) ) ) {
			dest.innerHTML = src.innerHTML;
		}

	} else if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		// IE6-8 fails to persist the checked state of a cloned checkbox
		// or radio button. Worse, IE6-7 fail to give the cloned element
		// a checked appearance if the defaultChecked value isn't also set

		dest.defaultChecked = dest.checked = src.checked;

		// IE6-7 get confused and end up setting the value of a cloned
		// checkbox/radio button to an empty string instead of "on"
		if ( dest.value !== src.value ) {
			dest.value = src.value;
		}

	// IE6-8 fails to return the selected option to the default selected
	// state when cloning options
	} else if ( nodeName === "option" ) {
		dest.defaultSelected = dest.selected = src.defaultSelected;

	// IE6-8 fails to set the defaultValue to the correct value when
	// cloning other types of input fields
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

jQuery.extend({
	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var destElements, node, clone, i, srcElements,
			inPage = jQuery.contains( elem.ownerDocument, elem );

		if ( support.html5Clone || jQuery.isXMLDoc(elem) || !rnoshimcache.test( "<" + elem.nodeName + ">" ) ) {
			clone = elem.cloneNode( true );

		// IE<=8 does not properly clone detached, unknown element nodes
		} else {
			fragmentDiv.innerHTML = elem.outerHTML;
			fragmentDiv.removeChild( clone = fragmentDiv.firstChild );
		}

		if ( (!support.noCloneEvent || !support.noCloneChecked) &&
				(elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem) ) {

			// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			// Fix all IE cloning issues
			for ( i = 0; (node = srcElements[i]) != null; ++i ) {
				// Ensure that the destination node is not null; Fixes #9587
				if ( destElements[i] ) {
					fixCloneNodeIssues( node, destElements[i] );
				}
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0; (node = srcElements[i]) != null; i++ ) {
					cloneCopyEvent( node, destElements[i] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		destElements = srcElements = node = null;

		// Return the cloned set
		return clone;
	},

	buildFragment: function( elems, context, scripts, selection ) {
		var j, elem, contains,
			tmp, tag, tbody, wrap,
			l = elems.length,

			// Ensure a safe fragment
			safe = createSafeFragment( context ),

			nodes = [],
			i = 0;

		for ( ; i < l; i++ ) {
			elem = elems[ i ];

			if ( elem || elem === 0 ) {

				// Add nodes directly
				if ( jQuery.type( elem ) === "object" ) {
					jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

				// Convert non-html into a text node
				} else if ( !rhtml.test( elem ) ) {
					nodes.push( context.createTextNode( elem ) );

				// Convert html into DOM nodes
				} else {
					tmp = tmp || safe.appendChild( context.createElement("div") );

					// Deserialize a standard representation
					tag = (rtagName.exec( elem ) || [ "", "" ])[ 1 ].toLowerCase();
					wrap = wrapMap[ tag ] || wrapMap._default;

					tmp.innerHTML = wrap[1] + elem.replace( rxhtmlTag, "<$1></$2>" ) + wrap[2];

					// Descend through wrappers to the right content
					j = wrap[0];
					while ( j-- ) {
						tmp = tmp.lastChild;
					}

					// Manually add leading whitespace removed by IE
					if ( !support.leadingWhitespace && rleadingWhitespace.test( elem ) ) {
						nodes.push( context.createTextNode( rleadingWhitespace.exec( elem )[0] ) );
					}

					// Remove IE's autoinserted <tbody> from table fragments
					if ( !support.tbody ) {

						// String was a <table>, *may* have spurious <tbody>
						elem = tag === "table" && !rtbody.test( elem ) ?
							tmp.firstChild :

							// String was a bare <thead> or <tfoot>
							wrap[1] === "<table>" && !rtbody.test( elem ) ?
								tmp :
								0;

						j = elem && elem.childNodes.length;
						while ( j-- ) {
							if ( jQuery.nodeName( (tbody = elem.childNodes[j]), "tbody" ) && !tbody.childNodes.length ) {
								elem.removeChild( tbody );
							}
						}
					}

					jQuery.merge( nodes, tmp.childNodes );

					// Fix #12392 for WebKit and IE > 9
					tmp.textContent = "";

					// Fix #12392 for oldIE
					while ( tmp.firstChild ) {
						tmp.removeChild( tmp.firstChild );
					}

					// Remember the top-level container for proper cleanup
					tmp = safe.lastChild;
				}
			}
		}

		// Fix #11356: Clear elements from fragment
		if ( tmp ) {
			safe.removeChild( tmp );
		}

		// Reset defaultChecked for any radios and checkboxes
		// about to be appended to the DOM in IE 6/7 (#8060)
		if ( !support.appendChecked ) {
			jQuery.grep( getAll( nodes, "input" ), fixDefaultChecked );
		}

		i = 0;
		while ( (elem = nodes[ i++ ]) ) {

			// #4087 - If origin and destination elements are the same, and this is
			// that element, do not do anything
			if ( selection && jQuery.inArray( elem, selection ) !== -1 ) {
				continue;
			}

			contains = jQuery.contains( elem.ownerDocument, elem );

			// Append to fragment
			tmp = getAll( safe.appendChild( elem ), "script" );

			// Preserve script evaluation history
			if ( contains ) {
				setGlobalEval( tmp );
			}

			// Capture executables
			if ( scripts ) {
				j = 0;
				while ( (elem = tmp[ j++ ]) ) {
					if ( rscriptType.test( elem.type || "" ) ) {
						scripts.push( elem );
					}
				}
			}
		}

		tmp = null;

		return safe;
	},

	cleanData: function( elems, /* internal */ acceptData ) {
		var elem, type, id, data,
			i = 0,
			internalKey = jQuery.expando,
			cache = jQuery.cache,
			deleteExpando = support.deleteExpando,
			special = jQuery.event.special;

		for ( ; (elem = elems[i]) != null; i++ ) {
			if ( acceptData || jQuery.acceptData( elem ) ) {

				id = elem[ internalKey ];
				data = id && cache[ id ];

				if ( data ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Remove cache only if it was not already removed by jQuery.event.remove
					if ( cache[ id ] ) {

						delete cache[ id ];

						// IE does not allow us to delete expando properties from nodes,
						// nor does it have a removeAttribute function on Document nodes;
						// we must handle all of these cases
						if ( deleteExpando ) {
							delete elem[ internalKey ];

						} else if ( typeof elem.removeAttribute !== strundefined ) {
							elem.removeAttribute( internalKey );

						} else {
							elem[ internalKey ] = null;
						}

						deletedIds.push( id );
					}
				}
			}
		}
	}
});

jQuery.fn.extend({
	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().append( ( this[0] && this[0].ownerDocument || document ).createTextNode( value ) );
		}, null, value, arguments.length );
	},

	append: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		});
	},

	prepend: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		});
	},

	before: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		});
	},

	after: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		});
	},

	remove: function( selector, keepData /* Internal Use Only */ ) {
		var elem,
			elems = selector ? jQuery.filter( selector, this ) : this,
			i = 0;

		for ( ; (elem = elems[i]) != null; i++ ) {

			if ( !keepData && elem.nodeType === 1 ) {
				jQuery.cleanData( getAll( elem ) );
			}

			if ( elem.parentNode ) {
				if ( keepData && jQuery.contains( elem.ownerDocument, elem ) ) {
					setGlobalEval( getAll( elem, "script" ) );
				}
				elem.parentNode.removeChild( elem );
			}
		}

		return this;
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; (elem = this[i]) != null; i++ ) {
			// Remove element nodes and prevent memory leaks
			if ( elem.nodeType === 1 ) {
				jQuery.cleanData( getAll( elem, false ) );
			}

			// Remove any remaining nodes
			while ( elem.firstChild ) {
				elem.removeChild( elem.firstChild );
			}

			// If this is a select, ensure that it displays empty (#12336)
			// Support: IE<9
			if ( elem.options && jQuery.nodeName( elem, "select" ) ) {
				elem.options.length = 0;
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map(function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		});
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined ) {
				return elem.nodeType === 1 ?
					elem.innerHTML.replace( rinlinejQuery, "" ) :
					undefined;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				( support.htmlSerialize || !rnoshimcache.test( value )  ) &&
				( support.leadingWhitespace || !rleadingWhitespace.test( value ) ) &&
				!wrapMap[ (rtagName.exec( value ) || [ "", "" ])[ 1 ].toLowerCase() ] ) {

				value = value.replace( rxhtmlTag, "<$1></$2>" );

				try {
					for (; i < l; i++ ) {
						// Remove element nodes and prevent memory leaks
						elem = this[i] || {};
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch(e) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var arg = arguments[ 0 ];

		// Make the changes, replacing each context element with the new content
		this.domManip( arguments, function( elem ) {
			arg = this.parentNode;

			jQuery.cleanData( getAll( this ) );

			if ( arg ) {
				arg.replaceChild( elem, this );
			}
		});

		// Force removal if there was no new content (e.g., from empty arguments)
		return arg && (arg.length || arg.nodeType) ? this : this.remove();
	},

	detach: function( selector ) {
		return this.remove( selector, true );
	},

	domManip: function( args, callback ) {

		// Flatten any nested arrays
		args = concat.apply( [], args );

		var first, node, hasScripts,
			scripts, doc, fragment,
			i = 0,
			l = this.length,
			set = this,
			iNoClone = l - 1,
			value = args[0],
			isFunction = jQuery.isFunction( value );

		// We can't cloneNode fragments that contain checked, in WebKit
		if ( isFunction ||
				( l > 1 && typeof value === "string" &&
					!support.checkClone && rchecked.test( value ) ) ) {
			return this.each(function( index ) {
				var self = set.eq( index );
				if ( isFunction ) {
					args[0] = value.call( this, index, self.html() );
				}
				self.domManip( args, callback );
			});
		}

		if ( l ) {
			fragment = jQuery.buildFragment( args, this[ 0 ].ownerDocument, false, this );
			first = fragment.firstChild;

			if ( fragment.childNodes.length === 1 ) {
				fragment = first;
			}

			if ( first ) {
				scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
				hasScripts = scripts.length;

				// Use the original fragment for the last item instead of the first because it can end up
				// being emptied incorrectly in certain situations (#8070).
				for ( ; i < l; i++ ) {
					node = fragment;

					if ( i !== iNoClone ) {
						node = jQuery.clone( node, true, true );

						// Keep references to cloned scripts for later restoration
						if ( hasScripts ) {
							jQuery.merge( scripts, getAll( node, "script" ) );
						}
					}

					callback.call( this[i], node, i );
				}

				if ( hasScripts ) {
					doc = scripts[ scripts.length - 1 ].ownerDocument;

					// Reenable scripts
					jQuery.map( scripts, restoreScript );

					// Evaluate executable scripts on first document insertion
					for ( i = 0; i < hasScripts; i++ ) {
						node = scripts[ i ];
						if ( rscriptType.test( node.type || "" ) &&
							!jQuery._data( node, "globalEval" ) && jQuery.contains( doc, node ) ) {

							if ( node.src ) {
								// Optional AJAX dependency, but won't run scripts if not present
								if ( jQuery._evalUrl ) {
									jQuery._evalUrl( node.src );
								}
							} else {
								jQuery.globalEval( ( node.text || node.textContent || node.innerHTML || "" ).replace( rcleanScript, "" ) );
							}
						}
					}
				}

				// Fix #11809: Avoid leaking memory
				fragment = first = null;
			}
		}

		return this;
	}
});

jQuery.each({
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			i = 0,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone(true);
			jQuery( insert[i] )[ original ]( elems );

			// Modern browsers can apply jQuery collections as arrays, but oldIE needs a .get()
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
});


var iframe,
	elemdisplay = {};

/**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */
// Called only from within defaultDisplay
function actualDisplay( name, doc ) {
	var elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),

		// getDefaultComputedStyle might be reliably used only on attached element
		display = window.getDefaultComputedStyle ?

			// Use of this method is a temporary fix (more like optmization) until something better comes along,
			// since it was removed from specification and supported only in FF
			window.getDefaultComputedStyle( elem[ 0 ] ).display : jQuery.css( elem[ 0 ], "display" );

	// We don't have any data stored on the element,
	// so use "detach" method as fast way to get rid of the element
	elem.detach();

	return display;
}

/**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
function defaultDisplay( nodeName ) {
	var doc = document,
		display = elemdisplay[ nodeName ];

	if ( !display ) {
		display = actualDisplay( nodeName, doc );

		// If the simple way fails, read from inside an iframe
		if ( display === "none" || !display ) {

			// Use the already-created iframe if possible
			iframe = (iframe || jQuery( "<iframe frameborder='0' width='0' height='0'/>" )).appendTo( doc.documentElement );

			// Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
			doc = ( iframe[ 0 ].contentWindow || iframe[ 0 ].contentDocument ).document;

			// Support: IE
			doc.write();
			doc.close();

			display = actualDisplay( nodeName, doc );
			iframe.detach();
		}

		// Store the correct default display
		elemdisplay[ nodeName ] = display;
	}

	return display;
}


(function() {
	var a, shrinkWrapBlocksVal,
		div = document.createElement( "div" ),
		divReset =
			"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;" +
			"display:block;padding:0;margin:0;border:0";

	// Setup
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
	a = div.getElementsByTagName( "a" )[ 0 ];

	a.style.cssText = "float:left;opacity:.5";

	// Make sure that element opacity exists
	// (IE uses filter instead)
	// Use a regex to work around a WebKit issue. See #5145
	support.opacity = /^0.5/.test( a.style.opacity );

	// Verify style float existence
	// (IE uses styleFloat instead of cssFloat)
	support.cssFloat = !!a.style.cssFloat;

	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	// Null elements to avoid leaks in IE.
	a = div = null;

	support.shrinkWrapBlocks = function() {
		var body, container, div, containerStyles;

		if ( shrinkWrapBlocksVal == null ) {
			body = document.getElementsByTagName( "body" )[ 0 ];
			if ( !body ) {
				// Test fired too early or in an unsupported environment, exit.
				return;
			}

			containerStyles = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px";
			container = document.createElement( "div" );
			div = document.createElement( "div" );

			body.appendChild( container ).appendChild( div );

			// Will be changed later if needed.
			shrinkWrapBlocksVal = false;

			if ( typeof div.style.zoom !== strundefined ) {
				// Support: IE6
				// Check if elements with layout shrink-wrap their children
				div.style.cssText = divReset + ";width:1px;padding:1px;zoom:1";
				div.innerHTML = "<div></div>";
				div.firstChild.style.width = "5px";
				shrinkWrapBlocksVal = div.offsetWidth !== 3;
			}

			body.removeChild( container );

			// Null elements to avoid leaks in IE.
			body = container = div = null;
		}

		return shrinkWrapBlocksVal;
	};

})();
var rmargin = (/^margin/);

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );



var getStyles, curCSS,
	rposition = /^(top|right|bottom|left)$/;

if ( window.getComputedStyle ) {
	getStyles = function( elem ) {
		return elem.ownerDocument.defaultView.getComputedStyle( elem, null );
	};

	curCSS = function( elem, name, computed ) {
		var width, minWidth, maxWidth, ret,
			style = elem.style;

		computed = computed || getStyles( elem );

		// getPropertyValue is only needed for .css('filter') in IE9, see #12537
		ret = computed ? computed.getPropertyValue( name ) || computed[ name ] : undefined;

		if ( computed ) {

			if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
				ret = jQuery.style( elem, name );
			}

			// A tribute to the "awesome hack by Dean Edwards"
			// Chrome < 17 and Safari 5.0 uses "computed value" instead of "used value" for margin-right
			// Safari 5.1.7 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels
			// this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
			if ( rnumnonpx.test( ret ) && rmargin.test( name ) ) {

				// Remember the original values
				width = style.width;
				minWidth = style.minWidth;
				maxWidth = style.maxWidth;

				// Put in the new values to get a computed value out
				style.minWidth = style.maxWidth = style.width = ret;
				ret = computed.width;

				// Revert the changed values
				style.width = width;
				style.minWidth = minWidth;
				style.maxWidth = maxWidth;
			}
		}

		// Support: IE
		// IE returns zIndex value as an integer.
		return ret === undefined ?
			ret :
			ret + "";
	};
} else if ( document.documentElement.currentStyle ) {
	getStyles = function( elem ) {
		return elem.currentStyle;
	};

	curCSS = function( elem, name, computed ) {
		var left, rs, rsLeft, ret,
			style = elem.style;

		computed = computed || getStyles( elem );
		ret = computed ? computed[ name ] : undefined;

		// Avoid setting ret to empty string here
		// so we don't default to auto
		if ( ret == null && style && style[ name ] ) {
			ret = style[ name ];
		}

		// From the awesome hack by Dean Edwards
		// http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291

		// If we're not dealing with a regular pixel number
		// but a number that has a weird ending, we need to convert it to pixels
		// but not position css attributes, as those are proportional to the parent element instead
		// and we can't measure the parent instead because it might trigger a "stacking dolls" problem
		if ( rnumnonpx.test( ret ) && !rposition.test( name ) ) {

			// Remember the original values
			left = style.left;
			rs = elem.runtimeStyle;
			rsLeft = rs && rs.left;

			// Put in the new values to get a computed value out
			if ( rsLeft ) {
				rs.left = elem.currentStyle.left;
			}
			style.left = name === "fontSize" ? "1em" : ret;
			ret = style.pixelLeft + "px";

			// Revert the changed values
			style.left = left;
			if ( rsLeft ) {
				rs.left = rsLeft;
			}
		}

		// Support: IE
		// IE returns zIndex value as an integer.
		return ret === undefined ?
			ret :
			ret + "" || "auto";
	};
}




function addGetHookIf( conditionFn, hookFn ) {
	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			var condition = conditionFn();

			if ( condition == null ) {
				// The test was not ready at this point; screw the hook this time
				// but check again when needed next time.
				return;
			}

			if ( condition ) {
				// Hook not needed (or it's not possible to use it due to missing dependency),
				// remove it.
				// Since there are no other hooks for marginRight, remove the whole object.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.

			return (this.get = hookFn).apply( this, arguments );
		}
	};
}


(function() {
	var a, reliableHiddenOffsetsVal, boxSizingVal, boxSizingReliableVal,
		pixelPositionVal, reliableMarginRightVal,
		div = document.createElement( "div" ),
		containerStyles = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px",
		divReset =
			"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;" +
			"display:block;padding:0;margin:0;border:0";

	// Setup
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
	a = div.getElementsByTagName( "a" )[ 0 ];

	a.style.cssText = "float:left;opacity:.5";

	// Make sure that element opacity exists
	// (IE uses filter instead)
	// Use a regex to work around a WebKit issue. See #5145
	support.opacity = /^0.5/.test( a.style.opacity );

	// Verify style float existence
	// (IE uses styleFloat instead of cssFloat)
	support.cssFloat = !!a.style.cssFloat;

	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	// Null elements to avoid leaks in IE.
	a = div = null;

	jQuery.extend(support, {
		reliableHiddenOffsets: function() {
			if ( reliableHiddenOffsetsVal != null ) {
				return reliableHiddenOffsetsVal;
			}

			var container, tds, isSupported,
				div = document.createElement( "div" ),
				body = document.getElementsByTagName( "body" )[ 0 ];

			if ( !body ) {
				// Return for frameset docs that don't have a body
				return;
			}

			// Setup
			div.setAttribute( "className", "t" );
			div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";

			container = document.createElement( "div" );
			container.style.cssText = containerStyles;

			body.appendChild( container ).appendChild( div );

			// Support: IE8
			// Check if table cells still have offsetWidth/Height when they are set
			// to display:none and there are still other visible table cells in a
			// table row; if so, offsetWidth/Height are not reliable for use when
			// determining if an element has been hidden directly using
			// display:none (it is still safe to use offsets if a parent element is
			// hidden; don safety goggles and see bug #4512 for more information).
			div.innerHTML = "<table><tr><td></td><td>t</td></tr></table>";
			tds = div.getElementsByTagName( "td" );
			tds[ 0 ].style.cssText = "padding:0;margin:0;border:0;display:none";
			isSupported = ( tds[ 0 ].offsetHeight === 0 );

			tds[ 0 ].style.display = "";
			tds[ 1 ].style.display = "none";

			// Support: IE8
			// Check if empty table cells still have offsetWidth/Height
			reliableHiddenOffsetsVal = isSupported && ( tds[ 0 ].offsetHeight === 0 );

			body.removeChild( container );

			// Null elements to avoid leaks in IE.
			div = body = null;

			return reliableHiddenOffsetsVal;
		},

		boxSizing: function() {
			if ( boxSizingVal == null ) {
				computeStyleTests();
			}
			return boxSizingVal;
		},

		boxSizingReliable: function() {
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return boxSizingReliableVal;
		},

		pixelPosition: function() {
			if ( pixelPositionVal == null ) {
				computeStyleTests();
			}
			return pixelPositionVal;
		},

		reliableMarginRight: function() {
			var body, container, div, marginDiv;

			// Use window.getComputedStyle because jsdom on node.js will break without it.
			if ( reliableMarginRightVal == null && window.getComputedStyle ) {
				body = document.getElementsByTagName( "body" )[ 0 ];
				if ( !body ) {
					// Test fired too early or in an unsupported environment, exit.
					return;
				}

				container = document.createElement( "div" );
				div = document.createElement( "div" );
				container.style.cssText = containerStyles;

				body.appendChild( container ).appendChild( div );

				// Check if div with explicit width and no margin-right incorrectly
				// gets computed margin-right based on width of container. (#3333)
				// Fails in WebKit before Feb 2011 nightlies
				// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
				marginDiv = div.appendChild( document.createElement( "div" ) );
				marginDiv.style.cssText = div.style.cssText = divReset;
				marginDiv.style.marginRight = marginDiv.style.width = "0";
				div.style.width = "1px";

				reliableMarginRightVal =
					!parseFloat( ( window.getComputedStyle( marginDiv, null ) || {} ).marginRight );

				body.removeChild( container );
			}

			return reliableMarginRightVal;
		}
	});

	function computeStyleTests() {
		var container, div,
			body = document.getElementsByTagName( "body" )[ 0 ];

		if ( !body ) {
			// Test fired too early or in an unsupported environment, exit.
			return;
		}

		container = document.createElement( "div" );
		div = document.createElement( "div" );
		container.style.cssText = containerStyles;

		body.appendChild( container ).appendChild( div );

		div.style.cssText =
			"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;" +
				"position:absolute;display:block;padding:1px;border:1px;width:4px;" +
				"margin-top:1%;top:1%";

		// Workaround failing boxSizing test due to offsetWidth returning wrong value
		// with some non-1 values of body zoom, ticket #13543
		jQuery.swap( body, body.style.zoom != null ? { zoom: 1 } : {}, function() {
			boxSizingVal = div.offsetWidth === 4;
		});

		// Will be changed later if needed.
		boxSizingReliableVal = true;
		pixelPositionVal = false;
		reliableMarginRightVal = true;

		// Use window.getComputedStyle because jsdom on node.js will break without it.
		if ( window.getComputedStyle ) {
			pixelPositionVal = ( window.getComputedStyle( div, null ) || {} ).top !== "1%";
			boxSizingReliableVal =
				( window.getComputedStyle( div, null ) || { width: "4px" } ).width === "4px";
		}

		body.removeChild( container );

		// Null elements to avoid leaks in IE.
		div = body = null;
	}

})();


// A method for quickly swapping in/out CSS properties to get correct calculations.
jQuery.swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};


var
		ralpha = /alpha\([^)]*\)/i,
	ropacity = /opacity\s*=\s*([^)]*)/,

	// swappable if display is none or starts with table except "table", "table-cell", or "table-caption"
	// see here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rnumsplit = new RegExp( "^(" + pnum + ")(.*)$", "i" ),
	rrelNum = new RegExp( "^([+-])=(" + pnum + ")", "i" ),

	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: 0,
		fontWeight: 400
	},

	cssPrefixes = [ "Webkit", "O", "Moz", "ms" ];


// return a css property mapped to a potentially vendor prefixed property
function vendorPropName( style, name ) {

	// shortcut for names that are not vendor prefixed
	if ( name in style ) {
		return name;
	}

	// check for vendor prefixed names
	var capName = name.charAt(0).toUpperCase() + name.slice(1),
		origName = name,
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in style ) {
			return name;
		}
	}

	return origName;
}

function showHide( elements, show ) {
	var display, elem, hidden,
		values = [],
		index = 0,
		length = elements.length;

	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		values[ index ] = jQuery._data( elem, "olddisplay" );
		display = elem.style.display;
		if ( show ) {
			// Reset the inline display of this element to learn if it is
			// being hidden by cascaded rules or not
			if ( !values[ index ] && display === "none" ) {
				elem.style.display = "";
			}

			// Set elements which have been overridden with display: none
			// in a stylesheet to whatever the default browser style is
			// for such an element
			if ( elem.style.display === "" && isHidden( elem ) ) {
				values[ index ] = jQuery._data( elem, "olddisplay", defaultDisplay(elem.nodeName) );
			}
		} else {

			if ( !values[ index ] ) {
				hidden = isHidden( elem );

				if ( display && display !== "none" || !hidden ) {
					jQuery._data( elem, "olddisplay", hidden ? display : jQuery.css( elem, "display" ) );
				}
			}
		}
	}

	// Set the display of most of the elements in a second loop
	// to avoid the constant reflow
	for ( index = 0; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}
		if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
			elem.style.display = show ? values[ index ] || "" : "none";
		}
	}

	return elements;
}

function setPositiveNumber( elem, value, subtract ) {
	var matches = rnumsplit.exec( value );
	return matches ?
		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 1 ] - ( subtract || 0 ) ) + ( matches[ 2 ] || "px" ) :
		value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
	var i = extra === ( isBorderBox ? "border" : "content" ) ?
		// If we already have the right measurement, avoid augmentation
		4 :
		// Otherwise initialize for horizontal or vertical properties
		name === "width" ? 1 : 0,

		val = 0;

	for ( ; i < 4; i += 2 ) {
		// both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {
			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// at this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {
			// at this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// at this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with offset property, which is equivalent to the border-box value
	var valueIsBorderBox = true,
		val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
		styles = getStyles( elem ),
		isBorderBox = support.boxSizing() && jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// some non-html elements return undefined for offsetWidth, so check for null/undefined
	// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
	// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
	if ( val <= 0 || val == null ) {
		// Fall back to computed then uncomputed css if necessary
		val = curCSS( elem, name, styles );
		if ( val < 0 || val == null ) {
			val = elem.style[ name ];
		}

		// Computed unit is not pixels. Stop here and return.
		if ( rnumnonpx.test(val) ) {
			return val;
		}

		// we need the check for style in case a browser which returns unreliable values
		// for getComputedStyle silently falls back to the reliable elem.style
		valueIsBorderBox = isBorderBox && ( support.boxSizingReliable() || val === elem.style[ name ] );

		// Normalize "", auto, and prepare for extra
		val = parseFloat( val ) || 0;
	}

	// use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
		)
	) + "px";
}

jQuery.extend({
	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {
					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"columnCount": true,
		"fillOpacity": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		// normalize float css property
		"float": support.cssFloat ? "cssFloat" : "styleFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {
		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = jQuery.camelCase( name ),
			style = elem.style;

		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( style, origName ) );

		// gets hook for the prefixed version
		// followed by the unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// convert relative number strings (+= or -=) to relative numbers. #7345
			if ( type === "string" && (ret = rrelNum.exec( value )) ) {
				value = ( ret[1] + 1 ) * ret[2] + parseFloat( jQuery.css( elem, name ) );
				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set. See: #7116
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add 'px' to the (except for certain CSS properties)
			if ( type === "number" && !jQuery.cssNumber[ origName ] ) {
				value += "px";
			}

			// Fixes #8908, it can be done more correctly by specifing setters in cssHooks,
			// but it would mean to define eight (for every problematic property) identical functions
			if ( !support.clearCloneStyle && value === "" && name.indexOf("background") === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !("set" in hooks) || (value = hooks.set( elem, value, extra )) !== undefined ) {

				// Support: IE
				// Swallow errors from 'invalid' CSS values (#5509)
				try {
					// Support: Chrome, Safari
					// Setting style to blank string required to delete "style: x !important;"
					style[ name ] = "";
					style[ name ] = value;
				} catch(e) {}
			}

		} else {
			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks && (ret = hooks.get( elem, false, extra )) !== undefined ) {
				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var num, val, hooks,
			origName = jQuery.camelCase( name );

		// Make sure that we're working with the right name
		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( elem.style, origName ) );

		// gets hook for the prefixed version
		// followed by the unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		//convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Return, converting to number if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || jQuery.isNumeric( num ) ? num || 0 : val;
		}
		return val;
	}
});

jQuery.each([ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {
				// certain elements can have dimension info if we invisibly show them
				// however, it must have a current display style that would benefit from this
				return elem.offsetWidth === 0 && rdisplayswap.test( jQuery.css( elem, "display" ) ) ?
					jQuery.swap( elem, cssShow, function() {
						return getWidthOrHeight( elem, name, extra );
					}) :
					getWidthOrHeight( elem, name, extra );
			}
		},

		set: function( elem, value, extra ) {
			var styles = extra && getStyles( elem );
			return setPositiveNumber( elem, value, extra ?
				augmentWidthOrHeight(
					elem,
					name,
					extra,
					support.boxSizing() && jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
					styles
				) : 0
			);
		}
	};
});

if ( !support.opacity ) {
	jQuery.cssHooks.opacity = {
		get: function( elem, computed ) {
			// IE uses filters for opacity
			return ropacity.test( (computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || "" ) ?
				( 0.01 * parseFloat( RegExp.$1 ) ) + "" :
				computed ? "1" : "";
		},

		set: function( elem, value ) {
			var style = elem.style,
				currentStyle = elem.currentStyle,
				opacity = jQuery.isNumeric( value ) ? "alpha(opacity=" + value * 100 + ")" : "",
				filter = currentStyle && currentStyle.filter || style.filter || "";

			// IE has trouble with opacity if it does not have layout
			// Force it by setting the zoom level
			style.zoom = 1;

			// if setting opacity to 1, and no other filters exist - attempt to remove filter attribute #6652
			// if value === "", then remove inline opacity #12685
			if ( ( value >= 1 || value === "" ) &&
					jQuery.trim( filter.replace( ralpha, "" ) ) === "" &&
					style.removeAttribute ) {

				// Setting style.filter to null, "" & " " still leave "filter:" in the cssText
				// if "filter:" is present at all, clearType is disabled, we want to avoid this
				// style.removeAttribute is IE Only, but so apparently is this code path...
				style.removeAttribute( "filter" );

				// if there is no filter style applied in a css rule or unset inline opacity, we are done
				if ( value === "" || currentStyle && !currentStyle.filter ) {
					return;
				}
			}

			// otherwise, set new filter values
			style.filter = ralpha.test( filter ) ?
				filter.replace( ralpha, opacity ) :
				filter + " " + opacity;
		}
	};
}

jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
	function( elem, computed ) {
		if ( computed ) {
			// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
			// Work around by temporarily setting element display to inline-block
			return jQuery.swap( elem, { "display": "inline-block" },
				curCSS, [ elem, "marginRight" ] );
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each({
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// assumes a single number if not a string
				parts = typeof value === "string" ? value.split(" ") : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( !rmargin.test( prefix ) ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
});

jQuery.fn.extend({
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( jQuery.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	},
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each(function() {
			if ( isHidden( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		});
	}
});


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || "swing";
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			if ( tween.elem[ tween.prop ] != null &&
				(!tween.elem.style || tween.elem.style[ tween.prop ] == null) ) {
				return tween.elem[ tween.prop ];
			}

			// passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails
			// so, simple values such as "10px" are parsed to Float.
			// complex values such as "rotate(1rad)" are returned as is.
			result = jQuery.css( tween.elem, tween.prop, "" );
			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {
			// use step hook for back compat - use cssHook if its there - use .style if its
			// available and use plain properties where available
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.style && ( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null || jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE <=9
// Panic based approach to setting things on disconnected nodes

Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	}
};

jQuery.fx = Tween.prototype.init;

// Back Compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, timerId,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rfxnum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" ),
	rrun = /queueHooks$/,
	animationPrefilters = [ defaultPrefilter ],
	tweeners = {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value ),
				target = tween.cur(),
				parts = rfxnum.exec( value ),
				unit = parts && parts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

				// Starting value computation is required for potential unit mismatches
				start = ( jQuery.cssNumber[ prop ] || unit !== "px" && +target ) &&
					rfxnum.exec( jQuery.css( tween.elem, prop ) ),
				scale = 1,
				maxIterations = 20;

			if ( start && start[ 3 ] !== unit ) {
				// Trust units reported by jQuery.css
				unit = unit || start[ 3 ];

				// Make sure we update the tween properties later on
				parts = parts || [];

				// Iteratively approximate from a nonzero starting point
				start = +target || 1;

				do {
					// If previous iteration zeroed out, double until we get *something*
					// Use a string for doubling factor so we don't accidentally see scale as unchanged below
					scale = scale || ".5";

					// Adjust and apply
					start = start / scale;
					jQuery.style( tween.elem, prop, start + unit );

				// Update scale, tolerating zero or NaN from tween.cur()
				// And breaking the loop if scale is unchanged or perfect, or if we've just had enough
				} while ( scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations );
			}

			// Update tween properties
			if ( parts ) {
				start = tween.start = +start || +target || 0;
				tween.unit = unit;
				// If a +=/-= token was provided, we're doing a relative animation
				tween.end = parts[ 1 ] ?
					start + ( parts[ 1 ] + 1 ) * parts[ 2 ] :
					+parts[ 2 ];
			}

			return tween;
		} ]
	};

// Animations created synchronously will run synchronously
function createFxNow() {
	setTimeout(function() {
		fxNow = undefined;
	});
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		attrs = { height: type },
		i = 0;

	// if we include width, step value is 1 to do all cssExpand values,
	// if we don't include width, step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4 ; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( tweeners[ prop ] || [] ).concat( tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( (tween = collection[ index ].call( animation, prop, value )) ) {

			// we're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	/* jshint validthis: true */
	var prop, value, toggle, tween, hooks, oldfire, display, dDisplay,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHidden( elem ),
		dataShow = jQuery._data( elem, "fxshow" );

	// handle queue: false promises
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always(function() {
			// doing this makes sure that the complete handler will be called
			// before this completes
			anim.always(function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			});
		});
	}

	// height/width overflow pass
	if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {
		// Make sure that nothing sneaks out
		// Record all 3 overflow attributes because IE does not
		// change the overflow attribute when overflowX and
		// overflowY are set to the same value
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Set display property to inline-block for height/width
		// animations on inline elements that are having width/height animated
		display = jQuery.css( elem, "display" );
		dDisplay = defaultDisplay( elem.nodeName );
		if ( display === "none" ) {
			display = dDisplay;
		}
		if ( display === "inline" &&
				jQuery.css( elem, "float" ) === "none" ) {

			// inline-level elements accept inline-block;
			// block-level elements need to be inline with layout
			if ( !support.inlineBlockNeedsLayout || dDisplay === "inline" ) {
				style.display = "inline-block";
			} else {
				style.zoom = 1;
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		if ( !support.shrinkWrapBlocks() ) {
			anim.always(function() {
				style.overflow = opts.overflow[ 0 ];
				style.overflowX = opts.overflow[ 1 ];
				style.overflowY = opts.overflow[ 2 ];
			});
		}
	}

	// show/hide pass
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.exec( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// If there is dataShow left over from a stopped hide or show and we are going to proceed with show, we should pretend to be hidden
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
		}
	}

	if ( !jQuery.isEmptyObject( orig ) ) {
		if ( dataShow ) {
			if ( "hidden" in dataShow ) {
				hidden = dataShow.hidden;
			}
		} else {
			dataShow = jQuery._data( elem, "fxshow", {} );
		}

		// store state if its toggle - enables .stop().toggle() to "reverse"
		if ( toggle ) {
			dataShow.hidden = !hidden;
		}
		if ( hidden ) {
			jQuery( elem ).show();
		} else {
			anim.done(function() {
				jQuery( elem ).hide();
			});
		}
		anim.done(function() {
			var prop;
			jQuery._removeData( elem, "fxshow" );
			for ( prop in orig ) {
				jQuery.style( elem, prop, orig[ prop ] );
			}
		});
		for ( prop in orig ) {
			tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );

			if ( !( prop in dataShow ) ) {
				dataShow[ prop ] = tween.start;
				if ( hidden ) {
					tween.end = tween.start;
					tween.start = prop === "width" || prop === "height" ? 1 : 0;
				}
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( jQuery.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// not quite $.extend, this wont overwrite keys already present.
			// also - reusing 'index' from above because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = animationPrefilters.length,
		deferred = jQuery.Deferred().always( function() {
			// don't match elem in the :animated selector
			delete tick.elem;
		}),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
				// archaic crash bug won't allow us to use 1 - ( 0.5 || 0 ) (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length ; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ]);

			if ( percent < 1 && length ) {
				return remaining;
			} else {
				deferred.resolveWith( elem, [ animation ] );
				return false;
			}
		},
		animation = deferred.promise({
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, { specialEasing: {} }, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,
					// if we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length ; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// resolve when we played the last frame
				// otherwise, reject
				if ( gotoEnd ) {
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		}),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length ; index++ ) {
		result = animationPrefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( jQuery.isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		})
	);

	// attach callbacks from options
	return animation.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );
}

jQuery.Animation = jQuery.extend( Animation, {
	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.split(" ");
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length ; index++ ) {
			prop = props[ index ];
			tweeners[ prop ] = tweeners[ prop ] || [];
			tweeners[ prop ].unshift( callback );
		}
	},

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			animationPrefilters.unshift( callback );
		} else {
			animationPrefilters.push( callback );
		}
	}
});

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration :
		opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

	// normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend({
	fadeTo: function( speed, to, easing, callback ) {

		// show any hidden elements after setting opacity to 0
		return this.filter( isHidden ).css( "opacity", 0 ).show()

			// animate to the value specified
			.end().animate({ opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {
				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || jQuery._data( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each(function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = jQuery._data( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && (type == null || timers[ index ].queue === type) ) {
					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// start the next in the queue if the last step wasn't forced
			// timers currently will call their complete callbacks, which will dequeue
			// but only if they were gotoEnd
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		});
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each(function() {
			var index,
				data = jQuery._data( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// enable finishing flag on private data
			data.finish = true;

			// empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// turn off finishing flag
			delete data.finish;
		});
	}
});

jQuery.each([ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
});

// Generate shortcuts for custom animations
jQuery.each({
	slideDown: genFx("show"),
	slideUp: genFx("hide"),
	slideToggle: genFx("toggle"),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
});

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		timers = jQuery.timers,
		i = 0;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];
		// Checks the timer has not already been removed
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	if ( timer() ) {
		jQuery.fx.start();
	} else {
		jQuery.timers.pop();
	}
};

jQuery.fx.interval = 13;

jQuery.fx.start = function() {
	if ( !timerId ) {
		timerId = setInterval( jQuery.fx.tick, jQuery.fx.interval );
	}
};

jQuery.fx.stop = function() {
	clearInterval( timerId );
	timerId = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,
	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = setTimeout( next, time );
		hooks.stop = function() {
			clearTimeout( timeout );
		};
	});
};


(function() {
	var a, input, select, opt,
		div = document.createElement("div" );

	// Setup
	div.setAttribute( "className", "t" );
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
	a = div.getElementsByTagName("a")[ 0 ];

	// First batch of tests.
	select = document.createElement("select");
	opt = select.appendChild( document.createElement("option") );
	input = div.getElementsByTagName("input")[ 0 ];

	a.style.cssText = "top:1px";

	// Test setAttribute on camelCase class. If it works, we need attrFixes when doing get/setAttribute (ie6/7)
	support.getSetAttribute = div.className !== "t";

	// Get the style information from getAttribute
	// (IE uses .cssText instead)
	support.style = /top/.test( a.getAttribute("style") );

	// Make sure that URLs aren't manipulated
	// (IE normalizes it by default)
	support.hrefNormalized = a.getAttribute("href") === "/a";

	// Check the default checkbox/radio value ("" on WebKit; "on" elsewhere)
	support.checkOn = !!input.value;

	// Make sure that a selected-by-default option has a working selected property.
	// (WebKit defaults to false instead of true, IE too, if it's in an optgroup)
	support.optSelected = opt.selected;

	// Tests for enctype support on a form (#6743)
	support.enctype = !!document.createElement("form").enctype;

	// Make sure that the options inside disabled selects aren't marked as disabled
	// (WebKit marks them as disabled)
	select.disabled = true;
	support.optDisabled = !opt.disabled;

	// Support: IE8 only
	// Check if we can trust getAttribute("value")
	input = document.createElement( "input" );
	input.setAttribute( "value", "" );
	support.input = input.getAttribute( "value" ) === "";

	// Check if an input maintains its value after becoming a radio
	input.value = "t";
	input.setAttribute( "type", "radio" );
	support.radioValue = input.value === "t";

	// Null elements to avoid leaks in IE.
	a = input = select = opt = div = null;
})();


var rreturn = /\r/g;

jQuery.fn.extend({
	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[0];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] || jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks && "get" in hooks && (ret = hooks.get( elem, "value" )) !== undefined ) {
					return ret;
				}

				ret = elem.value;

				return typeof ret === "string" ?
					// handle most common string cases
					ret.replace(rreturn, "") :
					// handle cases where value is null/undef or number
					ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each(function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";
			} else if ( typeof val === "number" ) {
				val += "";
			} else if ( jQuery.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				});
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !("set" in hooks) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		});
	}
});

jQuery.extend({
	valHooks: {
		option: {
			get: function( elem ) {
				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :
					jQuery.text( elem );
			}
		},
		select: {
			get: function( elem ) {
				var value, option,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one" || index < 0,
					values = one ? null : [],
					max = one ? index + 1 : options.length,
					i = index < 0 ?
						max :
						one ? index : 0;

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// oldIE doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&
							// Don't return options that are disabled or in a disabled optgroup
							( support.optDisabled ? !option.disabled : option.getAttribute("disabled") === null ) &&
							( !option.parentNode.disabled || !jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];

					if ( jQuery.inArray( jQuery.valHooks.option.get( option ), values ) >= 0 ) {

						// Support: IE6
						// When new option element is added to select box we need to
						// force reflow of newly added node in order to workaround delay
						// of initialization properties
						try {
							option.selected = optionSet = true;

						} catch ( _ ) {

							// Will be executed only in IE6
							option.scrollHeight;
						}

					} else {
						option.selected = false;
					}
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}

				return options;
			}
		}
	}
});

// Radios and checkboxes getter/setter
jQuery.each([ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( jQuery.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) >= 0 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			// Support: Webkit
			// "" is returned instead of "on" if a value isn't specified
			return elem.getAttribute("value") === null ? "on" : elem.value;
		};
	}
});




var nodeHook, boolHook,
	attrHandle = jQuery.expr.attrHandle,
	ruseDefault = /^(?:checked|selected)$/i,
	getSetAttribute = support.getSetAttribute,
	getSetInput = support.input;

jQuery.fn.extend({
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each(function() {
			jQuery.removeAttr( this, name );
		});
	}
});

jQuery.extend({
	attr: function( elem, name, value ) {
		var hooks, ret,
			nType = elem.nodeType;

		// don't get/set attributes on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === strundefined ) {
			return jQuery.prop( elem, name, value );
		}

		// All attributes are lowercase
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			name = name.toLowerCase();
			hooks = jQuery.attrHooks[ name ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : nodeHook );
		}

		if ( value !== undefined ) {

			if ( value === null ) {
				jQuery.removeAttr( elem, name );

			} else if ( hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ) {
				return ret;

			} else {
				elem.setAttribute( name, value + "" );
				return value;
			}

		} else if ( hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ) {
			return ret;

		} else {
			ret = jQuery.find.attr( elem, name );

			// Non-existent attributes return null, we normalize to undefined
			return ret == null ?
				undefined :
				ret;
		}
	},

	removeAttr: function( elem, value ) {
		var name, propName,
			i = 0,
			attrNames = value && value.match( rnotwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( (name = attrNames[i++]) ) {
				propName = jQuery.propFix[ name ] || name;

				// Boolean attributes get special treatment (#10870)
				if ( jQuery.expr.match.bool.test( name ) ) {
					// Set corresponding property to false
					if ( getSetInput && getSetAttribute || !ruseDefault.test( name ) ) {
						elem[ propName ] = false;
					// Support: IE<9
					// Also clear defaultChecked/defaultSelected (if appropriate)
					} else {
						elem[ jQuery.camelCase( "default-" + name ) ] =
							elem[ propName ] = false;
					}

				// See #9699 for explanation of this approach (setting first, then removal)
				} else {
					jQuery.attr( elem, name, "" );
				}

				elem.removeAttribute( getSetAttribute ? name : propName );
			}
		}
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" && jQuery.nodeName(elem, "input") ) {
					// Setting the type on a radio button after the value resets the value in IE6-9
					// Reset value to default in case type is set after value during creation
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	}
});

// Hook for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {
			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else if ( getSetInput && getSetAttribute || !ruseDefault.test( name ) ) {
			// IE<8 needs the *property* name
			elem.setAttribute( !getSetAttribute && jQuery.propFix[ name ] || name, name );

		// Use defaultChecked and defaultSelected for oldIE
		} else {
			elem[ jQuery.camelCase( "default-" + name ) ] = elem[ name ] = true;
		}

		return name;
	}
};

// Retrieve booleans specially
jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {

	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = getSetInput && getSetAttribute || !ruseDefault.test( name ) ?
		function( elem, name, isXML ) {
			var ret, handle;
			if ( !isXML ) {
				// Avoid an infinite loop by temporarily removing this function from the getter
				handle = attrHandle[ name ];
				attrHandle[ name ] = ret;
				ret = getter( elem, name, isXML ) != null ?
					name.toLowerCase() :
					null;
				attrHandle[ name ] = handle;
			}
			return ret;
		} :
		function( elem, name, isXML ) {
			if ( !isXML ) {
				return elem[ jQuery.camelCase( "default-" + name ) ] ?
					name.toLowerCase() :
					null;
			}
		};
});

// fix oldIE attroperties
if ( !getSetInput || !getSetAttribute ) {
	jQuery.attrHooks.value = {
		set: function( elem, value, name ) {
			if ( jQuery.nodeName( elem, "input" ) ) {
				// Does not return so that setAttribute is also used
				elem.defaultValue = value;
			} else {
				// Use nodeHook if defined (#1954); otherwise setAttribute is fine
				return nodeHook && nodeHook.set( elem, value, name );
			}
		}
	};
}

// IE6/7 do not support getting/setting some attributes with get/setAttribute
if ( !getSetAttribute ) {

	// Use this for any attribute in IE6/7
	// This fixes almost every IE6/7 issue
	nodeHook = {
		set: function( elem, value, name ) {
			// Set the existing or create a new attribute node
			var ret = elem.getAttributeNode( name );
			if ( !ret ) {
				elem.setAttributeNode(
					(ret = elem.ownerDocument.createAttribute( name ))
				);
			}

			ret.value = value += "";

			// Break association with cloned elements by also using setAttribute (#9646)
			if ( name === "value" || value === elem.getAttribute( name ) ) {
				return value;
			}
		}
	};

	// Some attributes are constructed with empty-string values when not defined
	attrHandle.id = attrHandle.name = attrHandle.coords =
		function( elem, name, isXML ) {
			var ret;
			if ( !isXML ) {
				return (ret = elem.getAttributeNode( name )) && ret.value !== "" ?
					ret.value :
					null;
			}
		};

	// Fixing value retrieval on a button requires this module
	jQuery.valHooks.button = {
		get: function( elem, name ) {
			var ret = elem.getAttributeNode( name );
			if ( ret && ret.specified ) {
				return ret.value;
			}
		},
		set: nodeHook.set
	};

	// Set contenteditable to false on removals(#10429)
	// Setting to empty string throws an error as an invalid value
	jQuery.attrHooks.contenteditable = {
		set: function( elem, value, name ) {
			nodeHook.set( elem, value === "" ? false : value, name );
		}
	};

	// Set width and height to auto instead of 0 on empty string( Bug #8150 )
	// This is for removals
	jQuery.each([ "width", "height" ], function( i, name ) {
		jQuery.attrHooks[ name ] = {
			set: function( elem, value ) {
				if ( value === "" ) {
					elem.setAttribute( name, "auto" );
					return value;
				}
			}
		};
	});
}

if ( !support.style ) {
	jQuery.attrHooks.style = {
		get: function( elem ) {
			// Return undefined in the case of empty string
			// Note: IE uppercases css property names, but if we were to .toLowerCase()
			// .cssText, that would destroy case senstitivity in URL's, like in "background"
			return elem.style.cssText || undefined;
		},
		set: function( elem, value ) {
			return ( elem.style.cssText = value + "" );
		}
	};
}




var rfocusable = /^(?:input|select|textarea|button|object)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend({
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		name = jQuery.propFix[ name ] || name;
		return this.each(function() {
			// try/catch handles cases where IE balks (such as removing a property on window)
			try {
				this[ name ] = undefined;
				delete this[ name ];
			} catch( e ) {}
		});
	}
});

jQuery.extend({
	propFix: {
		"for": "htmlFor",
		"class": "className"
	},

	prop: function( elem, name, value ) {
		var ret, hooks, notxml,
			nType = elem.nodeType;

		// don't get/set properties on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

		if ( notxml ) {
			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			return hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ?
				ret :
				( elem[ name ] = value );

		} else {
			return hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ?
				ret :
				elem[ name ];
		}
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {
				// elem.tabIndex doesn't always return the correct value when it hasn't been explicitly set
				// http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				return tabindex ?
					parseInt( tabindex, 10 ) :
					rfocusable.test( elem.nodeName ) || rclickable.test( elem.nodeName ) && elem.href ?
						0 :
						-1;
			}
		}
	}
});

// Some attributes require a special call on IE
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !support.hrefNormalized ) {
	// href/src property should get the full normalized URL (#10299/#12915)
	jQuery.each([ "href", "src" ], function( i, name ) {
		jQuery.propHooks[ name ] = {
			get: function( elem ) {
				return elem.getAttribute( name, 4 );
			}
		};
	});
}

// Support: Safari, IE9+
// mis-reports the default selected property of an option
// Accessing the parent's selectedIndex property fixes it
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {
			var parent = elem.parentNode;

			if ( parent ) {
				parent.selectedIndex;

				// Make sure that it also works with optgroups, see #5701
				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
			return null;
		}
	};
}

jQuery.each([
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
});

// IE6/7 call enctype encoding
if ( !support.enctype ) {
	jQuery.propFix.enctype = "encoding";
}




var rclass = /[\t\r\n\f]/g;

jQuery.fn.extend({
	addClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			i = 0,
			len = this.length,
			proceed = typeof value === "string" && value;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).addClass( value.call( this, j, this.className ) );
			});
		}

		if ( proceed ) {
			// The disjunction here is for better compressibility (see removeClass)
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					" "
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			i = 0,
			len = this.length,
			proceed = arguments.length === 0 || typeof value === "string" && value;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).removeClass( value.call( this, j, this.className ) );
			});
		}
		if ( proceed ) {
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					""
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) >= 0 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// only assign if different to avoid unneeded rendering.
					finalValue = value ? jQuery.trim( cur ) : "";
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( i ) {
				jQuery( this ).toggleClass( value.call(this, i, this.className, stateVal), stateVal );
			});
		}

		return this.each(function() {
			if ( type === "string" ) {
				// toggle individual class names
				var className,
					i = 0,
					self = jQuery( this ),
					classNames = value.match( rnotwhite ) || [];

				while ( (className = classNames[ i++ ]) ) {
					// check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( type === strundefined || type === "boolean" ) {
				if ( this.className ) {
					// store className if set
					jQuery._data( this, "__className__", this.className );
				}

				// If the element has a class name or if we're passed "false",
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				this.className = this.className || value === false ? "" : jQuery._data( this, "__className__" ) || "";
			}
		});
	},

	hasClass: function( selector ) {
		var className = " " + selector + " ",
			i = 0,
			l = this.length;
		for ( ; i < l; i++ ) {
			if ( this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf( className ) >= 0 ) {
				return true;
			}
		}

		return false;
	}
});




// Return jQuery for attributes-only inclusion


jQuery.each( ("blur focus focusin focusout load resize scroll unload click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup error contextmenu").split(" "), function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
});

jQuery.fn.extend({
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	},

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {
		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ? this.off( selector, "**" ) : this.off( types, selector || "**", fn );
	}
});


var nonce = jQuery.now();

var rquery = (/\?/);



var rvalidtokens = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;

jQuery.parseJSON = function( data ) {
	// Attempt to parse using the native JSON parser first
	if ( window.JSON && window.JSON.parse ) {
		// Support: Android 2.3
		// Workaround failure to string-cast null input
		return window.JSON.parse( data + "" );
	}

	var requireNonComma,
		depth = null,
		str = jQuery.trim( data + "" );

	// Guard against invalid (and possibly dangerous) input by ensuring that nothing remains
	// after removing valid tokens
	return str && !jQuery.trim( str.replace( rvalidtokens, function( token, comma, open, close ) {

		// Force termination if we see a misplaced comma
		if ( requireNonComma && comma ) {
			depth = 0;
		}

		// Perform no more replacements after returning to outermost depth
		if ( depth === 0 ) {
			return token;
		}

		// Commas must not follow "[", "{", or ","
		requireNonComma = open || comma;

		// Determine new depth
		// array/object open ("[" or "{"): depth += true - false (increment)
		// array/object close ("]" or "}"): depth += false - true (decrement)
		// other cases ("," or primitive): depth += true - true (numeric cast)
		depth += !close - !open;

		// Remove this token
		return "";
	}) ) ?
		( Function( "return " + str ) )() :
		jQuery.error( "Invalid JSON: " + data );
};


// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml, tmp;
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	try {
		if ( window.DOMParser ) { // Standard
			tmp = new DOMParser();
			xml = tmp.parseFromString( data, "text/xml" );
		} else { // IE
			xml = new ActiveXObject( "Microsoft.XMLDOM" );
			xml.async = "false";
			xml.loadXML( data );
		}
	} catch( e ) {
		xml = undefined;
	}
	if ( !xml || !xml.documentElement || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	// Document location
	ajaxLocParts,
	ajaxLocation,

	rhash = /#.*$/,
	rts = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg, // IE leaves an \r character at EOL
	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,
	rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat("*");

// #8138, IE may throw an exception when accessing
// a field from window.location if document.domain has been set
try {
	ajaxLocation = location.href;
} catch( e ) {
	// Use the href attribute of an A element
	// since IE will modify it given document.location
	ajaxLocation = document.createElement( "a" );
	ajaxLocation.href = "";
	ajaxLocation = ajaxLocation.href;
}

// Segment location into parts
ajaxLocParts = rurl.exec( ajaxLocation.toLowerCase() ) || [];

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

		if ( jQuery.isFunction( func ) ) {
			// For each dataType in the dataTypeExpression
			while ( (dataType = dataTypes[i++]) ) {
				// Prepend if requested
				if ( dataType.charAt( 0 ) === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					(structure[ dataType ] = structure[ dataType ] || []).unshift( func );

				// Otherwise append
				} else {
					(structure[ dataType ] = structure[ dataType ] || []).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[ dataTypeOrTransport ] ) {
				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		});
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var deep, key,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || (deep = {}) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {
	var firstDataType, ct, finalDataType, type,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {
		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}
		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},
		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

			// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {
								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s[ "throws" ] ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return { state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current };
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend({

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: ajaxLocation,
		type: "GET",
		isLocal: rlocalProtocol.test( ajaxLocParts[ 1 ] ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /xml/,
			html: /html/,
			json: /json/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": jQuery.parseJSON,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var // Cross-domain detection vars
			parts,
			// Loop variable
			i,
			// URL without anti-cache param
			cacheURL,
			// Response headers as string
			responseHeadersString,
			// timeout handle
			timeoutTimer,

			// To know if global events are to be dispatched
			fireGlobals,

			transport,
			// Response headers
			responseHeaders,
			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),
			// Callbacks context
			callbackContext = s.context || s,
			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context && ( callbackContext.nodeType || callbackContext.jquery ) ?
				jQuery( callbackContext ) :
				jQuery.event,
			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks("once memory"),
			// Status-dependent callbacks
			statusCode = s.statusCode || {},
			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},
			// The jqXHR state
			state = 0,
			// Default abort message
			strAbort = "canceled",
			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( state === 2 ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( (match = rheaders.exec( responseHeadersString )) ) {
								responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return state === 2 ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					var lname = name.toLowerCase();
					if ( !state ) {
						name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( !state ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( state < 2 ) {
							for ( code in map ) {
								// Lazy-add the new callback in a way that preserves old ones
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						} else {
							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR ).complete = completeDeferred.add;
		jqXHR.success = jqXHR.done;
		jqXHR.error = jqXHR.fail;

		// Remove hash character (#7531: and string promotion)
		// Add protocol if not provided (#5866: IE7 issue with protocol-less urls)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || ajaxLocation ) + "" ).replace( rhash, "" ).replace( rprotocol, ajaxLocParts[ 1 ] + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

		// A cross-domain request is in order when we have a protocol:host:port mismatch
		if ( s.crossDomain == null ) {
			parts = rurl.exec( s.url.toLowerCase() );
			s.crossDomain = !!( parts &&
				( parts[ 1 ] !== ajaxLocParts[ 1 ] || parts[ 2 ] !== ajaxLocParts[ 2 ] ||
					( parts[ 3 ] || ( parts[ 1 ] === "http:" ? "80" : "443" ) ) !==
						( ajaxLocParts[ 3 ] || ( ajaxLocParts[ 1 ] === "http:" ? "80" : "443" ) ) )
			);
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( state === 2 ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		fireGlobals = s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger("ajaxStart");
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		cacheURL = s.url;

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );
				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add anti-cache in url if needed
			if ( s.cache === false ) {
				s.url = rts.test( cacheURL ) ?

					// If there is already a '_' parameter, set its value
					cacheURL.replace( rts, "$1_=" + nonce++ ) :

					// Otherwise add one to the end
					cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
			}
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
				s.accepts[ s.dataTypes[0] ] + ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend && ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {
			// Abort if not done already and return
			return jqXHR.abort();
		}

		// aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		for ( i in { success: 1, error: 1, complete: 1 } ) {
			jqXHR[ i ]( s[ i ] );
		}

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}
			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = setTimeout(function() {
					jqXHR.abort("timeout");
				}, s.timeout );
			}

			try {
				state = 1;
				transport.send( requestHeaders, done );
			} catch ( e ) {
				// Propagate exception as error if not done
				if ( state < 2 ) {
					done( -1, e );
				// Simply rethrow otherwise
				} else {
					throw e;
				}
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Called once
			if ( state === 2 ) {
				return;
			}

			// State is "done" now
			state = 2;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader("Last-Modified");
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader("etag");
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {
				// We extract error from statusText
				// then normalize statusText and status for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger("ajaxStop");
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
});

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {
		// shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		return jQuery.ajax({
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		});
	};
});

// Attach a bunch of functions for handling common AJAX events
jQuery.each( [ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
});


jQuery._evalUrl = function( url ) {
	return jQuery.ajax({
		url: url,
		type: "GET",
		dataType: "script",
		async: false,
		global: false,
		"throws": true
	});
};


jQuery.fn.extend({
	wrapAll: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function(i) {
				jQuery(this).wrapAll( html.call(this, i) );
			});
		}

		if ( this[0] ) {
			// The elements to wrap the target around
			var wrap = jQuery( html, this[0].ownerDocument ).eq(0).clone(true);

			if ( this[0].parentNode ) {
				wrap.insertBefore( this[0] );
			}

			wrap.map(function() {
				var elem = this;

				while ( elem.firstChild && elem.firstChild.nodeType === 1 ) {
					elem = elem.firstChild;
				}

				return elem;
			}).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function(i) {
				jQuery(this).wrapInner( html.call(this, i) );
			});
		}

		return this.each(function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		});
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each(function(i) {
			jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );
		});
	},

	unwrap: function() {
		return this.parent().each(function() {
			if ( !jQuery.nodeName( this, "body" ) ) {
				jQuery( this ).replaceWith( this.childNodes );
			}
		}).end();
	}
});


jQuery.expr.filters.hidden = function( elem ) {
	// Support: Opera <= 12.12
	// Opera reports offsetWidths and offsetHeights less than zero on some elements
	return elem.offsetWidth <= 0 && elem.offsetHeight <= 0 ||
		(!support.reliableHiddenOffsets() &&
			((elem.style && elem.style.display) || jQuery.css( elem, "display" )) === "none");
};

jQuery.expr.filters.visible = function( elem ) {
	return !jQuery.expr.filters.hidden( elem );
};




var r20 = /%20/g,
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( jQuery.isArray( obj ) ) {
		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {
				// Treat each array item as a scalar.
				add( prefix, v );

			} else {
				// Item is non-scalar (array or object), encode its numeric index.
				buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add );
			}
		});

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {
		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {
		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, value ) {
			// If value is a function, invoke it and return its value
			value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
			s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
		};

	// Set traditional to true for jQuery <= 1.3.2 behavior.
	if ( traditional === undefined ) {
		traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		});

	} else {
		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" ).replace( r20, "+" );
};

jQuery.fn.extend({
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map(function() {
			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		})
		.filter(function() {
			var type = this.type;
			// Use .is(":disabled") so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		})
		.map(function( i, elem ) {
			var val = jQuery( this ).val();

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					}) :
					{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		}).get();
	}
});


// Create the request object
// (This is still attached to ajaxSettings for backward compatibility)
jQuery.ajaxSettings.xhr = window.ActiveXObject !== undefined ?
	// Support: IE6+
	function() {

		// XHR cannot access local files, always use ActiveX for that case
		return !this.isLocal &&

			// Support: IE7-8
			// oldIE XHR does not support non-RFC2616 methods (#13240)
			// See http://msdn.microsoft.com/en-us/library/ie/ms536648(v=vs.85).aspx
			// and http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9
			// Although this check for six methods instead of eight
			// since IE also does not support "trace" and "connect"
			/^(get|post|head|put|delete|options)$/i.test( this.type ) &&

			createStandardXHR() || createActiveXHR();
	} :
	// For all other browsers, use the standard XMLHttpRequest object
	createStandardXHR;

var xhrId = 0,
	xhrCallbacks = {},
	xhrSupported = jQuery.ajaxSettings.xhr();

// Support: IE<10
// Open requests must be manually aborted on unload (#5280)
if ( window.ActiveXObject ) {
	jQuery( window ).on( "unload", function() {
		for ( var key in xhrCallbacks ) {
			xhrCallbacks[ key ]( undefined, true );
		}
	});
}

// Determine support properties
support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
xhrSupported = support.ajax = !!xhrSupported;

// Create transport if the browser can provide an xhr
if ( xhrSupported ) {

	jQuery.ajaxTransport(function( options ) {
		// Cross domain only allowed if supported through XMLHttpRequest
		if ( !options.crossDomain || support.cors ) {

			var callback;

			return {
				send: function( headers, complete ) {
					var i,
						xhr = options.xhr(),
						id = ++xhrId;

					// Open the socket
					xhr.open( options.type, options.url, options.async, options.username, options.password );

					// Apply custom fields if provided
					if ( options.xhrFields ) {
						for ( i in options.xhrFields ) {
							xhr[ i ] = options.xhrFields[ i ];
						}
					}

					// Override mime type if needed
					if ( options.mimeType && xhr.overrideMimeType ) {
						xhr.overrideMimeType( options.mimeType );
					}

					// X-Requested-With header
					// For cross-domain requests, seeing as conditions for a preflight are
					// akin to a jigsaw puzzle, we simply never set it to be sure.
					// (it can always be set on a per-request basis or even using ajaxSetup)
					// For same-domain requests, won't change header if already provided.
					if ( !options.crossDomain && !headers["X-Requested-With"] ) {
						headers["X-Requested-With"] = "XMLHttpRequest";
					}

					// Set headers
					for ( i in headers ) {
						// Support: IE<9
						// IE's ActiveXObject throws a 'Type Mismatch' exception when setting
						// request header to a null-value.
						//
						// To keep consistent with other XHR implementations, cast the value
						// to string and ignore `undefined`.
						if ( headers[ i ] !== undefined ) {
							xhr.setRequestHeader( i, headers[ i ] + "" );
						}
					}

					// Do send the request
					// This may raise an exception which is actually
					// handled in jQuery.ajax (so no try/catch here)
					xhr.send( ( options.hasContent && options.data ) || null );

					// Listener
					callback = function( _, isAbort ) {
						var status, statusText, responses;

						// Was never called and is aborted or complete
						if ( callback && ( isAbort || xhr.readyState === 4 ) ) {
							// Clean up
							delete xhrCallbacks[ id ];
							callback = undefined;
							xhr.onreadystatechange = jQuery.noop;

							// Abort manually if needed
							if ( isAbort ) {
								if ( xhr.readyState !== 4 ) {
									xhr.abort();
								}
							} else {
								responses = {};
								status = xhr.status;

								// Support: IE<10
								// Accessing binary-data responseText throws an exception
								// (#11426)
								if ( typeof xhr.responseText === "string" ) {
									responses.text = xhr.responseText;
								}

								// Firefox throws an exception when accessing
								// statusText for faulty cross-domain requests
								try {
									statusText = xhr.statusText;
								} catch( e ) {
									// We normalize with Webkit giving an empty statusText
									statusText = "";
								}

								// Filter status for non standard behaviors

								// If the request is local and we have data: assume a success
								// (success with no data won't get notified, that's the best we
								// can do given current implementations)
								if ( !status && options.isLocal && !options.crossDomain ) {
									status = responses.text ? 200 : 404;
								// IE - #1450: sometimes returns 1223 when it should be 204
								} else if ( status === 1223 ) {
									status = 204;
								}
							}
						}

						// Call complete if needed
						if ( responses ) {
							complete( status, statusText, responses, xhr.getAllResponseHeaders() );
						}
					};

					if ( !options.async ) {
						// if we're in sync mode we fire the callback
						callback();
					} else if ( xhr.readyState === 4 ) {
						// (IE6 & IE7) if it's in cache and has been
						// retrieved directly we need to fire the callback
						setTimeout( callback );
					} else {
						// Add to the list of active xhr callbacks
						xhr.onreadystatechange = xhrCallbacks[ id ] = callback;
					}
				},

				abort: function() {
					if ( callback ) {
						callback( undefined, true );
					}
				}
			};
		}
	});
}

// Functions to create xhrs
function createStandardXHR() {
	try {
		return new window.XMLHttpRequest();
	} catch( e ) {}
}

function createActiveXHR() {
	try {
		return new window.ActiveXObject( "Microsoft.XMLHTTP" );
	} catch( e ) {}
}




// Install script dataType
jQuery.ajaxSetup({
	accepts: {
		script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /(?:java|ecma)script/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
});

// Handle cache's special case and global
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
		s.global = false;
	}
});

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function(s) {

	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {

		var script,
			head = document.head || jQuery("head")[0] || document.documentElement;

		return {

			send: function( _, callback ) {

				script = document.createElement("script");

				script.async = true;

				if ( s.scriptCharset ) {
					script.charset = s.scriptCharset;
				}

				script.src = s.url;

				// Attach handlers for all browsers
				script.onload = script.onreadystatechange = function( _, isAbort ) {

					if ( isAbort || !script.readyState || /loaded|complete/.test( script.readyState ) ) {

						// Handle memory leak in IE
						script.onload = script.onreadystatechange = null;

						// Remove the script
						if ( script.parentNode ) {
							script.parentNode.removeChild( script );
						}

						// Dereference the script
						script = null;

						// Callback if not abort
						if ( !isAbort ) {
							callback( 200, "success" );
						}
					}
				};

				// Circumvent IE6 bugs with base elements (#2709 and #4378) by prepending
				// Use native DOM manipulation to avoid our domManip AJAX trickery
				head.insertBefore( script, head.firstChild );
			},

			abort: function() {
				if ( script ) {
					script.onload( undefined, true );
				}
			}
		};
	}
});




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup({
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
});

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" && !( s.contentType || "" ).indexOf("application/x-www-form-urlencoded") && rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters["script json"] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always(function() {
			// Restore preexisting value
			window[ callbackName ] = overwritten;

			// Save back as free
			if ( s[ callbackName ] ) {
				// make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		});

		// Delegate to script
		return "script";
	}
});




// data: string of html
// context (optional): If specified, the fragment will be created in this context, defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}
	context = context || document;

	var parsed = rsingleTag.exec( data ),
		scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[1] ) ];
	}

	parsed = jQuery.buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


// Keep a copy of the old load method
var _load = jQuery.fn.load;

/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	if ( typeof url !== "string" && _load ) {
		return _load.apply( this, arguments );
	}

	var selector, response, type,
		self = this,
		off = url.indexOf(" ");

	if ( off >= 0 ) {
		selector = url.slice( off, url.length );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax({
			url: url,

			// if "type" variable is undefined, then "GET" method will be used
			type: type,
			dataType: "html",
			data: params
		}).done(function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery("<div>").append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		}).complete( callback && function( jqXHR, status ) {
			self.each( callback, response || [ jqXHR.responseText, status, jqXHR ] );
		});
	}

	return this;
};




jQuery.expr.filters.animated = function( elem ) {
	return jQuery.grep(jQuery.timers, function( fn ) {
		return elem === fn.elem;
	}).length;
};





var docElem = window.document.documentElement;

/**
 * Gets a window from an element
 */
function getWindow( elem ) {
	return jQuery.isWindow( elem ) ?
		elem :
		elem.nodeType === 9 ?
			elem.defaultView || elem.parentWindow :
			false;
}

jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			jQuery.inArray("auto", [ curCSSTop, curCSSLeft ] ) > -1;

		// need to be able to calculate position if either top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;
		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {
			options = options.call( elem, i, curOffset );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );
		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend({
	offset: function( options ) {
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each(function( i ) {
					jQuery.offset.setOffset( this, options, i );
				});
		}

		var docElem, win,
			box = { top: 0, left: 0 },
			elem = this[ 0 ],
			doc = elem && elem.ownerDocument;

		if ( !doc ) {
			return;
		}

		docElem = doc.documentElement;

		// Make sure it's not a disconnected DOM node
		if ( !jQuery.contains( docElem, elem ) ) {
			return box;
		}

		// If we don't have gBCR, just use 0,0 rather than error
		// BlackBerry 5, iOS 3 (original iPhone)
		if ( typeof elem.getBoundingClientRect !== strundefined ) {
			box = elem.getBoundingClientRect();
		}
		win = getWindow( doc );
		return {
			top: box.top  + ( win.pageYOffset || docElem.scrollTop )  - ( docElem.clientTop  || 0 ),
			left: box.left + ( win.pageXOffset || docElem.scrollLeft ) - ( docElem.clientLeft || 0 )
		};
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
			parentOffset = { top: 0, left: 0 },
			elem = this[ 0 ];

		// fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {
			// we assume that getBoundingClientRect is available when computed position is fixed
			offset = elem.getBoundingClientRect();
		} else {
			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			parentOffset.top  += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
			parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
		}

		// Subtract parent offsets and element margins
		// note: when an element has margin: auto the offsetLeft and marginLeft
		// are the same in Safari causing offset.left to incorrectly be 0
		return {
			top:  offset.top  - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true)
		};
	},

	offsetParent: function() {
		return this.map(function() {
			var offsetParent = this.offsetParent || docElem;

			while ( offsetParent && ( !jQuery.nodeName( offsetParent, "html" ) && jQuery.css( offsetParent, "position" ) === "static" ) ) {
				offsetParent = offsetParent.offsetParent;
			}
			return offsetParent || docElem;
		});
	}
});

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = /Y/.test( prop );

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {
			var win = getWindow( elem );

			if ( val === undefined ) {
				return win ? (prop in win) ? win[ prop ] :
					win.document.documentElement[ method ] :
					elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : jQuery( win ).scrollLeft(),
					top ? val : jQuery( win ).scrollTop()
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length, null );
	};
});

// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// getComputedStyle returns percent when specified for top/left/bottom/right
// rather than make the css module depend on the offset module, we just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );
				// if curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
});


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name }, function( defaultExtra, funcName ) {
		// margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {
					// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
					// isn't a whole lot we can do. See pull request at this URL for discussion:
					// https://github.com/jquery/jquery/pull/764
					return elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height], whichever is greatest
					// unfortunately, this causes bug #3838 in IE6/8 only, but there is currently no good, small way to fix it.
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?
					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable, null );
		};
	});
});


// The number of elements contained in the matched element set
jQuery.fn.size = function() {
	return this.length;
};

jQuery.fn.andSelf = jQuery.fn.addBack;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.
if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	});
}




var
	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in
// AMD (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( typeof noGlobal === strundefined ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;

}));
/*!
 * ionic.bundle.js is a concatenation of:
 * ionic.js, angular.js, angular-animate.js,
 * angular-ui-router.js, and ionic-angular.js
 */

/*!
 * Copyright 2014 Drifty Co.
 * http://drifty.com/
 *
 * Ionic, v1.0.0-beta.1
 * A powerful HTML5 mobile app framework.
 * http://ionicframework.com/
 *
 * By @maxlynch, @benjsperry, @adamdbradley <3
 *
 * Licensed under the MIT license. Please see LICENSE for more information.
 *
 */


!function(){window.ionic={controllers:{},views:{},version:"1.0.0-beta.1"},function(t){function e(t){return t*t*t}function i(t){return 3*t*t*(1-t)}function n(t){return 3*t*(1-t)*(1-t)}function r(t){return(1-t)*(1-t)*(1-t)}var o=function(t,e){return t||(t=0),e||(e=0),{x:t,y:e}};t.Animator={getQuadraticBezier:function(t,s,a,l,c){var u=new o;return u.x=s.x*e(t)+a.x*i(t)+l.x*n(t)+c.x*r(t),u.y=s.y*e(t)+a.y*i(t)+l.y*n(t)+c.y*r(t),u},getCubicBezier:function(t,e,i,n,r){epsilon=1e3/60/r/4;var o=function(e){var n=1-e;return 3*n*n*e*t+3*n*e*e*i+e*e*e},s=function(t){var i=1-t;return 3*i*i*t*e+3*i*t*t*n+t*t*t},a=function(e){var n=1-e;return 3*(2*(e-1)*e+n*n)*t+3*(-e*e*e+2*n*e)*i};return function(t){var e,i,n,r,l,c,u=t;for(n=u,c=0;8>c;c++){if(r=o(n)-u,Math.abs(r)<epsilon)return s(n);if(l=a(n),Math.abs(l)<1e-6)break;n-=r/l}if(e=0,i=1,n=u,e>n)return s(e);if(n>i)return s(i);for(;i>e;){if(r=o(n),Math.abs(r-u)<epsilon)return s(n);u>r?e=n:i=n,n=.5*(i-e)+e}return s(n)}},animate:function(t){return{leave:function(){var e=function(){t.classList.remove("leave"),t.classList.remove("leave-active"),t.removeEventListener("webkitTransitionEnd",e),t.removeEventListener("transitionEnd",e)};return t.addEventListener("webkitTransitionEnd",e),t.addEventListener("transitionEnd",e),t.classList.add("leave"),t.classList.add("leave-active"),this},enter:function(){var e=function(){t.classList.remove("enter"),t.classList.remove("enter-active"),t.removeEventListener("webkitTransitionEnd",e),t.removeEventListener("transitionEnd",e)};return t.addEventListener("webkitTransitionEnd",e),t.addEventListener("transitionEnd",e),t.classList.add("enter"),t.classList.add("enter-active"),this}}}}}(ionic),function(t,e,i){function n(){o=!0;for(var t=0;t<r.length;t++)i.requestAnimationFrame(r[t]);r=[],e.removeEventListener("DOMContentLoaded",n)}var r=[],o=!1;e.addEventListener("DOMContentLoaded",n),t._rAF=function(){return t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||function(e){t.setTimeout(e,16)}}(),i.DomUtil={requestAnimationFrame:function(e){t._rAF(e)},animationFrameThrottle:function(t){var e,n,r;return function(){e=arguments,r=this,n||(n=!0,i.requestAnimationFrame(function(){t.apply(r,e),n=!1}))}},getPositionInParent:function(t){return{left:t.offsetLeft,top:t.offsetTop}},ready:function(t){o||"complete"===e.readyState?i.requestAnimationFrame(t):r.push(t)},getTextBounds:function(i){if(e.createRange){var n=e.createRange();if(n.selectNodeContents(i),n.getBoundingClientRect){var r=n.getBoundingClientRect();if(r){var o=t.scrollX,s=t.scrollY;return{top:r.top+s,left:r.left+o,right:r.left+o+r.width,bottom:r.top+s+r.height,width:r.width,height:r.height}}}}return null},getChildIndex:function(t,e){if(e)for(var i,n=t.parentNode.children,r=0,o=0,s=n.length;s>r;r++)if(i=n[r],i.nodeName&&i.nodeName.toLowerCase()==e){if(i==t)return o;o++}return Array.prototype.slice.call(t.parentNode.children).indexOf(t)},swapNodes:function(t,e){e.parentNode.insertBefore(t,e)},getParentWithClass:function(t,e,i){for(i=i||10;t.parentNode&&i--;){if(t.parentNode.classList&&t.parentNode.classList.contains(e))return t.parentNode;t=t.parentNode}return null},getParentOrSelfWithClass:function(t,e,i){for(i=i||10;t&&i--;){if(t.classList&&t.classList.contains(e))return t;t=t.parentNode}return null},rectContains:function(t,e,i,n,r,o){return i>t||t>r?!1:n>e||e>o?!1:!0}},i.requestAnimationFrame=i.DomUtil.requestAnimationFrame,i.animationFrameThrottle=i.DomUtil.animationFrameThrottle}(this,document,ionic),function(t){window.CustomEvent||!function(){var t;t=function(t,e){var i;e=e||{bubbles:!1,cancelable:!1,detail:void 0};try{i=document.createEvent("CustomEvent"),i.initCustomEvent(t,e.bubbles,e.cancelable,e.detail)}catch(n){i=document.createEvent("Event");for(var r in e)i[r]=e[r];i.initEvent(t,e.bubbles,e.cancelable)}return i},t.prototype=window.Event.prototype,window.CustomEvent=t}(),t.EventController={VIRTUALIZED_EVENTS:["tap","swipe","swiperight","swipeleft","drag","hold","release"],trigger:function(t,e,i,n){var r=new CustomEvent(t,{detail:e,bubbles:!!i,cancelable:!!n});e&&e.target&&e.target.dispatchEvent(r)||window.dispatchEvent(r)},on:function(e,i,n){for(var r=n||window,o=0,s=this.VIRTUALIZED_EVENTS.length;s>o;o++)if(e==this.VIRTUALIZED_EVENTS[o]){var a=new t.Gesture(n);return a.on(e,i),a}r.addEventListener(e,i)},off:function(t,e,i){i.removeEventListener(t,e)},onGesture:function(e,i,n){var r=new t.Gesture(n);return r.on(e,i),r},offGesture:function(t,e,i){t.off(e,i)},handlePopState:function(){}},t.on=function(){t.EventController.on.apply(t.EventController,arguments)},t.off=function(){t.EventController.off.apply(t.EventController,arguments)},t.trigger=t.EventController.trigger,t.onGesture=function(){return t.EventController.onGesture.apply(t.EventController.onGesture,arguments)},t.offGesture=function(){return t.EventController.offGesture.apply(t.EventController.offGesture,arguments)}}(window.ionic),function(t){function e(){if(!t.Gestures.READY){t.Gestures.event.determineEventTypes();for(var e in t.Gestures.gestures)t.Gestures.gestures.hasOwnProperty(e)&&t.Gestures.detection.register(t.Gestures.gestures[e]);t.Gestures.event.onTouch(t.Gestures.DOCUMENT,t.Gestures.EVENT_MOVE,t.Gestures.detection.detect),t.Gestures.event.onTouch(t.Gestures.DOCUMENT,t.Gestures.EVENT_END,t.Gestures.detection.detect),t.Gestures.READY=!0}}t.Gesture=function(e,i){return new t.Gestures.Instance(e,i||{})},t.Gestures={},t.Gestures.defaults={stop_browser_behavior:"disable-user-behavior"},t.Gestures.HAS_POINTEREVENTS=window.navigator.pointerEnabled||window.navigator.msPointerEnabled,t.Gestures.HAS_TOUCHEVENTS="ontouchstart"in window,t.Gestures.MOBILE_REGEX=/mobile|tablet|ip(ad|hone|od)|android|silk/i,t.Gestures.NO_MOUSEEVENTS=t.Gestures.HAS_TOUCHEVENTS&&window.navigator.userAgent.match(t.Gestures.MOBILE_REGEX),t.Gestures.EVENT_TYPES={},t.Gestures.DIRECTION_DOWN="down",t.Gestures.DIRECTION_LEFT="left",t.Gestures.DIRECTION_UP="up",t.Gestures.DIRECTION_RIGHT="right",t.Gestures.POINTER_MOUSE="mouse",t.Gestures.POINTER_TOUCH="touch",t.Gestures.POINTER_PEN="pen",t.Gestures.EVENT_START="start",t.Gestures.EVENT_MOVE="move",t.Gestures.EVENT_END="end",t.Gestures.DOCUMENT=window.document,t.Gestures.plugins={},t.Gestures.READY=!1,t.Gestures.Instance=function(i,n){var r=this;if(null!==i)return e(),this.element=i,this.enabled=!0,this.options=t.Gestures.utils.extend(t.Gestures.utils.extend({},t.Gestures.defaults),n||{}),this.options.stop_browser_behavior&&t.Gestures.utils.stopDefaultBrowserBehavior(this.element,this.options.stop_browser_behavior),t.Gestures.event.onTouch(i,t.Gestures.EVENT_START,function(e){r.enabled&&t.Gestures.detection.startDetect(r,e)}),this},t.Gestures.Instance.prototype={on:function(t,e){for(var i=t.split(" "),n=0;n<i.length;n++)this.element.addEventListener(i[n],e,!1);return this},off:function(t,e){for(var i=t.split(" "),n=0;n<i.length;n++)this.element.removeEventListener(i[n],e,!1);return this},trigger:function(e,i){var n=t.Gestures.DOCUMENT.createEvent("Event");n.initEvent(e,!0,!0),n.gesture=i;var r=this.element;return t.Gestures.utils.hasParent(i.target,r)&&(r=i.target),r.dispatchEvent(n),this},enable:function(t){return this.enabled=t,this}};var i=null,n=!1,r=!1;t.Gestures.event={bindDom:function(t,e,i){for(var n=e.split(" "),r=0;r<n.length;r++)t.addEventListener(n[r],i,!1)},onTouch:function(e,o,s){var a=this;this.bindDom(e,t.Gestures.EVENT_TYPES[o],function(l){var c=l.type.toLowerCase();if(!c.match(/mouse/)||!r){c.match(/touch/)||c.match(/pointerdown/)||c.match(/mouse/)&&1===l.which?n=!0:c.match(/mouse/)&&1!==l.which&&(n=!1),c.match(/touch|pointer/)&&(r=!0);var u=0;n&&(t.Gestures.HAS_POINTEREVENTS&&o!=t.Gestures.EVENT_END?u=t.Gestures.PointerEvent.updatePointer(o,l):c.match(/touch/)?u=l.touches.length:r||(u=c.match(/up/)?0:1),u>0&&o==t.Gestures.EVENT_END?o=t.Gestures.EVENT_MOVE:u||(o=t.Gestures.EVENT_END),(u||null===i)&&(i=l),s.call(t.Gestures.detection,a.collectEventData(e,o,a.getTouchList(i,o),l)),t.Gestures.HAS_POINTEREVENTS&&o==t.Gestures.EVENT_END&&(u=t.Gestures.PointerEvent.updatePointer(o,l))),u||(i=null,n=!1,r=!1,t.Gestures.PointerEvent.reset())}})},determineEventTypes:function(){var e;e=t.Gestures.HAS_POINTEREVENTS?t.Gestures.PointerEvent.getEvents():t.Gestures.NO_MOUSEEVENTS?["touchstart","touchmove","touchend touchcancel"]:["touchstart mousedown","touchmove mousemove","touchend touchcancel mouseup"],t.Gestures.EVENT_TYPES[t.Gestures.EVENT_START]=e[0],t.Gestures.EVENT_TYPES[t.Gestures.EVENT_MOVE]=e[1],t.Gestures.EVENT_TYPES[t.Gestures.EVENT_END]=e[2]},getTouchList:function(e){return t.Gestures.HAS_POINTEREVENTS?t.Gestures.PointerEvent.getTouchList():e.touches?e.touches:(e.indentifier=1,[e])},collectEventData:function(e,i,n,r){var o=t.Gestures.POINTER_TOUCH;return(r.type.match(/mouse/)||t.Gestures.PointerEvent.matchType(t.Gestures.POINTER_MOUSE,r))&&(o=t.Gestures.POINTER_MOUSE),{center:t.Gestures.utils.getCenter(n),timeStamp:(new Date).getTime(),target:r.target,touches:n,eventType:i,pointerType:o,srcEvent:r,preventDefault:function(){this.srcEvent.preventManipulation&&this.srcEvent.preventManipulation(),this.srcEvent.preventDefault},stopPropagation:function(){this.srcEvent.stopPropagation()},stopDetect:function(){return t.Gestures.detection.stopDetect()}}}},t.Gestures.PointerEvent={pointers:{},getTouchList:function(){var t=this,e=[];return Object.keys(t.pointers).sort().forEach(function(i){e.push(t.pointers[i])}),e},updatePointer:function(e,i){return e==t.Gestures.EVENT_END?this.pointers={}:(i.identifier=i.pointerId,this.pointers[i.pointerId]=i),Object.keys(this.pointers).length},matchType:function(e,i){if(!i.pointerType)return!1;var n={};return n[t.Gestures.POINTER_MOUSE]=i.pointerType==i.MSPOINTER_TYPE_MOUSE||i.pointerType==t.Gestures.POINTER_MOUSE,n[t.Gestures.POINTER_TOUCH]=i.pointerType==i.MSPOINTER_TYPE_TOUCH||i.pointerType==t.Gestures.POINTER_TOUCH,n[t.Gestures.POINTER_PEN]=i.pointerType==i.MSPOINTER_TYPE_PEN||i.pointerType==t.Gestures.POINTER_PEN,n[e]},getEvents:function(){return["pointerdown MSPointerDown","pointermove MSPointerMove","pointerup pointercancel MSPointerUp MSPointerCancel"]},reset:function(){this.pointers={}}},t.Gestures.utils={extend:function(t,e,i){for(var n in e)void 0!==t[n]&&i||(t[n]=e[n]);return t},hasParent:function(t,e){for(;t;){if(t==e)return!0;t=t.parentNode}return!1},getCenter:function(t){for(var e=[],i=[],n=0,r=t.length;r>n;n++)e.push(t[n].pageX),i.push(t[n].pageY);return{pageX:(Math.min.apply(Math,e)+Math.max.apply(Math,e))/2,pageY:(Math.min.apply(Math,i)+Math.max.apply(Math,i))/2}},getVelocity:function(t,e,i){return{x:Math.abs(e/t)||0,y:Math.abs(i/t)||0}},getAngle:function(t,e){var i=e.pageY-t.pageY,n=e.pageX-t.pageX;return 180*Math.atan2(i,n)/Math.PI},getDirection:function(e,i){var n=Math.abs(e.pageX-i.pageX),r=Math.abs(e.pageY-i.pageY);return n>=r?e.pageX-i.pageX>0?t.Gestures.DIRECTION_LEFT:t.Gestures.DIRECTION_RIGHT:e.pageY-i.pageY>0?t.Gestures.DIRECTION_UP:t.Gestures.DIRECTION_DOWN},getDistance:function(t,e){var i=e.pageX-t.pageX,n=e.pageY-t.pageY;return Math.sqrt(i*i+n*n)},getScale:function(t,e){return t.length>=2&&e.length>=2?this.getDistance(e[0],e[1])/this.getDistance(t[0],t[1]):1},getRotation:function(t,e){return t.length>=2&&e.length>=2?this.getAngle(e[1],e[0])-this.getAngle(t[1],t[0]):0},isVertical:function(e){return e==t.Gestures.DIRECTION_UP||e==t.Gestures.DIRECTION_DOWN},stopDefaultBrowserBehavior:function(t,e){t&&t.classList&&(t.classList.add(e),t.onselectstart=function(){return!1})}},t.Gestures.detection={gestures:[],current:null,previous:null,stopped:!1,startDetect:function(e,i){this.current||(this.stopped=!1,this.current={inst:e,startEvent:t.Gestures.utils.extend({},i),lastEvent:!1,name:""},this.detect(i))},detect:function(e){if(this.current&&!this.stopped){e=this.extendEventData(e);for(var i=this.current.inst.options,n=0,r=this.gestures.length;r>n;n++){var o=this.gestures[n];if(!this.stopped&&i[o.name]!==!1&&o.handler.call(o,e,this.current.inst)===!1){this.stopDetect();break}}return this.current&&(this.current.lastEvent=e),e.eventType==t.Gestures.EVENT_END&&!e.touches.length-1&&this.stopDetect(),e}},stopDetect:function(){this.previous=t.Gestures.utils.extend({},this.current),this.current=null,this.stopped=!0},extendEventData:function(e){var i=this.current.startEvent;if(i&&(e.touches.length!=i.touches.length||e.touches===i.touches)){i.touches=[];for(var n=0,r=e.touches.length;r>n;n++)i.touches.push(t.Gestures.utils.extend({},e.touches[n]))}var o=e.timeStamp-i.timeStamp,s=e.center.pageX-i.center.pageX,a=e.center.pageY-i.center.pageY,l=t.Gestures.utils.getVelocity(o,s,a);return t.Gestures.utils.extend(e,{deltaTime:o,deltaX:s,deltaY:a,velocityX:l.x,velocityY:l.y,distance:t.Gestures.utils.getDistance(i.center,e.center),angle:t.Gestures.utils.getAngle(i.center,e.center),direction:t.Gestures.utils.getDirection(i.center,e.center),scale:t.Gestures.utils.getScale(i.touches,e.touches),rotation:t.Gestures.utils.getRotation(i.touches,e.touches),startEvent:i}),e},register:function(e){var i=e.defaults||{};return void 0===i[e.name]&&(i[e.name]=!0),t.Gestures.utils.extend(t.Gestures.defaults,i,!0),e.index=e.index||1e3,this.gestures.push(e),this.gestures.sort(function(t,e){return t.index<e.index?-1:t.index>e.index?1:0}),this.gestures}},t.Gestures.gestures=t.Gestures.gestures||{},t.Gestures.gestures.Hold={name:"hold",index:10,defaults:{hold_timeout:500,hold_threshold:1},timer:null,handler:function(e,i){switch(e.eventType){case t.Gestures.EVENT_START:clearTimeout(this.timer),t.Gestures.detection.current.name=this.name,this.timer=setTimeout(function(){"hold"==t.Gestures.detection.current.name&&i.trigger("hold",e)},i.options.hold_timeout);break;case t.Gestures.EVENT_MOVE:e.distance>i.options.hold_threshold&&clearTimeout(this.timer);break;case t.Gestures.EVENT_END:clearTimeout(this.timer)}}},t.Gestures.gestures.Tap={name:"tap",index:100,defaults:{tap_max_touchtime:250,tap_max_distance:10,tap_always:!0,doubletap_distance:20,doubletap_interval:300},handler:function(e,i){if(e.eventType==t.Gestures.EVENT_END){var n=t.Gestures.detection.previous,r=!1;if(e.deltaTime>i.options.tap_max_touchtime||e.distance>i.options.tap_max_distance)return;n&&"tap"==n.name&&e.timeStamp-n.lastEvent.timeStamp<i.options.doubletap_interval&&e.distance<i.options.doubletap_distance&&(i.trigger("doubletap",e),r=!0),(!r||i.options.tap_always)&&(t.Gestures.detection.current.name="tap",i.trigger(t.Gestures.detection.current.name,e))}}},t.Gestures.gestures.Swipe={name:"swipe",index:40,defaults:{swipe_max_touches:1,swipe_velocity:.7},handler:function(e,i){if(e.eventType==t.Gestures.EVENT_END){if(i.options.swipe_max_touches>0&&e.touches.length>i.options.swipe_max_touches)return;(e.velocityX>i.options.swipe_velocity||e.velocityY>i.options.swipe_velocity)&&(i.trigger(this.name,e),i.trigger(this.name+e.direction,e))}}},t.Gestures.gestures.Drag={name:"drag",index:50,defaults:{drag_min_distance:10,correct_for_drag_min_distance:!0,drag_max_touches:1,drag_block_horizontal:!0,drag_block_vertical:!0,drag_lock_to_axis:!1,drag_lock_min_distance:25},triggered:!1,handler:function(e,i){if(t.Gestures.detection.current.name!=this.name&&this.triggered)return i.trigger(this.name+"end",e),void(this.triggered=!1);if(!(i.options.drag_max_touches>0&&e.touches.length>i.options.drag_max_touches))switch(e.eventType){case t.Gestures.EVENT_START:this.triggered=!1;break;case t.Gestures.EVENT_MOVE:if(e.distance<i.options.drag_min_distance&&t.Gestures.detection.current.name!=this.name)return;if(t.Gestures.detection.current.name!=this.name&&(t.Gestures.detection.current.name=this.name,i.options.correct_for_drag_min_distance)){var n=Math.abs(i.options.drag_min_distance/e.distance);t.Gestures.detection.current.startEvent.center.pageX+=e.deltaX*n,t.Gestures.detection.current.startEvent.center.pageY+=e.deltaY*n,e=t.Gestures.detection.extendEventData(e)}(t.Gestures.detection.current.lastEvent.drag_locked_to_axis||i.options.drag_lock_to_axis&&i.options.drag_lock_min_distance<=e.distance)&&(e.drag_locked_to_axis=!0);var r=t.Gestures.detection.current.lastEvent.direction;e.drag_locked_to_axis&&r!==e.direction&&(e.direction=t.Gestures.utils.isVertical(r)?e.deltaY<0?t.Gestures.DIRECTION_UP:t.Gestures.DIRECTION_DOWN:e.deltaX<0?t.Gestures.DIRECTION_LEFT:t.Gestures.DIRECTION_RIGHT),this.triggered||(i.trigger(this.name+"start",e),this.triggered=!0),i.trigger(this.name,e),i.trigger(this.name+e.direction,e),(i.options.drag_block_vertical&&t.Gestures.utils.isVertical(e.direction)||i.options.drag_block_horizontal&&!t.Gestures.utils.isVertical(e.direction))&&e.preventDefault();break;case t.Gestures.EVENT_END:this.triggered&&i.trigger(this.name+"end",e),this.triggered=!1}}},t.Gestures.gestures.Transform={name:"transform",index:45,defaults:{transform_min_scale:.01,transform_min_rotation:1,transform_always_block:!1},triggered:!1,handler:function(e,i){if(t.Gestures.detection.current.name!=this.name&&this.triggered)return i.trigger(this.name+"end",e),void(this.triggered=!1);if(!(e.touches.length<2))switch(i.options.transform_always_block&&e.preventDefault(),e.eventType){case t.Gestures.EVENT_START:this.triggered=!1;break;case t.Gestures.EVENT_MOVE:var n=Math.abs(1-e.scale),r=Math.abs(e.rotation);if(n<i.options.transform_min_scale&&r<i.options.transform_min_rotation)return;t.Gestures.detection.current.name=this.name,this.triggered||(i.trigger(this.name+"start",e),this.triggered=!0),i.trigger(this.name,e),r>i.options.transform_min_rotation&&i.trigger("rotate",e),n>i.options.transform_min_scale&&(i.trigger("pinch",e),i.trigger("pinch"+(e.scale<1?"in":"out"),e));break;case t.Gestures.EVENT_END:this.triggered&&i.trigger(this.name+"end",e),this.triggered=!1}}},t.Gestures.gestures.Touch={name:"touch",index:-1/0,defaults:{prevent_default:!1,prevent_mouseevents:!1},handler:function(e,i){return i.options.prevent_mouseevents&&e.pointerType==t.Gestures.POINTER_MOUSE?void e.stopDetect():(i.options.prevent_default&&e.preventDefault(),void(e.eventType==t.Gestures.EVENT_START&&i.trigger(this.name,e)))}},t.Gestures.gestures.Release={name:"release",index:1/0,handler:function(e,i){e.eventType==t.Gestures.EVENT_END&&i.trigger(this.name,e)}}}(window.ionic),function(t,e,i){function n(){i.Platform.isCordova()?e.addEventListener("deviceready",r,!1):r(),t.removeEventListener("load",n,!1)}function r(){i.Platform.isReady=!0,i.Platform.detect();for(var t=0;t<a.length;t++)a[t]();a=[],i.trigger("platformready",{target:e}),i.requestAnimationFrame(function(){e.body.classList.add("platform-ready")})}i.Platform={isReady:!1,isFullScreen:!1,platforms:null,grade:null,ua:navigator.userAgent,ready:function(t){this.isReady?t():a.push(t)},detect:function(){i.Platform._checkPlatforms(),i.requestAnimationFrame(function(){for(var t=0;t<i.Platform.platforms.length;t++)e.body.classList.add("platform-"+i.Platform.platforms[t]);e.body.classList.add("grade-"+i.Platform.grade)})},device:function(){return t.device?t.device:(this.isCordova(),{})},_checkPlatforms:function(){this.platforms=[],this.grade="a",this.isCordova()&&this.platforms.push("cordova"),this.isIPad()&&this.platforms.push("ipad");var t=this.platform();if(t){this.platforms.push(t);var e=this.version();if(e){var i=e.toString();i.indexOf(".")>0?i=i.replace(".","_"):i+="_0",this.platforms.push(t+i.split("_")[0]),this.platforms.push(t+i),this.isAndroid()&&4.4>e&&(this.grade=4>e?"c":"b")}}},isCordova:function(){return!(!t.cordova&&!t.PhoneGap&&!t.phonegap)},isIPad:function(){return this.ua.toLowerCase().indexOf("ipad")>=0},isIOS:function(){return this.is("ios")},isAndroid:function(){return this.is("android")},platform:function(){return null===o&&this.setPlatform(this.device().platform),o},setPlatform:function(t){o="undefined"!=typeof t&&null!==t&&t.length?t.toLowerCase():this.ua.indexOf("Android")>0?"android":this.ua.indexOf("iPhone")>-1||this.ua.indexOf("iPad")>-1||this.ua.indexOf("iPod")>-1?"ios":""},version:function(){return null===s&&this.setVersion(this.device().version),s},setVersion:function(t){if("undefined"!=typeof t&&null!==t&&(t=t.split("."),t=parseFloat(t[0]+"."+(t.length>1?t[1]:0)),!isNaN(t)))return void(s=t);s=0;var e=this.platform(),i={android:/Android (\d+).(\d+)?/,ios:/OS (\d+)_(\d+)?/};i[e]&&(t=this.ua.match(i[e]),t.length>2&&(s=parseFloat(t[1]+"."+t[2])))},is:function(t){if(t=t.toLowerCase(),this.platforms)for(var e=0;e<this.platforms.length;e++)if(this.platforms[e]===t)return!0;var i=this.platform();return i?i===t.toLowerCase():this.ua.toLowerCase().indexOf(t)>=0},exitApp:function(){this.ready(function(){navigator.app&&navigator.app.exitApp&&navigator.app.exitApp()})},showStatusBar:function(n){this._showStatusBar=n,this.ready(function(){i.requestAnimationFrame(function(){i.Platform._showStatusBar?(t.StatusBar&&t.StatusBar.show(),e.body.classList.remove("status-bar-hide")):(t.StatusBar&&t.StatusBar.hide(),e.body.classList.add("status-bar-hide"))})})},fullScreen:function(t,n){this.isFullScreen=t!==!1,i.DomUtil.ready(function(){i.requestAnimationFrame(function(){i.Platform.isFullScreen?e.body.classList.add("fullscreen"):e.body.classList.remove("fullscreen")}),i.Platform.showStatusBar(n===!0)})}};var o=null,s=null,a=[];t.addEventListener("load",n,!1)}(this,document,ionic),function(t,e){"use strict";e.CSS={},function(){var i,n=["webkitTransform","transform","-webkit-transform","webkit-transform","-moz-transform","moz-transform","MozTransform","mozTransform"];for(i=0;i<n.length;i++)if(void 0!==t.documentElement.style[n[i]]){e.CSS.TRANSFORM=n[i];break}for(n=["webkitTransition","mozTransition","transition"],i=0;i<n.length;i++)if(void 0!==t.documentElement.style[n[i]]){e.CSS.TRANSITION=n[i];break}}(),"classList"in t.documentElement||!Object.defineProperty||"undefined"==typeof HTMLElement||Object.defineProperty(HTMLElement.prototype,"classList",{get:function(){function t(t){return function(){var i,n=e.className.split(/\s+/);for(i=0;i<arguments.length;i++)t(n,n.indexOf(arguments[i]),arguments[i]);e.className=n.join(" ")}}var e=this;return{add:t(function(t,e,i){~e||t.push(i)}),remove:t(function(t,e){~e&&t.splice(e,1)}),toggle:t(function(t,e,i){~e?t.splice(e,1):t.push(i)}),contains:function(t){return!!~e.className.split(/\s+/).indexOf(t)},item:function(t){return e.className.split(/\s+/)[t]||null}}}})}(document,ionic),function(t,e,i){"use strict";function n(t){if(t.gesture&&t.gesture.srcEvent){var e=t.gesture.srcEvent,n=e.target;if(o(e))return u(e);for(var r=0;5>r&&n;r++){if("INPUT"===n.tagName||"A"===n.tagName||"BUTTON"===n.tagName||"LABEL"===n.tagName||"TEXTAREA"===n.tagName)return i.tapElement(n,e);n=n.parentElement}h()}}function r(t){return t.target.control||o(t)||s(t)?u(t):void a(t)}function o(t){var e,i,n;for(e in f)if(i=f[e],n||(n=l(t)),n.x>i.x-v&&n.x<i.x+v&&n.y>i.y-v&&n.y<i.y+v)return i}function s(t){var e=l(t);return 0===e.x&&0===e.y?!1:e.x>g.x+2||e.x<g.x-2||e.y>g.y+2||e.y<g.y-2}function a(t){var e=l(t);if(e.x&&e.y){var i=Date.now();f[i]={x:e.x,y:e.y,id:i},setTimeout(function(){delete f[i]},p)}}function l(t){var e=t.gesture?t.gesture:t;if(e){var i=e.touches&&e.touches.length?e.touches:[e],n=e.changedTouches&&e.changedTouches[0]||e.originalEvent&&e.originalEvent.changedTouches&&e.originalEvent.changedTouches[0]||i[0].originalEvent||i[0];if(n)return{x:n.clientX||n.pageX,y:n.clientY||n.pageY}}return{x:0,y:0}}function c(t){clearTimeout(_),_=setTimeout(function(){var e=o(t);e&&delete f[e.id],g={}},m)}function u(t){return t.stopPropagation(),t.preventDefault(),!1}function h(){var t=e.activeElement;!t||"INPUT"!==t.tagName&&"TEXTAREA"!==t.tagName||setTimeout(function(){t.blur()},400)}function d(t){g=l(t)}i.tapElement=function(i,n){var r=i.control||i;if(!r.disabled&&"file"!==r.type&&"range"!==r.type){var s=l(n),c=e.createEvent("MouseEvents");return c.initMouseEvent("click",!0,!0,t,1,0,0,s.x,s.y,!1,!1,!1,!1,0,null),r.dispatchEvent(c),"INPUT"===r.tagName||"TEXTAREA"===r.tagName?(r.focus(),n.preventDefault()):h(),o(n)||a(n),i.control?u(n):void 0}};var _,f={},g={},p=1500,m=380,v=15;i.Platform.ready(function(){"c"===i.Platform.grade&&(m=800),e.addEventListener("click",r,!0),i.on("release",n,e),e.addEventListener("touchend",c,!1),e.addEventListener("mouseup",c,!1),e.addEventListener("touchstart",d,!1),e.addEventListener("mousedown",d,!1)})}(this,document,ionic),function(t,e){"use strict";function i(){for(var t in s)s[t]&&(s[t].classList.add("active"),a[t]=s[t]);s={}}function n(){for(var t in a)a[t]&&(a[t].classList.remove("active"),delete a[t])}function r(){setTimeout(o,200)}function o(){s={},e.requestAnimationFrame(n),t.body.removeEventListener("mousemove",o),t.body.removeEventListener("touchmove",o)}var s={},a={},l=0;e.activator={start:function(n){e.requestAnimationFrame(function(){for(var r,a=n.target,c=0;4>c&&a;c++){if(r&&a.classList.contains("item")){r=a;break}if(("A"==a.tagName||"BUTTON"==a.tagName||a.getAttribute("ng-click"))&&(r=a),a.classList.contains("button")){r=a;break}a=a.parentElement}r&&(s[l]=r,"touchstart"===n.type?(t.body.removeEventListener("mousedown",e.activator.start),t.body.addEventListener("touchmove",o,!1),setTimeout(i,85)):(t.body.addEventListener("mousemove",o,!1),e.requestAnimationFrame(i)),l=l>19?0:l+1)})}},window.addEventListener("load",function(){t.body.addEventListener("touchstart",e.activator.start,!1),t.body.addEventListener("mousedown",e.activator.start,!1),t.body.addEventListener("touchend",r,!1),t.body.addEventListener("mouseup",r,!1),t.body.addEventListener("touchcancel",r,!1)},!1)}(document,ionic),function(t){var e=["0","0","0"];t.Utils={arrayMove:function(t,e,i){if(i>=t.length)for(var n=i-t.length;n--+1;)t.push(void 0);return t.splice(i,0,t.splice(e,1)[0]),t},proxy:function(t,e){var i=Array.prototype.slice.call(arguments,2);return function(){return t.apply(e,i.concat(Array.prototype.slice.call(arguments)))}},debounce:function(t,e,i){var n,r,o,s,a;return function(){o=this,r=arguments,s=new Date;var l=function(){var c=new Date-s;e>c?n=setTimeout(l,e-c):(n=null,i||(a=t.apply(o,r)))},c=i&&!n;return n||(n=setTimeout(l,e)),c&&(a=t.apply(o,r)),a}},throttle:function(t,e,i){var n,r,o,s=null,a=0;i||(i={});var l=function(){a=i.leading===!1?0:Date.now(),s=null,o=t.apply(n,r)};return function(){var c=Date.now();a||i.leading!==!1||(a=c);var u=e-(c-a);return n=this,r=arguments,0>=u?(clearTimeout(s),s=null,a=c,o=t.apply(n,r)):s||i.trailing===!1||(s=setTimeout(l,u)),o}},inherit:function(e,i){var n,r=this;n=e&&e.hasOwnProperty("constructor")?e.constructor:function(){return r.apply(this,arguments)},t.extend(n,r,i);var o=function(){this.constructor=n};return o.prototype=r.prototype,n.prototype=new o,e&&t.extend(n.prototype,e),n.__super__=r.prototype,n},extend:function(t){for(var e=Array.prototype.slice.call(arguments,1),i=0;i<e.length;i++){var n=e[i];if(n)for(var r in n)t[r]=n[r]}return t},nextUid:function(){for(var t,i=e.length;i;){if(i--,t=e[i].charCodeAt(0),57==t)return e[i]="A",e.join("");if(90!=t)return e[i]=String.fromCharCode(t+1),e.join("");e[i]="0"}return e.unshift("0"),e.join("")}},t.inherit=t.Utils.inherit,t.extend=t.Utils.extend,t.throttle=t.Utils.throttle,t.proxy=t.Utils.proxy,t.debounce=t.Utils.debounce}(window.ionic),function(t){function e(){function e(){function e(){i=r-window.innerHeight;var e=document.activeElement;e&&t.trigger("scrollChildIntoView",{target:e},!0)}n!==window.innerWidth?(n=window.innerWidth,r=window.innerHeight):r!==window.innerHeight&&window.innerHeight<r?(document.body.classList.add("footer-hide"),t.requestAnimationFrame(e)):document.body.classList.remove("footer-hide")}var i,n=window.innerWidth,r=window.innerHeight;window.addEventListener("resize",e)}t.Platform.ready(function(){t.Platform.is("android")&&e()})}(window.ionic),function(t){"use strict";t.views.View=function(){this.initialize.apply(this,arguments)},t.views.View.inherit=t.inherit,t.extend(t.views.View.prototype,{initialize:function(){}})}(window.ionic);var t=/input|textarea|select/i,e=/object|embed/i;!function(t){var e=Date.now||function(){return+new Date},i=60,n=1e3,r={},o=1;if(t.core)s.effect||(s.effect={});else var s=t.core={effect:{}};s.effect.Animate={requestAnimationFrame:function(){var e=t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||t.oRequestAnimationFrame,i=!!e;if(e&&!/requestAnimationFrame\(\)\s*\{\s*\[native code\]\s*\}/i.test(e.toString())&&(i=!1),i)return function(t,i){e(t,i)};var n=60,r={},o=0,s=1,a=null,l=+new Date;return function(t){var e=s++;return r[e]=t,o++,null===a&&(a=setInterval(function(){var t=+new Date,e=r;r={},o=0;for(var i in e)e.hasOwnProperty(i)&&(e[i](t),l=t);t-l>2500&&(clearInterval(a),a=null)},1e3/n)),e}}(),stop:function(t){var e=null!=r[t];return e&&(r[t]=null),e},isRunning:function(t){return null!=r[t]},start:function(t,a,l,c,u,h){var d=e(),_=d,f=0,g=0,p=o++;if(h||(h=document.body),p%20===0){var m={};for(var v in r)m[v]=!0;r=m}var T=function(o){var m=o!==!0,v=e();if(!r[p]||a&&!a(p))return r[p]=null,void(l&&l(i-g/((v-d)/n),p,!1));if(m)for(var E=Math.round((v-_)/(n/i))-1,w=0;w<Math.min(E,4);w++)T(!0),g++;c&&(f=(v-d)/c,f>1&&(f=1));var y=u?u(f):f;t(y,v,m)!==!1&&1!==f||!m?m&&(_=v,s.effect.Animate.requestAnimationFrame(T,h)):(r[p]=null,l&&l(i-g/((v-d)/n),p,1===f||null==c))};return r[p]=!0,s.effect.Animate.requestAnimationFrame(T,h),p}}}(this);!function(i){var n=function(){},r=function(t){return Math.pow(t-1,3)+1},o=function(t){return(t/=.5)<1?.5*Math.pow(t,3):.5*(Math.pow(t-2,3)+2)};i.views.Scroll=i.views.View.inherit({initialize:function(t){var e=this;this.__container=t.el,this.__content=t.el.firstElementChild,setTimeout(function(){e.__container&&e.__content&&(e.__container.scrollTop=0,e.__content.scrollTop=0)}),this.options={scrollingX:!1,scrollbarX:!0,scrollingY:!0,scrollbarY:!0,startX:0,startY:0,wheelDampen:6,minScrollbarSizeX:5,minScrollbarSizeY:5,scrollbarsFade:!0,scrollbarFadeDelay:300,scrollbarResizeFadeDelay:1e3,animating:!0,animationDuration:250,bouncing:!0,locking:!0,paging:!1,snapping:!1,zooming:!1,minZoom:.5,maxZoom:3,speedMultiplier:1,scrollingComplete:n,penetrationDeceleration:.03,penetrationAcceleration:.08,scrollEventInterval:50};for(var r in t)this.options[r]=t[r];this.hintResize=i.debounce(function(){e.resize()},1e3,!0),this.triggerScrollEvent=i.throttle(function(){i.trigger("scroll",{scrollTop:e.__scrollTop,scrollLeft:e.__scrollLeft,target:e.__container})},this.options.scrollEventInterval),this.triggerScrollEndEvent=function(){i.trigger("scrollend",{scrollTop:e.__scrollTop,scrollLeft:e.__scrollLeft,target:e.__container})},this.__scrollLeft=this.options.startX,this.__scrollTop=this.options.startY,this.__callback=this.getRenderFn(),this.__initEventHandlers(),this.__createScrollbars()},run:function(){this.resize(),this.__fadeScrollbars("out",this.options.scrollbarResizeFadeDelay)},__isSingleTouch:!1,__isTracking:!1,__didDecelerationComplete:!1,__isGesturing:!1,__isDragging:!1,__isDecelerating:!1,__isAnimating:!1,__clientLeft:0,__clientTop:0,__clientWidth:0,__clientHeight:0,__contentWidth:0,__contentHeight:0,__snapWidth:100,__snapHeight:100,__refreshHeight:null,__refreshActive:!1,__refreshActivate:null,__refreshDeactivate:null,__refreshStart:null,__zoomLevel:1,__scrollLeft:0,__scrollTop:0,__maxScrollLeft:0,__maxScrollTop:0,__scheduledLeft:0,__scheduledTop:0,__scheduledZoom:0,__lastTouchLeft:null,__lastTouchTop:null,__lastTouchMove:null,__positions:null,__minDecelerationScrollLeft:null,__minDecelerationScrollTop:null,__maxDecelerationScrollLeft:null,__maxDecelerationScrollTop:null,__decelerationVelocityX:null,__decelerationVelocityY:null,__transformProperty:null,__perspectiveProperty:null,__indicatorX:null,__indicatorY:null,__scrollbarFadeTimeout:null,__didWaitForSize:null,__sizerTimeout:null,__initEventHandlers:function(){function n(i){return i.target.tagName.match(t)||i.target.isContentEditable||i.target.tagName.match(e)||i.target.dataset.preventScroll}var r=this,o=this.__container;if(o.addEventListener("scrollChildIntoView",function(t){var e=window.innerHeight,n=t.target,s=t.target.offsetHeight,a=n.getBoundingClientRect().top,l=i.DomUtil.getPositionInParent(n,o).top;a+s>e&&r.scrollTo(0,l+s-.5*e,!0),t.stopPropagation()
}),"ontouchstart"in window)o.addEventListener("touchstart",function(t){t.defaultPrevented||n(t)||(r.doTouchStart(t.touches,t.timeStamp),t.preventDefault())},!1),document.addEventListener("touchmove",function(t){t.defaultPrevented||r.doTouchMove(t.touches,t.timeStamp)},!1),document.addEventListener("touchend",function(t){r.doTouchEnd(t.timeStamp)},!1);else{var s=!1;o.addEventListener("mousedown",function(t){t.defaultPrevented||n(t)||(r.doTouchStart([{pageX:t.pageX,pageY:t.pageY}],t.timeStamp),t.preventDefault(),s=!0)},!1),document.addEventListener("mousemove",function(t){s&&!t.defaultPrevented&&(r.doTouchMove([{pageX:t.pageX,pageY:t.pageY}],t.timeStamp),s=!0)},!1),document.addEventListener("mouseup",function(t){s&&(r.doTouchEnd(t.timeStamp),s=!1)},!1);var a=i.debounce(function(){r.__fadeScrollbars("in")},500,!0),l=i.debounce(function(){r.__fadeScrollbars("out")},100,!1);document.addEventListener("mousewheel",function(t){a(),r.scrollBy(t.wheelDeltaX/r.options.wheelDampen,-t.wheelDeltaY/r.options.wheelDampen),l()})}},__createScrollbar:function(t){var e=document.createElement("div"),i=document.createElement("div");return i.className="scroll-bar-indicator",e.className="h"==t?"scroll-bar scroll-bar-h":"scroll-bar scroll-bar-v",e.appendChild(i),e},__createScrollbars:function(){var t,e;this.options.scrollingX&&(t={el:this.__createScrollbar("h"),sizeRatio:1},t.indicator=t.el.children[0],this.options.scrollbarX&&this.__container.appendChild(t.el),this.__indicatorX=t),this.options.scrollingY&&(e={el:this.__createScrollbar("v"),sizeRatio:1},e.indicator=e.el.children[0],this.options.scrollbarY&&this.__container.appendChild(e.el),this.__indicatorY=e)},__resizeScrollbars:function(){var t=this;if(t.__indicatorX){var e=Math.max(Math.round(t.__clientWidth*t.__clientWidth/t.__contentWidth),20);e>t.__contentWidth&&(e=0),t.__indicatorX.size=e,t.__indicatorX.minScale=this.options.minScrollbarSizeX/e,t.__indicatorX.indicator.style.width=e+"px",t.__indicatorX.maxPos=t.__clientWidth-e,t.__indicatorX.sizeRatio=t.__maxScrollLeft?t.__indicatorX.maxPos/t.__maxScrollLeft:1}if(t.__indicatorY){var i=Math.max(Math.round(t.__clientHeight*t.__clientHeight/t.__contentHeight),20);i>t.__contentHeight&&(i=0),t.__indicatorY.size=i,t.__indicatorY.minScale=this.options.minScrollbarSizeY/i,t.__indicatorY.maxPos=t.__clientHeight-i,t.__indicatorY.indicator.style.height=i+"px",t.__indicatorY.sizeRatio=t.__maxScrollTop?t.__indicatorY.maxPos/t.__maxScrollTop:1}},__repositionScrollbars:function(){var t,e,i,n,r,o=this,s=0,a=0;o.__indicatorX&&(o.__indicatorY&&(s=10),n=Math.round(o.__indicatorX.sizeRatio*o.__scrollLeft)||0,e=o.__scrollLeft-(o.__maxScrollLeft-s),o.__scrollLeft<0?(widthScale=Math.max(o.__indicatorX.minScale,(o.__indicatorX.size-Math.abs(o.__scrollLeft))/o.__indicatorX.size),n=0,o.__indicatorX.indicator.style[o.__transformOriginProperty]="left center"):e>0?(widthScale=Math.max(o.__indicatorX.minScale,(o.__indicatorX.size-e)/o.__indicatorX.size),n=o.__indicatorX.maxPos-s,o.__indicatorX.indicator.style[o.__transformOriginProperty]="right center"):(n=Math.min(o.__maxScrollLeft,Math.max(0,n)),widthScale=1),o.__indicatorX.indicator.style[o.__transformProperty]="translate3d("+n+"px, 0, 0) scaleX("+widthScale+")"),o.__indicatorY&&(r=Math.round(o.__indicatorY.sizeRatio*o.__scrollTop)||0,o.__indicatorX&&(a=10),i=o.__scrollTop-(o.__maxScrollTop-a),o.__scrollTop<0?(t=Math.max(o.__indicatorY.minScale,(o.__indicatorY.size-Math.abs(o.__scrollTop))/o.__indicatorY.size),r=0,o.__indicatorY.indicator.style[o.__transformOriginProperty]="center top"):i>0?(t=Math.max(o.__indicatorY.minScale,(o.__indicatorY.size-i)/o.__indicatorY.size),r=o.__indicatorY.maxPos-a,o.__indicatorY.indicator.style[o.__transformOriginProperty]="center bottom"):(r=Math.min(o.__maxScrollTop,Math.max(0,r)),t=1),o.__indicatorY.indicator.style[o.__transformProperty]="translate3d(0,"+r+"px, 0) scaleY("+t+")")},__fadeScrollbars:function(t,e){var i=this;if(this.options.scrollbarsFade){var n="scroll-bar-fade-out";i.options.scrollbarsFade===!0&&(clearTimeout(i.__scrollbarFadeTimeout),"in"==t?(i.__indicatorX&&i.__indicatorX.indicator.classList.remove(n),i.__indicatorY&&i.__indicatorY.indicator.classList.remove(n)):i.__scrollbarFadeTimeout=setTimeout(function(){i.__indicatorX&&i.__indicatorX.indicator.classList.add(n),i.__indicatorY&&i.__indicatorY.indicator.classList.add(n)},e||i.options.scrollbarFadeDelay))}},__scrollingComplete:function(){var t=this;t.options.scrollingComplete(),t.__fadeScrollbars("out")},resize:function(){this.setDimensions(this.__container.clientWidth,this.__container.clientHeight,Math.max(this.__content.scrollWidth,this.__content.offsetWidth),Math.max(this.__content.scrollHeight,this.__content.offsetHeight))},getRenderFn:function(){var t,e=this,i=this.__content,n=document.documentElement.style;"MozAppearance"in n?t="gecko":"WebkitAppearance"in n?t="webkit":"string"==typeof navigator.cpuClass&&(t="trident");var r,o={trident:"ms",gecko:"Moz",webkit:"Webkit",presto:"O"}[t],s=document.createElement("div"),a=o+"Perspective",l=o+"Transform",c=o+"TransformOrigin";return e.__perspectiveProperty=l,e.__transformProperty=l,e.__transformOriginProperty=c,s.style[a]!==r?function(t,n){i.style[l]="translate3d("+-t+"px,"+-n+"px,0)",e.__repositionScrollbars(),e.triggerScrollEvent()}:s.style[l]!==r?function(t,n){i.style[l]="translate("+-t+"px,"+-n+"px)",e.__repositionScrollbars(),e.triggerScrollEvent()}:function(t,n,r){i.style.marginLeft=t?-t/r+"px":"",i.style.marginTop=n?-n/r+"px":"",i.style.zoom=r||"",e.__repositionScrollbars(),e.triggerScrollEvent()}},setDimensions:function(t,e,i,n){var r=this;t===+t&&(r.__clientWidth=t),e===+e&&(r.__clientHeight=e),i===+i&&(r.__contentWidth=i),n===+n&&(r.__contentHeight=n),r.__computeScrollMax(),r.__resizeScrollbars(),r.scrollTo(r.__scrollLeft,r.__scrollTop,!0)},setPosition:function(t,e){var i=this;i.__clientLeft=t||0,i.__clientTop=e||0},setSnapSize:function(t,e){var i=this;i.__snapWidth=t,i.__snapHeight=e},activatePullToRefresh:function(t,e,i,n){var r=this;r.__refreshHeight=t,r.__refreshActivate=e,r.__refreshDeactivate=i,r.__refreshStart=n},triggerPullToRefresh:function(){this.__publish(this.__scrollLeft,-this.__refreshHeight,this.__zoomLevel,!0),this.__refreshStart&&this.__refreshStart()},finishPullToRefresh:function(){var t=this;t.__refreshActive=!1,t.__refreshDeactivate&&t.__refreshDeactivate(),t.scrollTo(t.__scrollLeft,t.__scrollTop,!0)},getValues:function(){var t=this;return{left:t.__scrollLeft,top:t.__scrollTop,zoom:t.__zoomLevel}},getScrollMax:function(){var t=this;return{left:t.__maxScrollLeft,top:t.__maxScrollTop}},zoomTo:function(t,e,i,n){var r=this;if(!r.options.zooming)throw new Error("Zooming is not enabled!");r.__isDecelerating&&(core.effect.Animate.stop(r.__isDecelerating),r.__isDecelerating=!1);var o=r.__zoomLevel;null==i&&(i=r.__clientWidth/2),null==n&&(n=r.__clientHeight/2),t=Math.max(Math.min(t,r.options.maxZoom),r.options.minZoom),r.__computeScrollMax(t);var s=(i+r.__scrollLeft)*t/o-i,a=(n+r.__scrollTop)*t/o-n;s>r.__maxScrollLeft?s=r.__maxScrollLeft:0>s&&(s=0),a>r.__maxScrollTop?a=r.__maxScrollTop:0>a&&(a=0),r.__publish(s,a,t,e)},zoomBy:function(t,e,i,n){var r=this;r.zoomTo(r.__zoomLevel*t,e,i,n)},scrollTo:function(t,e,i,n){var r=this;if(r.__isDecelerating&&(core.effect.Animate.stop(r.__isDecelerating),r.__isDecelerating=!1),null!=n&&n!==r.__zoomLevel){if(!r.options.zooming)throw new Error("Zooming is not enabled!");t*=n,e*=n,r.__computeScrollMax(n)}else n=r.__zoomLevel;r.options.scrollingX?r.options.paging?t=Math.round(t/r.__clientWidth)*r.__clientWidth:r.options.snapping&&(t=Math.round(t/r.__snapWidth)*r.__snapWidth):t=r.__scrollLeft,r.options.scrollingY?r.options.paging?e=Math.round(e/r.__clientHeight)*r.__clientHeight:r.options.snapping&&(e=Math.round(e/r.__snapHeight)*r.__snapHeight):e=r.__scrollTop,t=Math.max(Math.min(r.__maxScrollLeft,t),0),e=Math.max(Math.min(r.__maxScrollTop,e),0),t===r.__scrollLeft&&e===r.__scrollTop&&(i=!1),r.__publish(t,e,n,i)},scrollBy:function(t,e,i){var n=this,r=n.__isAnimating?n.__scheduledLeft:n.__scrollLeft,o=n.__isAnimating?n.__scheduledTop:n.__scrollTop;n.scrollTo(r+(t||0),o+(e||0),i)},doMouseZoom:function(t,e,i,n){var r=this,o=t>0?.97:1.03;return r.zoomTo(r.__zoomLevel*o,!1,i-r.__clientLeft,n-r.__clientTop)},doTouchStart:function(t,e){if(this.hintResize(),null==t.length)throw new Error("Invalid touch list: "+t);if(e instanceof Date&&(e=e.valueOf()),"number"!=typeof e)throw new Error("Invalid timestamp value: "+e);var i=this;i.__interruptedAnimation=!0,i.__isDecelerating&&(core.effect.Animate.stop(i.__isDecelerating),i.__isDecelerating=!1,i.__interruptedAnimation=!0),i.__isAnimating&&(core.effect.Animate.stop(i.__isAnimating),i.__isAnimating=!1,i.__interruptedAnimation=!0);var n,r,o=1===t.length;o?(n=t[0].pageX,r=t[0].pageY):(n=Math.abs(t[0].pageX+t[1].pageX)/2,r=Math.abs(t[0].pageY+t[1].pageY)/2),i.__initialTouchLeft=n,i.__initialTouchTop=r,i.__zoomLevelStart=i.__zoomLevel,i.__lastTouchLeft=n,i.__lastTouchTop=r,i.__lastTouchMove=e,i.__lastScale=1,i.__enableScrollX=!o&&i.options.scrollingX,i.__enableScrollY=!o&&i.options.scrollingY,i.__isTracking=!0,i.__didDecelerationComplete=!1,i.__isDragging=!o,i.__isSingleTouch=o,i.__positions=[]},doTouchMove:function(t,e,i){if(null==t.length)throw new Error("Invalid touch list: "+t);if(e instanceof Date&&(e=e.valueOf()),"number"!=typeof e)throw new Error("Invalid timestamp value: "+e);var n=this;if(n.__isTracking){var r,o;2===t.length?(r=Math.abs(t[0].pageX+t[1].pageX)/2,o=Math.abs(t[0].pageY+t[1].pageY)/2):(r=t[0].pageX,o=t[0].pageY);var s=n.__positions;if(n.__isDragging){var a=r-n.__lastTouchLeft,l=o-n.__lastTouchTop,c=n.__scrollLeft,u=n.__scrollTop,h=n.__zoomLevel;if(null!=i&&n.options.zooming){var d=h;if(h=h/n.__lastScale*i,h=Math.max(Math.min(h,n.options.maxZoom),n.options.minZoom),d!==h){var _=r-n.__clientLeft,f=o-n.__clientTop;c=(_+c)*h/d-_,u=(f+u)*h/d-f,n.__computeScrollMax(h)}}if(n.__enableScrollX){c-=a*this.options.speedMultiplier;var g=n.__maxScrollLeft;(c>g||0>c)&&(n.options.bouncing?c+=a/2*this.options.speedMultiplier:c=c>g?g:0)}if(n.__enableScrollY){u-=l*this.options.speedMultiplier;var p=n.__maxScrollTop;(u>p||0>u)&&(n.options.bouncing||n.__refreshHeight&&0>u?(u+=l/2*this.options.speedMultiplier,n.__enableScrollX||null==n.__refreshHeight||(!n.__refreshActive&&u<=-n.__refreshHeight?(n.__refreshActive=!0,n.__refreshActivate&&n.__refreshActivate()):n.__refreshActive&&u>-n.__refreshHeight&&(n.__refreshActive=!1,n.__refreshDeactivate&&n.__refreshDeactivate()))):u=u>p?p:0)}s.length>60&&s.splice(0,30),s.push(c,u,e),n.__publish(c,u,h)}else{var m=n.options.locking?3:0,v=5,T=Math.abs(r-n.__initialTouchLeft),E=Math.abs(o-n.__initialTouchTop);n.__enableScrollX=n.options.scrollingX&&T>=m,n.__enableScrollY=n.options.scrollingY&&E>=m,s.push(n.__scrollLeft,n.__scrollTop,e),n.__isDragging=(n.__enableScrollX||n.__enableScrollY)&&(T>=v||E>=v),n.__isDragging&&(n.__interruptedAnimation=!1,n.__fadeScrollbars("in"))}n.__lastTouchLeft=r,n.__lastTouchTop=o,n.__lastTouchMove=e,n.__lastScale=i}},doTouchEnd:function(t){if(t instanceof Date&&(t=t.valueOf()),"number"!=typeof t)throw new Error("Invalid timestamp value: "+t);var e=this;if(e.__isTracking){if(e.__isTracking=!1,e.__isDragging)if(e.__isDragging=!1,e.__isSingleTouch&&e.options.animating&&t-e.__lastTouchMove<=100){for(var i=e.__positions,n=i.length-1,r=n,o=n;o>0&&i[o]>e.__lastTouchMove-100;o-=3)r=o;if(r!==n){var s=i[n]-i[r],a=e.__scrollLeft-i[r-2],l=e.__scrollTop-i[r-1];e.__decelerationVelocityX=a/s*(1e3/60),e.__decelerationVelocityY=l/s*(1e3/60);var c=e.options.paging||e.options.snapping?4:1;(Math.abs(e.__decelerationVelocityX)>c||Math.abs(e.__decelerationVelocityY)>c)&&(e.__refreshActive||e.__startDeceleration(t))}else e.__scrollingComplete()}else t-e.__lastTouchMove>100&&e.__scrollingComplete();e.__isDecelerating||(e.__refreshActive&&e.__refreshStart?(e.__publish(e.__scrollLeft,-e.__refreshHeight,e.__zoomLevel,!0),e.__refreshStart&&e.__refreshStart()):((e.__interruptedAnimation||e.__isDragging)&&e.__scrollingComplete(),e.scrollTo(e.__scrollLeft,e.__scrollTop,!0,e.__zoomLevel),e.__refreshActive&&(e.__refreshActive=!1,e.__refreshDeactivate&&e.__refreshDeactivate()))),e.__positions.length=0}},__publish:function(t,e,i,n){var s=this,a=s.__isAnimating;if(a&&(core.effect.Animate.stop(a),s.__isAnimating=!1),n&&s.options.animating){s.__scheduledLeft=t,s.__scheduledTop=e,s.__scheduledZoom=i;var l=s.__scrollLeft,c=s.__scrollTop,u=s.__zoomLevel,h=t-l,d=e-c,_=i-u,f=function(t,e,i){i&&(s.__scrollLeft=l+h*t,s.__scrollTop=c+d*t,s.__zoomLevel=u+_*t,s.__callback&&s.__callback(s.__scrollLeft,s.__scrollTop,s.__zoomLevel))},g=function(t){return s.__isAnimating===t},p=function(t,e,i){e===s.__isAnimating&&(s.__isAnimating=!1),(s.__didDecelerationComplete||i)&&s.__scrollingComplete(),s.options.zooming&&s.__computeScrollMax()};s.__isAnimating=core.effect.Animate.start(f,g,p,s.options.animationDuration,a?r:o)}else s.__scheduledLeft=s.__scrollLeft=t,s.__scheduledTop=s.__scrollTop=e,s.__scheduledZoom=s.__zoomLevel=i,s.__callback&&s.__callback(t,e,i),s.options.zooming&&s.__computeScrollMax()},__computeScrollMax:function(t){var e=this;null==t&&(t=e.__zoomLevel),e.__maxScrollLeft=Math.max(e.__contentWidth*t-e.__clientWidth,0),e.__maxScrollTop=Math.max(e.__contentHeight*t-e.__clientHeight,0),e.__didWaitForSize||0!=e.__maxScrollLeft||0!=e.__maxScrollTop||(e.__didWaitForSize=!0,e.__waitForSize())},__waitForSize:function(){var t=this;clearTimeout(t.__sizerTimeout);var e=function(){t.resize(),t.options.scrollingX&&0==t.__maxScrollLeft||t.options.scrollingY&&0==t.__maxScrollTop};e(),t.__sizerTimeout=setTimeout(e,1e3)},__startDeceleration:function(){var t=this;if(t.options.paging){var e=Math.max(Math.min(t.__scrollLeft,t.__maxScrollLeft),0),i=Math.max(Math.min(t.__scrollTop,t.__maxScrollTop),0),n=t.__clientWidth,r=t.__clientHeight;t.__minDecelerationScrollLeft=Math.floor(e/n)*n,t.__minDecelerationScrollTop=Math.floor(i/r)*r,t.__maxDecelerationScrollLeft=Math.ceil(e/n)*n,t.__maxDecelerationScrollTop=Math.ceil(i/r)*r}else t.__minDecelerationScrollLeft=0,t.__minDecelerationScrollTop=0,t.__maxDecelerationScrollLeft=t.__maxScrollLeft,t.__maxDecelerationScrollTop=t.__maxScrollTop;var o=function(e,i,n){t.__stepThroughDeceleration(n)};t.__minVelocityToKeepDecelerating=t.options.snapping?4:.1;var s=function(){var e=Math.abs(t.__decelerationVelocityX)>=t.__minVelocityToKeepDecelerating||Math.abs(t.__decelerationVelocityY)>=t.__minVelocityToKeepDecelerating;return e||(t.__didDecelerationComplete=!0),e},a=function(){t.__isDecelerating=!1,t.__didDecelerationComplete&&t.__scrollingComplete(),t.options.paging&&t.scrollTo(t.__scrollLeft,t.__scrollTop,t.options.snapping)};t.__isDecelerating=core.effect.Animate.start(o,s,a)},__stepThroughDeceleration:function(t){var e=this,i=e.__scrollLeft+e.__decelerationVelocityX,n=e.__scrollTop+e.__decelerationVelocityY;if(!e.options.bouncing){var r=Math.max(Math.min(e.__maxDecelerationScrollLeft,i),e.__minDecelerationScrollLeft);r!==i&&(i=r,e.__decelerationVelocityX=0);var o=Math.max(Math.min(e.__maxDecelerationScrollTop,n),e.__minDecelerationScrollTop);o!==n&&(n=o,e.__decelerationVelocityY=0)}if(t?e.__publish(i,n,e.__zoomLevel):(e.__scrollLeft=i,e.__scrollTop=n),!e.options.paging){var s=.95;e.__decelerationVelocityX*=s,e.__decelerationVelocityY*=s}if(e.options.bouncing){var a=0,l=0,c=e.options.penetrationDeceleration,u=e.options.penetrationAcceleration;if(i<e.__minDecelerationScrollLeft?a=e.__minDecelerationScrollLeft-i:i>e.__maxDecelerationScrollLeft&&(a=e.__maxDecelerationScrollLeft-i),n<e.__minDecelerationScrollTop?l=e.__minDecelerationScrollTop-n:n>e.__maxDecelerationScrollTop&&(l=e.__maxDecelerationScrollTop-n),0!==a){var h=a*e.__decelerationVelocityX<=e.__minDecelerationScrollLeft;h&&(e.__decelerationVelocityX+=a*c);var d=Math.abs(e.__decelerationVelocityX)<=e.__minVelocityToKeepDecelerating;(!h||d)&&(e.__decelerationVelocityX=a*u)}if(0!==l){var _=l*e.__decelerationVelocityY<=e.__minDecelerationScrollTop;_&&(e.__decelerationVelocityY+=l*c);var f=Math.abs(e.__decelerationVelocityY)<=e.__minVelocityToKeepDecelerating;(!_||f)&&(e.__decelerationVelocityY=l*u)}}}})}(ionic),function(t){"use strict";t.views.ActionSheet=t.views.View.inherit({initialize:function(t){this.el=t.el},show:function(){this.el.offsetWidth,this.el.classList.add("active")},hide:function(){this.el.offsetWidth,this.el.classList.remove("active")}})}(ionic),function(t){"use strict";t.views.HeaderBar=t.views.View.inherit({initialize:function(e){this.el=e.el,t.extend(this,{alignTitle:"center"},e),this.align()},align:function(e){e||(e=this.alignTitle);var i=this.el.querySelector(".title");if(i){var n,r,o,s=this.el.childNodes,a=0,l=0,c=!1;for(n=0;n<s.length;n++)r=s[n],r.tagName&&"h1"==r.tagName.toLowerCase()?c=!0:(o=null,3==r.nodeType?o=t.DomUtil.getTextBounds(r):1==r.nodeType&&(o=r.getBoundingClientRect()),o&&(c?l+=o.width:a+=o.width));t.requestAnimationFrame(function(){var t=Math.max(a,l)+10;"center"==e?(t>10&&(i.style.left=t+"px",i.style.right=t+"px"),i.offsetWidth<i.scrollWidth&&l>0&&(i.style.right=l+5+"px")):"left"==e?(i.classList.add("title-left"),a>0&&(i.style.left=a+15+"px")):"right"==e&&(i.classList.add("title-right"),l>0&&(i.style.right=l+15+"px"))})}}})}(ionic),function(t){"use strict";var e="item",i="item-content",n="item-sliding",r="item-options",o="item-placeholder",s="item-reordering",a="item-reorder",l=function(){};l.prototype={start:function(){},drag:function(){},end:function(){},isSameItem:function(){return!1}};var c=function(t){this.dragThresholdX=t.dragThresholdX||10,this.el=t.el};c.prototype=new l,c.prototype.start=function(o){var s,a,l,c;s=o.target.classList.contains(i)?o.target:o.target.classList.contains(e)?o.target.querySelector("."+i):t.DomUtil.getParentWithClass(o.target,i),s&&(s.classList.remove(n),l=parseFloat(s.style[t.CSS.TRANSFORM].replace("translate3d(","").split(",")[0])||0,a=s.parentNode.querySelector("."+r),a&&(c=a.offsetWidth,this._currentDrag={buttonsWidth:c,content:s,startOffsetX:l}))},c.prototype.isSameItem=function(t){return t._lastDrag&&this._currentDrag?this._currentDrag.content==t._lastDrag.content:!1},c.prototype.clean=function(){var e=this._lastDrag;e&&t.requestAnimationFrame(function(){e.content.style[t.CSS.TRANSITION]="",e.content.style[t.CSS.TRANSFORM]="translate3d(0, 0, 0)"})},c.prototype.drag=t.animationFrameThrottle(function(e){var i;if(this._currentDrag&&(!this._isDragging&&(Math.abs(e.gesture.deltaX)>this.dragThresholdX||Math.abs(this._currentDrag.startOffsetX)>0)&&(this._isDragging=!0),this._isDragging)){i=this._currentDrag.buttonsWidth;var n=Math.min(0,this._currentDrag.startOffsetX+e.gesture.deltaX);-i>n&&(n=Math.min(-i,-i+.4*(e.gesture.deltaX+i))),this._currentDrag.content.style[t.CSS.TRANSFORM]="translate3d("+n+"px, 0, 0)",this._currentDrag.content.style[t.CSS.TRANSITION]="none"}}),c.prototype.end=function(e,i){var n=this;if(!this._currentDrag)return void(i&&i());var r=-this._currentDrag.buttonsWidth;e.gesture.deltaX>-(this._currentDrag.buttonsWidth/2)&&("left"==e.gesture.direction&&Math.abs(e.gesture.velocityX)<.3?r=0:"right"==e.gesture.direction&&(r=0)),t.requestAnimationFrame(function(){n._currentDrag.content.style[t.CSS.TRANSFORM]=0===r?"":"translate3d("+r+"px, 0, 0)",n._currentDrag.content.style[t.CSS.TRANSITION]="",n._lastDrag=n._currentDrag,n._currentDrag=null,i&&i()})};var u=function(t){this.dragThresholdY=t.dragThresholdY||0,this.onReorder=t.onReorder,this.el=t.el,this.scrollEl=t.scrollEl,this.scrollView=t.scrollView};u.prototype=new l,u.prototype._moveElement=function(e){var i=e.gesture.center.pageY-this._currentDrag.elementHeight/2;this.el.style[t.CSS.TRANSFORM]="translate3d(0, "+i+"px, 0)"},u.prototype.start=function(e){var i=(this.el.offsetTop,t.DomUtil.getChildIndex(this.el,this.el.nodeName.toLowerCase())),n=this.el.offsetHeight,r=this.el.cloneNode(!0),a=(this.scrollEl||this.el).parentNode;r.classList.add(o),this.el.parentNode.insertBefore(r,this.el),this.el.classList.add(s),a.parentNode.appendChild(this.el),this._currentDrag={elementHeight:n,startIndex:i,placeholder:r,scrollHeight:scroll,list:r.parentNode},this._moveElement(e)},u.prototype.drag=t.animationFrameThrottle(function(t){if(this._currentDrag){var e=0,i=t.gesture.center.pageY;if(this.scrollView){var n=this.scrollEl;e=this.scrollView.getValues().top;var r=n.offsetTop,o=r-i+this._currentDrag.elementHeight/2,s=i+this._currentDrag.elementHeight/2-r-n.offsetHeight;t.gesture.deltaY<0&&o>0&&e>0&&this.scrollView.scrollBy(null,-o),t.gesture.deltaY>0&&s>0&&e<this.scrollView.getScrollMax().top&&this.scrollView.scrollBy(null,s)}!this._isDragging&&Math.abs(t.gesture.deltaY)>this.dragThresholdY&&(this._isDragging=!0),this._isDragging&&(this._moveElement(t),this._currentDrag.currentY=e+i-this._currentDrag.placeholder.parentNode.offsetTop,this._reorderItems())}}),u.prototype._reorderItems=function(){var e=(this._currentDrag.placeholder,Array.prototype.slice.call(this._currentDrag.placeholder.parentNode.children)),i=e.indexOf(this._currentDrag.placeholder),n=e[Math.max(0,i-1)],r=e[Math.min(e.length,i+1)],o=this._currentDrag.currentY;return n&&o<n.offsetTop+n.offsetHeight/2?(t.DomUtil.swapNodes(this._currentDrag.placeholder,n),i-1):r&&o>r.offsetTop+r.offsetHeight/2?(t.DomUtil.swapNodes(r,this._currentDrag.placeholder),i+1):void 0},u.prototype.end=function(e,i){if(!this._currentDrag)return void(i&&i());var n=this._currentDrag.placeholder,r=t.DomUtil.getChildIndex(n,n.nodeName.toLowerCase());this.el.classList.remove(s),this.el.style[t.CSS.TRANSFORM]="",n.parentNode.insertBefore(this.el,n),n.parentNode.removeChild(n),this.onReorder&&this.onReorder(this.el,this._currentDrag.startIndex,r),this._currentDrag=null,i&&i()},t.views.ListView=t.views.View.inherit({initialize:function(e){var i=this;e=t.extend({onReorder:function(){},virtualRemoveThreshold:-200,virtualAddThreshold:200,canSwipe:!1},e),t.extend(this,e),!this.itemHeight&&this.listEl&&(this.itemHeight=this.listEl.children[0]&&parseInt(this.listEl.children[0].style.height,10)),this.onRefresh=e.onRefresh||function(){},this.onRefreshOpening=e.onRefreshOpening||function(){},this.onRefreshHolding=e.onRefreshHolding||function(){},window.ionic.onGesture("release",function(t){i._handleEndDrag(t)},this.el),window.ionic.onGesture("drag",function(t){i._handleDrag(t)},this.el),this._initDrag()},stopRefreshing:function(){var t=this.el.querySelector(".list-refresher");t.style.height="0px"},didScroll:function(t){if(this.isVirtual){var e=this.itemHeight,i=(this.listEl.children.length,t.target.scrollHeight),n=this.el.parentNode.offsetHeight,r=(t.scrollTop,Math.max(0,t.scrollTop+this.virtualRemoveThreshold)),o=Math.min(i,Math.abs(t.scrollTop)+n+this.virtualAddThreshold),s=Math.floor((o-r)/e),a=parseInt(Math.abs(r/e),10),l=parseInt(Math.abs(o/e),10);this._virtualItemsToRemove=Array.prototype.slice.call(this.listEl.children,0,a);{Array.prototype.slice.call(this.listEl.children,a,a+s)}this.renderViewport&&this.renderViewport(r,o,a,l)}},didStopScrolling:function(){if(this.isVirtual)for(var t=0;t<this._virtualItemsToRemove.length;t++){{this._virtualItemsToRemove[t]}this.didHideItem&&this.didHideItem(t)}},clearDragEffects:function(){this._lastDragOp&&(this._lastDragOp.clean&&this._lastDragOp.clean(),this._lastDragOp=null)},_initDrag:function(){this._lastDragOp=this._dragOp,this._dragOp=null},_getItem:function(t){for(;t;){if(t.classList.contains(e))return t;t=t.parentNode}return null},_startDrag:function(e){var i=this;this._isDragging=!1;var n=this._lastDragOp;if(!t.DomUtil.getParentOrSelfWithClass(e.target,a)||"up"!=e.gesture.direction&&"down"!=e.gesture.direction){if(!this._didDragUpOrDown&&("left"==e.gesture.direction||"right"==e.gesture.direction)&&Math.abs(e.gesture.deltaX)>5){var r=this._getItem(e.target);r&&r.querySelector(".item-options")&&(this._dragOp=new c({el:this.el}),this._dragOp.start(e),e.preventDefault())}}else{var r=this._getItem(e.target);r&&(this._dragOp=new u({el:r,scrollEl:this.scrollEl,scrollView:this.scrollView,onReorder:function(t,e,n){i.onReorder&&i.onReorder(t,e,n)}}),this._dragOp.start(e),e.preventDefault())}n&&this._dragOp&&!this._dragOp.isSameItem(n)&&e.defaultPrevented&&n.clean&&n.clean()},_handleEndDrag:function(t){var e=this;this._didDragUpOrDown=!1,this._dragOp&&this._dragOp.end(t,function(){e._initDrag()})},_handleDrag:function(t){this.canSwipe&&(Math.abs(t.gesture.deltaY)>5&&(this._didDragUpOrDown=!0),this.isDragging||this._dragOp||this._startDrag(t),this._dragOp&&(t.gesture.srcEvent.preventDefault(),this._dragOp.drag(t)))}})}(ionic),function(t){"use strict";t.views.Loading=t.views.View.inherit({initialize:function(t){this.el=t.el,this.maxWidth=t.maxWidth||200,this.showDelay=t.showDelay||0,this._loadingBox=this.el.querySelector(".loading")||this.el},show:function(){var t=this;if(this._loadingBox){var e=t._loadingBox,i=Math.min(t.maxWidth,Math.max(window.outerWidth-40,e.offsetWidth));e.style.width=i+"px",e.style.marginLeft=-e.offsetWidth/2+"px",e.style.marginTop=-e.offsetHeight/2+"px",this._showDelayTimeout=window.setTimeout(function(){t.el.classList.add("active")},t.showDelay)}},hide:function(){this.el.offsetWidth,window.clearTimeout(this._showDelayTimeout),this.el.classList.remove("active")},setContent:function(t){this._loadingBox&&(this._loadingBox.innerHTML=t||"")}})}(ionic),function(t){"use strict";t.views.Modal=t.views.View.inherit({initialize:function(e){e=t.extend({focusFirstInput:!1,unfocusOnHide:!0,focusFirstDelay:600},e),t.extend(this,e),this.el=e.el},show:function(){var t=this;t.focusFirstInput&&window.setTimeout(function(){var e=t.el.querySelector("input, textarea");e&&e.focus&&e.focus()},t.focusFirstDelay)},hide:function(){if(this.unfocusOnHide){var t=this.el.querySelectorAll("input, textarea");window.setTimeout(function(){for(var e=0;e<t.length;e++)t[e].blur&&t[e].blur()})}}})}(ionic),function(t){"use strict";t.views.NavBar=t.views.View.inherit({initialize:function(t){this.el=t.el,this._titleEl=this.el.querySelector(".title"),t.hidden&&this.hide()},hide:function(){this.el.classList.add("hidden")},show:function(){this.el.classList.remove("hidden")},shouldGoBack:function(){},setTitle:function(t){this._titleEl&&(this._titleEl.innerHTML=t)},showBackButton:function(t){var e=this;if(!this._currentBackButton){var i=document.createElement("a");i.className="button back",i.innerHTML="Back",this._currentBackButton=i,this._currentBackButton.onclick=function(){e.shouldGoBack&&e.shouldGoBack()}}t&&!this._currentBackButton.parentNode?this.el.insertBefore(this._currentBackButton,this.el.firstChild):!t&&this._currentBackButton.parentNode&&this._currentBackButton.parentNode.removeChild(this._currentBackButton)}})}(ionic),function(t){"use strict";t.views.SideMenu=t.views.View.inherit({initialize:function(t){this.el=t.el,this.isEnabled="undefined"==typeof t.isEnabled?!0:t.isEnabled,this.setWidth(t.width)},getFullWidth:function(){return this.width},setWidth:function(t){this.width=t,this.el.style.width=t+"px"},setIsEnabled:function(t){this.isEnabled=t},bringUp:function(){"0"!==this.el.style.zIndex&&(this.el.style.zIndex="0")},pushDown:function(){"-1"!==this.el.style.zIndex&&(this.el.style.zIndex="-1")}}),t.views.SideMenuContent=t.views.View.inherit({initialize:function(e){t.extend(this,{animationClass:"menu-animated",onDrag:function(){},onEndDrag:function(){}},e),t.onGesture("drag",t.proxy(this._onDrag,this),this.el),t.onGesture("release",t.proxy(this._onEndDrag,this),this.el)},_onDrag:function(t){this.onDrag&&this.onDrag(t)},_onEndDrag:function(t){this.onEndDrag&&this.onEndDrag(t)},disableAnimation:function(){this.el.classList.remove(this.animationClass)},enableAnimation:function(){this.el.classList.add(this.animationClass)},getTranslateX:function(){return parseFloat(this.el.style[t.CSS.TRANSFORM].replace("translate3d(","").split(",")[0])},setTranslateX:t.animationFrameThrottle(function(e){this.el.style[t.CSS.TRANSFORM]="translate3d("+e+"px, 0, 0)"})})}(ionic),function(t){"use strict";t.views.Slider=t.views.View.inherit({initialize:function(t){function e(){g=T.children,v=g.length,g.length<2&&(t.continuous=!1),_.transitions&&t.continuous&&g.length<3&&(T.appendChild(g[0].cloneNode(!0)),T.appendChild(T.children[1].cloneNode(!0)),g=T.children),p=new Array(g.length),m=f.getBoundingClientRect().width||f.offsetWidth,T.style.width=g.length*m+"px";for(var e=g.length;e--;){var i=g[e];i.style.width=m+"px",i.setAttribute("data-index",e),_.transitions&&(i.style.left=e*-m+"px",s(e,E>e?-m:e>E?m:0,0))}t.continuous&&_.transitions&&(s(r(E-1),-m,0),s(r(E+1),m,0)),_.transitions||(T.style.left=E*-m+"px"),f.style.visibility="visible",t.slidesChanged&&t.slidesChanged()}function i(){t.continuous?o(E-1):E&&o(E-1)}function n(){t.continuous?o(E+1):E<g.length-1&&o(E+1)}function r(t){return(g.length+t%g.length)%g.length}function o(e,i){if(E!=e){if(_.transitions){var n=Math.abs(E-e)/(E-e);if(t.continuous){var o=n;n=-p[r(e)]/m,n!==o&&(e=-n*g.length+e)}for(var a=Math.abs(E-e)-1;a--;)s(r((e>E?e:E)-a-1),m*n,0);e=r(e),s(E,m*n,i||w),s(e,0,i||w),t.continuous&&s(r(e-n),-(m*n),0)}else e=r(e),l(E*-m,e*-m,i||w);E=e,d(t.callback&&t.callback(E,g[E]))}}function s(t,e,i){a(t,e,i),p[t]=e}function a(t,e,i){var n=g[t],r=n&&n.style;r&&(r.webkitTransitionDuration=r.MozTransitionDuration=r.msTransitionDuration=r.OTransitionDuration=r.transitionDuration=i+"ms",r.webkitTransform="translate("+e+"px,0)translateZ(0)",r.msTransform=r.MozTransform=r.OTransform="translateX("+e+"px)")}function l(e,i,n){if(!n)return void(T.style.left=i+"px");var r=+new Date,o=setInterval(function(){var s=+new Date-r;return s>n?(T.style.left=i+"px",S&&c(),t.transitionEnd&&t.transitionEnd.call(event,E,g[E]),void clearInterval(o)):void(T.style.left=(i-e)*(Math.floor(s/n*100)/100)+e+"px")},4)}function c(){y=setTimeout(n,S)}function u(){S=t.auto||0,clearTimeout(y)}var h=function(){},d=function(t){setTimeout(t||h,0)},_={addEventListener:!!window.addEventListener,touch:"ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch,transitions:function(t){var e=["transitionProperty","WebkitTransition","MozTransition","OTransition","msTransition"];for(var i in e)if(void 0!==t.style[e[i]])return!0;return!1}(document.createElement("swipe"))},f=t.el;if(f){var g,p,m,v,T=f.children[0];t=t||{};var E=parseInt(t.startSlide,10)||0,w=t.speed||300;t.continuous=void 0!==t.continuous?t.continuous:!0;var y,b,S=t.auto||0,D={},x={},L={handleEvent:function(i){switch(("mousedown"==i.type||"mouseup"==i.type||"mousemove"==i.type)&&(i.touches=[{pageX:i.pageX,pageY:i.pageY}]),i.type){case"mousedown":this.start(i);break;case"touchstart":this.start(i);break;case"touchmove":this.move(i);break;case"mousemove":this.move(i);break;case"touchend":d(this.end(i));break;case"mouseup":d(this.end(i));break;case"webkitTransitionEnd":case"msTransitionEnd":case"oTransitionEnd":case"otransitionend":case"transitionend":d(this.transitionEnd(i));break;case"resize":d(e)}t.stopPropagation&&i.stopPropagation()},start:function(t){var e=t.touches[0];D={x:e.pageX,y:e.pageY,time:+new Date},b=void 0,x={},_.touch?(T.addEventListener("touchmove",this,!1),T.addEventListener("touchend",this,!1)):(T.addEventListener("mousemove",this,!1),T.addEventListener("mouseup",this,!1),document.addEventListener("mouseup",this,!1))},move:function(e){if(!(e.touches.length>1||e.scale&&1!==e.scale)){t.disableScroll&&e.preventDefault();var i=e.touches[0];x={x:i.pageX-D.x,y:i.pageY-D.y},"undefined"==typeof b&&(b=!!(b||Math.abs(x.x)<Math.abs(x.y))),b||(e.preventDefault(),u(),t.continuous?(a(r(E-1),x.x+p[r(E-1)],0),a(E,x.x+p[E],0),a(r(E+1),x.x+p[r(E+1)],0)):(x.x=x.x/(!E&&x.x>0||E==g.length-1&&x.x<0?Math.abs(x.x)/m+1:1),a(E-1,x.x+p[E-1],0),a(E,x.x+p[E],0),a(E+1,x.x+p[E+1],0)))}},end:function(){var e=+new Date-D.time,i=Number(e)<250&&Math.abs(x.x)>20||Math.abs(x.x)>m/2,n=!E&&x.x>0||E==g.length-1&&x.x<0;t.continuous&&(n=!1);var o=x.x<0;b||(i&&!n?(o?(t.continuous?(s(r(E-1),-m,0),s(r(E+2),m,0)):s(E-1,-m,0),s(E,p[E]-m,w),s(r(E+1),p[r(E+1)]-m,w),E=r(E+1)):(t.continuous?(s(r(E+1),m,0),s(r(E-2),-m,0)):s(E+1,m,0),s(E,p[E]+m,w),s(r(E-1),p[r(E-1)]+m,w),E=r(E-1)),t.callback&&t.callback(E,g[E])):t.continuous?(s(r(E-1),-m,w),s(E,0,w),s(r(E+1),m,w)):(s(E-1,-m,w),s(E,0,w),s(E+1,m,w))),_.touch?(T.removeEventListener("touchmove",L,!1),T.removeEventListener("touchend",L,!1)):(T.removeEventListener("mousemove",L,!1),T.removeEventListener("mouseup",L,!1),document.removeEventListener("mouseup",L,!1))
},transitionEnd:function(e){parseInt(e.target.getAttribute("data-index"),10)==E&&(S&&c(),t.transitionEnd&&t.transitionEnd.call(e,E,g[E]))}};this.update=function(){setTimeout(e)},this.setup=function(){e()},this.slide=function(t,e){u(),o(t,e)},this.prev=this.previous=function(){u(),i()},this.next=function(){u(),n()},this.stop=function(){u()},this.currentIndex=function(){return E},this.slidesCount=function(){return v},this.kill=function(){u(),T.style.width="",T.style.left="";for(var t=g.length;t--;){var e=g[t];e.style.width="",e.style.left="",_.transitions&&a(t,0,0)}_.addEventListener?(T.removeEventListener("touchstart",L,!1),T.removeEventListener("webkitTransitionEnd",L,!1),T.removeEventListener("msTransitionEnd",L,!1),T.removeEventListener("oTransitionEnd",L,!1),T.removeEventListener("otransitionend",L,!1),T.removeEventListener("transitionend",L,!1),window.removeEventListener("resize",L,!1)):window.onresize=null},this.load=function(){e(),S&&c(),_.addEventListener?(_.touch?T.addEventListener("touchstart",L,!1):T.addEventListener("mousedown",L,!1),_.transitions&&(T.addEventListener("webkitTransitionEnd",L,!1),T.addEventListener("msTransitionEnd",L,!1),T.addEventListener("oTransitionEnd",L,!1),T.addEventListener("otransitionend",L,!1),T.addEventListener("transitionend",L,!1)),window.addEventListener("resize",L,!1)):window.onresize=function(){e()}}}}})}(ionic),function(t){"use strict";t.views.TabBarItem=t.views.View.inherit({initialize:function(t){this.el=t,this._buildItem()},create:function(e){var i=document.createElement("a");if(i.className="tab-item",e.icon){var n=document.createElement("i");n.className=e.icon,i.appendChild(n)}if(e.badge){var r=document.createElement("i");r.className="badge",r.innerHTML=e.badge,i.appendChild(r),i.className="tab-item has-badge"}return i.appendChild(document.createTextNode(e.title)),new t.views.TabBarItem(i)},_buildItem:function(){for(var e,i=this,n=Array.prototype.slice.call(this.el.children),r=0,o=n.length;o>r;r++)e=n[r],"i"==e.tagName.toLowerCase()&&/icon/.test(e.className)&&(this.icon=e.className),"i"==e.tagName.toLowerCase()&&/badge/.test(e.className)&&(this.badge=e.textContent.trim());for(this.title="",r=0,o=this.el.childNodes.length;o>r;r++)e=this.el.childNodes[r],"#text"===e.nodeName&&(this.title+=e.nodeValue.trim());this._tapHandler=function(t){i.onTap&&i.onTap(t)},t.on("tap",this._tapHandler,this.el)},onTap:function(){},destroy:function(){t.off("tap",this._tapHandler,this.el)},getIcon:function(){return this.icon},getTitle:function(){return this.title},getBadge:function(){return this.badge},setSelected:function(t){this.isSelected=t,t?this.el.classList.add("active"):this.el.classList.remove("active")}}),t.views.TabBar=t.views.View.inherit({initialize:function(t){this.el=t.el,this.items=[],this._buildItems()},getItems:function(){return this.items},addItem:function(e){var i=t.views.TabBarItem.prototype.create(e);this.appendItemElement(i),this.items.push(i),this._bindEventsOnItem(i)},appendItemElement:function(t){this.el&&this.el.appendChild(t.el)},removeItem:function(t){var e=this.items[t];e&&(e.onTap=void 0,e.destroy())},_bindEventsOnItem:function(t){var e=this;this._itemTapHandler||(this._itemTapHandler=function(){e.trySelectItem(this)}),t.onTap=this._itemTapHandler},getSelectedItem:function(){return this.selectedItem},setSelectedItem:function(t){this.selectedItem=this.items[t];for(var e=0,i=this.items.length;i>e;e+=1)this.items[e].setSelected(!1);this.selectedItem&&this.selectedItem.setSelected(!0)},selectItem:function(t){for(var e=0,i=this.items.length;i>e;e+=1)if(this.items[e]==t)return void this.setSelectedItem(e)},trySelectItem:function(t){for(var e=0,i=this.items.length;i>e;e+=1)if(this.items[e]==t)return void(this.tryTabSelect&&this.tryTabSelect(e))},_buildItems:function(){for(var e,i=Array.prototype.slice.call(this.el.children),n=0,r=i.length;r>n;n+=1)e=new t.views.TabBarItem(i[n]),this.items[n]=e,this._bindEventsOnItem(e);this.items.length>0&&(this.selectedItem=this.items[0])},destroy:function(){for(var t=0,e=this.items.length;e>t;t+=1)this.items[t].destroy();this.items.length=0}})}(window.ionic),function(t){"use strict";t.views.Toggle=t.views.View.inherit({initialize:function(e){var i=this;this.el=e.el,this.checkbox=e.checkbox,this.track=e.track,this.handle=e.handle,this.openPercent=-1,this.onChange=e.onChange||function(){},this.triggerThreshold=e.triggerThreshold||20,this.dragStartHandler=function(t){i.dragStart(t)},this.dragHandler=function(t){i.drag(t)},this.holdHandler=function(t){i.hold(t)},this.releaseHandler=function(t){i.release(t)},this.dragStartGesture=t.onGesture("dragstart",this.dragStartHandler,this.el),this.dragGesture=t.onGesture("drag",this.dragHandler,this.el),this.dragHoldGesture=t.onGesture("hold",this.holdHandler,this.el),this.dragReleaseGesture=t.onGesture("release",this.releaseHandler,this.el)},destroy:function(){t.offGesture(this.dragStartGesture,"dragstart",this.dragStartGesture),t.offGesture(this.dragGesture,"drag",this.dragGesture),t.offGesture(this.dragHoldGesture,"hold",this.holdHandler),t.offGesture(this.dragReleaseGesture,"release",this.releaseHandler)},tap:function(){"disabled"!==this.el.getAttribute("disabled")&&this.val(!this.checkbox.checked)},dragStart:function(t){this.checkbox.disabled||(this._dragInfo={width:this.el.offsetWidth,left:this.el.offsetLeft,right:this.el.offsetLeft+this.el.offsetWidth,triggerX:this.el.offsetWidth/2,initialState:this.checkbox.checked},t.gesture.srcEvent.preventDefault(),this.hold(t))},drag:function(e){var i=this;this._dragInfo&&(e.gesture.srcEvent.preventDefault(),t.requestAnimationFrame(function(){var t=(i.track.offsetLeft+i.handle.offsetWidth/2,i.track.offsetLeft+i.track.offsetWidth-i.handle.offsetWidth/2,e.gesture.deltaX,e.gesture.touches[0].pageX-i._dragInfo.left),n=i._dragInfo.width-i.triggerThreshold;i._dragInfo.initialState?t<i.triggerThreshold?i.setOpenPercent(0):t>i._dragInfo.triggerX&&i.setOpenPercent(100):t<i._dragInfo.triggerX?i.setOpenPercent(0):t>n&&i.setOpenPercent(100)}))},endDrag:function(){this._dragInfo=null},hold:function(){this.el.classList.add("dragging")},release:function(t){this.el.classList.remove("dragging"),this.endDrag(t)},setOpenPercent:function(e){if(this.openPercent<0||e<this.openPercent-3||e>this.openPercent+3)if(this.openPercent=e,0===e)this.val(!1);else if(100===e)this.val(!0);else{var i=Math.round(e/100*this.track.offsetWidth-this.handle.offsetWidth);i=1>i?0:i,this.handle.style[t.CSS.TRANSFORM]="translate3d("+i+"px,0,0)"}},val:function(e){return(e===!0||e===!1)&&(""!==this.handle.style[t.CSS.TRANSFORM]&&(this.handle.style[t.CSS.TRANSFORM]=""),this.checkbox.checked=e,this.openPercent=e?100:0,this.onChange&&this.onChange()),this.checkbox.checked}})}(ionic),function(t){"use strict";t.controllers.ViewController=function(){this.initialize.apply(this,arguments)},t.controllers.ViewController.inherit=t.inherit,t.extend(t.controllers.ViewController.prototype,{initialize:function(){},destroy:function(){}})}(window.ionic),function(t){"use strict";t.controllers.NavController=t.controllers.ViewController.inherit({initialize:function(t){var e=this;this.navBar=t.navBar,this.content=t.content,this.controllers=t.controllers||[],this._updateNavBar(),this.navBar.shouldGoBack=function(){e.pop()}},getControllers:function(){return this.controllers},getTopController:function(){return this.controllers[this.controllers.length-1]},push:function(t){var e=this.controllers[this.controllers.length-1];this.controllers.push(t);var i=this.switchingController&&this.switchingController(t)||!0;if(i!==!1){e&&(e.isVisible=!1,e.visibilityChanged&&e.visibilityChanged("push"));var n=this.controllers[this.controllers.length-1];return n.isVisible=!0,n.visibilityChanged&&n.visibilityChanged(e?"push":"first"),this._updateNavBar(),t}},pop:function(){var t,e;if(!(this.controllers.length<2))return e=this.controllers.pop(),e&&(e.isVisible=!1,e.visibilityChanged&&e.visibilityChanged("pop")),t=this.controllers[this.controllers.length-1],t.isVisible=!0,t.visibilityChanged&&t.visibilityChanged("pop"),this._updateNavBar(),e},showNavBar:function(){this.navBar&&this.navBar.show()},hideNavBar:function(){this.navBar&&this.navBar.hide()},_updateNavBar:function(){this.getTopController()&&this.navBar&&(this.navBar.setTitle(this.getTopController().title),this.navBar.showBackButton(this.controllers.length>1?!0:!1))}})}(window.ionic),function(t){"use strict";t.controllers.SideMenuController=t.controllers.ViewController.inherit({initialize:function(t){var e=this;this.left=t.left,this.right=t.right,this.content=t.content,this.dragThresholdX=t.dragThresholdX||10,this._rightShowing=!1,this._leftShowing=!1,this._isDragging=!1,this.content&&(this.content.onDrag=function(t){e._handleDrag(t)},this.content.onEndDrag=function(t){e._endDrag(t)})},setContent:function(t){var e=this;this.content=t,this.content.onDrag=function(t){e._handleDrag(t)},this.content.endDrag=function(t){e._endDrag(t)}},isOpenLeft:function(){return this.getOpenAmount()>0},isOpenRight:function(){return this.getOpenAmount()<0},toggleLeft:function(t){var e=this.getOpenAmount();0===arguments.length&&(t=0>=e),this.content.enableAnimation(),this.openPercentage(t?100:0)},toggleRight:function(t){var e=this.getOpenAmount();0===arguments.length&&(t=e>=0),this.content.enableAnimation(),this.openPercentage(t?-100:0)},close:function(){this.openPercentage(0)},getOpenAmount:function(){return this.content&&this.content.getTranslateX()||0},getOpenRatio:function(){var t=this.getOpenAmount();return t>=0?t/this.left.width:t/this.right.width},isOpen:function(){return 1==this.getOpenRatio()},getOpenPercentage:function(){return 100*this.getOpenRatio()},openPercentage:function(t){var e=t/100;if(this.left&&t>=0)this.openAmount(this.left.width*e);else if(this.right&&0>t){{this.right.width}this.openAmount(this.right.width*e)}},openAmount:function(t){var e=this.left&&this.left.width||0,i=this.right&&this.right.width||0;return(this.left&&this.left.isEnabled||!(t>0))&&(this.right&&this.right.isEnabled||!(0>t))?this._leftShowing&&t>e?void this.content.setTranslateX(e):this._rightShowing&&-i>t?void this.content.setTranslateX(-i):(this.content.setTranslateX(t),void(t>=0?(this._leftShowing=!0,this._rightShowing=!1,t>0&&(this.right&&this.right.pushDown&&this.right.pushDown(),this.left&&this.left.bringUp&&this.left.bringUp())):(this._rightShowing=!0,this._leftShowing=!1,this.right&&this.right.bringUp&&this.right.bringUp(),this.left&&this.left.pushDown&&this.left.pushDown()))):void this.content.setTranslateX(0)},snapToRest:function(t){this.content.enableAnimation(),this._isDragging=!1;var e=this.getOpenRatio();if(0===e)return void this.openPercentage(0);var i=.3,n=t.gesture.velocityX,r=t.gesture.direction;this.openPercentage(e>0&&.5>e&&"right"==r&&i>n?0:e>.5&&"left"==r&&i>n?100:0>e&&e>-.5&&"left"==r&&i>n?0:.5>e&&"right"==r&&i>n?-100:"right"==r&&e>=0&&(e>=.5||n>i)?100:"left"==r&&0>=e&&(-.5>=e||n>i)?-100:0)},_endDrag:function(t){this._isDragging&&this.snapToRest(t),this._startX=null,this._lastX=null,this._offsetX=null},_handleDrag:function(t){this._startX?this._lastX=t.gesture.touches[0].pageX:(this._startX=t.gesture.touches[0].pageX,this._lastX=this._startX),!this._isDragging&&Math.abs(this._lastX-this._startX)>this.dragThresholdX&&(this._startX=this._lastX,this._isDragging=!0,this.content.disableAnimation(),this._offsetX=this.getOpenAmount()),this._isDragging&&this.openAmount(this._offsetX+(this._lastX-this._startX))}})}(ionic),function(t){"use strict";t.controllers.TabBarController=t.controllers.ViewController.inherit({initialize:function(t){this.tabBar=t.tabBar,this._bindEvents(),this.controllers=[];for(var e=t.controllers||[],i=0;i<e.length;i++)this.addController(e[i]);this.controllerWillChange=t.controllerWillChange||function(){},this.controllerChanged=t.controllerChanged||function(){},this.setSelectedController(0)},_bindEvents:function(){var t=this;this.tabBar.tryTabSelect=function(e){t.setSelectedController(e)}},selectController:function(t){var e=!0;this.controllerWillChange&&this.controllerWillChange(this.controllers[t],t)===!1&&(e=!1),e&&this.setSelectedController(t)},setSelectedController:function(t){if(!(t>=this.controllers.length)){var e=this.selectedController,i=this.selectedIndex;this.selectedController=this.controllers[t],this.selectedIndex=t,this._showController(t),this.tabBar.setSelectedItem(t),this.controllerChanged&&this.controllerChanged(e,i,this.selectedController,this.selectedIndex)}},_showController:function(t){for(var e,i=0,n=this.controllers.length;n>i;i++)e=this.controllers[i],e.isVisible=!1,e.visibilityChanged&&e.visibilityChanged();e=this.controllers[t],e.isVisible=!0,e.visibilityChanged&&e.visibilityChanged()},_clearSelected:function(){this.selectedController=null,this.selectedIndex=-1},getController:function(t){return this.controllers[t]},getControllers:function(){return this.controllers},getSelectedController:function(){return this.selectedController},getSelectedControllerIndex:function(){return this.selectedIndex},addController:function(t){this.controllers.push(t),this.tabBar.addItem({title:t.title,icon:t.icon,badge:t.badge}),this.selectedController||this.setSelectedController(0)},setControllers:function(t){this.controllers=t,this._clearSelected(),this.selectController(0)}})}(window.ionic)}();
/*!
 * ionic.bundle.js is a concatenation of:
 * ionic.js, angular.js, angular-animate.js,
 * angular-ui-router.js, and ionic-angular.js
 */

/*
 AngularJS v1.2.12
 (c) 2010-2014 Google, Inc. http://angularjs.org
 License: MIT
 */
(function(P,R,s){'use strict';function t(b){return function(){var a=arguments[0],c,a="["+(b?b+":":"")+a+"] http://errors.angularjs.org/1.2.12/"+(b?b+"/":"")+a;for(c=1;c<arguments.length;c++)a=a+(1==c?"?":"&")+"p"+(c-1)+"="+encodeURIComponent("function"==typeof arguments[c]?arguments[c].toString().replace(/ \{[\s\S]*$/,""):"undefined"==typeof arguments[c]?"undefined":"string"!=typeof arguments[c]?JSON.stringify(arguments[c]):arguments[c]);console.log(a);return Error(a)}}function qb(b){if(null==b||za(b))return!1;
    var a=b.length;return 1===b.nodeType&&a?!0:w(b)||L(b)||0===a||"number"===typeof a&&0<a&&a-1 in b}function q(b,a,c){var d;if(b)if(M(b))for(d in b)"prototype"==d||("length"==d||"name"==d||b.hasOwnProperty&&!b.hasOwnProperty(d))||a.call(c,b[d],d);else if(b.forEach&&b.forEach!==q)b.forEach(a,c);else if(qb(b))for(d=0;d<b.length;d++)a.call(c,b[d],d);else for(d in b)b.hasOwnProperty(d)&&a.call(c,b[d],d);return b}function Nb(b){var a=[],c;for(c in b)b.hasOwnProperty(c)&&a.push(c);return a.sort()}function Oc(b,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             a,c){for(var d=Nb(b),e=0;e<d.length;e++)a.call(c,b[d[e]],d[e]);return d}function Ob(b){return function(a,c){b(c,a)}}function Za(){for(var b=ia.length,a;b;){b--;a=ia[b].charCodeAt(0);if(57==a)return ia[b]="A",ia.join("");if(90==a)ia[b]="0";else return ia[b]=String.fromCharCode(a+1),ia.join("")}ia.unshift("0");return ia.join("")}function Pb(b,a){a?b.$$hashKey=a:delete b.$$hashKey}function y(b){var a=b.$$hashKey;q(arguments,function(a){a!==b&&q(a,function(a,c){b[c]=a})});Pb(b,a);return b}function V(b){return parseInt(b,
    10)}function Qb(b,a){return y(new (y(function(){},{prototype:b})),a)}function E(){}function Aa(b){return b}function Y(b){return function(){return b}}function u(b){return"undefined"===typeof b}function D(b){return"undefined"!==typeof b}function W(b){return null!=b&&"object"===typeof b}function w(b){return"string"===typeof b}function rb(b){return"number"===typeof b}function Ka(b){return"[object Date]"===La.call(b)}function L(b){return"[object Array]"===La.call(b)}function M(b){return"function"===typeof b}
    function $a(b){return"[object RegExp]"===La.call(b)}function za(b){return b&&b.document&&b.location&&b.alert&&b.setInterval}function Pc(b){return!(!b||!(b.nodeName||b.on&&b.find))}function Qc(b,a,c){var d=[];q(b,function(b,g,f){d.push(a.call(c,b,g,f))});return d}function ab(b,a){if(b.indexOf)return b.indexOf(a);for(var c=0;c<b.length;c++)if(a===b[c])return c;return-1}function Ma(b,a){var c=ab(b,a);0<=c&&b.splice(c,1);return a}function $(b,a){if(za(b)||b&&b.$evalAsync&&b.$watch)throw Na("cpws");if(a){if(b===
        a)throw Na("cpi");if(L(b))for(var c=a.length=0;c<b.length;c++)a.push($(b[c]));else{c=a.$$hashKey;q(a,function(b,c){delete a[c]});for(var d in b)a[d]=$(b[d]);Pb(a,c)}}else(a=b)&&(L(b)?a=$(b,[]):Ka(b)?a=new Date(b.getTime()):$a(b)?a=RegExp(b.source):W(b)&&(a=$(b,{})));return a}function Rb(b,a){a=a||{};for(var c in b)!b.hasOwnProperty(c)||"$"===c.charAt(0)&&"$"===c.charAt(1)||(a[c]=b[c]);return a}function ta(b,a){if(b===a)return!0;if(null===b||null===a)return!1;if(b!==b&&a!==a)return!0;var c=typeof b,
        d;if(c==typeof a&&"object"==c)if(L(b)){if(!L(a))return!1;if((c=b.length)==a.length){for(d=0;d<c;d++)if(!ta(b[d],a[d]))return!1;return!0}}else{if(Ka(b))return Ka(a)&&b.getTime()==a.getTime();if($a(b)&&$a(a))return b.toString()==a.toString();if(b&&b.$evalAsync&&b.$watch||a&&a.$evalAsync&&a.$watch||za(b)||za(a)||L(a))return!1;c={};for(d in b)if("$"!==d.charAt(0)&&!M(b[d])){if(!ta(b[d],a[d]))return!1;c[d]=!0}for(d in a)if(!c.hasOwnProperty(d)&&"$"!==d.charAt(0)&&a[d]!==s&&!M(a[d]))return!1;return!0}return!1}
    function Sb(){return R.securityPolicy&&R.securityPolicy.isActive||R.querySelector&&!(!R.querySelector("[ng-csp]")&&!R.querySelector("[data-ng-csp]"))}function bb(b,a){var c=2<arguments.length?ua.call(arguments,2):[];return!M(a)||a instanceof RegExp?a:c.length?function(){return arguments.length?a.apply(b,c.concat(ua.call(arguments,0))):a.apply(b,c)}:function(){return arguments.length?a.apply(b,arguments):a.call(b)}}function Rc(b,a){var c=a;"string"===typeof b&&"$"===b.charAt(0)?c=s:za(a)?c="$WINDOW":
        a&&R===a?c="$DOCUMENT":a&&(a.$evalAsync&&a.$watch)&&(c="$SCOPE");return c}function pa(b,a){return"undefined"===typeof b?s:JSON.stringify(b,Rc,a?"  ":null)}function Tb(b){return w(b)?JSON.parse(b):b}function Oa(b){"function"===typeof b?b=!0:b&&0!==b.length?(b=x(""+b),b=!("f"==b||"0"==b||"false"==b||"no"==b||"n"==b||"[]"==b)):b=!1;return b}function fa(b){b=z(b).clone();try{b.empty()}catch(a){}var c=z("<div>").append(b).html();try{return 3===b[0].nodeType?x(c):c.match(/^(<[^>]+>)/)[1].replace(/^<([\w\-]+)/,
        function(a,b){return"<"+x(b)})}catch(d){return x(c)}}function Ub(b){try{return decodeURIComponent(b)}catch(a){}}function Vb(b){var a={},c,d;q((b||"").split("&"),function(b){b&&(c=b.split("="),d=Ub(c[0]),D(d)&&(b=D(c[1])?Ub(c[1]):!0,a[d]?L(a[d])?a[d].push(b):a[d]=[a[d],b]:a[d]=b))});return a}function Wb(b){var a=[];q(b,function(b,d){L(b)?q(b,function(b){a.push(va(d,!0)+(!0===b?"":"="+va(b,!0)))}):a.push(va(d,!0)+(!0===b?"":"="+va(b,!0)))});return a.length?a.join("&"):""}function sb(b){return va(b,
        !0).replace(/%26/gi,"&").replace(/%3D/gi,"=").replace(/%2B/gi,"+")}function va(b,a){return encodeURIComponent(b).replace(/%40/gi,"@").replace(/%3A/gi,":").replace(/%24/g,"$").replace(/%2C/gi,",").replace(/%20/g,a?"%20":"+")}function Sc(b,a){function c(a){a&&d.push(a)}var d=[b],e,g,f=["ng:app","ng-app","x-ng-app","data-ng-app"],h=/\sng[:\-]app(:\s*([\w\d_]+);?)?\s/;q(f,function(a){f[a]=!0;c(R.getElementById(a));a=a.replace(":","\\:");b.querySelectorAll&&(q(b.querySelectorAll("."+a),c),q(b.querySelectorAll("."+
        a+"\\:"),c),q(b.querySelectorAll("["+a+"]"),c))});q(d,function(a){if(!e){var b=h.exec(" "+a.className+" ");b?(e=a,g=(b[2]||"").replace(/\s+/g,",")):q(a.attributes,function(b){!e&&f[b.name]&&(e=a,g=b.value)})}});e&&a(e,g?[g]:[])}function Xb(b,a){var c=function(){b=z(b);if(b.injector()){var c=b[0]===R?"document":fa(b);throw Na("btstrpd",c);}a=a||[];a.unshift(["$provide",function(a){a.value("$rootElement",b)}]);a.unshift("ng");c=Yb(a);c.invoke(["$rootScope","$rootElement","$compile","$injector","$animate",
        function(a,b,c,d,e){a.$apply(function(){b.data("$injector",d);c(b)(a)})}]);return c},d=/^NG_DEFER_BOOTSTRAP!/;if(P&&!d.test(P.name))return c();P.name=P.name.replace(d,"");Ba.resumeBootstrap=function(b){q(b,function(b){a.push(b)});c()}}function cb(b,a){a=a||"_";return b.replace(Tc,function(b,d){return(d?a:"")+b.toLowerCase()})}function tb(b,a,c){if(!b)throw Na("areq",a||"?",c||"required");return b}function Pa(b,a,c){c&&L(b)&&(b=b[b.length-1]);tb(M(b),a,"not a function, got "+(b&&"object"==typeof b?
        b.constructor.name||"Object":typeof b));return b}function wa(b,a){if("hasOwnProperty"===b)throw Na("badname",a);}function Zb(b,a,c){if(!a)return b;a=a.split(".");for(var d,e=b,g=a.length,f=0;f<g;f++)d=a[f],b&&(b=(e=b)[d]);return!c&&M(b)?bb(e,b):b}function ub(b){var a=b[0];b=b[b.length-1];if(a===b)return z(a);var c=[a];do{a=a.nextSibling;if(!a)break;c.push(a)}while(a!==b);return z(c)}function Uc(b){var a=t("$injector"),c=t("ng");b=b.angular||(b.angular={});b.$$minErr=b.$$minErr||t;return b.module||
        (b.module=function(){var b={};return function(e,g,f){if("hasOwnProperty"===e)throw c("badname","module");g&&b.hasOwnProperty(e)&&(b[e]=null);return b[e]||(b[e]=function(){function b(a,d,e){return function(){c[e||"push"]([a,d,arguments]);return n}}if(!g)throw a("nomod",e);var c=[],d=[],l=b("$injector","invoke"),n={_invokeQueue:c,_runBlocks:d,requires:g,name:e,provider:b("$provide","provider"),factory:b("$provide","factory"),service:b("$provide","service"),value:b("$provide","value"),constant:b("$provide",
            "constant","unshift"),animation:b("$animateProvider","register"),filter:b("$filterProvider","register"),controller:b("$controllerProvider","register"),directive:b("$compileProvider","directive"),config:l,run:function(a){d.push(a);return this}};f&&l(f);return n}())}}())}function Qa(b){return b.replace(Vc,function(a,b,d,e){return e?d.toUpperCase():d}).replace(Wc,"Moz$1")}function vb(b,a,c,d){function e(b){var e=c&&b?[this.filter(b)]:[this],m=a,k,l,n,p,r,F;if(!d||null!=b)for(;e.length;)for(k=e.shift(),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            l=0,n=k.length;l<n;l++)for(p=z(k[l]),m?p.triggerHandler("$destroy"):m=!m,r=0,p=(F=p.children()).length;r<p;r++)e.push(Ca(F[r]));return g.apply(this,arguments)}var g=Ca.fn[b],g=g.$original||g;e.$original=g;Ca.fn[b]=e}function O(b){if(b instanceof O)return b;w(b)&&(b=Z(b));if(!(this instanceof O)){if(w(b)&&"<"!=b.charAt(0))throw wb("nosel");return new O(b)}if(w(b)){var a=R.createElement("div");a.innerHTML="<div>&#160;</div>"+b;a.removeChild(a.firstChild);xb(this,a.childNodes);z(R.createDocumentFragment()).append(this)}else xb(this,
        b)}function yb(b){return b.cloneNode(!0)}function Da(b){$b(b);var a=0;for(b=b.childNodes||[];a<b.length;a++)Da(b[a])}function ac(b,a,c,d){if(D(d))throw wb("offargs");var e=ja(b,"events");ja(b,"handle")&&(u(a)?q(e,function(a,c){zb(b,c,a);delete e[c]}):q(a.split(" "),function(a){u(c)?(zb(b,a,e[a]),delete e[a]):Ma(e[a]||[],c)}))}function $b(b,a){var c=b[db],d=Ra[c];d&&(a?delete Ra[c].data[a]:(d.handle&&(d.events.$destroy&&d.handle({},"$destroy"),ac(b)),delete Ra[c],b[db]=s))}function ja(b,a,c){var d=
        b[db],d=Ra[d||-1];if(D(c))d||(b[db]=d=++Xc,d=Ra[d]={}),d[a]=c;else return d&&d[a]}function bc(b,a,c){var d=ja(b,"data"),e=D(c),g=!e&&D(a),f=g&&!W(a);d||f||ja(b,"data",d={});if(e)d[a]=c;else if(g){if(f)return d&&d[a];y(d,a)}else return d}function Ab(b,a){return b.getAttribute?-1<(" "+(b.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ").indexOf(" "+a+" "):!1}function Bb(b,a){a&&b.setAttribute&&q(a.split(" "),function(a){b.setAttribute("class",Z((" "+(b.getAttribute("class")||"")+" ").replace(/[\n\t]/g,
        " ").replace(" "+Z(a)+" "," ")))})}function Cb(b,a){if(a&&b.setAttribute){var c=(" "+(b.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ");q(a.split(" "),function(a){a=Z(a);-1===c.indexOf(" "+a+" ")&&(c+=a+" ")});b.setAttribute("class",Z(c))}}function xb(b,a){if(a){a=a.nodeName||!D(a.length)||za(a)?[a]:a;for(var c=0;c<a.length;c++)b.push(a[c])}}function cc(b,a){return eb(b,"$"+(a||"ngController")+"Controller")}function eb(b,a,c){b=z(b);9==b[0].nodeType&&(b=b.find("html"));for(a=L(a)?a:[a];b.length;){for(var d=
        0,e=a.length;d<e;d++)if((c=b.data(a[d]))!==s)return c;b=b.parent()}}function dc(b){for(var a=0,c=b.childNodes;a<c.length;a++)Da(c[a]);for(;b.firstChild;)b.removeChild(b.firstChild)}function ec(b,a){var c=fb[a.toLowerCase()];return c&&fc[b.nodeName]&&c}function Yc(b,a){var c=function(c,e){c.preventDefault||(c.preventDefault=function(){c.returnValue=!1});c.stopPropagation||(c.stopPropagation=function(){c.cancelBubble=!0});c.target||(c.target=c.srcElement||R);if(u(c.defaultPrevented)){var g=c.preventDefault;
        c.preventDefault=function(){c.defaultPrevented=!0;g.call(c)};c.defaultPrevented=!1}c.isDefaultPrevented=function(){return c.defaultPrevented||!1===c.returnValue};var f=Rb(a[e||c.type]||[]);q(f,function(a){a.call(b,c)});8>=N?(c.preventDefault=null,c.stopPropagation=null,c.isDefaultPrevented=null):(delete c.preventDefault,delete c.stopPropagation,delete c.isDefaultPrevented)};c.elem=b;return c}function Ea(b){var a=typeof b,c;"object"==a&&null!==b?"function"==typeof(c=b.$$hashKey)?c=b.$$hashKey():c===
        s&&(c=b.$$hashKey=Za()):c=b;return a+":"+c}function Sa(b){q(b,this.put,this)}function gc(b){var a,c;"function"==typeof b?(a=b.$inject)||(a=[],b.length&&(c=b.toString().replace(Zc,""),c=c.match($c),q(c[1].split(ad),function(b){b.replace(bd,function(b,c,d){a.push(d)})})),b.$inject=a):L(b)?(c=b.length-1,Pa(b[c],"fn"),a=b.slice(0,c)):Pa(b,"fn",!0);return a}function Yb(b){function a(a){return function(b,c){if(W(b))q(b,Ob(a));else return a(b,c)}}function c(a,b){wa(a,"service");if(M(b)||L(b))b=n.instantiate(b);
        if(!b.$get)throw Ta("pget",a);return l[a+h]=b}function d(a,b){return c(a,{$get:b})}function e(a){var b=[],c,d,g,h;q(a,function(a){if(!k.get(a)){k.put(a,!0);try{if(w(a))for(c=Ua(a),b=b.concat(e(c.requires)).concat(c._runBlocks),d=c._invokeQueue,g=0,h=d.length;g<h;g++){var f=d[g],m=n.get(f[0]);m[f[1]].apply(m,f[2])}else M(a)?b.push(n.invoke(a)):L(a)?b.push(n.invoke(a)):Pa(a,"module")}catch(r){throw L(a)&&(a=a[a.length-1]),r.message&&(r.stack&&-1==r.stack.indexOf(r.message))&&(r=r.message+"\n"+r.stack),
        Ta("modulerr",a,r.stack||r.message||r);}}});return b}function g(a,b){function c(d){if(a.hasOwnProperty(d)){if(a[d]===f)throw Ta("cdep",m.join(" <- "));return a[d]}try{return m.unshift(d),a[d]=f,a[d]=b(d)}catch(e){throw a[d]===f&&delete a[d],e;}finally{m.shift()}}function d(a,b,e){var g=[],h=gc(a),f,m,k;m=0;for(f=h.length;m<f;m++){k=h[m];if("string"!==typeof k)throw Ta("itkn",k);g.push(e&&e.hasOwnProperty(k)?e[k]:c(k))}a.$inject||(a=a[f]);return a.apply(b,g)}return{invoke:d,instantiate:function(a,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           b){var c=function(){},e;c.prototype=(L(a)?a[a.length-1]:a).prototype;c=new c;e=d(a,c,b);return W(e)||M(e)?e:c},get:c,annotate:gc,has:function(b){return l.hasOwnProperty(b+h)||a.hasOwnProperty(b)}}}var f={},h="Provider",m=[],k=new Sa,l={$provide:{provider:a(c),factory:a(d),service:a(function(a,b){return d(a,["$injector",function(a){return a.instantiate(b)}])}),value:a(function(a,b){return d(a,Y(b))}),constant:a(function(a,b){wa(a,"constant");l[a]=b;p[a]=b}),decorator:function(a,b){var c=n.get(a+h),
        d=c.$get;c.$get=function(){var a=r.invoke(d,c);return r.invoke(b,null,{$delegate:a})}}}},n=l.$injector=g(l,function(){throw Ta("unpr",m.join(" <- "));}),p={},r=p.$injector=g(p,function(a){a=n.get(a+h);return r.invoke(a.$get,a)});q(e(b),function(a){r.invoke(a||E)});return r}function cd(){var b=!0;this.disableAutoScrolling=function(){b=!1};this.$get=["$window","$location","$rootScope",function(a,c,d){function e(a){var b=null;q(a,function(a){b||"a"!==x(a.nodeName)||(b=a)});return b}function g(){var b=
        c.hash(),d;b?(d=f.getElementById(b))?d.scrollIntoView():(d=e(f.getElementsByName(b)))?d.scrollIntoView():"top"===b&&a.scrollTo(0,0):a.scrollTo(0,0)}var f=a.document;b&&d.$watch(function(){return c.hash()},function(){d.$evalAsync(g)});return g}]}function dd(b,a,c,d){function e(a){try{a.apply(null,ua.call(arguments,1))}finally{if(F--,0===F)for(;A.length;)try{A.pop()()}catch(b){c.error(b)}}}function g(a,b){(function S(){q(H,function(a){a()});v=b(S,a)})()}function f(){C=null;Q!=h.url()&&(Q=h.url(),q(ka,
        function(a){a(h.url())}))}var h=this,m=a[0],k=b.location,l=b.history,n=b.setTimeout,p=b.clearTimeout,r={};h.isMock=!1;var F=0,A=[];h.$$completeOutstandingRequest=e;h.$$incOutstandingRequestCount=function(){F++};h.notifyWhenNoOutstandingRequests=function(a){q(H,function(a){a()});0===F?a():A.push(a)};var H=[],v;h.addPollFn=function(a){u(v)&&g(100,n);H.push(a);return a};var Q=k.href,K=a.find("base"),C=null;h.url=function(a,c){k!==b.location&&(k=b.location);l!==b.history&&(l=b.history);if(a){if(Q!=a)return Q=
        a,d.history?c?l.replaceState(null,"",a):(l.pushState(null,"",a),K.attr("href",K.attr("href"))):(C=a,c?k.replace(a):k.href=a),h}else return C||k.href.replace(/%27/g,"'")};var ka=[],I=!1;h.onUrlChange=function(a){if(!I){if(d.history)z(b).on("popstate",f);if(d.hashchange)z(b).on("hashchange",f);else h.addPollFn(f);I=!0}ka.push(a);return a};h.baseHref=function(){var a=K.attr("href");return a?a.replace(/^(https?\:)?\/\/[^\/]*/,""):""};var U={},ba="",aa=h.baseHref();h.cookies=function(a,b){var d,e,g,h;
        if(a)b===s?m.cookie=escape(a)+"=;path="+aa+";expires=Thu, 01 Jan 1970 00:00:00 GMT":w(b)&&(d=(m.cookie=escape(a)+"="+escape(b)+";path="+aa).length+1,4096<d&&c.warn("Cookie '"+a+"' possibly not set or overflowed because it was too large ("+d+" > 4096 bytes)!"));else{if(m.cookie!==ba)for(ba=m.cookie,d=ba.split("; "),U={},g=0;g<d.length;g++)e=d[g],h=e.indexOf("="),0<h&&(a=unescape(e.substring(0,h)),U[a]===s&&(U[a]=unescape(e.substring(h+1))));return U}};h.defer=function(a,b){var c;F++;c=n(function(){delete r[c];
        e(a)},b||0);r[c]=!0;return c};h.defer.cancel=function(a){return r[a]?(delete r[a],p(a),e(E),!0):!1}}function ed(){this.$get=["$window","$log","$sniffer","$document",function(b,a,c,d){return new dd(b,d,a,c)}]}function fd(){this.$get=function(){function b(b,d){function e(a){a!=n&&(p?p==a&&(p=a.n):p=a,g(a.n,a.p),g(a,n),n=a,n.n=null)}function g(a,b){a!=b&&(a&&(a.p=b),b&&(b.n=a))}if(b in a)throw t("$cacheFactory")("iid",b);var f=0,h=y({},d,{id:b}),m={},k=d&&d.capacity||Number.MAX_VALUE,l={},n=null,p=null;
        return a[b]={put:function(a,b){var c=l[a]||(l[a]={key:a});e(c);if(!u(b))return a in m||f++,m[a]=b,f>k&&this.remove(p.key),b},get:function(a){var b=l[a];if(b)return e(b),m[a]},remove:function(a){var b=l[a];b&&(b==n&&(n=b.p),b==p&&(p=b.n),g(b.n,b.p),delete l[a],delete m[a],f--)},removeAll:function(){m={};f=0;l={};n=p=null},destroy:function(){l=h=m=null;delete a[b]},info:function(){return y({},h,{size:f})}}}var a={};b.info=function(){var b={};q(a,function(a,e){b[e]=a.info()});return b};b.get=function(b){return a[b]};
        return b}}function gd(){this.$get=["$cacheFactory",function(b){return b("templates")}]}function ic(b,a){var c={},d="Directive",e=/^\s*directive\:\s*([\d\w\-_]+)\s+(.*)$/,g=/(([\d\w\-_]+)(?:\:([^;]+))?;?)/,f=/^(on[a-z]+|formaction)$/;this.directive=function m(a,e){wa(a,"directive");w(a)?(tb(e,"directiveFactory"),c.hasOwnProperty(a)||(c[a]=[],b.factory(a+d,["$injector","$exceptionHandler",function(b,d){var e=[];q(c[a],function(c,g){try{var f=b.invoke(c);M(f)?f={compile:Y(f)}:!f.compile&&f.link&&(f.compile=
        Y(f.link));f.priority=f.priority||0;f.index=g;f.name=f.name||a;f.require=f.require||f.controller&&f.name;f.restrict=f.restrict||"A";e.push(f)}catch(m){d(m)}});return e}])),c[a].push(e)):q(a,Ob(m));return this};this.aHrefSanitizationWhitelist=function(b){return D(b)?(a.aHrefSanitizationWhitelist(b),this):a.aHrefSanitizationWhitelist()};this.imgSrcSanitizationWhitelist=function(b){return D(b)?(a.imgSrcSanitizationWhitelist(b),this):a.imgSrcSanitizationWhitelist()};this.$get=["$injector","$interpolate",
        "$exceptionHandler","$http","$templateCache","$parse","$controller","$rootScope","$document","$sce","$animate","$$sanitizeUri",function(a,b,l,n,p,r,F,A,H,v,Q,K){function C(a,b,c,d,e){a instanceof z||(a=z(a));q(a,function(b,c){3==b.nodeType&&b.nodeValue.match(/\S+/)&&(a[c]=z(b).wrap("<span></span>").parent()[0])});var g=I(a,b,a,c,d,e);ka(a,"ng-scope");return function(b,c,d){tb(b,"scope");var e=c?Fa.clone.call(a):a;q(d,function(a,b){e.data("$"+b+"Controller",a)});d=0;for(var f=e.length;d<f;d++){var m=
            e[d].nodeType;1!==m&&9!==m||e.eq(d).data("$scope",b)}c&&c(e,b);g&&g(b,e,e);return e}}function ka(a,b){try{a.addClass(b)}catch(c){}}function I(a,b,c,d,e,g){function f(a,c,d,e){var g,k,r,l,n,p,J;g=c.length;var F=Array(g);for(n=0;n<g;n++)F[n]=c[n];J=n=0;for(p=m.length;n<p;J++)k=F[J],c=m[n++],g=m[n++],r=z(k),c?(c.scope?(l=a.$new(),r.data("$scope",l)):l=a,(r=c.transclude)||!e&&b?c(g,l,k,d,U(a,r||b)):c(g,l,k,d,e)):g&&g(a,k.childNodes,s,e)}for(var m=[],k,r,l,n,p=0;p<a.length;p++)k=new Db,r=ba(a[p],[],k,
            0===p?d:s,e),(g=r.length?ga(r,a[p],k,b,c,null,[],[],g):null)&&g.scope&&ka(z(a[p]),"ng-scope"),k=g&&g.terminal||!(l=a[p].childNodes)||!l.length?null:I(l,g?g.transclude:b),m.push(g,k),n=n||g||k,g=null;return n?f:null}function U(a,b){return function(c,d,e){var g=!1;c||(c=a.$new(),g=c.$$transcluded=!0);d=b(c,d,e);if(g)d.on("$destroy",bb(c,c.$destroy));return d}}function ba(a,b,c,d,f){var m=c.$attr,k;switch(a.nodeType){case 1:S(b,la(Ga(a).toLowerCase()),"E",d,f);var r,l,n;k=a.attributes;for(var p=0,F=
            k&&k.length;p<F;p++){var A=!1,Q=!1;r=k[p];if(!N||8<=N||r.specified){l=r.name;n=la(l);T.test(n)&&(l=cb(n.substr(6),"-"));var C=n.replace(/(Start|End)$/,"");n===C+"Start"&&(A=l,Q=l.substr(0,l.length-5)+"end",l=l.substr(0,l.length-6));n=la(l.toLowerCase());m[n]=l;c[n]=r=Z(r.value);ec(a,n)&&(c[n]=!0);O(a,b,r,n);S(b,n,"A",d,f,A,Q)}}a=a.className;if(w(a)&&""!==a)for(;k=g.exec(a);)n=la(k[2]),S(b,n,"C",d,f)&&(c[n]=Z(k[3])),a=a.substr(k.index+k[0].length);break;case 3:t(b,a.nodeValue);break;case 8:try{if(k=
            e.exec(a.nodeValue))n=la(k[1]),S(b,n,"M",d,f)&&(c[n]=Z(k[2]))}catch(H){}}b.sort(u);return b}function aa(a,b,c){var d=[],e=0;if(b&&a.hasAttribute&&a.hasAttribute(b)){do{if(!a)throw ha("uterdir",b,c);1==a.nodeType&&(a.hasAttribute(b)&&e++,a.hasAttribute(c)&&e--);d.push(a);a=a.nextSibling}while(0<e)}else d.push(a);return z(d)}function B(a,b,c){return function(d,e,g,f,k){e=aa(e[0],b,c);return a(d,e,g,f,k)}}function ga(a,c,d,e,g,f,m,n,p){function A(a,b,c,d){if(a){c&&(a=B(a,c,d));a.require=G.require;if(K===
            G||G.$$isolateScope)a=jc(a,{isolateScope:!0});m.push(a)}if(b){c&&(b=B(b,c,d));b.require=G.require;if(K===G||G.$$isolateScope)b=jc(b,{isolateScope:!0});n.push(b)}}function Q(a,b,c){var d,e="data",g=!1;if(w(a)){for(;"^"==(d=a.charAt(0))||"?"==d;)a=a.substr(1),"^"==d&&(e="inheritedData"),g=g||"?"==d;d=null;c&&"data"===e&&(d=c[a]);d=d||b[e]("$"+a+"Controller");if(!d&&!g)throw ha("ctreq",a,ca);}else L(a)&&(d=[],q(a,function(a){d.push(Q(a,b,c))}));return d}function H(a,e,g,f,p){function A(a,b){var c;2>
            arguments.length&&(b=a,a=s);u&&(c=aa);return p(a,b,c)}var J,C,v,I,ba,B,aa={},gb;J=c===g?d:Rb(d,new Db(z(g),d.$attr));C=J.$$element;if(K){var t=/^\s*([@=&])(\??)\s*(\w*)\s*$/;f=z(g);B=e.$new(!0);ga&&ga===K.$$originalDirective?f.data("$isolateScope",B):f.data("$isolateScopeNoTemplate",B);ka(f,"ng-isolate-scope");q(K.scope,function(a,c){var d=a.match(t)||[],g=d[3]||c,f="?"==d[2],d=d[1],m,l,n,p;B.$$isolateBindings[c]=d+g;switch(d){case "@":J.$observe(g,function(a){B[c]=a});J.$$observers[g].$$scope=e;
            J[g]&&(B[c]=b(J[g])(e));break;case "=":if(f&&!J[g])break;l=r(J[g]);p=l.literal?ta:function(a,b){return a===b};n=l.assign||function(){m=B[c]=l(e);throw ha("nonassign",J[g],K.name);};m=B[c]=l(e);B.$watch(function(){var a=l(e);p(a,B[c])||(p(a,m)?n(e,a=B[c]):B[c]=a);return m=a},null,l.literal);break;case "&":l=r(J[g]);B[c]=function(a){return l(e,a)};break;default:throw ha("iscp",K.name,c,a);}})}gb=p&&A;U&&q(U,function(a){var b={$scope:a===K||a.$$isolateScope?B:e,$element:C,$attrs:J,$transclude:gb},c;
            ba=a.controller;"@"==ba&&(ba=J[a.name]);c=F(ba,b);aa[a.name]=c;u||C.data("$"+a.name+"Controller",c);a.controllerAs&&(b.$scope[a.controllerAs]=c)});f=0;for(v=m.length;f<v;f++)try{I=m[f],I(I.isolateScope?B:e,C,J,I.require&&Q(I.require,C,aa),gb)}catch(S){l(S,fa(C))}f=e;K&&(K.template||null===K.templateUrl)&&(f=B);a&&a(f,g.childNodes,s,p);for(f=n.length-1;0<=f;f--)try{I=n[f],I(I.isolateScope?B:e,C,J,I.require&&Q(I.require,C,aa),gb)}catch(G){l(G,fa(C))}}p=p||{};var v=-Number.MAX_VALUE,I,U=p.controllerDirectives,
            K=p.newIsolateScopeDirective,ga=p.templateDirective;p=p.nonTlbTranscludeDirective;for(var S=!1,u=!1,y=d.$$element=z(c),G,ca,t,P=e,O,N=0,ma=a.length;N<ma;N++){G=a[N];var Va=G.$$start,T=G.$$end;Va&&(y=aa(c,Va,T));t=s;if(v>G.priority)break;if(t=G.scope)I=I||G,G.templateUrl||(x("new/isolated scope",K,G,y),W(t)&&(K=G));ca=G.name;!G.templateUrl&&G.controller&&(t=G.controller,U=U||{},x("'"+ca+"' controller",U[ca],G,y),U[ca]=G);if(t=G.transclude)S=!0,G.$$tlb||(x("transclusion",p,G,y),p=G),"element"==t?(u=
            !0,v=G.priority,t=aa(c,Va,T),y=d.$$element=z(R.createComment(" "+ca+": "+d[ca]+" ")),c=y[0],hb(g,z(ua.call(t,0)),c),P=C(t,e,v,f&&f.name,{nonTlbTranscludeDirective:p})):(t=z(yb(c)).contents(),y.empty(),P=C(t,e));if(G.template)if(x("template",ga,G,y),ga=G,t=M(G.template)?G.template(y,d):G.template,t=V(t),G.replace){f=G;t=z("<div>"+Z(t)+"</div>").contents();c=t[0];if(1!=t.length||1!==c.nodeType)throw ha("tplrt",ca,"");hb(g,y,c);ma={$attr:{}};t=ba(c,[],ma);var X=a.splice(N+1,a.length-(N+1));K&&hc(t);
            a=a.concat(t).concat(X);D(d,ma);ma=a.length}else y.html(t);if(G.templateUrl)x("template",ga,G,y),ga=G,G.replace&&(f=G),H=E(a.splice(N,a.length-N),y,d,g,P,m,n,{controllerDirectives:U,newIsolateScopeDirective:K,templateDirective:ga,nonTlbTranscludeDirective:p}),ma=a.length;else if(G.compile)try{O=G.compile(y,d,P),M(O)?A(null,O,Va,T):O&&A(O.pre,O.post,Va,T)}catch(Y){l(Y,fa(y))}G.terminal&&(H.terminal=!0,v=Math.max(v,G.priority))}H.scope=I&&!0===I.scope;H.transclude=S&&P;return H}function hc(a){for(var b=
            0,c=a.length;b<c;b++)a[b]=Qb(a[b],{$$isolateScope:!0})}function S(b,e,g,f,k,r,n){if(e===k)return null;k=null;if(c.hasOwnProperty(e)){var p;e=a.get(e+d);for(var F=0,A=e.length;F<A;F++)try{p=e[F],(f===s||f>p.priority)&&-1!=p.restrict.indexOf(g)&&(r&&(p=Qb(p,{$$start:r,$$end:n})),b.push(p),k=p)}catch(Q){l(Q)}}return k}function D(a,b){var c=b.$attr,d=a.$attr,e=a.$$element;q(a,function(d,e){"$"!=e.charAt(0)&&(b[e]&&(d+=("style"===e?";":" ")+b[e]),a.$set(e,d,!0,c[e]))});q(b,function(b,g){"class"==g?(ka(e,
            b),a["class"]=(a["class"]?a["class"]+" ":"")+b):"style"==g?(e.attr("style",e.attr("style")+";"+b),a.style=(a.style?a.style+";":"")+b):"$"==g.charAt(0)||a.hasOwnProperty(g)||(a[g]=b,d[g]=c[g])})}function E(a,b,c,d,e,g,f,k){var m=[],r,l,F=b[0],A=a.shift(),Q=y({},A,{templateUrl:null,transclude:null,replace:null,$$originalDirective:A}),C=M(A.templateUrl)?A.templateUrl(b,c):A.templateUrl;b.empty();n.get(v.getTrustedResourceUrl(C),{cache:p}).success(function(n){var p,H;n=V(n);if(A.replace){n=z("<div>"+
            Z(n)+"</div>").contents();p=n[0];if(1!=n.length||1!==p.nodeType)throw ha("tplrt",A.name,C);n={$attr:{}};hb(d,b,p);var v=ba(p,[],n);W(A.scope)&&hc(v);a=v.concat(a);D(c,n)}else p=F,b.html(n);a.unshift(Q);r=ga(a,p,c,e,b,A,g,f,k);q(d,function(a,c){a==p&&(d[c]=b[0])});for(l=I(b[0].childNodes,e);m.length;){n=m.shift();H=m.shift();var K=m.shift(),B=m.shift(),v=b[0];if(H!==F){var aa=H.className,v=yb(p);hb(K,z(H),v);ka(z(v),aa)}H=r.transclude?U(n,r.transclude):B;r(l,n,v,d,H)}m=null}).error(function(a,b,c,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           d){throw ha("tpload",d.url);});return function(a,b,c,d,e){m?(m.push(b),m.push(c),m.push(d),m.push(e)):r(l,b,c,d,e)}}function u(a,b){var c=b.priority-a.priority;return 0!==c?c:a.name!==b.name?a.name<b.name?-1:1:a.index-b.index}function x(a,b,c,d){if(b)throw ha("multidir",b.name,c.name,a,fa(d));}function t(a,c){var d=b(c,!0);d&&a.push({priority:0,compile:Y(function(a,b){var c=b.parent(),e=c.data("$binding")||[];e.push(d);ka(c.data("$binding",e),"ng-binding");a.$watch(d,function(a){b[0].nodeValue=a})})})}
            function P(a,b){if("srcdoc"==b)return v.HTML;var c=Ga(a);if("xlinkHref"==b||"FORM"==c&&"action"==b||"IMG"!=c&&("src"==b||"ngSrc"==b))return v.RESOURCE_URL}function O(a,c,d,e){var g=b(d,!0);if(g){if("multiple"===e&&"SELECT"===Ga(a))throw ha("selmulti",fa(a));c.push({priority:100,compile:function(){return{pre:function(c,d,m){d=m.$$observers||(m.$$observers={});if(f.test(e))throw ha("nodomevents");if(g=b(m[e],!0,P(a,e)))m[e]=g(c),(d[e]||(d[e]=[])).$$inter=!0,(m.$$observers&&m.$$observers[e].$$scope||
                c).$watch(g,function(a,b){"class"===e&&a!=b?m.$updateClass(a,b):m.$set(e,a)})}}}})}}function hb(a,b,c){var d=b[0],e=b.length,g=d.parentNode,f,m;if(a)for(f=0,m=a.length;f<m;f++)if(a[f]==d){a[f++]=c;m=f+e-1;for(var k=a.length;f<k;f++,m++)m<k?a[f]=a[m]:delete a[f];a.length-=e-1;break}g&&g.replaceChild(c,d);a=R.createDocumentFragment();a.appendChild(d);c[z.expando]=d[z.expando];d=1;for(e=b.length;d<e;d++)g=b[d],z(g).remove(),a.appendChild(g),delete b[d];b[0]=c;b.length=1}function jc(a,b){return y(function(){return a.apply(null,
                arguments)},a,b)}var Db=function(a,b){this.$$element=a;this.$attr=b||{}};Db.prototype={$normalize:la,$addClass:function(a){a&&0<a.length&&Q.addClass(this.$$element,a)},$removeClass:function(a){a&&0<a.length&&Q.removeClass(this.$$element,a)},$updateClass:function(a,b){this.$removeClass(kc(b,a));this.$addClass(kc(a,b))},$set:function(a,b,c,d){var e=ec(this.$$element[0],a);e&&(this.$$element.prop(a,b),d=e);this[a]=b;d?this.$attr[a]=d:(d=this.$attr[a])||(this.$attr[a]=d=cb(a,"-"));e=Ga(this.$$element);
                if("A"===e&&"href"===a||"IMG"===e&&"src"===a)this[a]=b=K(b,"src"===a);!1!==c&&(null===b||b===s?this.$$element.removeAttr(d):this.$$element.attr(d,b));(c=this.$$observers)&&q(c[a],function(a){try{a(b)}catch(c){l(c)}})},$observe:function(a,b){var c=this,d=c.$$observers||(c.$$observers={}),e=d[a]||(d[a]=[]);e.push(b);A.$evalAsync(function(){e.$$inter||b(c[a])});return b}};var ca=b.startSymbol(),ma=b.endSymbol(),V="{{"==ca||"}}"==ma?Aa:function(a){return a.replace(/\{\{/g,ca).replace(/}}/g,ma)},T=/^ngAttr[A-Z]/;
            return C}]}function la(b){return Qa(b.replace(hd,""))}function kc(b,a){var c="",d=b.split(/\s+/),e=a.split(/\s+/),g=0;a:for(;g<d.length;g++){for(var f=d[g],h=0;h<e.length;h++)if(f==e[h])continue a;c+=(0<c.length?" ":"")+f}return c}function id(){var b={},a=/^(\S+)(\s+as\s+(\w+))?$/;this.register=function(a,d){wa(a,"controller");W(a)?y(b,a):b[a]=d};this.$get=["$injector","$window",function(c,d){return function(e,g){var f,h,m;w(e)&&(f=e.match(a),h=f[1],m=f[3],e=b.hasOwnProperty(h)?b[h]:Zb(g.$scope,h,
        !0)||Zb(d,h,!0),Pa(e,h,!0));f=c.instantiate(e,g);if(m){if(!g||"object"!=typeof g.$scope)throw t("$controller")("noscp",h||e.name,m);g.$scope[m]=f}return f}}]}function jd(){this.$get=["$window",function(b){return z(b.document)}]}function kd(){this.$get=["$log",function(b){return function(a,c){b.error.apply(b,arguments)}}]}function lc(b){var a={},c,d,e;if(!b)return a;q(b.split("\n"),function(b){e=b.indexOf(":");c=x(Z(b.substr(0,e)));d=Z(b.substr(e+1));c&&(a[c]=a[c]?a[c]+(", "+d):d)});return a}function mc(b){var a=
        W(b)?b:s;return function(c){a||(a=lc(b));return c?a[x(c)]||null:a}}function nc(b,a,c){if(M(c))return c(b,a);q(c,function(c){b=c(b,a)});return b}function ld(){var b=/^\s*(\[|\{[^\{])/,a=/[\}\]]\s*$/,c=/^\)\]\}',?\n/,d={"Content-Type":"application/json;charset=utf-8"},e=this.defaults={transformResponse:[function(d){w(d)&&(d=d.replace(c,""),b.test(d)&&a.test(d)&&(d=Tb(d)));return d}],transformRequest:[function(a){return W(a)&&"[object File]"!==La.call(a)?pa(a):a}],headers:{common:{Accept:"application/json, text/plain, */*"},
        post:$(d),put:$(d),patch:$(d)},xsrfCookieName:"XSRF-TOKEN",xsrfHeaderName:"X-XSRF-TOKEN"},g=this.interceptors=[],f=this.responseInterceptors=[];this.$get=["$httpBackend","$browser","$cacheFactory","$rootScope","$q","$injector",function(a,b,c,d,n,p){function r(a){function c(a){var b=y({},a,{data:nc(a.data,a.headers,d.transformResponse)});return 200<=a.status&&300>a.status?b:n.reject(b)}var d={transformRequest:e.transformRequest,transformResponse:e.transformResponse},g=function(a){function b(a){var c;
        q(a,function(b,d){M(b)&&(c=b(),null!=c?a[d]=c:delete a[d])})}var c=e.headers,d=y({},a.headers),g,f,c=y({},c.common,c[x(a.method)]);b(c);b(d);a:for(g in c){a=x(g);for(f in d)if(x(f)===a)continue a;d[g]=c[g]}return d}(a);y(d,a);d.headers=g;d.method=Ha(d.method);(a=Eb(d.url)?b.cookies()[d.xsrfCookieName||e.xsrfCookieName]:s)&&(g[d.xsrfHeaderName||e.xsrfHeaderName]=a);var f=[function(a){g=a.headers;var b=nc(a.data,mc(g),a.transformRequest);u(a.data)&&q(g,function(a,b){"content-type"===x(b)&&delete g[b]});
        u(a.withCredentials)&&!u(e.withCredentials)&&(a.withCredentials=e.withCredentials);return F(a,b,g).then(c,c)},s],k=n.when(d);for(q(v,function(a){(a.request||a.requestError)&&f.unshift(a.request,a.requestError);(a.response||a.responseError)&&f.push(a.response,a.responseError)});f.length;){a=f.shift();var h=f.shift(),k=k.then(a,h)}k.success=function(a){k.then(function(b){a(b.data,b.status,b.headers,d)});return k};k.error=function(a){k.then(null,function(b){a(b.data,b.status,b.headers,d)});return k};
        return k}function F(b,c,g){function f(a,b,c){v&&(200<=a&&300>a?v.put(s,[a,b,lc(c)]):v.remove(s));m(b,a,c);d.$$phase||d.$apply()}function m(a,c,d){c=Math.max(c,0);(200<=c&&300>c?p.resolve:p.reject)({data:a,status:c,headers:mc(d),config:b})}function k(){var a=ab(r.pendingRequests,b);-1!==a&&r.pendingRequests.splice(a,1)}var p=n.defer(),F=p.promise,v,q,s=A(b.url,b.params);r.pendingRequests.push(b);F.then(k,k);(b.cache||e.cache)&&(!1!==b.cache&&"GET"==b.method)&&(v=W(b.cache)?b.cache:W(e.cache)?e.cache:
        H);if(v)if(q=v.get(s),D(q)){if(q.then)return q.then(k,k),q;L(q)?m(q[1],q[0],$(q[2])):m(q,200,{})}else v.put(s,F);u(q)&&a(b.method,s,c,f,g,b.timeout,b.withCredentials,b.responseType);return F}function A(a,b){if(!b)return a;var c=[];Oc(b,function(a,b){null===a||u(a)||(L(a)||(a=[a]),q(a,function(a){W(a)&&(a=pa(a));c.push(va(b)+"="+va(a))}))});return a+(-1==a.indexOf("?")?"?":"&")+c.join("&")}var H=c("$http"),v=[];q(g,function(a){v.unshift(w(a)?p.get(a):p.invoke(a))});q(f,function(a,b){var c=w(a)?p.get(a):
        p.invoke(a);v.splice(b,0,{response:function(a){return c(n.when(a))},responseError:function(a){return c(n.reject(a))}})});r.pendingRequests=[];(function(a){q(arguments,function(a){r[a]=function(b,c){return r(y(c||{},{method:a,url:b}))}})})("get","delete","head","jsonp");(function(a){q(arguments,function(a){r[a]=function(b,c,d){return r(y(d||{},{method:a,url:b,data:c}))}})})("post","put");r.defaults=e;return r}]}function md(b){if(8>=N&&(!b.match(/^(get|post|head|put|delete|options)$/i)||!P.XMLHttpRequest))return new P.ActiveXObject("Microsoft.XMLHTTP");
        if(P.XMLHttpRequest)return new P.XMLHttpRequest;throw t("$httpBackend")("noxhr");}function nd(){this.$get=["$browser","$window","$document",function(b,a,c){return od(b,md,b.defer,a.angular.callbacks,c[0])}]}function od(b,a,c,d,e){function g(a,b){var c=e.createElement("script"),d=function(){c.onreadystatechange=c.onload=c.onerror=null;e.body.removeChild(c);b&&b()};c.type="text/javascript";c.src=a;N&&8>=N?c.onreadystatechange=function(){/loaded|complete/.test(c.readyState)&&d()}:c.onload=c.onerror=
        function(){d()};e.body.appendChild(c);return d}var f=-1;return function(e,m,k,l,n,p,r,F){function A(){v=f;K&&K();C&&C.abort()}function H(a,d,e,g){I&&c.cancel(I);K=C=null;d=0===d?e?200:404:d;a(1223==d?204:d,e,g);b.$$completeOutstandingRequest(E)}var v;b.$$incOutstandingRequestCount();m=m||b.url();if("jsonp"==x(e)){var Q="_"+(d.counter++).toString(36);d[Q]=function(a){d[Q].data=a};var K=g(m.replace("JSON_CALLBACK","angular.callbacks."+Q),function(){d[Q].data?H(l,200,d[Q].data):H(l,v||-2);d[Q]=Ba.noop})}else{var C=
        a(e);C.open(e,m,!0);q(n,function(a,b){D(a)&&C.setRequestHeader(b,a)});C.onreadystatechange=function(){if(C&&4==C.readyState){var a=null,b=null;v!==f&&(a=C.getAllResponseHeaders(),b="response"in C?C.response:C.responseText);H(l,v||C.status,b,a)}};r&&(C.withCredentials=!0);if(F)try{C.responseType=F}catch(s){if("json"!==F)throw s;}C.send(k||null)}if(0<p)var I=c(A,p);else p&&p.then&&p.then(A)}}function pd(){var b="{{",a="}}";this.startSymbol=function(a){return a?(b=a,this):b};this.endSymbol=function(b){return b?
        (a=b,this):a};this.$get=["$parse","$exceptionHandler","$sce",function(c,d,e){function g(g,k,l){for(var n,p,r=0,F=[],A=g.length,H=!1,v=[];r<A;)-1!=(n=g.indexOf(b,r))&&-1!=(p=g.indexOf(a,n+f))?(r!=n&&F.push(g.substring(r,n)),F.push(r=c(H=g.substring(n+f,p))),r.exp=H,r=p+h,H=!0):(r!=A&&F.push(g.substring(r)),r=A);(A=F.length)||(F.push(""),A=1);if(l&&1<F.length)throw oc("noconcat",g);if(!k||H)return v.length=A,r=function(a){try{for(var b=0,c=A,f;b<c;b++)"function"==typeof(f=F[b])&&(f=f(a),f=l?e.getTrusted(l,
        f):e.valueOf(f),null===f||u(f)?f="":"string"!=typeof f&&(f=pa(f))),v[b]=f;return v.join("")}catch(k){a=oc("interr",g,k.toString()),d(a)}},r.exp=g,r.parts=F,r}var f=b.length,h=a.length;g.startSymbol=function(){return b};g.endSymbol=function(){return a};return g}]}function qd(){this.$get=["$rootScope","$window","$q",function(b,a,c){function d(d,f,h,m){var k=a.setInterval,l=a.clearInterval,n=c.defer(),p=n.promise,r=0,F=D(m)&&!m;h=D(h)?h:0;p.then(null,null,d);p.$$intervalId=k(function(){n.notify(r++);
        0<h&&r>=h&&(n.resolve(r),l(p.$$intervalId),delete e[p.$$intervalId]);F||b.$apply()},f);e[p.$$intervalId]=n;return p}var e={};d.cancel=function(a){return a&&a.$$intervalId in e?(e[a.$$intervalId].reject("canceled"),clearInterval(a.$$intervalId),delete e[a.$$intervalId],!0):!1};return d}]}function rd(){this.$get=function(){return{id:"en-us",NUMBER_FORMATS:{DECIMAL_SEP:".",GROUP_SEP:",",PATTERNS:[{minInt:1,minFrac:0,maxFrac:3,posPre:"",posSuf:"",negPre:"-",negSuf:"",gSize:3,lgSize:3},{minInt:1,minFrac:2,
        maxFrac:2,posPre:"\u00a4",posSuf:"",negPre:"(\u00a4",negSuf:")",gSize:3,lgSize:3}],CURRENCY_SYM:"$"},DATETIME_FORMATS:{MONTH:"January February March April May June July August September October November December".split(" "),SHORTMONTH:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),DAY:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),SHORTDAY:"Sun Mon Tue Wed Thu Fri Sat".split(" "),AMPMS:["AM","PM"],medium:"MMM d, y h:mm:ss a","short":"M/d/yy h:mm a",fullDate:"EEEE, MMMM d, y",
        longDate:"MMMM d, y",mediumDate:"MMM d, y",shortDate:"M/d/yy",mediumTime:"h:mm:ss a",shortTime:"h:mm a"},pluralCat:function(b){return 1===b?"one":"other"}}}}function pc(b){b=b.split("/");for(var a=b.length;a--;)b[a]=sb(b[a]);return b.join("/")}function qc(b,a,c){b=xa(b,c);a.$$protocol=b.protocol;a.$$host=b.hostname;a.$$port=V(b.port)||sd[b.protocol]||null}function rc(b,a,c){var d="/"!==b.charAt(0);d&&(b="/"+b);b=xa(b,c);a.$$path=decodeURIComponent(d&&"/"===b.pathname.charAt(0)?b.pathname.substring(1):
        b.pathname);a.$$search=Vb(b.search);a.$$hash=decodeURIComponent(b.hash);a.$$path&&"/"!=a.$$path.charAt(0)&&(a.$$path="/"+a.$$path)}function na(b,a){if(0===a.indexOf(b))return a.substr(b.length)}function Wa(b){var a=b.indexOf("#");return-1==a?b:b.substr(0,a)}function Fb(b){return b.substr(0,Wa(b).lastIndexOf("/")+1)}function sc(b,a){this.$$html5=!0;a=a||"";var c=Fb(b);qc(b,this,b);this.$$parse=function(a){var e=na(c,a);if(!w(e))throw Gb("ipthprfx",a,c);rc(e,this,b);this.$$path||(this.$$path="/");this.$$compose()};
        this.$$compose=function(){var a=Wb(this.$$search),b=this.$$hash?"#"+sb(this.$$hash):"";this.$$url=pc(this.$$path)+(a?"?"+a:"")+b;this.$$absUrl=c+this.$$url.substr(1)};this.$$rewrite=function(d){var e;if((e=na(b,d))!==s)return d=e,(e=na(a,e))!==s?c+(na("/",e)||e):b+d;if((e=na(c,d))!==s)return c+e;if(c==d+"/")return c}}function Hb(b,a){var c=Fb(b);qc(b,this,b);this.$$parse=function(d){var e=na(b,d)||na(c,d),e="#"==e.charAt(0)?na(a,e):this.$$html5?e:"";if(!w(e))throw Gb("ihshprfx",d,a);rc(e,this,b);
        d=this.$$path;var g=/^\/?.*?:(\/.*)/;0===e.indexOf(b)&&(e=e.replace(b,""));g.exec(e)||(d=(e=g.exec(d))?e[1]:d);this.$$path=d;this.$$compose()};this.$$compose=function(){var c=Wb(this.$$search),e=this.$$hash?"#"+sb(this.$$hash):"";this.$$url=pc(this.$$path)+(c?"?"+c:"")+e;this.$$absUrl=b+(this.$$url?a+this.$$url:"")};this.$$rewrite=function(a){if(Wa(b)==Wa(a))return a}}function tc(b,a){this.$$html5=!0;Hb.apply(this,arguments);var c=Fb(b);this.$$rewrite=function(d){var e;if(b==Wa(d))return d;if(e=na(c,
        d))return b+a+e;if(c===d+"/")return c}}function ib(b){return function(){return this[b]}}function uc(b,a){return function(c){if(u(c))return this[b];this[b]=a(c);this.$$compose();return this}}function td(){var b="",a=!1;this.hashPrefix=function(a){return D(a)?(b=a,this):b};this.html5Mode=function(b){return D(b)?(a=b,this):a};this.$get=["$rootScope","$browser","$sniffer","$rootElement",function(c,d,e,g){function f(a){c.$broadcast("$locationChangeSuccess",h.absUrl(),a)}var h,m=d.baseHref(),k=d.url();
        a?(m=k.substring(0,k.indexOf("/",k.indexOf("//")+2))+(m||"/"),e=e.history?sc:tc):(m=Wa(k),e=Hb);h=new e(m,"#"+b);h.$$parse(h.$$rewrite(k));g.on("click",function(a){if(!a.ctrlKey&&!a.metaKey&&2!=a.which){for(var b=z(a.target);"a"!==x(b[0].nodeName);)if(b[0]===g[0]||!(b=b.parent())[0])return;var e=b.prop("href");W(e)&&"[object SVGAnimatedString]"===e.toString()&&(e=xa(e.animVal).href);var f=h.$$rewrite(e);e&&(!b.attr("target")&&f&&!a.isDefaultPrevented())&&(a.preventDefault(),f!=d.url()&&(h.$$parse(f),
            c.$apply(),P.angular["ff-684208-preventDefault"]=!0))}});h.absUrl()!=k&&d.url(h.absUrl(),!0);d.onUrlChange(function(a){h.absUrl()!=a&&(c.$evalAsync(function(){var b=h.absUrl();h.$$parse(a);c.$broadcast("$locationChangeStart",a,b).defaultPrevented?(h.$$parse(b),d.url(b)):f(b)}),c.$$phase||c.$digest())});var l=0;c.$watch(function(){var a=d.url(),b=h.$$replace;l&&a==h.absUrl()||(l++,c.$evalAsync(function(){c.$broadcast("$locationChangeStart",h.absUrl(),a).defaultPrevented?h.$$parse(a):(d.url(h.absUrl(),
            b),f(a))}));h.$$replace=!1;return l});return h}]}function ud(){var b=!0,a=this;this.debugEnabled=function(a){return D(a)?(b=a,this):b};this.$get=["$window",function(c){function d(a){a instanceof Error&&(a.stack?a=a.message&&-1===a.stack.indexOf(a.message)?"Error: "+a.message+"\n"+a.stack:a.stack:a.sourceURL&&(a=a.message+"\n"+a.sourceURL+":"+a.line));return a}function e(a){var b=c.console||{},e=b[a]||b.log||E;a=!1;try{a=!!e.apply}catch(m){}return a?function(){var a=[];q(arguments,function(b){a.push(d(b))});
        return e.apply(b,a)}:function(a,b){e(a,null==b?"":b)}}return{log:e("log"),info:e("info"),warn:e("warn"),error:e("error"),debug:function(){var c=e("debug");return function(){b&&c.apply(a,arguments)}}()}}]}function da(b,a){if("constructor"===b)throw ya("isecfld",a);return b}function Xa(b,a){if(b){if(b.constructor===b)throw ya("isecfn",a);if(b.document&&b.location&&b.alert&&b.setInterval)throw ya("isecwindow",a);if(b.children&&(b.nodeName||b.on&&b.find))throw ya("isecdom",a);}return b}function jb(b,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           a,c,d,e){e=e||{};a=a.split(".");for(var g,f=0;1<a.length;f++){g=da(a.shift(),d);var h=b[g];h||(h={},b[g]=h);b=h;b.then&&e.unwrapPromises&&(qa(d),"$$v"in b||function(a){a.then(function(b){a.$$v=b})}(b),b.$$v===s&&(b.$$v={}),b=b.$$v)}g=da(a.shift(),d);return b[g]=c}function vc(b,a,c,d,e,g,f){da(b,g);da(a,g);da(c,g);da(d,g);da(e,g);return f.unwrapPromises?function(f,m){var k=m&&m.hasOwnProperty(b)?m:f,l;if(null==k)return k;(k=k[b])&&k.then&&(qa(g),"$$v"in k||(l=k,l.$$v=s,l.then(function(a){l.$$v=a})),
        k=k.$$v);if(!a)return k;if(null==k)return s;(k=k[a])&&k.then&&(qa(g),"$$v"in k||(l=k,l.$$v=s,l.then(function(a){l.$$v=a})),k=k.$$v);if(!c)return k;if(null==k)return s;(k=k[c])&&k.then&&(qa(g),"$$v"in k||(l=k,l.$$v=s,l.then(function(a){l.$$v=a})),k=k.$$v);if(!d)return k;if(null==k)return s;(k=k[d])&&k.then&&(qa(g),"$$v"in k||(l=k,l.$$v=s,l.then(function(a){l.$$v=a})),k=k.$$v);if(!e)return k;if(null==k)return s;(k=k[e])&&k.then&&(qa(g),"$$v"in k||(l=k,l.$$v=s,l.then(function(a){l.$$v=a})),k=k.$$v);
        return k}:function(g,f){var k=f&&f.hasOwnProperty(b)?f:g;if(null==k)return k;k=k[b];if(!a)return k;if(null==k)return s;k=k[a];if(!c)return k;if(null==k)return s;k=k[c];if(!d)return k;if(null==k)return s;k=k[d];return e?null==k?s:k=k[e]:k}}function vd(b,a){da(b,a);return function(a,d){return null==a?s:(d&&d.hasOwnProperty(b)?d:a)[b]}}function wd(b,a,c){da(b,c);da(a,c);return function(c,e){if(null==c)return s;c=(e&&e.hasOwnProperty(b)?e:c)[b];return null==c?s:c[a]}}function wc(b,a,c){if(Ib.hasOwnProperty(b))return Ib[b];
        var d=b.split("."),e=d.length,g;if(a.unwrapPromises||1!==e)if(a.unwrapPromises||2!==e)if(a.csp)g=6>e?vc(d[0],d[1],d[2],d[3],d[4],c,a):function(b,g){var f=0,h;do h=vc(d[f++],d[f++],d[f++],d[f++],d[f++],c,a)(b,g),g=s,b=h;while(f<e);return h};else{var f="var p;\n";q(d,function(b,d){da(b,c);f+="if(s == null) return undefined;\ns="+(d?"s":'((k&&k.hasOwnProperty("'+b+'"))?k:s)')+'["'+b+'"];\n'+(a.unwrapPromises?'if (s && s.then) {\n pw("'+c.replace(/(["\r\n])/g,"\\$1")+'");\n if (!("$$v" in s)) {\n p=s;\n p.$$v = undefined;\n p.then(function(v) {p.$$v=v;});\n}\n s=s.$$v\n}\n':
            "")});var f=f+"return s;",h=new Function("s","k","pw",f);h.toString=Y(f);g=a.unwrapPromises?function(a,b){return h(a,b,qa)}:h}else g=wd(d[0],d[1],c);else g=vd(d[0],c);"hasOwnProperty"!==b&&(Ib[b]=g);return g}function xd(){var b={},a={csp:!1,unwrapPromises:!1,logPromiseWarnings:!0};this.unwrapPromises=function(b){return D(b)?(a.unwrapPromises=!!b,this):a.unwrapPromises};this.logPromiseWarnings=function(b){return D(b)?(a.logPromiseWarnings=b,this):a.logPromiseWarnings};this.$get=["$filter","$sniffer",
        "$log",function(c,d,e){a.csp=d.csp;qa=function(b){a.logPromiseWarnings&&!xc.hasOwnProperty(b)&&(xc[b]=!0,e.warn("[$parse] Promise found in the expression `"+b+"`. Automatic unwrapping of promises in Angular expressions is deprecated."))};return function(d){var e;switch(typeof d){case "string":if(b.hasOwnProperty(d))return b[d];e=new Jb(a);e=(new Ya(e,c,a)).parse(d,!1);"hasOwnProperty"!==d&&(b[d]=e);return e;case "function":return d;default:return E}}}]}function yd(){this.$get=["$rootScope","$exceptionHandler",
        function(b,a){return zd(function(a){b.$evalAsync(a)},a)}]}function zd(b,a){function c(a){return a}function d(a){return f(a)}var e=function(){var f=[],k,l;return l={resolve:function(a){if(f){var c=f;f=s;k=g(a);c.length&&b(function(){for(var a,b=0,d=c.length;b<d;b++)a=c[b],k.then(a[0],a[1],a[2])})}},reject:function(a){l.resolve(h(a))},notify:function(a){if(f){var c=f;f.length&&b(function(){for(var b,d=0,e=c.length;d<e;d++)b=c[d],b[2](a)})}},promise:{then:function(b,g,h){var l=e(),A=function(d){try{l.resolve((M(b)?
        b:c)(d))}catch(e){l.reject(e),a(e)}},H=function(b){try{l.resolve((M(g)?g:d)(b))}catch(c){l.reject(c),a(c)}},v=function(b){try{l.notify((M(h)?h:c)(b))}catch(d){a(d)}};f?f.push([A,H,v]):k.then(A,H,v);return l.promise},"catch":function(a){return this.then(null,a)},"finally":function(a){function b(a,c){var d=e();c?d.resolve(a):d.reject(a);return d.promise}function d(e,g){var f=null;try{f=(a||c)()}catch(k){return b(k,!1)}return f&&M(f.then)?f.then(function(){return b(e,g)},function(a){return b(a,!1)}):
        b(e,g)}return this.then(function(a){return d(a,!0)},function(a){return d(a,!1)})}}}},g=function(a){return a&&M(a.then)?a:{then:function(c){var d=e();b(function(){d.resolve(c(a))});return d.promise}}},f=function(a){var b=e();b.reject(a);return b.promise},h=function(c){return{then:function(g,f){var h=e();b(function(){try{h.resolve((M(f)?f:d)(c))}catch(b){h.reject(b),a(b)}});return h.promise}}};return{defer:e,reject:f,when:function(h,k,l,n){var p=e(),r,F=function(b){try{return(M(k)?k:c)(b)}catch(d){return a(d),
        f(d)}},A=function(b){try{return(M(l)?l:d)(b)}catch(c){return a(c),f(c)}},q=function(b){try{return(M(n)?n:c)(b)}catch(d){a(d)}};b(function(){g(h).then(function(a){r||(r=!0,p.resolve(g(a).then(F,A,q)))},function(a){r||(r=!0,p.resolve(A(a)))},function(a){r||p.notify(q(a))})});return p.promise},all:function(a){var b=e(),c=0,d=L(a)?[]:{};q(a,function(a,e){c++;g(a).then(function(a){d.hasOwnProperty(e)||(d[e]=a,--c||b.resolve(d))},function(a){d.hasOwnProperty(e)||b.reject(a)})});0===c&&b.resolve(d);return b.promise}}}
    function Ad(){var b=10,a=t("$rootScope"),c=null;this.digestTtl=function(a){arguments.length&&(b=a);return b};this.$get=["$injector","$exceptionHandler","$parse","$browser",function(d,e,g,f){function h(){this.$id=Za();this.$$phase=this.$parent=this.$$watchers=this.$$nextSibling=this.$$prevSibling=this.$$childHead=this.$$childTail=null;this["this"]=this.$root=this;this.$$destroyed=!1;this.$$asyncQueue=[];this.$$postDigestQueue=[];this.$$listeners={};this.$$listenerCount={};this.$$isolateBindings={}}
        function m(b){if(p.$$phase)throw a("inprog",p.$$phase);p.$$phase=b}function k(a,b){var c=g(a);Pa(c,b);return c}function l(a,b,c){do a.$$listenerCount[c]-=b,0===a.$$listenerCount[c]&&delete a.$$listenerCount[c];while(a=a.$parent)}function n(){}h.prototype={constructor:h,$new:function(a){a?(a=new h,a.$root=this.$root,a.$$asyncQueue=this.$$asyncQueue,a.$$postDigestQueue=this.$$postDigestQueue):(a=function(){},a.prototype=this,a=new a,a.$id=Za());a["this"]=a;a.$$listeners={};a.$$listenerCount={};a.$parent=
            this;a.$$watchers=a.$$nextSibling=a.$$childHead=a.$$childTail=null;a.$$prevSibling=this.$$childTail;this.$$childHead?this.$$childTail=this.$$childTail.$$nextSibling=a:this.$$childHead=this.$$childTail=a;return a},$watch:function(a,b,d){var e=k(a,"watch"),g=this.$$watchers,f={fn:b,last:n,get:e,exp:a,eq:!!d};c=null;if(!M(b)){var h=k(b||E,"listener");f.fn=function(a,b,c){h(c)}}if("string"==typeof a&&e.constant){var m=f.fn;f.fn=function(a,b,c){m.call(this,a,b,c);Ma(g,f)}}g||(g=this.$$watchers=[]);g.unshift(f);
            return function(){Ma(g,f);c=null}},$watchCollection:function(a,b){var c=this,d,e,f=0,h=g(a),k=[],m={},l=0;return this.$watch(function(){e=h(c);var a,b;if(W(e))if(qb(e))for(d!==k&&(d=k,l=d.length=0,f++),a=e.length,l!==a&&(f++,d.length=l=a),b=0;b<a;b++)d[b]!==e[b]&&(f++,d[b]=e[b]);else{d!==m&&(d=m={},l=0,f++);a=0;for(b in e)e.hasOwnProperty(b)&&(a++,d.hasOwnProperty(b)?d[b]!==e[b]&&(f++,d[b]=e[b]):(l++,d[b]=e[b],f++));if(l>a)for(b in f++,d)d.hasOwnProperty(b)&&!e.hasOwnProperty(b)&&(l--,delete d[b])}else d!==
            e&&(d=e,f++);return f},function(){b(e,d,c)})},$digest:function(){var d,f,g,h,k=this.$$asyncQueue,l=this.$$postDigestQueue,q,C,s=b,I,U=[],t,z,B;m("$digest");c=null;do{C=!1;for(I=this;k.length;){try{B=k.shift(),B.scope.$eval(B.expression)}catch(D){p.$$phase=null,e(D)}c=null}a:do{if(h=I.$$watchers)for(q=h.length;q--;)try{if(d=h[q])if((f=d.get(I))!==(g=d.last)&&!(d.eq?ta(f,g):"number"==typeof f&&"number"==typeof g&&isNaN(f)&&isNaN(g)))C=!0,c=d,d.last=d.eq?$(f):f,d.fn(f,g===n?f:g,I),5>s&&(t=4-s,U[t]||
            (U[t]=[]),z=M(d.exp)?"fn: "+(d.exp.name||d.exp.toString()):d.exp,z+="; newVal: "+pa(f)+"; oldVal: "+pa(g),U[t].push(z));else if(d===c){C=!1;break a}}catch(y){p.$$phase=null,e(y)}if(!(h=I.$$childHead||I!==this&&I.$$nextSibling))for(;I!==this&&!(h=I.$$nextSibling);)I=I.$parent}while(I=h);if((C||k.length)&&!s--)throw p.$$phase=null,a("infdig",b,pa(U));}while(C||k.length);for(p.$$phase=null;l.length;)try{l.shift()()}catch(w){e(w)}},$destroy:function(){if(!this.$$destroyed){var a=this.$parent;this.$broadcast("$destroy");
            this.$$destroyed=!0;this!==p&&(q(this.$$listenerCount,bb(null,l,this)),a.$$childHead==this&&(a.$$childHead=this.$$nextSibling),a.$$childTail==this&&(a.$$childTail=this.$$prevSibling),this.$$prevSibling&&(this.$$prevSibling.$$nextSibling=this.$$nextSibling),this.$$nextSibling&&(this.$$nextSibling.$$prevSibling=this.$$prevSibling),this.$parent=this.$$nextSibling=this.$$prevSibling=this.$$childHead=this.$$childTail=null)}},$eval:function(a,b){return g(a)(this,b)},$evalAsync:function(a){p.$$phase||p.$$asyncQueue.length||
        f.defer(function(){p.$$asyncQueue.length&&p.$digest()});this.$$asyncQueue.push({scope:this,expression:a})},$$postDigest:function(a){this.$$postDigestQueue.push(a)},$apply:function(a){try{return m("$apply"),this.$eval(a)}catch(b){e(b)}finally{p.$$phase=null;try{p.$digest()}catch(c){throw e(c),c;}}},$on:function(a,b){var c=this.$$listeners[a];c||(this.$$listeners[a]=c=[]);c.push(b);var d=this;do d.$$listenerCount[a]||(d.$$listenerCount[a]=0),d.$$listenerCount[a]++;while(d=d.$parent);var e=this;return function(){c[ab(c,
            b)]=null;l(e,1,a)}},$emit:function(a,b){var c=[],d,f=this,g=!1,h={name:a,targetScope:f,stopPropagation:function(){g=!0},preventDefault:function(){h.defaultPrevented=!0},defaultPrevented:!1},k=[h].concat(ua.call(arguments,1)),m,l;do{d=f.$$listeners[a]||c;h.currentScope=f;m=0;for(l=d.length;m<l;m++)if(d[m])try{d[m].apply(null,k)}catch(p){e(p)}else d.splice(m,1),m--,l--;if(g)break;f=f.$parent}while(f);return h},$broadcast:function(a,b){for(var c=this,d=this,f={name:a,targetScope:this,preventDefault:function(){f.defaultPrevented=
            !0},defaultPrevented:!1},g=[f].concat(ua.call(arguments,1)),h,k;c=d;){f.currentScope=c;d=c.$$listeners[a]||[];h=0;for(k=d.length;h<k;h++)if(d[h])try{d[h].apply(null,g)}catch(m){e(m)}else d.splice(h,1),h--,k--;if(!(d=c.$$listenerCount[a]&&c.$$childHead||c!==this&&c.$$nextSibling))for(;c!==this&&!(d=c.$$nextSibling);)c=c.$parent}return f}};var p=new h;return p}]}function Bd(){var b=/^\s*(https?|ftp|mailto|tel|file):/,a=/^\s*(https?|ftp|file):|data:image\//;this.aHrefSanitizationWhitelist=function(a){return D(a)?
        (b=a,this):b};this.imgSrcSanitizationWhitelist=function(b){return D(b)?(a=b,this):a};this.$get=function(){return function(c,d){var e=d?a:b,g;if(!N||8<=N)if(g=xa(c).href,""!==g&&!g.match(e))return"unsafe:"+g;return c}}}function Cd(b){if("self"===b)return b;if(w(b)){if(-1<b.indexOf("***"))throw ra("iwcard",b);b=b.replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g,"\\$1").replace(/\x08/g,"\\x08").replace("\\*\\*",".*").replace("\\*","[^:/.?&;]*");return RegExp("^"+b+"$")}if($a(b))return RegExp("^"+b.source+"$");
        throw ra("imatcher");}function yc(b){var a=[];D(b)&&q(b,function(b){a.push(Cd(b))});return a}function Dd(){this.SCE_CONTEXTS=ea;var b=["self"],a=[];this.resourceUrlWhitelist=function(a){arguments.length&&(b=yc(a));return b};this.resourceUrlBlacklist=function(b){arguments.length&&(a=yc(b));return a};this.$get=["$injector",function(c){function d(a){var b=function(a){this.$$unwrapTrustedValue=function(){return a}};a&&(b.prototype=new a);b.prototype.valueOf=function(){return this.$$unwrapTrustedValue()};
        b.prototype.toString=function(){return this.$$unwrapTrustedValue().toString()};return b}var e=function(a){throw ra("unsafe");};c.has("$sanitize")&&(e=c.get("$sanitize"));var g=d(),f={};f[ea.HTML]=d(g);f[ea.CSS]=d(g);f[ea.URL]=d(g);f[ea.JS]=d(g);f[ea.RESOURCE_URL]=d(f[ea.URL]);return{trustAs:function(a,b){var c=f.hasOwnProperty(a)?f[a]:null;if(!c)throw ra("icontext",a,b);if(null===b||b===s||""===b)return b;if("string"!==typeof b)throw ra("itype",a);return new c(b)},getTrusted:function(c,d){if(null===
        d||d===s||""===d)return d;var g=f.hasOwnProperty(c)?f[c]:null;if(g&&d instanceof g)return d.$$unwrapTrustedValue();if(c===ea.RESOURCE_URL){var g=xa(d.toString()),l,n,p=!1;l=0;for(n=b.length;l<n;l++)if("self"===b[l]?Eb(g):b[l].exec(g.href)){p=!0;break}if(p)for(l=0,n=a.length;l<n;l++)if("self"===a[l]?Eb(g):a[l].exec(g.href)){p=!1;break}if(p)return d;throw ra("insecurl",d.toString());}if(c===ea.HTML)return e(d);throw ra("unsafe");},valueOf:function(a){return a instanceof g?a.$$unwrapTrustedValue():a}}}]}
    function Ed(){var b=!0;this.enabled=function(a){arguments.length&&(b=!!a);return b};this.$get=["$parse","$sniffer","$sceDelegate",function(a,c,d){if(b&&c.msie&&8>c.msieDocumentMode)throw ra("iequirks");var e=$(ea);e.isEnabled=function(){return b};e.trustAs=d.trustAs;e.getTrusted=d.getTrusted;e.valueOf=d.valueOf;b||(e.trustAs=e.getTrusted=function(a,b){return b},e.valueOf=Aa);e.parseAs=function(b,c){var d=a(c);return d.literal&&d.constant?d:function(a,c){return e.getTrusted(b,d(a,c))}};var g=e.parseAs,
        f=e.getTrusted,h=e.trustAs;q(ea,function(a,b){var c=x(b);e[Qa("parse_as_"+c)]=function(b){return g(a,b)};e[Qa("get_trusted_"+c)]=function(b){return f(a,b)};e[Qa("trust_as_"+c)]=function(b){return h(a,b)}});return e}]}function Fd(){this.$get=["$window","$document",function(b,a){var c={},d=V((/android (\d+)/.exec(x((b.navigator||{}).userAgent))||[])[1]),e=/Boxee/i.test((b.navigator||{}).userAgent),g=a[0]||{},f=g.documentMode,h,m=/^(Moz|webkit|O|ms)(?=[A-Z])/,k=g.body&&g.body.style,l=!1,n=!1;if(k){for(var p in k)if(l=
        m.exec(p)){h=l[0];h=h.substr(0,1).toUpperCase()+h.substr(1);break}h||(h="WebkitOpacity"in k&&"webkit");l=!!("transition"in k||h+"Transition"in k);n=!!("animation"in k||h+"Animation"in k);!d||l&&n||(l=w(g.body.style.webkitTransition),n=w(g.body.style.webkitAnimation))}return{history:!(!b.history||!b.history.pushState||4>d||e),hashchange:"onhashchange"in b&&(!f||7<f),hasEvent:function(a){if("input"==a&&9==N)return!1;if(u(c[a])){var b=g.createElement("div");c[a]="on"+a in b}return c[a]},csp:Sb(),vendorPrefix:h,
        transitions:l,animations:n,android:d,msie:N,msieDocumentMode:f}}]}function Gd(){this.$get=["$rootScope","$browser","$q","$exceptionHandler",function(b,a,c,d){function e(e,h,m){var k=c.defer(),l=k.promise,n=D(m)&&!m;h=a.defer(function(){try{k.resolve(e())}catch(a){k.reject(a),d(a)}finally{delete g[l.$$timeoutId]}n||b.$apply()},h);l.$$timeoutId=h;g[h]=k;return l}var g={};e.cancel=function(b){return b&&b.$$timeoutId in g?(g[b.$$timeoutId].reject("canceled"),delete g[b.$$timeoutId],a.defer.cancel(b.$$timeoutId)):
        !1};return e}]}function xa(b,a){var c=b;N&&(T.setAttribute("href",c),c=T.href);T.setAttribute("href",c);return{href:T.href,protocol:T.protocol?T.protocol.replace(/:$/,""):"",host:T.host,search:T.search?T.search.replace(/^\?/,""):"",hash:T.hash?T.hash.replace(/^#/,""):"",hostname:T.hostname,port:T.port,pathname:"/"===T.pathname.charAt(0)?T.pathname:"/"+T.pathname}}function Eb(b){b=w(b)?xa(b):b;return b.protocol===zc.protocol&&b.host===zc.host}function Hd(){this.$get=Y(P)}function Ac(b){function a(d,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             e){if(W(d)){var g={};q(d,function(b,c){g[c]=a(c,b)});return g}return b.factory(d+c,e)}var c="Filter";this.register=a;this.$get=["$injector",function(a){return function(b){return a.get(b+c)}}];a("currency",Bc);a("date",Cc);a("filter",Id);a("json",Jd);a("limitTo",Kd);a("lowercase",Ld);a("number",Dc);a("orderBy",Ec);a("uppercase",Md)}function Id(){return function(b,a,c){if(!L(b))return b;var d=typeof c,e=[];e.check=function(a){for(var b=0;b<e.length;b++)if(!e[b](a))return!1;return!0};"function"!==d&&
    (c="boolean"===d&&c?function(a,b){return Ba.equals(a,b)}:function(a,b){b=(""+b).toLowerCase();return-1<(""+a).toLowerCase().indexOf(b)});var g=function(a,b){if("string"==typeof b&&"!"===b.charAt(0))return!g(a,b.substr(1));switch(typeof a){case "boolean":case "number":case "string":return c(a,b);case "object":switch(typeof b){case "object":return c(a,b);default:for(var d in a)if("$"!==d.charAt(0)&&g(a[d],b))return!0}return!1;case "array":for(d=0;d<a.length;d++)if(g(a[d],b))return!0;return!1;default:return!1}};
        switch(typeof a){case "boolean":case "number":case "string":a={$:a};case "object":for(var f in a)(function(b){"undefined"!=typeof a[b]&&e.push(function(c){return g("$"==b?c:c&&c[b],a[b])})})(f);break;case "function":e.push(a);break;default:return b}d=[];for(f=0;f<b.length;f++){var h=b[f];e.check(h)&&d.push(h)}return d}}function Bc(b){var a=b.NUMBER_FORMATS;return function(b,d){u(d)&&(d=a.CURRENCY_SYM);return Fc(b,a.PATTERNS[1],a.GROUP_SEP,a.DECIMAL_SEP,2).replace(/\u00A4/g,d)}}function Dc(b){var a=
        b.NUMBER_FORMATS;return function(b,d){return Fc(b,a.PATTERNS[0],a.GROUP_SEP,a.DECIMAL_SEP,d)}}function Fc(b,a,c,d,e){if(isNaN(b)||!isFinite(b))return"";var g=0>b;b=Math.abs(b);var f=b+"",h="",m=[],k=!1;if(-1!==f.indexOf("e")){var l=f.match(/([\d\.]+)e(-?)(\d+)/);l&&"-"==l[2]&&l[3]>e+1?f="0":(h=f,k=!0)}if(k)0<e&&(-1<b&&1>b)&&(h=b.toFixed(e));else{f=(f.split(Gc)[1]||"").length;u(e)&&(e=Math.min(Math.max(a.minFrac,f),a.maxFrac));f=Math.pow(10,e);b=Math.round(b*f)/f;b=(""+b).split(Gc);f=b[0];b=b[1]||
        "";var l=0,n=a.lgSize,p=a.gSize;if(f.length>=n+p)for(l=f.length-n,k=0;k<l;k++)0===(l-k)%p&&0!==k&&(h+=c),h+=f.charAt(k);for(k=l;k<f.length;k++)0===(f.length-k)%n&&0!==k&&(h+=c),h+=f.charAt(k);for(;b.length<e;)b+="0";e&&"0"!==e&&(h+=d+b.substr(0,e))}m.push(g?a.negPre:a.posPre);m.push(h);m.push(g?a.negSuf:a.posSuf);return m.join("")}function Kb(b,a,c){var d="";0>b&&(d="-",b=-b);for(b=""+b;b.length<a;)b="0"+b;c&&(b=b.substr(b.length-a));return d+b}function X(b,a,c,d){c=c||0;return function(e){e=e["get"+
        b]();if(0<c||e>-c)e+=c;0===e&&-12==c&&(e=12);return Kb(e,a,d)}}function kb(b,a){return function(c,d){var e=c["get"+b](),g=Ha(a?"SHORT"+b:b);return d[g][e]}}function Cc(b){function a(a){var b;if(b=a.match(c)){a=new Date(0);var g=0,f=0,h=b[8]?a.setUTCFullYear:a.setFullYear,m=b[8]?a.setUTCHours:a.setHours;b[9]&&(g=V(b[9]+b[10]),f=V(b[9]+b[11]));h.call(a,V(b[1]),V(b[2])-1,V(b[3]));g=V(b[4]||0)-g;f=V(b[5]||0)-f;h=V(b[6]||0);b=Math.round(1E3*parseFloat("0."+(b[7]||0)));m.call(a,g,f,h,b)}return a}var c=
        /^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?::?(\d\d)(?::?(\d\d)(?:\.(\d+))?)?)?(Z|([+-])(\d\d):?(\d\d))?)?$/;return function(c,e){var g="",f=[],h,m;e=e||"mediumDate";e=b.DATETIME_FORMATS[e]||e;w(c)&&(c=Nd.test(c)?V(c):a(c));rb(c)&&(c=new Date(c));if(!Ka(c))return c;for(;e;)(m=Od.exec(e))?(f=f.concat(ua.call(m,1)),e=f.pop()):(f.push(e),e=null);q(f,function(a){h=Pd[a];g+=h?h(c,b.DATETIME_FORMATS):a.replace(/(^'|'$)/g,"").replace(/''/g,"'")});return g}}function Jd(){return function(b){return pa(b,!0)}}function Kd(){return function(b,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        a){if(!L(b)&&!w(b))return b;a=V(a);if(w(b))return a?0<=a?b.slice(0,a):b.slice(a,b.length):"";var c=[],d,e;a>b.length?a=b.length:a<-b.length&&(a=-b.length);0<a?(d=0,e=a):(d=b.length+a,e=b.length);for(;d<e;d++)c.push(b[d]);return c}}function Ec(b){return function(a,c,d){function e(a,b){return Oa(b)?function(b,c){return a(c,b)}:a}if(!L(a)||!c)return a;c=L(c)?c:[c];c=Qc(c,function(a){var c=!1,d=a||Aa;if(w(a)){if("+"==a.charAt(0)||"-"==a.charAt(0))c="-"==a.charAt(0),a=a.substring(1);d=b(a)}return e(function(a,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    b){var c;c=d(a);var e=d(b),f=typeof c,g=typeof e;f==g?("string"==f&&(c=c.toLowerCase(),e=e.toLowerCase()),c=c===e?0:c<e?-1:1):c=f<g?-1:1;return c},c)});for(var g=[],f=0;f<a.length;f++)g.push(a[f]);return g.sort(e(function(a,b){for(var d=0;d<c.length;d++){var e=c[d](a,b);if(0!==e)return e}return 0},d))}}function sa(b){M(b)&&(b={link:b});b.restrict=b.restrict||"AC";return Y(b)}function Hc(b,a){function c(a,c){c=c?"-"+cb(c,"-"):"";b.removeClass((a?lb:mb)+c).addClass((a?mb:lb)+c)}var d=this,e=b.parent().controller("form")||
        nb,g=0,f=d.$error={},h=[];d.$name=a.name||a.ngForm;d.$dirty=!1;d.$pristine=!0;d.$valid=!0;d.$invalid=!1;e.$addControl(d);b.addClass(Ia);c(!0);d.$addControl=function(a){wa(a.$name,"input");h.push(a);a.$name&&(d[a.$name]=a)};d.$removeControl=function(a){a.$name&&d[a.$name]===a&&delete d[a.$name];q(f,function(b,c){d.$setValidity(c,!0,a)});Ma(h,a)};d.$setValidity=function(a,b,h){var n=f[a];if(b)n&&(Ma(n,h),n.length||(g--,g||(c(b),d.$valid=!0,d.$invalid=!1),f[a]=!1,c(!0,a),e.$setValidity(a,!0,d)));else{g||
    c(b);if(n){if(-1!=ab(n,h))return}else f[a]=n=[],g++,c(!1,a),e.$setValidity(a,!1,d);n.push(h);d.$valid=!1;d.$invalid=!0}};d.$setDirty=function(){b.removeClass(Ia).addClass(ob);d.$dirty=!0;d.$pristine=!1;e.$setDirty()};d.$setPristine=function(){b.removeClass(ob).addClass(Ia);d.$dirty=!1;d.$pristine=!0;q(h,function(a){a.$setPristine()})}}function oa(b,a,c,d){b.$setValidity(a,c);return c?d:s}function pb(b,a,c,d,e,g){if(!e.android){var f=!1;a.on("compositionstart",function(a){f=!0});a.on("compositionend",
        function(){f=!1})}var h=function(){if(!f){var e=a.val();Oa(c.ngTrim||"T")&&(e=Z(e));d.$viewValue!==e&&(b.$$phase?d.$setViewValue(e):b.$apply(function(){d.$setViewValue(e)}))}};if(e.hasEvent("input"))a.on("input",h);else{var m,k=function(){m||(m=g.defer(function(){h();m=null}))};a.on("keydown",function(a){a=a.keyCode;91===a||(15<a&&19>a||37<=a&&40>=a)||k()});if(e.hasEvent("paste"))a.on("paste cut",k)}a.on("change",h);d.$render=function(){a.val(d.$isEmpty(d.$viewValue)?"":d.$viewValue)};var l=c.ngPattern;
        l&&((e=l.match(/^\/(.*)\/([gim]*)$/))?(l=RegExp(e[1],e[2]),e=function(a){return oa(d,"pattern",d.$isEmpty(a)||l.test(a),a)}):e=function(c){var e=b.$eval(l);if(!e||!e.test)throw t("ngPattern")("noregexp",l,e,fa(a));return oa(d,"pattern",d.$isEmpty(c)||e.test(c),c)},d.$formatters.push(e),d.$parsers.push(e));if(c.ngMinlength){var n=V(c.ngMinlength);e=function(a){return oa(d,"minlength",d.$isEmpty(a)||a.length>=n,a)};d.$parsers.push(e);d.$formatters.push(e)}if(c.ngMaxlength){var p=V(c.ngMaxlength);e=
            function(a){return oa(d,"maxlength",d.$isEmpty(a)||a.length<=p,a)};d.$parsers.push(e);d.$formatters.push(e)}}function Lb(b,a){b="ngClass"+b;return function(){return{restrict:"AC",link:function(c,d,e){function g(b){if(!0===a||c.$index%2===a){var d=f(b||"");h?ta(b,h)||e.$updateClass(d,f(h)):e.$addClass(d)}h=$(b)}function f(a){if(L(a))return a.join(" ");if(W(a)){var b=[];q(a,function(a,c){a&&b.push(c)});return b.join(" ")}return a}var h;c.$watch(e[b],g,!0);e.$observe("class",function(a){g(c.$eval(e[b]))});
        "ngClass"!==b&&c.$watch("$index",function(d,g){var h=d&1;if(h!==g&1){var n=f(c.$eval(e[b]));h===a?e.$addClass(n):e.$removeClass(n)}})}}}}var x=function(b){return w(b)?b.toLowerCase():b},Ha=function(b){return w(b)?b.toUpperCase():b},N,z,Ca,ua=[].slice,Qd=[].push,La=Object.prototype.toString,Na=t("ng"),Ba=P.angular||(P.angular={}),Ua,Ga,ia=["0","0","0"];N=V((/msie (\d+)/.exec(x(navigator.userAgent))||[])[1]);isNaN(N)&&(N=V((/trident\/.*; rv:(\d+)/.exec(x(navigator.userAgent))||[])[1]));E.$inject=[];
    Aa.$inject=[];var Z=function(){return String.prototype.trim?function(b){return w(b)?b.trim():b}:function(b){return w(b)?b.replace(/^\s\s*/,"").replace(/\s\s*$/,""):b}}();Ga=9>N?function(b){b=b.nodeName?b:b[0];return b.scopeName&&"HTML"!=b.scopeName?Ha(b.scopeName+":"+b.nodeName):b.nodeName}:function(b){return b.nodeName?b.nodeName:b[0].nodeName};var Tc=/[A-Z]/g,Rd={full:"1.2.12",major:1,minor:2,dot:12,codeName:"cauliflower-eradication"},Ra=O.cache={},db=O.expando="ng-"+(new Date).getTime(),Xc=1,Ic=
        P.document.addEventListener?function(b,a,c){b.addEventListener(a,c,!1)}:function(b,a,c){b.attachEvent("on"+a,c)},zb=P.document.removeEventListener?function(b,a,c){b.removeEventListener(a,c,!1)}:function(b,a,c){b.detachEvent("on"+a,c)},Vc=/([\:\-\_]+(.))/g,Wc=/^moz([A-Z])/,wb=t("jqLite"),Fa=O.prototype={ready:function(b){function a(){c||(c=!0,b())}var c=!1;"complete"===R.readyState?setTimeout(a):(this.on("DOMContentLoaded",a),O(P).on("load",a))},toString:function(){var b=[];q(this,function(a){b.push(""+
        a)});return"["+b.join(", ")+"]"},eq:function(b){return 0<=b?z(this[b]):z(this[this.length+b])},length:0,push:Qd,sort:[].sort,splice:[].splice},fb={};q("multiple selected checked disabled readOnly required open".split(" "),function(b){fb[x(b)]=b});var fc={};q("input select option textarea button form details".split(" "),function(b){fc[Ha(b)]=!0});q({data:bc,inheritedData:eb,scope:function(b){return z(b).data("$scope")||eb(b.parentNode||b,["$isolateScope","$scope"])},isolateScope:function(b){return z(b).data("$isolateScope")||
        z(b).data("$isolateScopeNoTemplate")},controller:cc,injector:function(b){return eb(b,"$injector")},removeAttr:function(b,a){b.removeAttribute(a)},hasClass:Ab,css:function(b,a,c){a=Qa(a);if(D(c))b.style[a]=c;else{var d;8>=N&&(d=b.currentStyle&&b.currentStyle[a],""===d&&(d="auto"));d=d||b.style[a];8>=N&&(d=""===d?s:d);return d}},attr:function(b,a,c){var d=x(a);if(fb[d])if(D(c))c?(b[a]=!0,b.setAttribute(a,d)):(b[a]=!1,b.removeAttribute(d));else return b[a]||(b.attributes.getNamedItem(a)||E).specified?
        d:s;else if(D(c))b.setAttribute(a,c);else if(b.getAttribute)return b=b.getAttribute(a,2),null===b?s:b},prop:function(b,a,c){if(D(c))b[a]=c;else return b[a]},text:function(){function b(b,d){var e=a[b.nodeType];if(u(d))return e?b[e]:"";b[e]=d}var a=[];9>N?(a[1]="innerText",a[3]="nodeValue"):a[1]=a[3]="textContent";b.$dv="";return b}(),val:function(b,a){if(u(a)){if("SELECT"===Ga(b)&&b.multiple){var c=[];q(b.options,function(a){a.selected&&c.push(a.value||a.text)});return 0===c.length?null:c}return b.value}b.value=
        a},html:function(b,a){if(u(a))return b.innerHTML;for(var c=0,d=b.childNodes;c<d.length;c++)Da(d[c]);b.innerHTML=a},empty:dc},function(b,a){O.prototype[a]=function(a,d){var e,g;if(b!==dc&&(2==b.length&&b!==Ab&&b!==cc?a:d)===s){if(W(a)){for(e=0;e<this.length;e++)if(b===bc)b(this[e],a);else for(g in a)b(this[e],g,a[g]);return this}e=b.$dv;g=e===s?Math.min(this.length,1):this.length;for(var f=0;f<g;f++){var h=b(this[f],a,d);e=e?e+h:h}return e}for(e=0;e<this.length;e++)b(this[e],a,d);return this}});q({removeData:$b,
        dealoc:Da,on:function a(c,d,e,g){if(D(g))throw wb("onargs");var f=ja(c,"events"),h=ja(c,"handle");f||ja(c,"events",f={});h||ja(c,"handle",h=Yc(c,f));q(d.split(" "),function(d){var g=f[d];if(!g){if("mouseenter"==d||"mouseleave"==d){var l=R.body.contains||R.body.compareDocumentPosition?function(a,c){var d=9===a.nodeType?a.documentElement:a,e=c&&c.parentNode;return a===e||!!(e&&1===e.nodeType&&(d.contains?d.contains(e):a.compareDocumentPosition&&a.compareDocumentPosition(e)&16))}:function(a,c){if(c)for(;c=
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      c.parentNode;)if(c===a)return!0;return!1};f[d]=[];a(c,{mouseleave:"mouseout",mouseenter:"mouseover"}[d],function(a){var c=a.relatedTarget;c&&(c===this||l(this,c))||h(a,d)})}else Ic(c,d,h),f[d]=[];g=f[d]}g.push(e)})},off:ac,one:function(a,c,d){a=z(a);a.on(c,function g(){a.off(c,d);a.off(c,g)});a.on(c,d)},replaceWith:function(a,c){var d,e=a.parentNode;Da(a);q(new O(c),function(c){d?e.insertBefore(c,d.nextSibling):e.replaceChild(c,a);d=c})},children:function(a){var c=[];q(a.childNodes,function(a){1===
            a.nodeType&&c.push(a)});return c},contents:function(a){return a.childNodes||[]},append:function(a,c){q(new O(c),function(c){1!==a.nodeType&&11!==a.nodeType||a.appendChild(c)})},prepend:function(a,c){if(1===a.nodeType){var d=a.firstChild;q(new O(c),function(c){a.insertBefore(c,d)})}},wrap:function(a,c){c=z(c)[0];var d=a.parentNode;d&&d.replaceChild(c,a);c.appendChild(a)},remove:function(a){Da(a);var c=a.parentNode;c&&c.removeChild(a)},after:function(a,c){var d=a,e=a.parentNode;q(new O(c),function(a){e.insertBefore(a,
            d.nextSibling);d=a})},addClass:Cb,removeClass:Bb,toggleClass:function(a,c,d){u(d)&&(d=!Ab(a,c));(d?Cb:Bb)(a,c)},parent:function(a){return(a=a.parentNode)&&11!==a.nodeType?a:null},next:function(a){if(a.nextElementSibling)return a.nextElementSibling;for(a=a.nextSibling;null!=a&&1!==a.nodeType;)a=a.nextSibling;return a},find:function(a,c){return a.getElementsByTagName?a.getElementsByTagName(c):[]},clone:yb,triggerHandler:function(a,c,d){c=(ja(a,"events")||{})[c];d=d||[];var e=[{preventDefault:E,stopPropagation:E}];
            q(c,function(c){c.apply(a,e.concat(d))})}},function(a,c){O.prototype[c]=function(c,e,g){for(var f,h=0;h<this.length;h++)u(f)?(f=a(this[h],c,e,g),D(f)&&(f=z(f))):xb(f,a(this[h],c,e,g));return D(f)?f:this};O.prototype.bind=O.prototype.on;O.prototype.unbind=O.prototype.off});Sa.prototype={put:function(a,c){this[Ea(a)]=c},get:function(a){return this[Ea(a)]},remove:function(a){var c=this[a=Ea(a)];delete this[a];return c}};var $c=/^function\s*[^\(]*\(\s*([^\)]*)\)/m,ad=/,/,bd=/^\s*(_?)(\S+?)\1\s*$/,Zc=
        /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg,Ta=t("$injector"),Sd=t("$animate"),Td=["$provide",function(a){this.$$selectors={};this.register=function(c,d){var e=c+"-animation";if(c&&"."!=c.charAt(0))throw Sd("notcsel",c);this.$$selectors[c.substr(1)]=e;a.factory(e,d)};this.classNameFilter=function(a){1===arguments.length&&(this.$$classNameFilter=a instanceof RegExp?a:null);return this.$$classNameFilter};this.$get=["$timeout",function(a){return{enter:function(d,e,g,f){g?g.after(d):(e&&e[0]||(e=g.parent()),e.append(d));
        f&&a(f,0,!1)},leave:function(d,e){d.remove();e&&a(e,0,!1)},move:function(a,c,g,f){this.enter(a,c,g,f)},addClass:function(d,e,g){e=w(e)?e:L(e)?e.join(" "):"";q(d,function(a){Cb(a,e)});g&&a(g,0,!1)},removeClass:function(d,e,g){e=w(e)?e:L(e)?e.join(" "):"";q(d,function(a){Bb(a,e)});g&&a(g,0,!1)},enabled:E}}]}],ha=t("$compile");ic.$inject=["$provide","$$sanitizeUriProvider"];var hd=/^(x[\:\-_]|data[\:\-_])/i,oc=t("$interpolate"),Ud=/^([^\?#]*)(\?([^#]*))?(#(.*))?$/,sd={http:80,https:443,ftp:21},Gb=t("$location");
    tc.prototype=Hb.prototype=sc.prototype={$$html5:!1,$$replace:!1,absUrl:ib("$$absUrl"),url:function(a,c){if(u(a))return this.$$url;var d=Ud.exec(a);d[1]&&this.path(decodeURIComponent(d[1]));(d[2]||d[1])&&this.search(d[3]||"");this.hash(d[5]||"",c);return this},protocol:ib("$$protocol"),host:ib("$$host"),port:ib("$$port"),path:uc("$$path",function(a){return"/"==a.charAt(0)?a:"/"+a}),search:function(a,c){switch(arguments.length){case 0:return this.$$search;case 1:if(w(a))this.$$search=Vb(a);else if(W(a))this.$$search=
        a;else throw Gb("isrcharg");break;default:u(c)||null===c?delete this.$$search[a]:this.$$search[a]=c}this.$$compose();return this},hash:uc("$$hash",Aa),replace:function(){this.$$replace=!0;return this}};var ya=t("$parse"),xc={},qa,Ja={"null":function(){return null},"true":function(){return!0},"false":function(){return!1},undefined:E,"+":function(a,c,d,e){d=d(a,c);e=e(a,c);return D(d)?D(e)?d+e:d:D(e)?e:s},"-":function(a,c,d,e){d=d(a,c);e=e(a,c);return(D(d)?d:0)-(D(e)?e:0)},"*":function(a,c,d,e){return d(a,
        c)*e(a,c)},"/":function(a,c,d,e){return d(a,c)/e(a,c)},"%":function(a,c,d,e){return d(a,c)%e(a,c)},"^":function(a,c,d,e){return d(a,c)^e(a,c)},"=":E,"===":function(a,c,d,e){return d(a,c)===e(a,c)},"!==":function(a,c,d,e){return d(a,c)!==e(a,c)},"==":function(a,c,d,e){return d(a,c)==e(a,c)},"!=":function(a,c,d,e){return d(a,c)!=e(a,c)},"<":function(a,c,d,e){return d(a,c)<e(a,c)},">":function(a,c,d,e){return d(a,c)>e(a,c)},"<=":function(a,c,d,e){return d(a,c)<=e(a,c)},">=":function(a,c,d,e){return d(a,
        c)>=e(a,c)},"&&":function(a,c,d,e){return d(a,c)&&e(a,c)},"||":function(a,c,d,e){return d(a,c)||e(a,c)},"&":function(a,c,d,e){return d(a,c)&e(a,c)},"|":function(a,c,d,e){return e(a,c)(a,c,d(a,c))},"!":function(a,c,d){return!d(a,c)}},Vd={n:"\n",f:"\f",r:"\r",t:"\t",v:"\v","'":"'",'"':'"'},Jb=function(a){this.options=a};Jb.prototype={constructor:Jb,lex:function(a){this.text=a;this.index=0;this.ch=s;this.lastCh=":";this.tokens=[];var c;for(a=[];this.index<this.text.length;){this.ch=this.text.charAt(this.index);
        if(this.is("\"'"))this.readString(this.ch);else if(this.isNumber(this.ch)||this.is(".")&&this.isNumber(this.peek()))this.readNumber();else if(this.isIdent(this.ch))this.readIdent(),this.was("{,")&&("{"===a[0]&&(c=this.tokens[this.tokens.length-1]))&&(c.json=-1===c.text.indexOf("."));else if(this.is("(){}[].,;:?"))this.tokens.push({index:this.index,text:this.ch,json:this.was(":[,")&&this.is("{[")||this.is("}]:,")}),this.is("{[")&&a.unshift(this.ch),this.is("}]")&&a.shift(),this.index++;else if(this.isWhitespace(this.ch)){this.index++;
            continue}else{var d=this.ch+this.peek(),e=d+this.peek(2),g=Ja[this.ch],f=Ja[d],h=Ja[e];h?(this.tokens.push({index:this.index,text:e,fn:h}),this.index+=3):f?(this.tokens.push({index:this.index,text:d,fn:f}),this.index+=2):g?(this.tokens.push({index:this.index,text:this.ch,fn:g,json:this.was("[,:")&&this.is("+-")}),this.index+=1):this.throwError("Unexpected next character ",this.index,this.index+1)}this.lastCh=this.ch}return this.tokens},is:function(a){return-1!==a.indexOf(this.ch)},was:function(a){return-1!==
        a.indexOf(this.lastCh)},peek:function(a){a=a||1;return this.index+a<this.text.length?this.text.charAt(this.index+a):!1},isNumber:function(a){return"0"<=a&&"9">=a},isWhitespace:function(a){return" "===a||"\r"===a||"\t"===a||"\n"===a||"\v"===a||"\u00a0"===a},isIdent:function(a){return"a"<=a&&"z">=a||"A"<=a&&"Z">=a||"_"===a||"$"===a},isExpOperator:function(a){return"-"===a||"+"===a||this.isNumber(a)},throwError:function(a,c,d){d=d||this.index;c=D(c)?"s "+c+"-"+this.index+" ["+this.text.substring(c,d)+
        "]":" "+d;throw ya("lexerr",a,c,this.text);},readNumber:function(){for(var a="",c=this.index;this.index<this.text.length;){var d=x(this.text.charAt(this.index));if("."==d||this.isNumber(d))a+=d;else{var e=this.peek();if("e"==d&&this.isExpOperator(e))a+=d;else if(this.isExpOperator(d)&&e&&this.isNumber(e)&&"e"==a.charAt(a.length-1))a+=d;else if(!this.isExpOperator(d)||e&&this.isNumber(e)||"e"!=a.charAt(a.length-1))break;else this.throwError("Invalid exponent")}this.index++}a*=1;this.tokens.push({index:c,
        text:a,json:!0,fn:function(){return a}})},readIdent:function(){for(var a=this,c="",d=this.index,e,g,f,h;this.index<this.text.length;){h=this.text.charAt(this.index);if("."===h||this.isIdent(h)||this.isNumber(h))"."===h&&(e=this.index),c+=h;else break;this.index++}if(e)for(g=this.index;g<this.text.length;){h=this.text.charAt(g);if("("===h){f=c.substr(e-d+1);c=c.substr(0,e-d);this.index=g;break}if(this.isWhitespace(h))g++;else break}d={index:d,text:c};if(Ja.hasOwnProperty(c))d.fn=Ja[c],d.json=Ja[c];
    else{var m=wc(c,this.options,this.text);d.fn=y(function(a,c){return m(a,c)},{assign:function(d,e){return jb(d,c,e,a.text,a.options)}})}this.tokens.push(d);f&&(this.tokens.push({index:e,text:".",json:!1}),this.tokens.push({index:e+1,text:f,json:!1}))},readString:function(a){var c=this.index;this.index++;for(var d="",e=a,g=!1;this.index<this.text.length;){var f=this.text.charAt(this.index),e=e+f;if(g)"u"===f?(f=this.text.substring(this.index+1,this.index+5),f.match(/[\da-f]{4}/i)||this.throwError("Invalid unicode escape [\\u"+
        f+"]"),this.index+=4,d+=String.fromCharCode(parseInt(f,16))):d=(g=Vd[f])?d+g:d+f,g=!1;else if("\\"===f)g=!0;else{if(f===a){this.index++;this.tokens.push({index:c,text:e,string:d,json:!0,fn:function(){return d}});return}d+=f}this.index++}this.throwError("Unterminated quote",c)}};var Ya=function(a,c,d){this.lexer=a;this.$filter=c;this.options=d};Ya.ZERO=function(){return 0};Ya.prototype={constructor:Ya,parse:function(a,c){this.text=a;this.json=c;this.tokens=this.lexer.lex(a);c&&(this.assignment=this.logicalOR,
        this.functionCall=this.fieldAccess=this.objectIndex=this.filterChain=function(){this.throwError("is not valid json",{text:a,index:0})});var d=c?this.primary():this.statements();0!==this.tokens.length&&this.throwError("is an unexpected token",this.tokens[0]);d.literal=!!d.literal;d.constant=!!d.constant;return d},primary:function(){var a;if(this.expect("("))a=this.filterChain(),this.consume(")");else if(this.expect("["))a=this.arrayDeclaration();else if(this.expect("{"))a=this.object();else{var c=
        this.expect();(a=c.fn)||this.throwError("not a primary expression",c);c.json&&(a.constant=!0,a.literal=!0)}for(var d;c=this.expect("(","[",".");)"("===c.text?(a=this.functionCall(a,d),d=null):"["===c.text?(d=a,a=this.objectIndex(a)):"."===c.text?(d=a,a=this.fieldAccess(a)):this.throwError("IMPOSSIBLE");return a},throwError:function(a,c){throw ya("syntax",c.text,a,c.index+1,this.text,this.text.substring(c.index));},peekToken:function(){if(0===this.tokens.length)throw ya("ueoe",this.text);return this.tokens[0]},
        peek:function(a,c,d,e){if(0<this.tokens.length){var g=this.tokens[0],f=g.text;if(f===a||f===c||f===d||f===e||!(a||c||d||e))return g}return!1},expect:function(a,c,d,e){return(a=this.peek(a,c,d,e))?(this.json&&!a.json&&this.throwError("is not valid json",a),this.tokens.shift(),a):!1},consume:function(a){this.expect(a)||this.throwError("is unexpected, expecting ["+a+"]",this.peek())},unaryFn:function(a,c){return y(function(d,e){return a(d,e,c)},{constant:c.constant})},ternaryFn:function(a,c,d){return y(function(e,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          g){return a(e,g)?c(e,g):d(e,g)},{constant:a.constant&&c.constant&&d.constant})},binaryFn:function(a,c,d){return y(function(e,g){return c(e,g,a,d)},{constant:a.constant&&d.constant})},statements:function(){for(var a=[];;)if(0<this.tokens.length&&!this.peek("}",")",";","]")&&a.push(this.filterChain()),!this.expect(";"))return 1===a.length?a[0]:function(c,d){for(var e,g=0;g<a.length;g++){var f=a[g];f&&(e=f(c,d))}return e}},filterChain:function(){for(var a=this.expression(),c;;)if(c=this.expect("|"))a=
            this.binaryFn(a,c.fn,this.filter());else return a},filter:function(){for(var a=this.expect(),c=this.$filter(a.text),d=[];;)if(a=this.expect(":"))d.push(this.expression());else{var e=function(a,e,h){h=[h];for(var m=0;m<d.length;m++)h.push(d[m](a,e));return c.apply(a,h)};return function(){return e}}},expression:function(){return this.assignment()},assignment:function(){var a=this.ternary(),c,d;return(d=this.expect("="))?(a.assign||this.throwError("implies assignment but ["+this.text.substring(0,d.index)+
            "] can not be assigned to",d),c=this.ternary(),function(d,g){return a.assign(d,c(d,g),g)}):a},ternary:function(){var a=this.logicalOR(),c,d;if(this.expect("?")){c=this.ternary();if(d=this.expect(":"))return this.ternaryFn(a,c,this.ternary());this.throwError("expected :",d)}else return a},logicalOR:function(){for(var a=this.logicalAND(),c;;)if(c=this.expect("||"))a=this.binaryFn(a,c.fn,this.logicalAND());else return a},logicalAND:function(){var a=this.equality(),c;if(c=this.expect("&&"))a=this.binaryFn(a,
            c.fn,this.logicalAND());return a},equality:function(){var a=this.relational(),c;if(c=this.expect("==","!=","===","!=="))a=this.binaryFn(a,c.fn,this.equality());return a},relational:function(){var a=this.additive(),c;if(c=this.expect("<",">","<=",">="))a=this.binaryFn(a,c.fn,this.relational());return a},additive:function(){for(var a=this.multiplicative(),c;c=this.expect("+","-");)a=this.binaryFn(a,c.fn,this.multiplicative());return a},multiplicative:function(){for(var a=this.unary(),c;c=this.expect("*",
            "/","%");)a=this.binaryFn(a,c.fn,this.unary());return a},unary:function(){var a;return this.expect("+")?this.primary():(a=this.expect("-"))?this.binaryFn(Ya.ZERO,a.fn,this.unary()):(a=this.expect("!"))?this.unaryFn(a.fn,this.unary()):this.primary()},fieldAccess:function(a){var c=this,d=this.expect().text,e=wc(d,this.options,this.text);return y(function(c,d,h){return e(h||a(c,d))},{assign:function(e,f,h){return jb(a(e,h),d,f,c.text,c.options)}})},objectIndex:function(a){var c=this,d=this.expression();
            this.consume("]");return y(function(e,g){var f=a(e,g),h=d(e,g),m;if(!f)return s;(f=Xa(f[h],c.text))&&(f.then&&c.options.unwrapPromises)&&(m=f,"$$v"in f||(m.$$v=s,m.then(function(a){m.$$v=a})),f=f.$$v);return f},{assign:function(e,g,f){var h=d(e,f);return Xa(a(e,f),c.text)[h]=g}})},functionCall:function(a,c){var d=[];if(")"!==this.peekToken().text){do d.push(this.expression());while(this.expect(","))}this.consume(")");var e=this;return function(g,f){for(var h=[],m=c?c(g,f):g,k=0;k<d.length;k++)h.push(d[k](g,
            f));k=a(g,f,m)||E;Xa(m,e.text);Xa(k,e.text);h=k.apply?k.apply(m,h):k(h[0],h[1],h[2],h[3],h[4]);return Xa(h,e.text)}},arrayDeclaration:function(){var a=[],c=!0;if("]"!==this.peekToken().text){do{var d=this.expression();a.push(d);d.constant||(c=!1)}while(this.expect(","))}this.consume("]");return y(function(c,d){for(var f=[],h=0;h<a.length;h++)f.push(a[h](c,d));return f},{literal:!0,constant:c})},object:function(){var a=[],c=!0;if("}"!==this.peekToken().text){do{var d=this.expect(),d=d.string||d.text;
            this.consume(":");var e=this.expression();a.push({key:d,value:e});e.constant||(c=!1)}while(this.expect(","))}this.consume("}");return y(function(c,d){for(var e={},m=0;m<a.length;m++){var k=a[m];e[k.key]=k.value(c,d)}return e},{literal:!0,constant:c})}};var Ib={},ra=t("$sce"),ea={HTML:"html",CSS:"css",URL:"url",RESOURCE_URL:"resourceUrl",JS:"js"},T=R.createElement("a"),zc=xa(P.location.href,!0);Ac.$inject=["$provide"];Bc.$inject=["$locale"];Dc.$inject=["$locale"];var Gc=".",Pd={yyyy:X("FullYear",4),
        yy:X("FullYear",2,0,!0),y:X("FullYear",1),MMMM:kb("Month"),MMM:kb("Month",!0),MM:X("Month",2,1),M:X("Month",1,1),dd:X("Date",2),d:X("Date",1),HH:X("Hours",2),H:X("Hours",1),hh:X("Hours",2,-12),h:X("Hours",1,-12),mm:X("Minutes",2),m:X("Minutes",1),ss:X("Seconds",2),s:X("Seconds",1),sss:X("Milliseconds",3),EEEE:kb("Day"),EEE:kb("Day",!0),a:function(a,c){return 12>a.getHours()?c.AMPMS[0]:c.AMPMS[1]},Z:function(a){a=-1*a.getTimezoneOffset();return a=(0<=a?"+":"")+(Kb(Math[0<a?"floor":"ceil"](a/60),2)+
            Kb(Math.abs(a%60),2))}},Od=/((?:[^yMdHhmsaZE']+)|(?:'(?:[^']|'')*')|(?:E+|y+|M+|d+|H+|h+|m+|s+|a|Z))(.*)/,Nd=/^\-?\d+$/;Cc.$inject=["$locale"];var Ld=Y(x),Md=Y(Ha);Ec.$inject=["$parse"];var Wd=Y({restrict:"E",compile:function(a,c){8>=N&&(c.href||c.name||c.$set("href",""),a.append(R.createComment("IE fix")));if(!c.href&&!c.xlinkHref&&!c.name)return function(a,c){var g="[object SVGAnimatedString]"===La.call(c.prop("href"))?"xlink:href":"href";c.on("click",function(a){c.attr(g)||a.preventDefault()})}}}),
        Mb={};q(fb,function(a,c){if("multiple"!=a){var d=la("ng-"+c);Mb[d]=function(){return{priority:100,link:function(a,g,f){a.$watch(f[d],function(a){f.$set(c,!!a)})}}}}});q(["src","srcset","href"],function(a){var c=la("ng-"+a);Mb[c]=function(){return{priority:99,link:function(d,e,g){g.$observe(c,function(c){c&&(g.$set(a,c),N&&e.prop(a,g[a]))})}}}});var nb={$addControl:E,$removeControl:E,$setValidity:E,$setDirty:E,$setPristine:E};Hc.$inject=["$element","$attrs","$scope"];var Jc=function(a){return["$timeout",
            function(c){return{name:"form",restrict:a?"EAC":"E",controller:Hc,compile:function(){return{pre:function(a,e,g,f){if(!g.action){var h=function(a){a.preventDefault?a.preventDefault():a.returnValue=!1};Ic(e[0],"submit",h);e.on("$destroy",function(){c(function(){zb(e[0],"submit",h)},0,!1)})}var m=e.parent().controller("form"),k=g.name||g.ngForm;k&&jb(a,k,f,k);if(m)e.on("$destroy",function(){m.$removeControl(f);k&&jb(a,k,s,k);y(f,nb)})}}}}}]},Xd=Jc(),Yd=Jc(!0),Zd=/^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,
        $d=/^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/i,ae=/^\s*(\-|\+)?(\d+|(\d*(\.\d*)))\s*$/,Kc={text:pb,number:function(a,c,d,e,g,f){pb(a,c,d,e,g,f);e.$parsers.push(function(a){var c=e.$isEmpty(a);if(c||ae.test(a))return e.$setValidity("number",!0),""===a?null:c?a:parseFloat(a);e.$setValidity("number",!1);return s});e.$formatters.push(function(a){return e.$isEmpty(a)?"":""+a});d.min&&(a=function(a){var c=parseFloat(d.min);return oa(e,"min",e.$isEmpty(a)||a>=c,a)},e.$parsers.push(a),e.$formatters.push(a));
            d.max&&(a=function(a){var c=parseFloat(d.max);return oa(e,"max",e.$isEmpty(a)||a<=c,a)},e.$parsers.push(a),e.$formatters.push(a));e.$formatters.push(function(a){return oa(e,"number",e.$isEmpty(a)||rb(a),a)})},url:function(a,c,d,e,g,f){pb(a,c,d,e,g,f);a=function(a){return oa(e,"url",e.$isEmpty(a)||Zd.test(a),a)};e.$formatters.push(a);e.$parsers.push(a)},email:function(a,c,d,e,g,f){pb(a,c,d,e,g,f);a=function(a){return oa(e,"email",e.$isEmpty(a)||$d.test(a),a)};e.$formatters.push(a);e.$parsers.push(a)},
            radio:function(a,c,d,e){u(d.name)&&c.attr("name",Za());c.on("click",function(){c[0].checked&&a.$apply(function(){e.$setViewValue(d.value)})});e.$render=function(){c[0].checked=d.value==e.$viewValue};d.$observe("value",e.$render)},checkbox:function(a,c,d,e){var g=d.ngTrueValue,f=d.ngFalseValue;w(g)||(g=!0);w(f)||(f=!1);c.on("click",function(){a.$apply(function(){e.$setViewValue(c[0].checked)})});e.$render=function(){c[0].checked=e.$viewValue};e.$isEmpty=function(a){return a!==g};e.$formatters.push(function(a){return a===
                g});e.$parsers.push(function(a){return a?g:f})},hidden:E,button:E,submit:E,reset:E},Lc=["$browser","$sniffer",function(a,c){return{restrict:"E",require:"?ngModel",link:function(d,e,g,f){f&&(Kc[x(g.type)]||Kc.text)(d,e,g,f,c,a)}}}],mb="ng-valid",lb="ng-invalid",Ia="ng-pristine",ob="ng-dirty",be=["$scope","$exceptionHandler","$attrs","$element","$parse",function(a,c,d,e,g){function f(a,c){c=c?"-"+cb(c,"-"):"";e.removeClass((a?lb:mb)+c).addClass((a?mb:lb)+c)}this.$modelValue=this.$viewValue=Number.NaN;
            this.$parsers=[];this.$formatters=[];this.$viewChangeListeners=[];this.$pristine=!0;this.$dirty=!1;this.$valid=!0;this.$invalid=!1;this.$name=d.name;var h=g(d.ngModel),m=h.assign;if(!m)throw t("ngModel")("nonassign",d.ngModel,fa(e));this.$render=E;this.$isEmpty=function(a){return u(a)||""===a||null===a||a!==a};var k=e.inheritedData("$formController")||nb,l=0,n=this.$error={};e.addClass(Ia);f(!0);this.$setValidity=function(a,c){n[a]!==!c&&(c?(n[a]&&l--,l||(f(!0),this.$valid=!0,this.$invalid=!1)):(f(!1),
                this.$invalid=!0,this.$valid=!1,l++),n[a]=!c,f(c,a),k.$setValidity(a,c,this))};this.$setPristine=function(){this.$dirty=!1;this.$pristine=!0;e.removeClass(ob).addClass(Ia)};this.$setViewValue=function(d){this.$viewValue=d;this.$pristine&&(this.$dirty=!0,this.$pristine=!1,e.removeClass(Ia).addClass(ob),k.$setDirty());q(this.$parsers,function(a){d=a(d)});this.$modelValue!==d&&(this.$modelValue=d,m(a,d),q(this.$viewChangeListeners,function(a){try{a()}catch(d){c(d)}}))};var p=this;a.$watch(function(){var c=
                h(a);if(p.$modelValue!==c){var d=p.$formatters,e=d.length;for(p.$modelValue=c;e--;)c=d[e](c);p.$viewValue!==c&&(p.$viewValue=c,p.$render())}return c})}],ce=function(){return{require:["ngModel","^?form"],controller:be,link:function(a,c,d,e){var g=e[0],f=e[1]||nb;f.$addControl(g);a.$on("$destroy",function(){f.$removeControl(g)})}}},de=Y({require:"ngModel",link:function(a,c,d,e){e.$viewChangeListeners.push(function(){a.$eval(d.ngChange)})}}),Mc=function(){return{require:"?ngModel",link:function(a,c,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 d,e){if(e){d.required=!0;var g=function(a){if(d.required&&e.$isEmpty(a))e.$setValidity("required",!1);else return e.$setValidity("required",!0),a};e.$formatters.push(g);e.$parsers.unshift(g);d.$observe("required",function(){g(e.$viewValue)})}}}},ee=function(){return{require:"ngModel",link:function(a,c,d,e){var g=(a=/\/(.*)\//.exec(d.ngList))&&RegExp(a[1])||d.ngList||",";e.$parsers.push(function(a){if(!u(a)){var c=[];a&&q(a.split(g),function(a){a&&c.push(Z(a))});return c}});e.$formatters.push(function(a){return L(a)?
            a.join(", "):s});e.$isEmpty=function(a){return!a||!a.length}}}},fe=/^(true|false|\d+)$/,ge=function(){return{priority:100,compile:function(a,c){return fe.test(c.ngValue)?function(a,c,g){g.$set("value",a.$eval(g.ngValue))}:function(a,c,g){a.$watch(g.ngValue,function(a){g.$set("value",a)})}}}},he=sa(function(a,c,d){c.addClass("ng-binding").data("$binding",d.ngBind);a.$watch(d.ngBind,function(a){c.text(a==s?"":a)})}),ie=["$interpolate",function(a){return function(c,d,e){c=a(d.attr(e.$attr.ngBindTemplate));
            d.addClass("ng-binding").data("$binding",c);e.$observe("ngBindTemplate",function(a){d.text(a)})}}],je=["$sce","$parse",function(a,c){return function(d,e,g){e.addClass("ng-binding").data("$binding",g.ngBindHtml);var f=c(g.ngBindHtml);d.$watch(function(){return(f(d)||"").toString()},function(c){e.html(a.getTrustedHtml(f(d))||"")})}}],ke=Lb("",!0),le=Lb("Odd",0),me=Lb("Even",1),ne=sa({compile:function(a,c){c.$set("ngCloak",s);a.removeClass("ng-cloak")}}),oe=[function(){return{scope:!0,controller:"@",
            priority:500}}],Nc={};q("click dblclick mousedown mouseup mouseover mouseout mousemove mouseenter mouseleave keydown keyup keypress submit focus blur copy cut paste".split(" "),function(a){var c=la("ng-"+a);Nc[c]=["$parse",function(d){return{compile:function(e,g){var f=d(g[c]);return function(c,d,e){d.on(x(a),function(a){c.$apply(function(){f(c,{$event:a})})})}}}}]});var pe=["$animate",function(a){return{transclude:"element",priority:600,terminal:!0,restrict:"A",$$tlb:!0,link:function(c,d,e,g,f){var h,
        m;c.$watch(e.ngIf,function(g){Oa(g)?m||(m=c.$new(),f(m,function(c){c[c.length++]=R.createComment(" end ngIf: "+e.ngIf+" ");h={clone:c};a.enter(c,d.parent(),d)})):(m&&(m.$destroy(),m=null),h&&(a.leave(ub(h.clone)),h=null))})}}}],qe=["$http","$templateCache","$anchorScroll","$animate","$sce",function(a,c,d,e,g){return{restrict:"ECA",priority:400,terminal:!0,transclude:"element",controller:Ba.noop,compile:function(f,h){var m=h.ngInclude||h.src,k=h.onload||"",l=h.autoscroll;return function(f,h,q,s,A){var t=
        0,v,z,K=function(){v&&(v.$destroy(),v=null);z&&(e.leave(z),z=null)};f.$watch(g.parseAsResourceUrl(m),function(g){var m=function(){!D(l)||l&&!f.$eval(l)||d()},q=++t;g?(a.get(g,{cache:c}).success(function(a){if(q===t){var c=f.$new();s.template=a;a=A(c,function(a){K();e.enter(a,null,h,m)});v=c;z=a;v.$emit("$includeContentLoaded");f.$eval(k)}}).error(function(){q===t&&K()}),f.$emit("$includeContentRequested")):(K(),s.template=null)})}}}}],re=["$compile",function(a){return{restrict:"ECA",priority:-400,
        require:"ngInclude",link:function(c,d,e,g){d.html(g.template);a(d.contents())(c)}}}],se=sa({priority:450,compile:function(){return{pre:function(a,c,d){a.$eval(d.ngInit)}}}}),te=sa({terminal:!0,priority:1E3}),ue=["$locale","$interpolate",function(a,c){var d=/{}/g;return{restrict:"EA",link:function(e,g,f){var h=f.count,m=f.$attr.when&&g.attr(f.$attr.when),k=f.offset||0,l=e.$eval(m)||{},n={},p=c.startSymbol(),r=c.endSymbol(),s=/^when(Minus)?(.+)$/;q(f,function(a,c){s.test(c)&&(l[x(c.replace("when","").replace("Minus",
        "-"))]=g.attr(f.$attr[c]))});q(l,function(a,e){n[e]=c(a.replace(d,p+h+"-"+k+r))});e.$watch(function(){var c=parseFloat(e.$eval(h));if(isNaN(c))return"";c in l||(c=a.pluralCat(c-k));return n[c](e,g,!0)},function(a){g.text(a)})}}}],ve=["$parse","$animate",function(a,c){var d=t("ngRepeat");return{transclude:"element",priority:1E3,terminal:!0,$$tlb:!0,link:function(e,g,f,h,m){var k=f.ngRepeat,l=k.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/),n,p,r,s,A,t,v={$id:Ea};if(!l)throw d("iexp",
        k);f=l[1];h=l[2];(l=l[3])?(n=a(l),p=function(a,c,d){t&&(v[t]=a);v[A]=c;v.$index=d;return n(e,v)}):(r=function(a,c){return Ea(c)},s=function(a){return a});l=f.match(/^(?:([\$\w]+)|\(([\$\w]+)\s*,\s*([\$\w]+)\))$/);if(!l)throw d("iidexp",f);A=l[3]||l[1];t=l[2];var D={};e.$watchCollection(h,function(a){var f,h,l=g[0],n,v={},y,B,w,u,S,E,x=[];if(qb(a))S=a,n=p||r;else{n=p||s;S=[];for(w in a)a.hasOwnProperty(w)&&"$"!=w.charAt(0)&&S.push(w);S.sort()}y=S.length;h=x.length=S.length;for(f=0;f<h;f++)if(w=a===
        S?f:S[f],u=a[w],u=n(w,u,f),wa(u,"`track by` id"),D.hasOwnProperty(u))E=D[u],delete D[u],v[u]=E,x[f]=E;else{if(v.hasOwnProperty(u))throw q(x,function(a){a&&a.scope&&(D[a.id]=a)}),d("dupes",k,u);x[f]={id:u};v[u]=!1}for(w in D)D.hasOwnProperty(w)&&(E=D[w],f=ub(E.clone),c.leave(f),q(f,function(a){a.$$NG_REMOVED=!0}),E.scope.$destroy());f=0;for(h=S.length;f<h;f++){w=a===S?f:S[f];u=a[w];E=x[f];x[f-1]&&(l=x[f-1].clone[x[f-1].clone.length-1]);if(E.scope){B=E.scope;n=l;do n=n.nextSibling;while(n&&n.$$NG_REMOVED);
        E.clone[0]!=n&&c.move(ub(E.clone),null,z(l));l=E.clone[E.clone.length-1]}else B=e.$new();B[A]=u;t&&(B[t]=w);B.$index=f;B.$first=0===f;B.$last=f===y-1;B.$middle=!(B.$first||B.$last);B.$odd=!(B.$even=0===(f&1));E.scope||m(B,function(a){a[a.length++]=R.createComment(" end ngRepeat: "+k+" ");c.enter(a,null,z(l));l=a;E.scope=B;E.clone=a;v[E.id]=E})}D=v})}}}],we=["$animate",function(a){return function(c,d,e){c.$watch(e.ngShow,function(c){a[Oa(c)?"removeClass":"addClass"](d,"ng-hide")})}}],xe=["$animate",
        function(a){return function(c,d,e){c.$watch(e.ngHide,function(c){a[Oa(c)?"addClass":"removeClass"](d,"ng-hide")})}}],ye=sa(function(a,c,d){a.$watch(d.ngStyle,function(a,d){d&&a!==d&&q(d,function(a,d){c.css(d,"")});a&&c.css(a)},!0)}),ze=["$animate",function(a){return{restrict:"EA",require:"ngSwitch",controller:["$scope",function(){this.cases={}}],link:function(c,d,e,g){var f,h,m=[];c.$watch(e.ngSwitch||e.on,function(d){for(var l=0,n=m.length;l<n;l++)m[l].$destroy(),a.leave(h[l]);h=[];m=[];if(f=g.cases["!"+
        d]||g.cases["?"])c.$eval(e.change),q(f,function(d){var e=c.$new();m.push(e);d.transclude(e,function(c){var e=d.element;h.push(c);a.enter(c,e.parent(),e)})})})}}}],Ae=sa({transclude:"element",priority:800,require:"^ngSwitch",link:function(a,c,d,e,g){e.cases["!"+d.ngSwitchWhen]=e.cases["!"+d.ngSwitchWhen]||[];e.cases["!"+d.ngSwitchWhen].push({transclude:g,element:c})}}),Be=sa({transclude:"element",priority:800,require:"^ngSwitch",link:function(a,c,d,e,g){e.cases["?"]=e.cases["?"]||[];e.cases["?"].push({transclude:g,
        element:c})}}),Ce=sa({controller:["$element","$transclude",function(a,c){if(!c)throw t("ngTransclude")("orphan",fa(a));this.$transclude=c}],link:function(a,c,d,e){e.$transclude(function(a){c.empty();c.append(a)})}}),De=["$templateCache",function(a){return{restrict:"E",terminal:!0,compile:function(c,d){"text/ng-template"==d.type&&a.put(d.id,c[0].text)}}}],Ee=t("ngOptions"),Fe=Y({terminal:!0}),Ge=["$compile","$parse",function(a,c){var d=/^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/,
        e={$setViewValue:E};return{restrict:"E",require:["select","?ngModel"],controller:["$element","$scope","$attrs",function(a,c,d){var m=this,k={},l=e,n;m.databound=d.ngModel;m.init=function(a,c,d){l=a;n=d};m.addOption=function(c){wa(c,'"option value"');k[c]=!0;l.$viewValue==c&&(a.val(c),n.parent()&&n.remove())};m.removeOption=function(a){this.hasOption(a)&&(delete k[a],l.$viewValue==a&&this.renderUnknownOption(a))};m.renderUnknownOption=function(c){c="? "+Ea(c)+" ?";n.val(c);a.prepend(n);a.val(c);n.prop("selected",
        !0)};m.hasOption=function(a){return k.hasOwnProperty(a)};c.$on("$destroy",function(){m.renderUnknownOption=E})}],link:function(e,f,h,m){function k(a,c,d,e){d.$render=function(){var a=d.$viewValue;e.hasOption(a)?(y.parent()&&y.remove(),c.val(a),""===a&&w.prop("selected",!0)):u(a)&&w?c.val(""):e.renderUnknownOption(a)};c.on("change",function(){a.$apply(function(){y.parent()&&y.remove();d.$setViewValue(c.val())})})}function l(a,c,d){var e;d.$render=function(){var a=new Sa(d.$viewValue);q(c.find("option"),
        function(c){c.selected=D(a.get(c.value))})};a.$watch(function(){ta(e,d.$viewValue)||(e=$(d.$viewValue),d.$render())});c.on("change",function(){a.$apply(function(){var a=[];q(c.find("option"),function(c){c.selected&&a.push(c.value)});d.$setViewValue(a)})})}function n(e,f,g){function h(){var a={"":[]},c=[""],d,k,s,t,u;t=g.$modelValue;u=z(e)||[];var C=n?Nb(u):u,F,J,x;J={};s=!1;var B,H;if(r)if(w&&L(t))for(s=new Sa([]),x=0;x<t.length;x++)J[m]=t[x],s.put(w(e,J),t[x]);else s=new Sa(t);for(x=0;F=C.length,
        x<F;x++){k=x;if(n){k=C[x];if("$"===k.charAt(0))continue;J[n]=k}J[m]=u[k];d=p(e,J)||"";(k=a[d])||(k=a[d]=[],c.push(d));r?d=D(s.remove(w?w(e,J):q(e,J))):(w?(d={},d[m]=t,d=w(e,d)===w(e,J)):d=t===q(e,J),s=s||d);B=l(e,J);B=D(B)?B:"";k.push({id:w?w(e,J):n?C[x]:x,label:B,selected:d})}r||(A||null===t?a[""].unshift({id:"",label:"",selected:!s}):s||a[""].unshift({id:"?",label:"",selected:!0}));J=0;for(C=c.length;J<C;J++){d=c[J];k=a[d];y.length<=J?(t={element:E.clone().attr("label",d),label:k.label},u=[t],y.push(u),
        f.append(t.element)):(u=y[J],t=u[0],t.label!=d&&t.element.attr("label",t.label=d));B=null;x=0;for(F=k.length;x<F;x++)s=k[x],(d=u[x+1])?(B=d.element,d.label!==s.label&&B.text(d.label=s.label),d.id!==s.id&&B.val(d.id=s.id),B[0].selected!==s.selected&&B.prop("selected",d.selected=s.selected)):(""===s.id&&A?H=A:(H=v.clone()).val(s.id).attr("selected",s.selected).text(s.label),u.push({element:H,label:s.label,id:s.id,selected:s.selected}),B?B.after(H):t.element.append(H),B=H);for(x++;u.length>x;)u.pop().element.remove()}for(;y.length>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         J;)y.pop()[0].element.remove()}var k;if(!(k=t.match(d)))throw Ee("iexp",t,fa(f));var l=c(k[2]||k[1]),m=k[4]||k[6],n=k[5],p=c(k[3]||""),q=c(k[2]?k[1]:m),z=c(k[7]),w=k[8]?c(k[8]):null,y=[[{element:f,label:""}]];A&&(a(A)(e),A.removeClass("ng-scope"),A.remove());f.empty();f.on("change",function(){e.$apply(function(){var a,c=z(e)||[],d={},h,k,l,p,t,v,u;if(r)for(k=[],p=0,v=y.length;p<v;p++)for(a=y[p],l=1,t=a.length;l<t;l++){if((h=a[l].element)[0].selected){h=h.val();n&&(d[n]=h);if(w)for(u=0;u<c.length&&
        (d[m]=c[u],w(e,d)!=h);u++);else d[m]=c[h];k.push(q(e,d))}}else if(h=f.val(),"?"==h)k=s;else if(""===h)k=null;else if(w)for(u=0;u<c.length;u++){if(d[m]=c[u],w(e,d)==h){k=q(e,d);break}}else d[m]=c[h],n&&(d[n]=h),k=q(e,d);g.$setViewValue(k)})});g.$render=h;e.$watch(h)}if(m[1]){var p=m[0];m=m[1];var r=h.multiple,t=h.ngOptions,A=!1,w,v=z(R.createElement("option")),E=z(R.createElement("optgroup")),y=v.clone();h=0;for(var C=f.children(),x=C.length;h<x;h++)if(""===C[h].value){w=A=C.eq(h);break}p.init(m,A,
        y);r&&(m.$isEmpty=function(a){return!a||0===a.length});t?n(e,f,m):r?l(e,f,m):k(e,f,m,p)}}}}],He=["$interpolate",function(a){var c={addOption:E,removeOption:E};return{restrict:"E",priority:100,compile:function(d,e){if(u(e.value)){var g=a(d.text(),!0);g||e.$set("value",d.text())}return function(a,d,e){var k=d.parent(),l=k.data("$selectController")||k.parent().data("$selectController");l&&l.databound?d.prop("selected",!1):l=c;g?a.$watch(g,function(a,c){e.$set("value",a);a!==c&&l.removeOption(c);l.addOption(a)}):
        l.addOption(e.value);d.on("$destroy",function(){l.removeOption(e.value)})}}}}],Ie=Y({restrict:"E",terminal:!0});(Ca=P.jQuery)?(z=Ca,y(Ca.fn,{scope:Fa.scope,isolateScope:Fa.isolateScope,controller:Fa.controller,injector:Fa.injector,inheritedData:Fa.inheritedData}),vb("remove",!0,!0,!1),vb("empty",!1,!1,!1),vb("html",!1,!1,!0)):z=O;Ba.element=z;(function(a){y(a,{bootstrap:Xb,copy:$,extend:y,equals:ta,element:z,forEach:q,injector:Yb,noop:E,bind:bb,toJson:pa,fromJson:Tb,identity:Aa,isUndefined:u,isDefined:D,
        isString:w,isFunction:M,isObject:W,isNumber:rb,isElement:Pc,isArray:L,version:Rd,isDate:Ka,lowercase:x,uppercase:Ha,callbacks:{counter:0},$$minErr:t,$$csp:Sb});Ua=Uc(P);try{Ua("ngLocale")}catch(c){Ua("ngLocale",[]).provider("$locale",rd)}Ua("ng",["ngLocale"],["$provide",function(a){a.provider({$$sanitizeUri:Bd});a.provider("$compile",ic).directive({a:Wd,input:Lc,textarea:Lc,form:Xd,script:De,select:Ge,style:Ie,option:He,ngBind:he,ngBindHtml:je,ngBindTemplate:ie,ngClass:ke,ngClassEven:me,ngClassOdd:le,
        ngCloak:ne,ngController:oe,ngForm:Yd,ngHide:xe,ngIf:pe,ngInclude:qe,ngInit:se,ngNonBindable:te,ngPluralize:ue,ngRepeat:ve,ngShow:we,ngStyle:ye,ngSwitch:ze,ngSwitchWhen:Ae,ngSwitchDefault:Be,ngOptions:Fe,ngTransclude:Ce,ngModel:ce,ngList:ee,ngChange:de,required:Mc,ngRequired:Mc,ngValue:ge}).directive({ngInclude:re}).directive(Mb).directive(Nc);a.provider({$anchorScroll:cd,$animate:Td,$browser:ed,$cacheFactory:fd,$controller:id,$document:jd,$exceptionHandler:kd,$filter:Ac,$interpolate:pd,$interval:qd,
        $http:ld,$httpBackend:nd,$location:td,$log:ud,$parse:xd,$rootScope:Ad,$q:yd,$sce:Ed,$sceDelegate:Dd,$sniffer:Fd,$templateCache:gd,$timeout:Gd,$window:Hd})}])})(Ba);z(R).ready(function(){Sc(R,Xb)})})(window,document);!angular.$$csp()&&angular.element(document).find("head").prepend('<style type="text/css">@charset "UTF-8";[ng\\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\\:form{display:block;}</style>');
//# sourceMappingURL=angular.min.js.map

/*!
 * ionic.bundle.js is a concatenation of:
 * ionic.js, angular.js, angular-animate.js,
 * angular-ui-router.js, and ionic-angular.js
 */

/*
 AngularJS v1.2.12
 (c) 2010-2014 Google, Inc. http://angularjs.org
 License: MIT
 */
(function(v,k,t){'use strict';k.module("ngAnimate",["ng"]).factory("$$animateReflow",["$window","$timeout",function(k,B){var d=k.requestAnimationFrame||k.webkitRequestAnimationFrame||function(d){return B(d,10,!1)},q=k.cancelAnimationFrame||k.webkitCancelAnimationFrame||function(d){return B.cancel(d)};return function(p){var k=d(p);return function(){q(k)}}}]).config(["$provide","$animateProvider",function(R,B){function d(d){for(var k=0;k<d.length;k++){var p=d[k];if(p.nodeType==X)return p}}var q=k.noop,
    p=k.forEach,$=B.$$selectors,X=1,l="$$ngAnimateState",K="ng-animate",m={running:!0};R.decorator("$animate",["$delegate","$injector","$sniffer","$rootElement","$timeout","$rootScope","$document",function(C,v,t,H,y,w,N){function I(a){if(a){var g=[],e={};a=a.substr(1).split(".");(t.transitions||t.animations)&&a.push("");for(var c=0;c<a.length;c++){var f=a[c],d=$[f];d&&!e[f]&&(g.push(v.get(d)),e[f]=!0)}return g}}function r(a,g,e,c,f,k,m){function t(a){n();if(!0===a)z();else{if(a=e.data(l))a.done=z,e.data(l,
    a);C(D,"after",z)}}function C(c,d,f){"after"==d?r():E();var k=d+"End";p(c,function(b,aa){var h=function(){a:{var b=d+"Complete",a=c[aa];a[b]=!0;(a[k]||q)();for(a=0;a<c.length;a++)if(!c[a][b])break a;f()}};"before"!=d||"enter"!=a&&"move"!=a?b[d]?b[k]=u?b[d](e,g,h):b[d](e,h):h():h()})}function w(c){e.triggerHandler("$animate:"+c,{event:a,className:g})}function E(){y(function(){w("before")},0,!1)}function r(){y(function(){w("after")},0,!1)}function v(){y(function(){w("close");m&&m()},0,!1)}function n(){n.hasBeenRun||
(n.hasBeenRun=!0,k())}function z(){if(!z.hasBeenRun){z.hasBeenRun=!0;var a=e.data(l);a&&(u?A(e):(a.closeAnimationTimeout=y(function(){A(e)},0,!1),e.data(l,a)));v()}}var s,x,G=d(e);G&&(s=G.className,x=s+" "+g);if(G&&L(x)){x=(" "+x).replace(/\s+/g,".");c||(c=f?f.parent():e.parent());x=I(x);var u="addClass"==a||"removeClass"==a;f=e.data(l)||{};if(ba(e,c)||0===x.length)n(),E(),r(),z();else{var D=[];u&&(f.disabled||f.running&&f.structural)||p(x,function(c){if(!c.allowCancel||c.allowCancel(e,a,g)){var d=
    c[a];"leave"==a?(c=d,d=null):c=c["before"+a.charAt(0).toUpperCase()+a.substr(1)];D.push({before:c,after:d})}});0===D.length?(n(),E(),r(),v()):(c=" "+s+" ",f.running&&(y.cancel(f.closeAnimationTimeout),A(e),J(f.animations),x=(s=u&&!f.structural)&&f.className==g&&a!=f.event,f.beforeComplete||x?(f.done||q)(!0):s&&(c="removeClass"==f.event?c.replace(" "+f.className+" "," "):c+f.className+" ")),s=" "+g+" ","addClass"==a&&0<=c.indexOf(s)||"removeClass"==a&&-1==c.indexOf(s)?(n(),E(),r(),v()):(e.addClass(K),
    e.data(l,{running:!0,event:a,className:g,structural:!u,animations:D,done:t}),C(D,"before",t)))}}else n(),E(),r(),z()}function Q(a){a=d(a);p(a.querySelectorAll("."+K),function(a){a=k.element(a);var e=a.data(l);e&&(J(e.animations),A(a))})}function J(a){p(a,function(a){a.beforeComplete||(a.beforeEnd||q)(!0);a.afterComplete||(a.afterEnd||q)(!0)})}function A(a){d(a)==d(H)?m.disabled||(m.running=!1,m.structural=!1):(a.removeClass(K),a.removeData(l))}function ba(a,g){if(m.disabled)return!0;if(d(a)==d(H))return m.disabled||
    m.running;do{if(0===g.length)break;var e=d(g)==d(H),c=e?m:g.data(l),c=c&&(!!c.disabled||!!c.running);if(e||c)return c;if(e)break}while(g=g.parent());return!0}H.data(l,m);w.$$postDigest(function(){w.$$postDigest(function(){m.running=!1})});var M=B.classNameFilter(),L=M?function(a){return M.test(a)}:function(){return!0};return{enter:function(a,d,e,c){this.enabled(!1,a);C.enter(a,d,e);w.$$postDigest(function(){r("enter","ng-enter",a,d,e,q,c)})},leave:function(a,d){Q(a);this.enabled(!1,a);w.$$postDigest(function(){r("leave",
    "ng-leave",a,null,null,function(){C.leave(a)},d)})},move:function(a,d,e,c){Q(a);this.enabled(!1,a);C.move(a,d,e);w.$$postDigest(function(){r("move","ng-move",a,d,e,q,c)})},addClass:function(a,d,e){r("addClass",d,a,null,null,function(){C.addClass(a,d)},e)},removeClass:function(a,d,e){r("removeClass",d,a,null,null,function(){C.removeClass(a,d)},e)},enabled:function(a,d){switch(arguments.length){case 2:if(a)A(d);else{var e=d.data(l)||{};e.disabled=!0;d.data(l,e)}break;case 1:m.disabled=!a;break;default:a=
    !m.disabled}return!!a}}}]);B.register("",["$window","$sniffer","$timeout","$$animateReflow",function(m,l,B,H){function y(b,a){O&&O();U.push(a);var h=d(b);b=k.element(h);V.push(b);var h=b.data(n),c=h.stagger,c=h.itemIndex*(Math.max(c.animationDelay,c.transitionDelay)||0);P=Math.max(P,(c+(h.maxDelay+h.maxDuration)*s)*x);h.animationCount=G;O=H(function(){p(U,function(b){b()});var b=[],a=G;p(V,function(a){b.push(a)});B(function(){w(b,a);b=null},P,!1);U=[];V=[];O=null;u={};P=0;G++})}function w(b,a){p(b,
    function(b){(b=b.data(n))&&b.animationCount==a&&(b.closeAnimationFn||q)()})}function N(b,a){var h=a?u[a]:null;if(!h){var d=0,c=0,e=0,k=0,g,n,l,r;p(b,function(b){if(b.nodeType==X){b=m.getComputedStyle(b)||{};l=b[f+Y];d=Math.max(I(l),d);r=b[f+W];g=b[f+E];c=Math.max(I(g),c);n=b[F+E];k=Math.max(I(n),k);var a=I(b[F+Y]);0<a&&(a*=parseInt(b[F+R],10)||1);e=Math.max(a,e)}});h={total:0,transitionPropertyStyle:r,transitionDurationStyle:l,transitionDelayStyle:g,transitionDelay:c,transitionDuration:d,animationDelayStyle:n,
    animationDelay:k,animationDuration:e};a&&(u[a]=h)}return h}function I(b){var a=0;b=k.isString(b)?b.split(/\s*,\s*/):[];p(b,function(b){a=Math.max(parseFloat(b)||0,a)});return a}function r(b){var a=b.parent(),h=a.data(Z);h||(a.data(Z,++D),h=D);return h+"-"+d(b).className}function Q(b,a,h){var c=r(b),e=c+" "+a,k={},g=u[e]?++u[e].total:0;if(0<g){var l=a+"-stagger",k=c+" "+l;(c=!u[k])&&b.addClass(l);k=N(b,k);c&&b.removeClass(l)}h=h||function(b){return b()};b.addClass(a);h=h(function(){return N(b,e)});
    l=Math.max(h.transitionDelay,h.animationDelay);c=Math.max(h.transitionDuration,h.animationDuration);if(0===c)return b.removeClass(a),!1;var m="";0<h.transitionDuration?d(b).style[f+W]="none":d(b).style[F]="none 0s";p(a.split(" "),function(b,a){m+=(0<a?" ":"")+b+"-active"});b.data(n,{className:a,activeClassName:m,maxDuration:c,maxDelay:l,classes:a+" "+m,timings:h,stagger:k,itemIndex:g});return!0}function J(b){var a=f+W;b=d(b);b.style[a]&&0<b.style[a].length&&(b.style[a]="")}function A(b){var a=F;b=
    d(b);b.style[a]&&0<b.style[a].length&&(b.style[a]="")}function K(b,a,h){function e(c){b.off(v,k);b.removeClass(r);c=b;c.removeClass(a);c.removeData(n);c=d(b);for(var h in s)c.style.removeProperty(s[h])}function k(b){b.stopPropagation();var a=b.originalEvent||b;b=a.$manualTimeStamp||a.timeStamp||Date.now();a=parseFloat(a.elapsedTime.toFixed(z));Math.max(b-w,0)>=u&&a>=p&&h()}var f=b.data(n),g=d(b);if(-1!=g.className.indexOf(a)&&f){var l=f.timings,m=f.stagger,p=f.maxDuration,r=f.activeClassName,u=Math.max(l.transitionDelay,
    l.animationDelay)*x,w=Date.now(),v=T+" "+S,t=f.itemIndex,q="",s=[];if(0<l.transitionDuration){var y=l.transitionPropertyStyle;-1==y.indexOf("all")&&(q+=c+"transition-property: "+y+";",q+=c+"transition-duration: "+l.transitionDurationStyle+";",s.push(c+"transition-property"),s.push(c+"transition-duration"))}0<t&&(0<m.transitionDelay&&0===m.transitionDuration&&(q+=c+"transition-delay: "+M(l.transitionDelayStyle,m.transitionDelay,t)+"; ",s.push(c+"transition-delay")),0<m.animationDelay&&0===m.animationDuration&&
    (q+=c+"animation-delay: "+M(l.animationDelayStyle,m.animationDelay,t)+"; ",s.push(c+"animation-delay")));0<s.length&&(l=g.getAttribute("style")||"",g.setAttribute("style",l+" "+q));b.on(v,k);b.addClass(r);f.closeAnimationFn=function(){e();h()};return e}h()}function M(b,a,c){var d="";p(b.split(","),function(b,e){d+=(0<e?",":"")+(c*a+parseInt(b,10))+"s"});return d}function L(b,a,c){if(Q(b,a,c))return function(c){c&&(b.removeClass(a),b.removeData(n))}}function a(a,c,d){if(a.data(n))return K(a,c,d);a.removeClass(c);
    a.removeData(n);d()}function g(b,c,d){var e=L(b,c);if(e){var f=e;y(b,function(){J(b);A(b);f=a(b,c,d)});return function(a){(f||q)(a)}}d()}function e(a,c){var d="";a=k.isArray(a)?a:a.split(/\s+/);p(a,function(a,b){a&&0<a.length&&(d+=(0<b?" ":"")+a+c)});return d}var c="",f,S,F,T;v.ontransitionend===t&&v.onwebkittransitionend!==t?(c="-webkit-",f="WebkitTransition",S="webkitTransitionEnd transitionend"):(f="transition",S="transitionend");v.onanimationend===t&&v.onwebkitanimationend!==t?(c="-webkit-",F=
    "WebkitAnimation",T="webkitAnimationEnd animationend"):(F="animation",T="animationend");var Y="Duration",W="Property",E="Delay",R="IterationCount",Z="$$ngAnimateKey",n="$$ngAnimateCSS3Data",z=3,s=1.5,x=1E3,G=0,u={},D=0,U=[],V=[],O,P=0;return{allowCancel:function(a,c,h){var f=(a.data(n)||{}).classes;if(!f||0<=["enter","leave","move"].indexOf(c))return!0;var l=a.parent(),g=k.element(d(a).cloneNode());g.attr("style","position:absolute; top:-9999px; left:-9999px");g.removeAttr("id");g.empty();p(f.split(" "),
    function(a){g.removeClass(a)});g.addClass(e(h,"addClass"==c?"-add":"-remove"));l.append(g);a=N(g);g.remove();return 0<Math.max(a.transitionDuration,a.animationDuration)},enter:function(a,c){return g(a,"ng-enter",c)},leave:function(a,c){return g(a,"ng-leave",c)},move:function(a,c){return g(a,"ng-move",c)},beforeAddClass:function(a,c,d){var f=L(a,e(c,"-add"),function(d){a.addClass(c);d=d();a.removeClass(c);return d});if(f)return y(a,function(){J(a);A(a);d()}),f;d()},addClass:function(b,c,d){return a(b,
    e(c,"-add"),d)},beforeRemoveClass:function(a,c,d){var f=L(a,e(c,"-remove"),function(d){var e=a.attr("class");a.removeClass(c);d=d();a.attr("class",e);return d});if(f)return y(a,function(){J(a);A(a);d()}),f;d()},removeClass:function(b,c,d){return a(b,e(c,"-remove"),d)}}}])}])})(window,window.angular);
//# sourceMappingURL=angular-animate.min.js.map

/*!
 * ionic.bundle.js is a concatenation of:
 * ionic.js, angular.js, angular-animate.js,
 * angular-ui-router.js, and ionic-angular.js
 */

/*
 AngularJS v1.2.12
 (c) 2010-2014 Google, Inc. http://angularjs.org
 License: MIT
 */
(function(p,h,q){'use strict';function E(a){var e=[];s(e,h.noop).chars(a);return e.join("")}function k(a){var e={};a=a.split(",");var d;for(d=0;d<a.length;d++)e[a[d]]=!0;return e}function F(a,e){function d(a,b,d,g){b=h.lowercase(b);if(t[b])for(;f.last()&&u[f.last()];)c("",f.last());v[b]&&f.last()==b&&c("",b);(g=w[b]||!!g)||f.push(b);var l={};d.replace(G,function(a,b,e,c,d){l[b]=r(e||c||d||"")});e.start&&e.start(b,l,g)}function c(a,b){var c=0,d;if(b=h.lowercase(b))for(c=f.length-1;0<=c&&f[c]!=b;c--);
    if(0<=c){for(d=f.length-1;d>=c;d--)e.end&&e.end(f[d]);f.length=c}}var b,g,f=[],l=a;for(f.last=function(){return f[f.length-1]};a;){g=!0;if(f.last()&&x[f.last()])a=a.replace(RegExp("(.*)<\\s*\\/\\s*"+f.last()+"[^>]*>","i"),function(b,a){a=a.replace(H,"$1").replace(I,"$1");e.chars&&e.chars(r(a));return""}),c("",f.last());else{if(0===a.indexOf("\x3c!--"))b=a.indexOf("--",4),0<=b&&a.lastIndexOf("--\x3e",b)===b&&(e.comment&&e.comment(a.substring(4,b)),a=a.substring(b+3),g=!1);else if(y.test(a)){if(b=a.match(y))a=
    a.replace(b[0],""),g=!1}else if(J.test(a)){if(b=a.match(z))a=a.substring(b[0].length),b[0].replace(z,c),g=!1}else K.test(a)&&(b=a.match(A))&&(a=a.substring(b[0].length),b[0].replace(A,d),g=!1);g&&(b=a.indexOf("<"),g=0>b?a:a.substring(0,b),a=0>b?"":a.substring(b),e.chars&&e.chars(r(g)))}if(a==l)throw L("badparse",a);l=a}c()}function r(a){if(!a)return"";var e=M.exec(a);a=e[1];var d=e[3];if(e=e[2])n.innerHTML=e.replace(/</g,"&lt;"),e="textContent"in n?n.textContent:n.innerText;return a+e+d}function B(a){return a.replace(/&/g,
    "&amp;").replace(N,function(a){return"&#"+a.charCodeAt(0)+";"}).replace(/</g,"&lt;").replace(/>/g,"&gt;")}function s(a,e){var d=!1,c=h.bind(a,a.push);return{start:function(a,g,f){a=h.lowercase(a);!d&&x[a]&&(d=a);d||!0!==C[a]||(c("<"),c(a),h.forEach(g,function(d,f){var g=h.lowercase(f),k="img"===a&&"src"===g||"background"===g;!0!==O[g]||!0===D[g]&&!e(d,k)||(c(" "),c(f),c('="'),c(B(d)),c('"'))}),c(f?"/>":">"))},end:function(a){a=h.lowercase(a);d||!0!==C[a]||(c("</"),c(a),c(">"));a==d&&(d=!1)},chars:function(a){d||
c(B(a))}}}var L=h.$$minErr("$sanitize"),A=/^<\s*([\w:-]+)((?:\s+[\w:-]+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)\s*>/,z=/^<\s*\/\s*([\w:-]+)[^>]*>/,G=/([\w:-]+)(?:\s*=\s*(?:(?:"((?:[^"])*)")|(?:'((?:[^'])*)')|([^>\s]+)))?/g,K=/^</,J=/^<\s*\//,H=/\x3c!--(.*?)--\x3e/g,y=/<!DOCTYPE([^>]*?)>/i,I=/<!\[CDATA\[(.*?)]]\x3e/g,N=/([^\#-~| |!])/g,w=k("area,br,col,hr,img,wbr");p=k("colgroup,dd,dt,li,p,tbody,td,tfoot,th,thead,tr");q=k("rp,rt");var v=h.extend({},q,p),t=h.extend({},p,k("address,article,aside,blockquote,caption,center,del,dir,div,dl,figure,figcaption,footer,h1,h2,h3,h4,h5,h6,header,hgroup,hr,ins,map,menu,nav,ol,pre,script,section,table,ul")),
    u=h.extend({},q,k("a,abbr,acronym,b,bdi,bdo,big,br,cite,code,del,dfn,em,font,i,img,ins,kbd,label,map,mark,q,ruby,rp,rt,s,samp,small,span,strike,strong,sub,sup,time,tt,u,var")),x=k("script,style"),C=h.extend({},w,t,u,v),D=k("background,cite,href,longdesc,src,usemap"),O=h.extend({},D,k("abbr,align,alt,axis,bgcolor,border,cellpadding,cellspacing,class,clear,color,cols,colspan,compact,coords,dir,face,headers,height,hreflang,hspace,ismap,lang,language,nohref,nowrap,rel,rev,rows,rowspan,rules,scope,scrolling,shape,size,span,start,summary,target,title,type,valign,value,vspace,width")),
    n=document.createElement("pre"),M=/^(\s*)([\s\S]*?)(\s*)$/;h.module("ngSanitize",[]).provider("$sanitize",function(){this.$get=["$$sanitizeUri",function(a){return function(e){var d=[];F(e,s(d,function(c,b){return!/^unsafe/.test(a(c,b))}));return d.join("")}}]});h.module("ngSanitize").filter("linky",["$sanitize",function(a){var e=/((ftp|https?):\/\/|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s.;,(){}<>]/,d=/^mailto:/;return function(c,b){function g(a){a&&m.push(E(a))}function f(a,c){m.push("<a ");h.isDefined(b)&&
(m.push('target="'),m.push(b),m.push('" '));m.push('href="');m.push(a);m.push('">');g(c);m.push("</a>")}if(!c)return c;for(var l,k=c,m=[],n,p;l=k.match(e);)n=l[0],l[2]==l[3]&&(n="mailto:"+n),p=l.index,g(k.substr(0,p)),f(n,l[0].replace(d,"")),k=k.substring(p+l[0].length);g(k);return a(m.join(""))}}])})(window,window.angular);
//# sourceMappingURL=angular-sanitize.min.js.map

/*!
 * ionic.bundle.js is a concatenation of:
 * ionic.js, angular.js, angular-animate.js,
 * angular-ui-router.js, and ionic-angular.js
 */

/**
 * State-based routing for AngularJS
 * @version v0.2.7
 * @link http://angular-ui.github.com/
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
"undefined"!=typeof module&&"undefined"!=typeof exports&&module.exports===exports&&(module.exports="ui.router"),function(a,b,c){"use strict";function d(a,b){return E(new(E(function(){},{prototype:a})),b)}function e(a){return D(arguments,function(b){b!==a&&D(b,function(b,c){a.hasOwnProperty(c)||(a[c]=b)})}),a}function f(a,b){var c=[];for(var d in a.path)if(""!==a.path[d]){if(!b.path[d])break;c.push(a.path[d])}return c}function g(a,b){if(Array.prototype.indexOf)return a.indexOf(b,Number(arguments[2])||0);var c=a.length>>>0,d=Number(arguments[2])||0;for(d=0>d?Math.ceil(d):Math.floor(d),0>d&&(d+=c);c>d;d++)if(d in a&&a[d]===b)return d;return-1}function h(a,b,c,d){var e,h=f(c,d),i={},j=[];for(var k in h)if(h[k].params&&h[k].params.length){e=h[k].params;for(var l in e)g(j,e[l])>=0||(j.push(e[l]),i[e[l]]=a[e[l]])}return E({},i,b)}function i(a,b){var c={};return D(a,function(a){var d=b[a];c[a]=null!=d?String(d):null}),c}function j(a,b,c){if(!c){c=[];for(var d in a)c.push(d)}for(var e=0;e<c.length;e++){var f=c[e];if(a[f]!=b[f])return!1}return!0}function k(a,b){var c={};return D(a,function(a){c[a]=b[a]}),c}function l(a,b){var d=1,f=2,g={},h=[],i=g,j=E(a.when(g),{$$promises:g,$$values:g});this.study=function(g){function k(a,c){if(o[c]!==f){if(n.push(c),o[c]===d)throw n.splice(0,n.indexOf(c)),new Error("Cyclic dependency: "+n.join(" -> "));if(o[c]=d,A(a))m.push(c,[function(){return b.get(a)}],h);else{var e=b.annotate(a);D(e,function(a){a!==c&&g.hasOwnProperty(a)&&k(g[a],a)}),m.push(c,a,e)}n.pop(),o[c]=f}}function l(a){return B(a)&&a.then&&a.$$promises}if(!B(g))throw new Error("'invocables' must be an object");var m=[],n=[],o={};return D(g,k),g=n=o=null,function(d,f,g){function h(){--s||(t||e(r,f.$$values),p.$$values=r,p.$$promises=!0,o.resolve(r))}function k(a){p.$$failure=a,o.reject(a)}function n(c,e,f){function i(a){l.reject(a),k(a)}function j(){if(!y(p.$$failure))try{l.resolve(b.invoke(e,g,r)),l.promise.then(function(a){r[c]=a,h()},i)}catch(a){i(a)}}var l=a.defer(),m=0;D(f,function(a){q.hasOwnProperty(a)&&!d.hasOwnProperty(a)&&(m++,q[a].then(function(b){r[a]=b,--m||j()},i))}),m||j(),q[c]=l.promise}if(l(d)&&g===c&&(g=f,f=d,d=null),d){if(!B(d))throw new Error("'locals' must be an object")}else d=i;if(f){if(!l(f))throw new Error("'parent' must be a promise returned by $resolve.resolve()")}else f=j;var o=a.defer(),p=o.promise,q=p.$$promises={},r=E({},d),s=1+m.length/3,t=!1;if(y(f.$$failure))return k(f.$$failure),p;f.$$values?(t=e(r,f.$$values),h()):(E(q,f.$$promises),f.then(h,k));for(var u=0,v=m.length;v>u;u+=3)d.hasOwnProperty(m[u])?h():n(m[u],m[u+1],m[u+2]);return p}},this.resolve=function(a,b,c,d){return this.study(a)(b,c,d)}}function m(a,b,c){this.fromConfig=function(a,b,c){return y(a.template)?this.fromString(a.template,b):y(a.templateUrl)?this.fromUrl(a.templateUrl,b):y(a.templateProvider)?this.fromProvider(a.templateProvider,b,c):null},this.fromString=function(a,b){return z(a)?a(b):a},this.fromUrl=function(c,d){return z(c)&&(c=c(d)),null==c?null:a.get(c,{cache:b}).then(function(a){return a.data})},this.fromProvider=function(a,b,d){return c.invoke(a,null,d||{params:b})}}function n(a){function b(b){if(!/^\w+(-+\w+)*$/.test(b))throw new Error("Invalid parameter name '"+b+"' in pattern '"+a+"'");if(f[b])throw new Error("Duplicate parameter name '"+b+"' in pattern '"+a+"'");f[b]=!0,j.push(b)}function c(a){return a.replace(/[\\\[\]\^$*+?.()|{}]/g,"\\$&")}var d,e=/([:*])(\w+)|\{(\w+)(?:\:((?:[^{}\\]+|\\.|\{(?:[^{}\\]+|\\.)*\})+))?\}/g,f={},g="^",h=0,i=this.segments=[],j=this.params=[];this.source=a;for(var k,l,m;(d=e.exec(a))&&(k=d[2]||d[3],l=d[4]||("*"==d[1]?".*":"[^/]*"),m=a.substring(h,d.index),!(m.indexOf("?")>=0));)g+=c(m)+"("+l+")",b(k),i.push(m),h=e.lastIndex;m=a.substring(h);var n=m.indexOf("?");if(n>=0){var o=this.sourceSearch=m.substring(n);m=m.substring(0,n),this.sourcePath=a.substring(0,h+n),D(o.substring(1).split(/[&?]/),b)}else this.sourcePath=a,this.sourceSearch="";g+=c(m)+"$",i.push(m),this.regexp=new RegExp(g),this.prefix=i[0]}function o(){this.compile=function(a){return new n(a)},this.isMatcher=function(a){return B(a)&&z(a.exec)&&z(a.format)&&z(a.concat)},this.$get=function(){return this}}function p(a){function b(a){var b=/^\^((?:\\[^a-zA-Z0-9]|[^\\\[\]\^$*+?.()|{}]+)*)/.exec(a.source);return null!=b?b[1].replace(/\\(.)/g,"$1"):""}function c(a,b){return a.replace(/\$(\$|\d{1,2})/,function(a,c){return b["$"===c?0:Number(c)]})}function d(a,b,c){if(!c)return!1;var d=a.invoke(b,b,{$match:c});return y(d)?d:!0}var e=[],f=null;this.rule=function(a){if(!z(a))throw new Error("'rule' must be a function");return e.push(a),this},this.otherwise=function(a){if(A(a)){var b=a;a=function(){return b}}else if(!z(a))throw new Error("'rule' must be a function");return f=a,this},this.when=function(e,f){var g,h=A(f);if(A(e)&&(e=a.compile(e)),!h&&!z(f)&&!C(f))throw new Error("invalid 'handler' in when()");var i={matcher:function(b,c){return h&&(g=a.compile(c),c=["$match",function(a){return g.format(a)}]),E(function(a,e){return d(a,c,b.exec(e.path(),e.search()))},{prefix:A(b.prefix)?b.prefix:""})},regex:function(a,e){if(a.global||a.sticky)throw new Error("when() RegExp must not be global or sticky");return h&&(g=e,e=["$match",function(a){return c(g,a)}]),E(function(b,c){return d(b,e,a.exec(c.path()))},{prefix:b(a)})}},j={matcher:a.isMatcher(e),regex:e instanceof RegExp};for(var k in j)if(j[k])return this.rule(i[k](e,f));throw new Error("invalid 'what' in when()")},this.$get=["$location","$rootScope","$injector",function(a,b,c){function d(b){function d(b){var d=b(c,a);return d?(A(d)&&a.replace().url(d),!0):!1}if(!b||!b.defaultPrevented){var g,h=e.length;for(g=0;h>g;g++)if(d(e[g]))return;f&&d(f)}}return b.$on("$locationChangeSuccess",d),{sync:function(){d()}}}]}function q(a,e,f){function g(a){return 0===a.indexOf(".")||0===a.indexOf("^")}function l(a,b){var d=A(a),e=d?a:a.name,f=g(e);if(f){if(!b)throw new Error("No reference point given for path '"+e+"'");for(var h=e.split("."),i=0,j=h.length,k=b;j>i;i++)if(""!==h[i]||0!==i){if("^"!==h[i])break;if(!k.parent)throw new Error("Path '"+e+"' not valid for state '"+b.name+"'");k=k.parent}else k=b;h=h.slice(i).join("."),e=k.name+(k.name&&h?".":"")+h}var l=u[e];return!l||!d&&(d||l!==a&&l.self!==a)?c:l}function m(a,b){v[a]||(v[a]=[]),v[a].push(b)}function n(b){b=d(b,{self:b,resolve:b.resolve||{},toString:function(){return this.name}});var c=b.name;if(!A(c)||c.indexOf("@")>=0)throw new Error("State must have a valid name");if(u.hasOwnProperty(c))throw new Error("State '"+c+"'' is already defined");var e=-1!==c.indexOf(".")?c.substring(0,c.lastIndexOf(".")):A(b.parent)?b.parent:"";if(e&&!u[e])return m(e,b.self);for(var f in x)z(x[f])&&(b[f]=x[f](b,x.$delegates[f]));if(u[c]=b,!b[w]&&b.url&&a.when(b.url,["$match","$stateParams",function(a,c){t.$current.navigable==b&&j(a,c)||t.transitionTo(b,a,{location:!1})}]),v[c])for(var g=0;g<v[c].length;g++)n(v[c][g]);return b}function o(a,b){return A(a)&&!y(b)?x[a]:z(b)&&A(a)?(x[a]&&!x.$delegates[a]&&(x.$delegates[a]=x[a]),x[a]=b,this):this}function p(a,b){return B(a)?b=a:b.name=a,n(b),this}function q(a,e,g,m,n,o,p){function q(){p.url()!==H&&(p.url(H),p.replace())}function v(a,c,d,f,h){var i=d?c:k(a.params,c),j={$stateParams:i};h.resolve=n.resolve(a.resolve,j,h.resolve,a);var l=[h.resolve.then(function(a){h.globals=a})];return f&&l.push(f),D(a.views,function(c,d){var e=c.resolve&&c.resolve!==a.resolve?c.resolve:{};e.$template=[function(){return g.load(d,{view:c,locals:j,params:i,notify:!1})||""}],l.push(n.resolve(e,j,h.resolve,a).then(function(f){if(z(c.controllerProvider)||C(c.controllerProvider)){var g=b.extend({},e,j);f.$$controller=m.invoke(c.controllerProvider,null,g)}else f.$$controller=c.controller;f.$$state=a,h[d]=f}))}),e.all(l).then(function(){return h})}var x=e.reject(new Error("transition superseded")),A=e.reject(new Error("transition prevented")),B=e.reject(new Error("transition aborted")),G=e.reject(new Error("transition failed")),H=p.url();return s.locals={resolve:null,globals:{$stateParams:{}}},t={params:{},current:s.self,$current:s,transition:null},t.reload=function(){t.transitionTo(t.current,o,{reload:!0,inherit:!1,notify:!1})},t.go=function(a,b,c){return this.transitionTo(a,b,E({inherit:!0,relative:t.$current},c))},t.transitionTo=function(b,c,f){c=c||{},f=E({location:!0,inherit:!1,relative:null,notify:!0,reload:!1,$retry:!1},f||{});var g,k=t.$current,n=t.params,u=k.path,z=l(b,f.relative);if(!y(z)){var C={to:b,toParams:c,options:f};if(g=a.$broadcast("$stateNotFound",C,k.self,n),g.defaultPrevented)return q(),B;if(g.retry){if(f.$retry)return q(),G;var D=t.transition=e.when(g.retry);return D.then(function(){return D!==t.transition?x:(C.options.$retry=!0,t.transitionTo(C.to,C.toParams,C.options))},function(){return B}),q(),D}if(b=C.to,c=C.toParams,f=C.options,z=l(b,f.relative),!y(z)){if(f.relative)throw new Error("Could not resolve '"+b+"' from state '"+f.relative+"'");throw new Error("No such state '"+b+"'")}}if(z[w])throw new Error("Cannot transition to abstract state '"+b+"'");f.inherit&&(c=h(o,c||{},t.$current,z)),b=z;var I,J,K=b.path,L=s.locals,M=[];for(I=0,J=K[I];J&&J===u[I]&&j(c,n,J.ownParams)&&!f.reload;I++,J=K[I])L=M[I]=J.locals;if(r(b,k,L,f))return b.self.reloadOnSearch!==!1&&q(),t.transition=null,e.when(t.current);if(c=i(b.params,c||{}),f.notify&&(g=a.$broadcast("$stateChangeStart",b.self,c,k.self,n),g.defaultPrevented))return q(),A;for(var N=e.when(L),O=I;O<K.length;O++,J=K[O])L=M[O]=d(L),N=v(J,c,J===b,N,L);var P=t.transition=N.then(function(){var d,e,g;if(t.transition!==P)return x;for(d=u.length-1;d>=I;d--)g=u[d],g.self.onExit&&m.invoke(g.self.onExit,g.self,g.locals.globals),g.locals=null;for(d=I;d<K.length;d++)e=K[d],e.locals=M[d],e.self.onEnter&&m.invoke(e.self.onEnter,e.self,e.locals.globals);if(t.transition!==P)return x;t.$current=b,t.current=b.self,t.params=c,F(t.params,o),t.transition=null;var h=b.navigable;return f.location&&h&&(p.url(h.url.format(h.locals.globals.$stateParams)),"replace"===f.location&&p.replace()),f.notify&&a.$broadcast("$stateChangeSuccess",b.self,c,k.self,n),H=p.url(),t.current},function(d){return t.transition!==P?x:(t.transition=null,a.$broadcast("$stateChangeError",b.self,c,k.self,n,d),q(),e.reject(d))});return P},t.is=function(a,d){var e=l(a);return y(e)?t.$current!==e?!1:y(d)?b.equals(o,d):!0:c},t.includes=function(a,d){var e=l(a);if(!y(e))return c;if(!y(t.$current.includes[e.name]))return!1;var f=!0;return b.forEach(d,function(a,b){y(o[b])&&o[b]===a||(f=!1)}),f},t.href=function(a,b,c){c=E({lossy:!0,inherit:!1,absolute:!1,relative:t.$current},c||{});var d=l(a,c.relative);if(!y(d))return null;b=h(o,b||{},t.$current,d);var e=d&&c.lossy?d.navigable:d,g=e&&e.url?e.url.format(i(d.params,b||{})):null;return!f.html5Mode()&&g&&(g="#"+f.hashPrefix()+g),c.absolute&&g&&(g=p.protocol()+"://"+p.host()+(80==p.port()||443==p.port()?"":":"+p.port())+(!f.html5Mode()&&g?"/":"")+g),g},t.get=function(a,b){if(!y(a)){var c=[];return D(u,function(a){c.push(a.self)}),c}var d=l(a,b);return d&&d.self?d.self:null},t}function r(a,b,c,d){return a!==b||(c!==b.locals||d.reload)&&a.self.reloadOnSearch!==!1?void 0:!0}var s,t,u={},v={},w="abstract",x={parent:function(a){if(y(a.parent)&&a.parent)return l(a.parent);var b=/^(.+)\.[^.]+$/.exec(a.name);return b?l(b[1]):s},data:function(a){return a.parent&&a.parent.data&&(a.data=a.self.data=E({},a.parent.data,a.data)),a.data},url:function(a){var b=a.url;if(A(b))return"^"==b.charAt(0)?e.compile(b.substring(1)):(a.parent.navigable||s).url.concat(b);if(e.isMatcher(b)||null==b)return b;throw new Error("Invalid url '"+b+"' in state '"+a+"'")},navigable:function(a){return a.url?a:a.parent?a.parent.navigable:null},params:function(a){if(!a.params)return a.url?a.url.parameters():a.parent.params;if(!C(a.params))throw new Error("Invalid params in state '"+a+"'");if(a.url)throw new Error("Both params and url specicified in state '"+a+"'");return a.params},views:function(a){var b={};return D(y(a.views)?a.views:{"":a},function(c,d){d.indexOf("@")<0&&(d+="@"+a.parent.name),b[d]=c}),b},ownParams:function(a){if(!a.parent)return a.params;var b={};D(a.params,function(a){b[a]=!0}),D(a.parent.params,function(c){if(!b[c])throw new Error("Missing required parameter '"+c+"' in state '"+a.name+"'");b[c]=!1});var c=[];return D(b,function(a,b){a&&c.push(b)}),c},path:function(a){return a.parent?a.parent.path.concat(a):[]},includes:function(a){var b=a.parent?E({},a.parent.includes):{};return b[a.name]=!0,b},$delegates:{}};s=n({name:"",url:"^",views:null,"abstract":!0}),s.navigable=null,this.decorator=o,this.state=p,this.$get=q,q.$inject=["$rootScope","$q","$view","$injector","$resolve","$stateParams","$location","$urlRouter"]}function r(){function a(a,b){return{load:function(c,d){var e,f={template:null,controller:null,view:null,locals:null,notify:!0,async:!0,params:{}};return d=E(f,d),d.view&&(e=b.fromConfig(d.view,d.params,d.locals)),e&&d.notify&&a.$broadcast("$viewContentLoading",d),e}}}this.$get=a,a.$inject=["$rootScope","$templateFactory"]}function s(a,c,d,e,f){var g=e.has("$animator")?e.get("$animator"):!1,h=!1,i={restrict:"ECA",terminal:!0,priority:1e3,transclude:!0,compile:function(e,j,k){return function(e,j,l){function m(b){var g=a.$current&&a.$current.locals[p];if(g!==o){var h=t(r&&b);if(h.remove(j),n&&(n.$destroy(),n=null),!g)return o=null,v.state=null,h.restore(s,j);o=g,v.state=g.$$state;var i=c(h.populate(g.$template,j));if(n=e.$new(),g.$$controller){g.$scope=n;var k=d(g.$$controller,g);j.children().data("$ngControllerController",k)}i(n),n.$emit("$viewContentLoaded"),q&&n.$eval(q),f()}}var n,o,p=l[i.name]||l.name||"",q=l.onload||"",r=g&&g(e,l),s=k(e),t=function(a){return{"true":{remove:function(a){r.leave(a.contents(),a)},restore:function(a,b){r.enter(a,b)},populate:function(a,c){var d=b.element("<div></div>").html(a).contents();return r.enter(d,c),d}},"false":{remove:function(a){a.html("")},restore:function(a,b){b.append(a)},populate:function(a,b){return b.html(a),b.contents()}}}[a.toString()]};j.append(s);var u=j.parent().inheritedData("$uiView");p.indexOf("@")<0&&(p=p+"@"+(u?u.state.name:""));var v={name:p,state:null};j.data("$uiView",v);var w=function(){if(!h){h=!0;try{m(!0)}catch(a){throw h=!1,a}h=!1}};e.$on("$stateChangeSuccess",w),e.$on("$viewContentLoading",w),m(!1)}}};return i}function t(a){var b=a.replace(/\n/g," ").match(/^([^(]+?)\s*(\((.*)\))?$/);if(!b||4!==b.length)throw new Error("Invalid state ref '"+a+"'");return{state:b[1],paramExpr:b[3]||null}}function u(a){var b=a.parent().inheritedData("$uiView");return b&&b.state&&b.state.name?b.state:void 0}function v(a,b){return{restrict:"A",require:"?^uiSrefActive",link:function(c,d,e,f){var g=t(e.uiSref),h=null,i=u(d)||a.$current,j="FORM"===d[0].nodeName,k=j?"action":"href",l=!0,m=function(b){if(b&&(h=b),l){var c=a.href(g.state,h,{relative:i});if(!c)return l=!1,!1;d[0][k]=c,f&&f.$$setStateInfo(g.state,h)}};g.paramExpr&&(c.$watch(g.paramExpr,function(a){a!==h&&m(a)},!0),h=c.$eval(g.paramExpr)),m(),j||d.bind("click",function(d){var e=d.which||d.button;0!==e&&1!=e||d.ctrlKey||d.metaKey||d.shiftKey||(b(function(){c.$apply(function(){a.go(g.state,h,{relative:i})})}),d.preventDefault())})}}}function w(a,b,c){return{restrict:"A",controller:function(d,e,f){function g(){a.$current.self===i&&h()?e.addClass(l):e.removeClass(l)}function h(){return!k||j(k,b)}var i,k,l;l=c(f.uiSrefActive||"",!1)(d),this.$$setStateInfo=function(b,c){i=a.get(b,u(e)),k=c,g()},d.$on("$stateChangeSuccess",g)}}}function x(a,b){function e(a){this.locals=a.locals.globals,this.params=this.locals.$stateParams}function f(){this.locals=null,this.params=null}function g(c,g){if(null!=g.redirectTo){var h,j=g.redirectTo;if(A(j))h=j;else{if(!z(j))throw new Error("Invalid 'redirectTo' in when()");h=function(a,b){return j(a,b.path(),b.search())}}b.when(c,h)}else a.state(d(g,{parent:null,name:"route:"+encodeURIComponent(c),url:c,onEnter:e,onExit:f}));return i.push(g),this}function h(a,b,d){function e(a){return""!==a.name?a:c}var f={routes:i,params:d,current:c};return b.$on("$stateChangeStart",function(a,c,d,f){b.$broadcast("$routeChangeStart",e(c),e(f))}),b.$on("$stateChangeSuccess",function(a,c,d,g){f.current=e(c),b.$broadcast("$routeChangeSuccess",e(c),e(g)),F(d,f.params)}),b.$on("$stateChangeError",function(a,c,d,f,g,h){b.$broadcast("$routeChangeError",e(c),e(f),h)}),f}var i=[];e.$inject=["$$state"],this.when=g,this.$get=h,h.$inject=["$state","$rootScope","$routeParams"]}var y=b.isDefined,z=b.isFunction,A=b.isString,B=b.isObject,C=b.isArray,D=b.forEach,E=b.extend,F=b.copy;b.module("ui.router.util",["ng"]),b.module("ui.router.router",["ui.router.util"]),b.module("ui.router.state",["ui.router.router","ui.router.util"]),b.module("ui.router",["ui.router.state"]),b.module("ui.router.compat",["ui.router"]),l.$inject=["$q","$injector"],b.module("ui.router.util").service("$resolve",l),m.$inject=["$http","$templateCache","$injector"],b.module("ui.router.util").service("$templateFactory",m),n.prototype.concat=function(a){return new n(this.sourcePath+a+this.sourceSearch)},n.prototype.toString=function(){return this.source},n.prototype.exec=function(a,b){var c=this.regexp.exec(a);if(!c)return null;var d,e=this.params,f=e.length,g=this.segments.length-1,h={};if(g!==c.length-1)throw new Error("Unbalanced capture group in route '"+this.source+"'");for(d=0;g>d;d++)h[e[d]]=c[d+1];for(;f>d;d++)h[e[d]]=b[e[d]];return h},n.prototype.parameters=function(){return this.params},n.prototype.format=function(a){var b=this.segments,c=this.params;if(!a)return b.join("");var d,e,f,g=b.length-1,h=c.length,i=b[0];for(d=0;g>d;d++)f=a[c[d]],null!=f&&(i+=encodeURIComponent(f)),i+=b[d+1];for(;h>d;d++)f=a[c[d]],null!=f&&(i+=(e?"&":"?")+c[d]+"="+encodeURIComponent(f),e=!0);return i},b.module("ui.router.util").provider("$urlMatcherFactory",o),p.$inject=["$urlMatcherFactoryProvider"],b.module("ui.router.router").provider("$urlRouter",p),q.$inject=["$urlRouterProvider","$urlMatcherFactoryProvider","$locationProvider"],b.module("ui.router.state").value("$stateParams",{}).provider("$state",q),r.$inject=[],b.module("ui.router.state").provider("$view",r),s.$inject=["$state","$compile","$controller","$injector","$anchorScroll"],b.module("ui.router.state").directive("uiView",s),v.$inject=["$state","$timeout"],w.$inject=["$state","$stateParams","$interpolate"],b.module("ui.router.state").directive("uiSref",v).directive("uiSrefActive",w),x.$inject=["$stateProvider","$urlRouterProvider"],b.module("ui.router.compat").provider("$route",x).directive("ngView",s)}(window,window.angular);

/*!
 * ionic.bundle.js is a concatenation of:
 * ionic.js, angular.js, angular-animate.js,
 * angular-ui-router.js, and ionic-angular.js
 */

/*!
 * Copyright 2014 Drifty Co.
 * http://drifty.com/
 *
 * Ionic, v1.0.0-beta.1
 * A powerful HTML5 mobile app framework.
 * http://ionicframework.com/
 *
 * By @maxlynch, @benjsperry, @adamdbradley <3
 *
 * Licensed under the MIT license. Please see LICENSE for more information.
 *
 */

!function(){function e(e){return["$log",function(t){function n(e){this.handle=e}var i=this,o=this._instances=[];this._registerInstance=function(e,t){return t||(t=ionic.Utils.nextUid()),e.$$delegateHandle=t,o.push(e),function(){var t=o.indexOf(e);-1!==t&&o.splice(t,1)}},this.$getByHandle=function(e){return e?new n(e):i},e.forEach(function(e){function r(e,t,n){var i,o;return e.forEach(function(e,r){o=e[t].apply(e,n),0===r&&(i=o)}),i}n.prototype[e]=function(){var n=this.handle,i=o.filter(function(e){return e.$$delegateHandle===n});return i.length?r(i,e,arguments):t.warn('Delegate for handle "'+this.handle+'" could not find a','corresponding element with delegate-handle="'+this.handle+'"!',e,"was not called!")},i[e]=function(){return r(o,e,arguments)}})}]}angular.module("ionic.service",["ionic.service.bind","ionic.service.platform","ionic.service.actionSheet","ionic.service.gesture","ionic.service.loading","ionic.service.modal","ionic.service.popup","ionic.service.templateLoad","ionic.service.view","ionic.decorator.location"]),angular.module("ionic.ui",["ionic.ui.checkbox","ionic.ui.content","ionic.ui.header","ionic.ui.list","ionic.ui.navBar","ionic.ui.popup","ionic.ui.radio","ionic.ui.scroll","ionic.ui.sideMenu","ionic.ui.slideBox","ionic.ui.tabs","ionic.ui.toggle","ionic.ui.touch","ionic.ui.viewState"]),angular.module("ionic",["ionic.service","ionic.ui","ngAnimate","ngSanitize","ui.router"]),angular.element.prototype.addClass=function(e){var t,n,i,o,r,a;if(e&&"ng-scope"!=e&&"ng-isolate-scope"!=e)for(t=0;t<this.length;t++)if(o=this[t],o.setAttribute)if(e.indexOf(" ")<0)o.classList.add(e);else{for(a=(" "+(o.getAttribute("class")||"")+" ").replace(/[\n\t]/g," "),r=e.split(" "),n=0;n<r.length;n++)i=r[n].trim(),-1===a.indexOf(" "+i+" ")&&(a+=i+" ");o.setAttribute("class",a.trim())}return this},angular.element.prototype.removeClass=function(e){var t,n,i,o,r;if(e)for(t=0;t<this.length;t++)if(r=this[t],r.getAttribute)if(e.indexOf(" ")<0)r.classList.remove(e);else for(i=e.split(" "),n=0;n<i.length;n++)o=i[n],r.setAttribute("class",(" "+(r.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ").replace(" "+o.trim()+" "," ").trim());return this},angular.module("ionic.service.actionSheet",["ionic.service.templateLoad","ionic.service.platform","ionic.ui.actionSheet","ngAnimate"]).factory("$ionicActionSheet",["$rootScope","$document","$compile","$animate","$timeout","$ionicTemplateLoader","$ionicPlatform",function(e,t,n,i,o,r,a){return{show:function(r){var c=e.$new(!0);angular.extend(c,r);var s=n('<ion-action-sheet buttons="buttons"></ion-action-sheet>')(c),l=angular.element(s[0].querySelector(".action-sheet-wrapper")),u=function(e){l.removeClass("action-sheet-up"),e&&o(function(){r.cancel()},200),i.removeClass(s,"active",function(){c.$destroy()}),t[0].body.classList.remove("action-sheet-open"),c.$deregisterBackButton&&c.$deregisterBackButton()};c.$deregisterBackButton=a.registerBackButtonAction(function(){u()},300),c.cancel=function(){u(!0)},c.buttonClicked=function(e){(r.buttonClicked&&r.buttonClicked(e))===!0&&u(!1)},c.destructiveButtonClicked=function(){(r.destructiveButtonClicked&&r.destructiveButtonClicked())===!0&&u(!1)},t[0].body.appendChild(s[0]),t[0].body.classList.add("action-sheet-open");var d=new ionic.views.ActionSheet({el:s[0]});return c.sheet=d,i.addClass(s,"active"),o(function(){l.addClass("action-sheet-up")},20),d}}}]),angular.module("ionic.service.bind",[]).factory("$ionicBind",["$parse","$interpolate",function(e,t){var n=/^\s*([@=&])(\??)\s*(\w*)\s*$/;return function(i,o,r){angular.forEach(r||{},function(r,a){var c,s,l=r.match(n)||[],u=l[3]||a,d=l[1];switch(d){case"@":if(!o[u])return;o.$observe(u,function(e){i[a]=e}),o[u]&&(i[a]=t(o[u])(i));break;case"=":if(!o[u])return;s=i.$watch(o[u],function(e){i[a]=e}),i.$on("$destroy",s);break;case"&":if(o[u]&&o[u].match(RegExp(a+"(.*?)")))throw new Error('& expression binding "'+a+'" looks like it will recursively call "'+o[u]+'" and cause a stack overflow! Please choose a different scopeName.');c=e(o[u]),i[a]=function(e){return c(i,e)}}})}}]),angular.module("ionic.service.gesture",[]).factory("$ionicGesture",[function(){return{on:function(e,t,n){return window.ionic.onGesture(e,t,n[0])},off:function(e,t,n){return window.ionic.offGesture(e,t,n)}}}]),angular.module("ionic.service.loading",["ionic.ui.loading"]).factory("$ionicLoading",["$rootScope","$document","$compile",function(e,t,n){return{show:function(i){var o={content:"",animation:"fade-in",showBackdrop:!0,maxWidth:200,showDelay:0};i=angular.extend(o,i);var r=e.$new(!0);angular.extend(r,i);var a=angular.element(t[0].querySelector(".loading-backdrop"));a.length&&a.remove();var c=n("<ion-loading>"+i.content+"</ion-loading>")(r);t[0].body.appendChild(c[0]);var s=new ionic.views.Loading({el:c[0],maxWidth:i.maxWidth,showDelay:i.showDelay});return s.show(),r.loading=s,s}}}]),angular.module("ionic.service.modal",["ionic.service.templateLoad","ionic.service.platform","ionic.ui.modal"]).factory("$ionicModal",["$rootScope","$document","$compile","$timeout","$ionicPlatform","$ionicTemplateLoader",function(e,t,n,i,o,r){var a=ionic.views.Modal.inherit({initialize:function(e){ionic.views.Modal.prototype.initialize.call(this,e),this.animation=e.animation||"slide-in-up"},show:function(){var e=this,n=angular.element(e.modalEl);e.el.classList.remove("hide"),t[0].body.classList.add("modal-open"),e._isShown=!0,e.el.parentElement||(n.addClass(e.animation),t[0].body.appendChild(e.el)),n.addClass("ng-enter active").removeClass("ng-leave ng-leave-active"),i(function(){n.addClass("ng-enter-active"),e.scope.$parent&&e.scope.$parent.$broadcast("modal.shown"),e.el.classList.add("active")},20),e._deregisterBackButton=o.registerBackButtonAction(function(){e.hide()},200),ionic.views.Modal.prototype.show.call(e)},hide:function(){var e=this;e._isShown=!1;var n=angular.element(e.modalEl);e.el.classList.remove("active"),n.addClass("ng-leave"),i(function(){n.addClass("ng-leave-active").removeClass("ng-enter ng-enter-active active")},20),i(function(){t[0].body.classList.remove("modal-open"),e.el.classList.add("hide")},350),ionic.views.Modal.prototype.hide.call(e),e.scope.$parent&&e.scope.$parent.$broadcast("modal.hidden"),e._deregisterBackButton&&e._deregisterBackButton()},remove:function(){var e=this;e.hide(),e.scope.$parent&&e.scope.$parent.$broadcast("modal.removed"),i(function(){e.scope.$destroy(),e.el&&e.el.parentElement&&e.el.parentElement.removeChild(e.el)},750)},isShown:function(){return!!this._isShown}}),c=function(t,i){var o=i.scope&&i.scope.$new()||e.$new(!0),r=n("<ion-modal>"+t+"</ion-modal>")(o);i.el=r[0],i.modalEl=i.el.querySelector(".modal");var c=new a(i);return c.scope=o,i.scope||(o.modal=c),c};return{fromTemplate:function(e,t){var n=c(e,t||{});return n},fromTemplateUrl:function(e,t,n){var i;return angular.isFunction(t)&&(i=t,t=n),r.load(e).then(function(e){var n=c(e,t||{});return i&&i(n),n})}}}]),function(e){"use strict";angular.module("ionic.service.platform",[]).provider("$ionicPlatform",function(){return{$get:["$q","$rootScope",function(t,n){return{onHardwareBackButton:function(t){e.Platform.ready(function(){document.addEventListener("backbutton",t,!1)})},offHardwareBackButton:function(t){e.Platform.ready(function(){document.removeEventListener("backbutton",t)})},registerBackButtonAction:function(t,i,o){var r=this;r._hasBackButtonHandler||(n.$backButtonActions={},r.onHardwareBackButton(r.hardwareBackButtonClick),r._hasBackButtonHandler=!0);var a={id:o?o:e.Utils.nextUid(),priority:i?i:0,fn:t};return n.$backButtonActions[a.id]=a,function(){delete n.$backButtonActions[a.id]}},hardwareBackButtonClick:function(e){var t,i;for(i in n.$backButtonActions)(!t||n.$backButtonActions[i].priority>=t.priority)&&(t=n.$backButtonActions[i]);return t?(t.fn(e),t):void 0},is:function(t){return e.Platform.is(t)},ready:function(n){var i=t.defer();return e.Platform.ready(function(){i.resolve(),n()}),i.promise}}}]}})}(ionic),function(e){"use strict";angular.module("ionic.service.popup",["ionic.service.templateLoad"]).factory("$ionicPopup",["$rootScope","$q","$document","$compile","$timeout","$ionicTemplateLoader",function(t,n,i,o,r,a){var c={stackPushDelay:50},s=function(e){e.el.style.marginLeft=-e.el.offsetWidth/2+"px",e.el.style.marginTop=-e.el.offsetHeight/2+"px"},l=function(e){var t=e.el.querySelector(".popup-body");t&&""==t.innerHTML.trim()&&(t.style.display="none")},u=function(e){var t,n;t=e.el.querySelectorAll("button"),n=t[t.length-1],n&&n.focus()},d=function(t){e.requestAnimationFrame(function(){l(t),s(t),t.el.classList.remove("popup-hidden"),t.el.classList.add("popup-showing"),t.el.classList.add("active"),u(t)})},f=function(t){e.requestAnimationFrame(function(){t.el.classList.remove("popup-hidden"),t.el.classList.add("popup-showing"),t.el.classList.add("active"),u(t)})},v=function(t){e.requestAnimationFrame(function(){t.el.classList.remove("active"),t.el.classList.add("popup-hidden")})},p=function(e){e.el.offsetWidth,e.el.classList.remove("active"),e.el.classList.add("popup-hidden"),r(function(){e.el.remove()},400)},g=[],h=null,m=function(){var e=o("<ion-popup-backdrop></ion-popup-backdrop>")(t.$new(!0));i[0].body.appendChild(e[0]),h=e,i[0].body.classList.add("popup-open")},$=function(){h.remove(),r(function(){i[0].body.classList.remove("popup-open")},300)},b=function(e){var t=g[g.length-1];g.push(e),1==g.length&&m(),t?(v(t),r(function(){d(e)},c.stackPushDelay)):d(e)},w=function(){var e=g.pop(),t=g[g.length-1];p(e),t?f(t):$()},y=function(e,t){var n={el:e[0],scope:t,close:function(){w(this)}};return t.popup=n,n},k=function(e,t){return'<ion-popup title="'+e.title+'" buttons="buttons" on-button-tap="onButtonTap(button, event)" on-close="onClose(button, result, event)">'+(t||"")+"</ion-popup>"},S=function(e,r){var c=n.defer(),s={title:"",animation:"fade-in"};e=angular.extend(s,e);var l=e.scope&&e.scope.$new()||t.$new(!0);if(angular.extend(l,e),l.onClose=function(e,t){w(l.popup),r.resolve(t)},e.templateUrl)a.load(e.templateUrl).then(function(t){var n=k(e,t),r=o(n)(l);i[0].body.appendChild(r[0]),c.resolve(y(r,l))},function(e){c.reject(e)});else{var u=k(e,e.content),d=o(u)(l);i[0].body.appendChild(d[0]),c.resolve(y(d,l))}return c.promise};return{showPopup:function(e){var t=n.defer();return S(e,t).then(function(n){t.notify(n),b(n,e)},function(){}),t.promise},show:function(e){return this.showPopup(e)},alert:function(e){return this.showPopup({content:e.content||"",title:e.title||"",buttons:[{text:e.okText||"OK",type:e.okType||"button-positive",onTap:function(){return!0}}]})},confirm:function(e){return this.showPopup({content:e.content||"",title:e.title||"",buttons:[{text:e.cancelText||"Cancel",type:e.cancelType||"button-default",onTap:function(e){e.preventDefault()}},{text:e.okText||"OK",type:e.okType||"button-positive",onTap:function(){return!0}}]})},prompt:function(e){var n=t.$new(!0);return n.data={},this.showPopup({content:e.content||'<input ng-model="data.response" type="'+(e.inputType||"text")+'" placeholder="'+(e.inputPlaceholder||"")+'">',title:e.title||"",subTitle:e.subTitle||"",scope:n,buttons:[{text:e.cancelText||"Cancel",type:e.cancelType||"button-default",onTap:function(e){e.preventDefault()}},{text:e.okText||"OK",type:e.okType||"button-positive",onTap:function(){return n.data.response}}]})}}}])}(ionic),angular.module("ionic.service.templateLoad",[]).factory("$ionicTemplateLoader",["$q","$http","$templateCache",function(e,t,n){return{load:function(e){return t.get(e,{cache:n}).then(function(e){return e.data&&e.data.trim()})}}}]),angular.module("ionic.service.view",["ui.router","ionic.service.platform"]).run(["$rootScope","$state","$location","$document","$animate","$ionicPlatform",function(e,t,n,i,o,r){function a(t){return e.$viewHistory.backView?e.$viewHistory.backView.go():ionic.Platform.exitApp(),t.preventDefault(),!1}e.$viewHistory={histories:{root:{historyId:"root",parentHistoryId:null,stack:[],cursor:-1}},views:{},backView:null,forwardView:null,currentView:null,disabledRegistrableTagNames:[]},e.$on("viewState.changeHistory",function(i,o){if(o){var r=o.historyId?e.$viewHistory.histories[o.historyId]:null;if(r&&r.cursor>-1&&r.cursor<r.stack.length){var a=r.stack[r.cursor];return a.go(o)}!o.url&&o.uiSref&&(o.url=t.href(o.uiSref)),o.url&&(0===o.url.indexOf("#")&&(o.url=o.url.replace("#","")),o.url!==n.url()&&n.url(o.url))}}),e.$on("viewState.viewEnter",function(e,t){t&&t.title&&(i[0].title=t.title)}),r.registerBackButtonAction(a,100)}]).factory("$ionicViewService",["$rootScope","$state","$location","$window","$injector",function(e,t,n,i,o){function r(){return ionic.Utils.nextUid()}var a=o.has("$animate")?o.get("$animate"):!1,c=function(){};return c.prototype.initialize=function(e){if(e){for(var t in e)this[t]=e[t];return this}return null},c.prototype.go=function(){return this.stateName?t.go(this.stateName,this.stateParams):this.url&&this.url!==n.url()?e.$viewHistory.backView===this?i.history.go(-1):e.$viewHistory.forwardView===this?i.history.go(1):void n.url(this.url):null},c.prototype.destroy=function(){this.scope&&(this.scope.$destroy&&this.scope.$destroy(),this.scope=null)},{register:function(t,i){var o=e.$viewHistory,a=this.getCurrentStateId(),c=this._getHistory(t),s=o.currentView,l=o.backView,u=o.forwardView,d=this.nextViewOptions(),f={viewId:null,navAction:null,navDirection:null,historyId:c.historyId};if(i&&!this.isTagNameRegistrable(i))return f.navAction="disabledByTagName",f;if(s&&s.stateId===a&&s.historyId===c.historyId)return f.navAction="noChange",f;if(o.forcedNav)ionic.Utils.extend(f,o.forcedNav),e.$viewHistory.forcedNav=null;else if(l&&l.stateId===a)f.viewId=l.viewId,f.navAction="moveBack",f.viewId=l.viewId,l.historyId===s.historyId&&(f.navDirection="back");else if(u&&u.stateId===a){f.viewId=u.viewId,f.navAction="moveForward",u.historyId===s.historyId&&(f.navDirection="forward");var v=this._getParentHistoryObj(t);u.historyId&&v.scope&&(v.scope.$historyId=u.historyId,f.historyId=u.historyId)}else if(s&&s.historyId!==c.historyId&&c.cursor>-1&&c.stack.length>0&&c.cursor<c.stack.length&&c.stack[c.cursor].stateId===a)f.viewId=c.stack[c.cursor].viewId,f.navAction="moveBack";else{if(f.viewId=r(a),s){if(s.forwardViewId=f.viewId,c.historyId===s.historyId&&(f.navDirection="forward"),f.navAction="newView",u&&s.stateId!==u.stateId){var p=this._getHistoryById(u.historyId);if(p)for(var g=p.stack.length-1;g>=u.index;g--)p.stack[g].destroy(),p.stack.splice(g)}}else f.navAction="initialView";o.views[f.viewId]=this.createView({viewId:f.viewId,index:c.stack.length,historyId:c.historyId,backViewId:s&&s.viewId?s.viewId:null,forwardViewId:null,stateId:a,stateName:this.getCurrentStateName(),stateParams:this.getCurrentStateParams(),url:n.url()}),"moveBack"==f.navAction&&e.$emit("$viewHistory.viewBack",s.viewId,f.viewId),c.stack.push(o.views[f.viewId])}return d&&(d.disableAnimate&&(f.navDirection=null),d.disableBack&&(o.views[f.viewId].backViewId=null),this.nextViewOptions(null)),this.setNavViews(f.viewId),c.cursor=o.currentView.index,f},setNavViews:function(t){var n=e.$viewHistory;n.currentView=this._getViewById(t),n.backView=this._getBackView(n.currentView),n.forwardView=this._getForwardView(n.currentView),e.$broadcast("$viewHistory.historyChange",{showBack:n.backView&&n.backView.historyId===n.currentView.historyId})},registerHistory:function(e){e.$historyId=ionic.Utils.nextUid()},createView:function(e){var t=new c;return t.initialize(e)},getCurrentView:function(){return e.$viewHistory.currentView},getBackView:function(){return e.$viewHistory.backView},getForwardView:function(){return e.$viewHistory.forwardView},getNavDirection:function(){return e.$viewHistory.navDirection},getCurrentStateName:function(){return t&&t.current?t.current.name:null},isCurrentStateNavView:function(e){return t&&t.current&&t.current.views&&t.current.views[e]?!0:!1},getCurrentStateParams:function(){var e;if(t&&t.params)for(var n in t.params)t.params.hasOwnProperty(n)&&(e=e||{},e[n]=t.params[n]);return e},getCurrentStateId:function(){var e;if(t&&t.current&&t.current.name){if(e=t.current.name,t.params)for(var n in t.params)t.params.hasOwnProperty(n)&&t.params[n]&&(e+="_"+n+"="+t.params[n]);return e}return ionic.Utils.nextUid()},goToHistoryRoot:function(t){if(t){var n=e.$viewHistory.histories[t];if(n&&n.stack.length){if(e.$viewHistory.currentView&&e.$viewHistory.currentView.viewId===n.stack[0].viewId)return;e.$viewHistory.forcedNav={viewId:n.stack[0].viewId,navAction:"moveBack",navDirection:"back"},n.stack[0].go()}}},_getViewById:function(t){return t?e.$viewHistory.views[t]:null},_getBackView:function(e){return e?this._getViewById(e.backViewId):null},_getForwardView:function(e){return e?this._getViewById(e.forwardViewId):null},_getHistoryById:function(t){return t?e.$viewHistory.histories[t]:null},_getHistory:function(t){var n=this._getParentHistoryObj(t);return e.$viewHistory.histories[n.historyId]||(e.$viewHistory.histories[n.historyId]={historyId:n.historyId,parentHistoryId:this._getParentHistoryObj(n.scope.$parent).historyId,stack:[],cursor:-1}),e.$viewHistory.histories[n.historyId]},_getParentHistoryObj:function(t){for(var n=t;n;){if(n.hasOwnProperty("$historyId"))return{historyId:n.$historyId,scope:n};n=n.$parent}return{historyId:"root",scope:e}},nextViewOptions:function(e){return arguments.length?void(this._nextOpts=e):this._nextOpts},getRenderer:function(e,t,n){function i(e){for(var t="";!t&&e;)t=e.getAttribute("animation"),e=e.parentElement;return t}function o(){l&&e[0].classList.add(l),"back"===r.navDirection?e[0].classList.add("reverse"):e[0].classList.remove("reverse")}var r,c,s=this,l=angular.isDefined(n.$nextAnimation)?n.$nextAnimation:i(e[0]);return n.$nextAnimation=void 0,function(t){return{enter:function(n){return c&&t?(o(),n.addClass("ng-enter"),document.body.classList.add("disable-pointer-events"),void a.enter(n,e,null,function(){document.body.classList.remove("disable-pointer-events"),l&&e[0].classList.remove(l)})):void e.append(n)},leave:function(){var n=e.contents();return c&&t?(o(),void a.leave(n,function(){n.remove()})):void n.remove()},register:function(e){return r=s.register(n,e),c=null!==l&&null!==r.navDirection,r}}}},disableRegisterByTagName:function(t){e.$viewHistory.disabledRegistrableTagNames.push(t.toUpperCase())},isTagNameRegistrable:function(t){var n,i,o=e.$viewHistory.disabledRegistrableTagNames;for(n=0;n<t.length;n++)if(1===t[n].nodeType)for(i=0;i<o.length;i++)if(t[n].tagName===o[i])return!1;return!0},clearHistory:function(){var t=e.$viewHistory.histories,n=e.$viewHistory.currentView;for(var i in t)t[i].stack&&(t[i].stack=[],t[i].cursor=-1),n.historyId===i?(n.backViewId=null,n.forwardViewId=null,t[i].stack.push(n)):t[i].destroy&&t[i].destroy();for(var o in e.$viewHistory.views)o!==n.viewId&&delete e.$viewHistory.views[o];this.setNavViews(n.viewId)}}}]),angular.module("ionic.decorator.location",[]).config(["$provide",function(e){function t(e,t){return e.__hash=e.hash,e.hash=function(n){return angular.isDefined(n)&&t(function(){var e=document.querySelector(".scroll-content");e&&(e.scrollTop=0)},0,!1),e.__hash(n)},e}e.decorator("$location",["$delegate","$timeout",t])}]),function(){"use strict";angular.module("ionic.ui.actionSheet",[]).directive("ionActionSheet",["$document",function(e){return{restrict:"E",scope:!0,replace:!0,link:function(t,n){var i=function(e){27==e.which&&(t.cancel(),t.$apply())},o=function(e){e.target==n[0]&&(t.cancel(),t.$apply())};t.$on("$destroy",function(){n.remove(),e.unbind("keyup",i)}),e.bind("keyup",i),n.bind("click",o)},template:'<div class="action-sheet-backdrop"><div class="action-sheet-wrapper"><div class="action-sheet"><div class="action-sheet-group"><div class="action-sheet-title" ng-if="titleText">{{titleText}}</div><button class="button" ng-click="buttonClicked($index)" ng-repeat="button in buttons">{{button.text}}</button></div><div class="action-sheet-group" ng-if="destructiveText"><button class="button destructive" ng-click="destructiveButtonClicked()">{{destructiveText}}</button></div><div class="action-sheet-group" ng-if="cancelText"><button class="button" ng-click="cancel()">{{cancelText}}</button></div></div></div></div>'}}])}(),function(e){"use strict";function t(){return["$ionicScrollDelegate",function(t){return{restrict:"E",link:function(n,i){function o(n){if(!e.DomUtil.getParentOrSelfWithClass(n.target,"button",4)){var o=n.gesture&&n.gesture.touches[0]||n.detail.touches[0],r=i[0].getBoundingClientRect();e.DomUtil.rectContains(o.pageX,o.pageY,r.left,r.top-20,r.left+r.width,r.top+r.height)&&t.scrollTop(!0)}}e.on("tap",o,i[0]),n.$on("$destroy",function(){e.off("tap",o,i[0])})}}}]}function n(t){return[function(){return{restrict:"E",compile:function(n){function i(n,i,o){var r=(new e.views.HeaderBar({el:i[0],alignTitle:o.alignTitle||"center"}),i[0]),a=n.$parent||n;t?(n.$watch(function(){return r.className},function(e){var t=-1!==e.indexOf("bar-subheader");a.$hasHeader=!t,a.$hasSubheader=t}),n.$on("$destroy",function(){a.$hasHeader=a.$hasSubheader=null})):(n.$watch(function(){return r.className},function(e){var t=-1!==e.indexOf("bar-subfooter");a.$hasFooter=!t,a.$hasSubfooter=t}),n.$on("$destroy",function(){a.$hasFooter=a.$hasSubfooter=null}),n.$watch("$hasTabs",function(e){i.toggleClass("has-tabs",!!e)}))}return n.addClass(t?"bar bar-header":"bar bar-footer"),{pre:i}}}}]}angular.module("ionic.ui.header",["ngAnimate","ngSanitize"]).directive("ionNavBar",t()).directive("ionHeaderBar",t()).directive("ionHeaderBar",n(!0)).directive("ionFooterBar",n(!1))}(ionic),function(){"use strict";angular.module("ionic.ui.checkbox",[]).directive("ionCheckbox",function(){return{restrict:"E",replace:!0,require:"?ngModel",scope:{ngModel:"=?",ngValue:"=?",ngChecked:"=?",ngChange:"&"},transclude:!0,template:'<div class="item item-checkbox disable-pointer-events"><label class="checkbox enable-pointer-events"><input type="checkbox" ng-model="ngModel" ng-value="ngValue" ng-change="ngChange()"></label><div class="item-content" ng-transclude></div></div>',compile:function(e,t){var n=e.find("input");t.name&&n.attr("name",t.name),t.ngChecked&&n.attr("ng-checked","ngChecked"),t.ngTrueValue&&n.attr("ng-true-value",t.ngTrueValue),t.ngFalseValue&&n.attr("ng-false-value",t.ngFalseValue)}}})}(),function(){"use strict";angular.module("ionic.ui.content",["ionic.ui.scroll"]).directive("ionPane",function(){return{restrict:"E",link:function(e,t){t.addClass("pane")}}}).directive("ionContent",["$timeout","$controller","$ionicBind",function(e,t,n){return{restrict:"E",require:"^?ionNavView",scope:!0,compile:function(e,i){function o(e,o,a){e.$watch(function(){return(e.$hasHeader?" has-header":"")+(e.$hasSubheader?" has-subheader":"")+(e.$hasFooter?" has-footer":"")+(e.$hasSubfooter?" has-subfooter":"")+(e.$hasTabs?" has-tabs":"")+(e.$hasTabsTop?" has-tabs-top":"")},function(e,t){o.removeClass(t),o.addClass(e)}),n(e,a,{$onScroll:"&onScroll",$onScrollComplete:"&onScrollComplete",hasBouncing:"@",scroll:"@",padding:"@",hasScrollX:"@",hasScrollY:"@",scrollbarX:"@",scrollbarY:"@",startX:"@",startY:"@",scrollEventInterval:"@"}),angular.isDefined(a.padding)&&e.$watch(a.padding,function(e){r.toggleClass("padding",!!e)}),"false"===e.scroll||("true"===i.overflowScroll?o.addClass("overflow-scroll"):t("$ionicScroll",{$scope:e,scrollViewOptions:{el:o[0],delegateHandle:i.delegateHandle,bouncing:e.$eval(e.hasBouncing),startX:e.$eval(e.startX)||0,startY:e.$eval(e.startY)||0,scrollbarX:e.$eval(e.scrollbarX)!==!1,scrollbarY:e.$eval(e.scrollbarY)!==!1,scrollingX:e.$eval(e.hasScrollX)===!0,scrollingY:e.$eval(e.hasScrollY)!==!1,scrollEventInterval:parseInt(e.scrollEventInterval,10)||20,scrollingComplete:function(){e.$onScrollComplete({scrollTop:this.__scrollTop,scrollLeft:this.__scrollLeft})}}}))}e.addClass("scroll-content");var r=angular.element('<div class="scroll"></div>');return r.append(e.contents()),e.append(r),{pre:o}}}}]).directive("ionRefresher",["$ionicBind",function(e){return{restrict:"E",replace:!0,require:"^$ionicScroll",template:'<div class="scroll-refresher"><div class="ionic-refresher-content"><i class="icon {{pullingIcon}} icon-pulling"></i><span class="icon-pulling" ng-bind-html="pullingText"></span><i class="icon {{refreshingIcon}} icon-refreshing"></i><span class="icon-refreshing" ng-bind-html="refreshingText"></span></div></div>',compile:function(t,n){return angular.isUndefined(n.pullingIcon)&&n.$set("pullingIcon","ion-arrow-down-c"),angular.isUndefined(n.refreshingIcon)&&n.$set("refreshingIcon","ion-loading-d"),function(t,n,i,o){e(t,i,{pullingIcon:"@",pullingText:"@",refreshingIcon:"@",refreshingText:"@",$onRefresh:"&onRefresh",$onPulling:"&onPulling"}),o._setRefresher(t,n[0]),t.$on("scroll.refreshComplete",function(){n[0].classList.remove("active"),o.scrollView.finishPullToRefresh()})}}}}]).directive("ionInfiniteScroll",["$timeout",function(e){return{restrict:"E",require:["^$ionicScroll","ionInfiniteScroll"],template:'<div class="scroll-infinite"><div class="scroll-infinite-content"><i class="icon {{icon()}} icon-refreshing"></i></div></div>',scope:!0,controller:["$scope","$attrs",function(e,t){this.isLoading=!1,this.scrollView=null,this.getMaxScroll=function(){var e=t.distance||"1%";return e.indexOf("%")>-1?this.scrollView.getScrollMax().top*(1-parseInt(e,10)/100):this.scrollView.getScrollMax().top-parseInt(e,10)}}],link:function(t,n,i,o){var r=o[0],a=o[1],c=a.scrollView=r.scrollView;t.icon=function(){return angular.isDefined(i.icon)?i.icon:"ion-loading-d"},t.$on("scroll.infiniteScrollComplete",function(){n[0].classList.remove("active"),e(function(){c.resize()},0,!1),a.isLoading=!1}),r.$element.on("scroll",ionic.animationFrameThrottle(function(){!a.isLoading&&c.getValues().top>=a.getMaxScroll()&&(n[0].classList.add("active"),a.isLoading=!0,t.$parent.$apply(i.onInfinite||""))}))}}}])}(),function(){"use strict";angular.module("ionic.ui.list",["ngAnimate"]).directive("ionItem",["$timeout","$parse",function(){return{restrict:"E",require:"?^ionList",replace:!0,transclude:!0,scope:{item:"=",itemType:"@",canDelete:"@",canReorder:"@",canSwipe:"@",onDelete:"&",optionButtons:"&",deleteIcon:"@",reorderIcon:"@"},template:'<div class="item item-complex">            <div class="item-left-edit item-delete" ng-if="deleteClick !== undefined">              <button class="button button-icon icon" ng-class="deleteIconClass" ng-click="deleteClick()" ion-stop-event="click"></button>            </div>            <a class="item-content" ng-href="{{ href }}" ng-transclude></a>            <div class="item-right-edit item-reorder" ng-if="reorderIconClass !== undefined">              <button data-ionic-action="reorder" data-prevent-scroll="true" class="button button-icon icon" ng-class="reorderIconClass"></button>            </div>            <div class="item-options" ng-if="itemOptionButtons">             <button ng-click="b.onTap(item, b)" ion-stop-event="click" class="button" ng-class="b.type" ng-repeat="b in itemOptionButtons" ng-bind="b.text"></button>           </div>          </div>',link:function(e,t,n,i){if(i){var o=i.scope,r=i.attrs;n.$observe("href",function(t){t&&(e.href=t.trim())}),e.itemType||(e.itemType=o.itemType),t.addClass(e.itemType||o.itemType),e.itemClass=e.itemType,"false"!==(n.canDelete?e.canDelete:o.canDelete)&&(n.onDelete||r.onDelete)&&(e.deleteClick=function(){n.onDelete?e.onDelete({item:e.item,index:e.$parent.$index}):r.onDelete&&o.onDelete({item:e.item,index:e.$parent.$index})},e.deleteIconClass=e.deleteIcon||o.deleteIcon||"ion-minus-circled",t.addClass("item-left-editable")),"true"===(n.canReorder?e.canReorder:o.canReorder)&&(e.reorderIconClass=e.reorderIcon||o.reorderIcon||"ion-navicon",t.addClass("item-right-editable")),"false"!==(n.canSwipe?e.canSwipe:o.canSwipe)&&(e.itemOptionButtons=e.optionButtons(),"undefined"==typeof e.itemOptionButtons&&(e.itemOptionButtons=o.optionButtons()),t.addClass("item-swipeable"))}}}}]).directive("ionList",["$timeout",function(e){return{restrict:"E",replace:!0,transclude:!0,require:"^?$ionicScroll",scope:{itemType:"@",canDelete:"@",canReorder:"@",canSwipe:"@",showDelete:"=",showReorder:"=",onDelete:"&",onReorder:"&",optionButtons:"&",deleteIcon:"@",reorderIcon:"@"},template:"<div class=\"list\" ng-class=\"{'list-left-editing': showDelete, 'list-right-editing': showReorder}\" ng-transclude></div>",controller:["$scope","$attrs",function(e,t){this.scope=e,this.attrs=t}],link:function(t,n,i,o){t.listView=new ionic.views.ListView({canSwipe:"false"!==t.canSwipe&&!!t.optionButtons(),el:n[0],listEl:n[0].children[0],scrollEl:o&&o.element,scrollView:o&&o.scrollView,onReorder:function(e,n,i){t.$apply(function(){t.onReorder({el:e,start:n,end:i})})}}),i.animation&&n[0].classList.add(i.animation);var r=t.$watch("showReorder",function(i){i?(n[0].classList.add("item-options-hide"),t.listView&&t.listView.clearDragEffects()):i===!1&&e(function(){n[0].classList.remove("item-options-hide")},250)});t.$on("$destroy",function(){r()})}}}])}(),function(){"use strict";angular.module("ionic.ui.loading",[]).directive("ionLoading",function(){return{restrict:"E",replace:!0,transclude:!0,link:function(e,t){t.addClass(e.animation||"")},template:'<div class="loading-backdrop" ng-class="{\'show-backdrop\': showBackdrop}"><div class="loading" ng-transclude></div></div>'}})}(),function(){"use strict";angular.module("ionic.ui.modal",[]).directive("ionModal",[function(){return{restrict:"E",transclude:!0,replace:!0,template:'<div class="modal-backdrop"><div class="modal-wrapper" ng-transclude></div></div>'}}])}(),angular.module("ionic.ui.navBar",["ionic.service.view","ngSanitize"]).service("$ionicNavBarDelegate",e(["back","align","showBackButton","showBar","setTitle","changeTitle","getTitle","getPreviousTitle"])).controller("$ionicNavBar",["$scope","$element","$attrs","$ionicViewService","$animate","$compile","$ionicNavBarDelegate",function(e,t,n,i,o,r,a){t.parent().data("$ionNavBarController",this);var c=a._registerInstance(this,n.delegateHandle);e.$on("$destroy",c);var s=this;this.leftButtonsElement=angular.element(t[0].querySelector(".buttons.left-buttons")),this.rightButtonsElement=angular.element(t[0].querySelector(".buttons.right-buttons")),this.back=function(e){var t=i.getBackView();return t&&t.go(),e&&(e.alreadyHandled=!0),!1},this.align=function(e){this._headerBarView.align(e)},this.showBackButton=function(t){e.backButtonShown=!!t},this.showBar=function(t){e.isInvisible=!t},this.setTitle=function(t){e.oldTitle=e.title,e.title=t||""},this.changeTitle=function(t,n){return e.title===t?!1:(this.setTitle(t),e.isReverse="back"==n,e.shouldAnimate=!!n,e.shouldAnimate?this._animateTitles():this._headerBarView.align(),!0)},this.getTitle=function(){return e.title||""},this.getPreviousTitle=function(){return e.oldTitle||""},this._animateTitles=function(){var n,i,a;a=t[0].querySelectorAll(".title"),a.length&&(n=r('<h1 class="title" ng-bind-html="oldTitle"></h1>')(e),angular.element(a[0]).replaceWith(n)),i=r('<h1 class="title invisible" ng-bind-html="title"></h1>')(e),ionic.requestAnimationFrame(function(){n&&o.leave(angular.element(n));var r=n&&angular.element(n)||null;o.enter(i,t,r,function(){s._headerBarView.align()}),angular.forEach(a,function(e){e&&e.parentNode&&angular.element(e).remove()}),e.$digest(),ionic.requestAnimationFrame(function(){i[0].classList.remove("invisible")})})}}]).directive("ionNavBar",["$ionicViewService","$rootScope","$animate","$compile",function(){return{restrict:"E",controller:"$ionicNavBar",scope:!0,compile:function(e){function t(e,t,n,i){i._headerBarView=new ionic.views.HeaderBar({el:t[0],alignTitle:n.alignTitle||"center"}),e.backButtonShown=!1,e.shouldAnimate=!0,e.isReverse=!1,e.isInvisible=!0,e.$parent.$hasHeader=!0,e.$on("$destroy",function(){e.$parent.$hasHeader=!1}),e.$watch(function(){return(e.isReverse?" reverse":"")+(e.isInvisible?" invisible":"")+(e.shouldAnimate?"":" no-animation")},function(e,n){t.removeClass(n),t.addClass(e)})}return e.addClass("bar bar-header nav-bar").append('<div class="buttons left-buttons"> </div><h1 ng-bind-html="title" class="title"></h1><div class="buttons right-buttons"> </div>'),{pre:t}
}}}]).directive("ionNavBackButton",["$ionicNgClick",function(e){return{restrict:"E",require:"^ionNavBar",compile:function(t){return t.addClass("button back-button"),function(t,n,i,o){i.ngClick||(t.$navBack=o.back,e(t,n,"$navBack($event)"));var r=t.$parent.$on("$viewHistory.historyChange",function(e,n){t.hasBackButton=!!n.showBack});t.$on("$destroy",r),t.$watch("!!(backButtonShown && hasBackButton)",function(e){n.toggleClass("hide",!e)})}}}}]).directive("ionNavButtons",["$compile","$animate",function(e,t){return{require:"^ionNavBar",restrict:"E",compile:function(n){var i=n.contents().remove();return function(n,o,r,a){var c="right"===r.side?a.rightButtonsElement:a.leftButtonsElement,s=angular.element("<div>").append(i);o.append(s),e(s)(n),t.enter(s,c),n.$on("$destroy",function(){t.leave(s)}),o.css("display","none")}}}}]),function(){"use strict";angular.module("ionic.ui.popup",[]).directive("ionPopupBackdrop",function(){return{restrict:"E",replace:!0,template:'<div class="popup-backdrop"></div>'}}).directive("ionPopup",["$ionicBind",function(e){return{restrict:"E",replace:!0,transclude:!0,scope:!0,template:'<div class="popup"><div class="popup-head"><h3 class="popup-title" ng-bind-html="title"></h3><h5 class="popup-sub-title" ng-bind-html="subTitle" ng-if="subTitle"></h5></div><div class="popup-body" ng-transclude></div><div class="popup-buttons row"><button ng-repeat="button in buttons" ng-click="_buttonTapped(button, $event)" class="button col" ng-class="button.type || \'button-default\'" ng-bind-html="button.text"></button></div></div>',link:function(t,n,i){e(t,i,{title:"@",buttons:"=",$onButtonTap:"&onButtonTap",$onClose:"&onClose"}),t._buttonTapped=function(e,n){var i=e.onTap&&e.onTap(n);return n.defaultPrevented?t.$onClose({button:e,result:!1,event:n}):i?t.$onClose({button:e,result:i,event:n}):void t.$onButtonTap({button:e,event:n})}}}}])}(),function(e){"use strict";angular.module("ionic.ui.radio",[]).directive("ionRadio",function(){return{restrict:"E",replace:!0,require:"?ngModel",scope:{ngModel:"=?",ngValue:"=?",ngChange:"&",icon:"@"},transclude:!0,template:'<label class="item item-radio"><input type="radio" name="radio-group" ng-model="ngModel" ng-value="ngValue" ng-change="ngChange()"><div class="item-content disable-pointer-events" ng-transclude></div><i class="radio-icon disable-pointer-events icon ion-checkmark"></i></label>',compile:function(e,t){t.name&&e.children().eq(0).attr("name",t.name),t.icon&&e.children().eq(2).removeClass("ion-checkmark").addClass(t.icon)}}}).directive("ionRadioButtons",function(){return{restrict:"E",replace:!0,require:"?ngModel",scope:{value:"@"},transclude:!0,template:'<div class="button-bar button-bar-inline" ng-transclude></div>',controller:["$scope","$element",function(e,t){this.select=function(e){for(var n,i=t.children(),o=0;o<i.length;o++)n=i[o],n!=e[0]&&n.classList.remove("active")}}],link:function(e,t,n,i){i&&(i.$render=function(){for(var n=t.children(),o=0;o<n.length;o++)n[o].classList.remove("active");e.$parent.$broadcast("radioButton.select",i.$viewValue)})}}}).directive("ionButtonRadio",function(){return{restrict:"CA",require:["?^ngModel","?^ionRadioButtons"],link:function(t,n,i,o){var r=o[0],a=o[1];if(r&&a){var c=function(){n.addClass("active"),r.$setViewValue(t.$eval(i.ngValue)),a.select(n)},s=function(){c()};t.$on("radioButton.select",function(e,o){o==t.$eval(i.ngValue)&&n.addClass("active")}),e.on("tap",s,n[0]),t.$on("$destroy",function(){e.off("tap",s)})}}}})}(window.ionic),function(){"use strict";angular.module("ionic.ui.scroll",[]).directive("ionScroll",["$timeout","$controller","$ionicBind",function(e,t,n){return{restrict:"E",scope:!0,controller:function(){},compile:function(e){function i(e,i,r){var a,c;n(e,r,{direction:"@",paging:"@",$onScroll:"&onScroll",scroll:"@",scrollbarX:"@",scrollbarY:"@"}),angular.isDefined(r.padding)&&e.$watch(r.padding,function(e){o.toggleClass("padding",!!e)}),e.$eval(e.paging)===!0&&o.addClass("scroll-paging"),e.direction||(e.direction="y");var s=e.$eval(e.paging)===!0,l={el:i[0],delegateHandle:r.delegateHandle,paging:s,scrollbarX:e.$eval(e.scrollbarX)!==!1,scrollbarY:e.$eval(e.scrollbarY)!==!1,scrollingX:e.direction.indexOf("x")>=0,scrollingY:e.direction.indexOf("y")>=0};s&&(l.speedMultiplier=.8,l.bouncing=!1),c=t("$ionicScroll",{$scope:e,scrollViewOptions:l}),a=e.$parent.scrollView=c.scrollView}e.addClass("scroll-view");var o=angular.element('<div class="scroll"></div>');return o.append(e.contents()),e.append(o),{pre:i}}}}])}(),function(){"use strict";angular.module("ionic.ui.sideMenu",["ionic.service.gesture","ionic.service.view"]).run(["$ionicViewService",function(e){e.disableRegisterByTagName("ion-side-menus")}]).service("$ionicSideMenuDelegate",e(["toggleLeft","toggleRight","isOpenLeft","isOpenRight","canDragContent"])).directive("ionSideMenus",function(){return{restrict:"ECA",controller:["$scope","$attrs","$ionicSideMenuDelegate",function(e,t,n){angular.extend(this,ionic.controllers.SideMenuController.prototype),ionic.controllers.SideMenuController.call(this,{left:{width:275},right:{width:275}}),this.canDragContent=function(t){return arguments.length&&(e.dragContent=!!t),e.dragContent},e.sideMenuContentTranslateX=0;var i=n._registerInstance(this,t.delegateHandle);e.$on("$destroy",i)}],replace:!0,transclude:!0,template:'<div class="view" ng-transclude></div>'}}).directive("ionSideMenuContent",["$timeout","$ionicGesture",function(e,t){return{restrict:"EA",require:"^ionSideMenus",scope:!0,compile:function(n,i){function o(n,o,r,a){function c(e){0!==a.getOpenAmount()&&(a.close(),e.gesture.srcEvent.preventDefault())}o.addClass("menu-content pane"),angular.isDefined(i.dragContent)?n.$watch(i.dragContent,function(e){a.canDragContent(e)}):a.canDragContent(!0);var s=!1,l=!1;ionic.on("tap",c,o[0]);var u=function(e){if(n.dragContent){if(s||e.gesture.srcEvent.defaultPrevented)return;l=!0,a._handleDrag(e),e.gesture.srcEvent.preventDefault()}},d=function(e){l&&e.gesture.srcEvent.preventDefault()},f=t.on("dragright",u,o),v=t.on("dragleft",u,o),p=t.on("dragup",d,o),g=t.on("dragdown",d,o),h=function(e){l=!1,s||a._endDrag(e),s=!1},m=t.on("release",h,o);a.setContent({onDrag:function(){},endDrag:function(){},getTranslateX:function(){return n.sideMenuContentTranslateX||0},setTranslateX:ionic.animationFrameThrottle(function(t){o[0].style[ionic.CSS.TRANSFORM]="translate3d("+t+"px, 0, 0)",e(function(){n.sideMenuContentTranslateX=t})}),enableAnimation:function(){n.animationEnabled=!0,o[0].classList.add("menu-animated")},disableAnimation:function(){n.animationEnabled=!1,o[0].classList.remove("menu-animated")}}),n.$on("$destroy",function(){t.off(v,"dragleft",u),t.off(f,"dragright",u),t.off(p,"dragup",u),t.off(g,"dragdown",u),t.off(m,"release",h),ionic.off("tap",c,o[0])})}return{pre:o}}}}]).directive("ionSideMenu",function(){return{restrict:"E",require:"^ionSideMenus",scope:!0,compile:function(e,t){return angular.isUndefined(t.isEnabled)&&t.$set("isEnabled","true"),angular.isUndefined(t.width)&&t.$set("width","275"),e.addClass("menu menu-"+t.side),function(e,t,n,i){e.side=n.side||"left";var o=i[e.side]=new ionic.views.SideMenu({width:275,el:t[0],isEnabled:!0});e.$watch(n.width,function(e){var t=+e;t&&t==e&&o.setWidth(+e)}),e.$watch(n.isEnabled,function(e){o.setIsEnabled(!!e)})}}}}).directive("menuToggle",["$ionicViewService",function(){return{restrict:"AC",require:"^ionSideMenus",link:function(e,t,n,i){var o=e.$eval(n.menuToggle)||"left";t.bind("click",function(){"left"===o?i.toggleLeft():"right"===o&&i.toggleRight()})}}}]).directive("menuClose",["$ionicViewService",function(){return{restrict:"AC",require:"^ionSideMenus",link:function(e,t,n,i){t.bind("click",function(){i.close()})}}}])}(),function(){"use strict";angular.module("ionic.ui.slideBox",[]).service("$ionicSlideBoxDelegate",e(["update","slide","previous","next","stop","currentIndex","slidesCount"])).directive("ionSlideBox",["$timeout","$compile","$ionicSlideBoxDelegate",function(e,t,n){return{restrict:"E",replace:!0,transclude:!0,scope:{doesContinue:"@",slideInterval:"@",showPager:"@",disableScroll:"@",onSlideChanged:"&",activeSlide:"=?"},controller:["$scope","$element","$attrs",function(t,i,o){var r=t.$eval(t.doesContinue)===!0,a=r?t.$eval(t.slideInterval)||4e3:0,c=new ionic.views.Slider({el:i[0],auto:a,disableScroll:t.$eval(t.disableScroll)===!0||!1,continuous:r,startSlide:t.activeSlide,slidesChanged:function(){t.currentSlide=c.currentIndex(),e(function(){})},callback:function(n){t.currentSlide=n,t.onSlideChanged({index:t.currentSlide}),t.$parent.$broadcast("slideBox.slideChanged",n),t.activeSlide=n,e(function(){})}});t.$watch("activeSlide",function(e){angular.isDefined(e)&&c.slide(e)}),t.$on("slideBox.nextSlide",function(){c.next()}),t.$on("slideBox.prevSlide",function(){c.prev()}),t.$on("slideBox.setSlide",function(e,t){c.slide(t)}),this.__slider=c;var s=n._registerInstance(c,o.delegateHandle);t.$on("$destroy",s),this.slidesCount=function(){return c.slidesCount()},e(function(){c.load()})}],template:'<div class="slider">            <div class="slider-slides" ng-transclude>            </div>          </div>',link:function(e,n){if(e.$eval(e.showPager)!==!1){var i=e.$new(),o=angular.element("<ion-pager></ion-pager>");n.append(o),t(o)(i)}}}}]).directive("ionSlide",function(){return{restrict:"E",require:"^ionSlideBox",compile:function(e){return e.addClass("slider-slide"),function(){}}}}).directive("ionPager",function(){return{restrict:"E",replace:!0,require:"^ionSlideBox",template:'<div class="slider-pager"><span class="slider-pager-page" ng-repeat="slide in numSlides() track by $index" ng-class="{active: $index == currentSlide}"><i class="icon ion-record"></i></span></div>',link:function(e,t,n,i){var o=function(e){for(var n=t[0].children,i=n.length,o=0;i>o;o++)o==e?n[o].classList.add("active"):n[o].classList.remove("active")};e.numSlides=function(){return new Array(i.slidesCount())},e.$watch("currentSlide",function(e){o(e)})}}})}(),angular.module("ionic.ui.tabs",["ionic.service.view"]).run(["$ionicViewService",function(e){e.disableRegisterByTagName("ion-tabs")}]).service("$ionicTabsDelegate",e(["select","selectedIndex"])).controller("ionicTabs",["$scope","$ionicViewService","$element",function(e,t){var n=null,i=this;i.tabs=[],i.selectedIndex=function(){return i.tabs.indexOf(n)},i.selectedTab=function(){return n},i.add=function(e){t.registerHistory(e),i.tabs.push(e),1===i.tabs.length&&i.select(e)},i.remove=function(e){var t=i.tabs.indexOf(e);if(-1!==t){if(e.$tabSelected)if(i.deselect(e),1===i.tabs.length);else{var n=t===i.tabs.length-1?t-1:t+1;i.select(i.tabs[n])}i.tabs.splice(t,1)}},i.deselect=function(e){e.$tabSelected&&(n=null,e.$tabSelected=!1,(e.onDeselect||angular.noop)())},i.select=function(o,r){var a;if(angular.isNumber(o)?(a=o,o=i.tabs[a]):a=i.tabs.indexOf(o),!o||-1==a)throw new Error('Cannot select tab "'+a+'"!');if(n&&n.$historyId==o.$historyId)r&&t.goToHistoryRoot(o.$historyId);else if(angular.forEach(i.tabs,function(e){i.deselect(e)}),n=o,o.$tabSelected=!0,(o.onSelect||angular.noop)(),r){var c={type:"tab",tabIndex:a,historyId:o.$historyId,navViewName:o.navViewName,hasNavView:!!o.navViewName,title:o.title,url:o.href,uiSref:o.uiSref};e.$emit("viewState.changeHistory",c)}}}]).directive("ionTabs",["$ionicViewService","$ionicTabsDelegate",function(e,t){return{restrict:"E",scope:!0,controller:"ionicTabs",compile:function(e){function n(e,n,i,o){var r=t._registerInstance(o,i.delegateHandle);e.$on("$destroy",r),o.$scope=e,o.$element=n,o.$tabsElement=angular.element(n[0].querySelector(".tabs"));var a=n[0];e.$watch(function(){return a.className},function(t){var n=-1!==t.indexOf("tabs-top"),i=-1!==t.indexOf("tabs-item-hide");e.$hasTabs=!n&&!i,e.$hasTabsTop=n&&!i}),e.$on("$destroy",function(){e.$hasTabs=e.$hasTabsTop=null})}e.addClass("view");var i=angular.element('<div class="tabs"></div>');return i.append(e.contents()),e.append(i),{pre:n}}}}]).controller("ionicTab",["$scope","$ionicViewService","$rootScope","$element",function(e){this.$scope=e}]).directive("ionTab",["$rootScope","$animate","$ionicBind","$compile","$ionicViewService",function(e,t,n,i,o){function r(e,t){return angular.isDefined(t)?" "+e+'="'+t+'"':""}return{restrict:"E",require:["^ionTabs","ionTab"],replace:!0,controller:"ionicTab",scope:!0,compile:function(e,a){var c=e[0].querySelector("ion-nav-view")||e[0].querySelector("data-ion-nav-view"),s=c&&c.getAttribute("name"),l=(angular.element(e[0].querySelector("ion-tab-nav")||e[0].querySelector("data-ion-tab-nav")).remove(),angular.element('<div class="pane">').append(e.contents().remove()));return function(e,c,u,d){function f(){o.isCurrentStateNavView(e.navViewName)&&tabsCtrl.select(e)}var v,p,g;tabsCtrl=d[0],tabCtrl=d[1],c[0].removeAttribute("title"),n(e,u,{animate:"=",onSelect:"&",onDeselect:"&",title:"@",uiSref:"@",href:"@"}),tabsCtrl.add(e),e.$on("$destroy",function(){tabsCtrl.remove(e),g.isolateScope().$destroy(),g.remove()}),s&&(e.navViewName=s,e.$on("$stateChangeSuccess",f),f()),g=angular.element("<ion-tab-nav"+r("ng-click",a.ngClick)+r("title",a.title)+r("icon",a.icon)+r("icon-on",a.iconOn)+r("icon-off",a.iconOff)+r("badge",a.badge)+r("badge-style",a.badgeStyle)+"></ion-tab-nav>"),g.data("$ionTabsController",tabsCtrl),g.data("$ionTabController",tabCtrl),tabsCtrl.$tabsElement.append(i(g)(e)),e.$watch("$tabSelected",function(n){v&&v.$destroy(),v=null,p&&t.leave(p),p=null,n&&(v=e.$new(),p=l.clone(),t.enter(p,tabsCtrl.$element),i(p)(v))})}}}}]).directive("ionTabNav",["$ionicNgClick",function(e){return{restrict:"E",replace:!0,require:["^ionTabs","^ionTab"],template:'<a ng-class="{\'tab-item-active\': isTabActive(), \'has-badge\':badge}"  class="tab-item"><span class="badge {{badgeStyle}}" ng-if="badge">{{badge}}</span><i class="icon {{getIconOn()}}" ng-if="getIconOn() && isTabActive()"></i><i class="icon {{getIconOff()}}" ng-if="getIconOff() && !isTabActive()"></i><span class="tab-title" ng-bind-html="title"></span></a>',scope:{title:"@",icon:"@",iconOn:"@",iconOff:"@",badge:"=",badgeStyle:"@"},compile:function(){return function(t,n,i,o){var r=o[0],a=o[1];n[0].removeAttribute("title"),t.selectTab=function(e){e.preventDefault(),r.select(a.$scope,!0)},i.ngClick||e(t,n,"selectTab($event)"),t.getIconOn=function(){return t.iconOn||t.icon},t.getIconOff=function(){return t.iconOff||t.icon},t.isTabActive=function(){return r.selectedTab()===a.$scope}}}}}]),function(e){"use strict";angular.module("ionic.ui.toggle",[]).directive("ionToggle",["$ionicGesture","$timeout",function(){return{restrict:"E",replace:!0,require:"?ngModel",scope:{ngModel:"=?",ngValue:"=?",ngChecked:"=?",ngChange:"&",ngDisabled:"=?"},transclude:!0,template:'<div class="item item-toggle disable-pointer-events"><div ng-transclude></div><label class="toggle enable-pointer-events"><input type="checkbox" ng-model="ngModel" ng-value="ngValue" ng-change="ngChange()" ng-disabled="ngDisabled"><div class="track disable-pointer-events"><div class="handle"></div></div></label></div>',compile:function(t,n){var i=t.find("input");return n.name&&i.attr("name",n.name),n.ngChecked&&i.attr("ng-checked","ngChecked"),n.ngTrueValue&&i.attr("ng-true-value",n.ngTrueValue),n.ngFalseValue&&i.attr("ng-false-value",n.ngFalseValue),function(t,n){var i,o,r,a;i=n[0].getElementsByTagName("label")[0],o=i.children[0],r=i.children[1],a=r.children[0];var c=angular.element(o).controller("ngModel");t.toggle=new e.views.Toggle({el:i,track:r,checkbox:o,handle:a,onChange:function(){c.$setViewValue(o.checked?!0:!1),t.$apply()}}),t.$on("$destroy",function(){t.toggle.destroy()})}}}}])}(window.ionic),function(e,t){"use strict";e.module("ionic.ui.touch",[]).config(["$provide",function(e){e.decorator("ngClickDirective",["$delegate",function(e){return e.shift(),e}])}]).factory("$ionicNgClick",["$parse",function(e){function n(e){t.tapElement(e.target,e)}return function(i,o,r){var a=e(r);o.on("click",function(e){i.$apply(function(){a(i,{$event:e})})}),t.on("release",n,o[0]),o.onclick=function(){},i.$on("$destroy",function(){t.off("release",n,o[0])})}}]).directive("ngClick",["$ionicNgClick",function(e){return function(t,n,i){e(t,n,i.ngClick)}}]).directive("ionStopEvent",function(){function e(e){e.stopPropagation()}return{restrict:"A",link:function(t,n,i){n.bind(i.ionStopEvent,e)}}})}(window.angular,window.ionic),function(){"use strict";angular.module("ionic.ui.viewState",["ionic.service.view","ionic.service.gesture","ngSanitize"]).directive("ionView",["$ionicViewService","$rootScope","$animate",function(){return{restrict:"EA",priority:1e3,require:"^?ionNavBar",compile:function(e){return e.addClass("pane"),e[0].removeAttribute("title"),function(e,t,n,i){if(i){var o=n.title;i.changeTitle(o,e.$navDirection),n.$observe("title",function(e){e!==o&&i.setTitle(e)}),e.$watch(n.hideBackButton,function(e){i.showBackButton(!e)}),e.$watch(n.hideNavBar,function(e){i.showBar(!e)})}}}}}]).directive("ionNavView",["$ionicViewService","$state","$compile","$controller","$animate",function(e,t,n,i,o){var r=!1,a={restrict:"E",terminal:!0,priority:2e3,transclude:!0,controller:[function(){}],compile:function(c,s,l){return function(c,s,u){function d(r){o.enabled()===!1&&(r=!1);var a=t.$current&&t.$current.locals[p];if(a!==v){var l=e.getRenderer(s,u,c);if(f&&(f.$destroy(),f=null),!a)return v=null,$.state=null,s.append(h);var d=angular.element("<div></div>").html(a.$template).contents(),m=l().register(d);l(r).leave(),v=a,$.state=a.$$state,l(r).enter(d);var b=n(d);if(f=c.$new(),f.$navDirection=m.navDirection,a.$$controller){a.$scope=f;var w=i(a.$$controller,a);s.children().data("$ngControllerController",w)}b(f);var y=e._getViewById(m.viewId)||{};f.$broadcast("$viewContentLoaded",y),g&&f.$eval(g),d=null}}var f,v,p=u[a.name]||u.name||"",g=u.onload||"",h=l(c);s.append(h);var m=s.parent().inheritedData("$uiView");p.indexOf("@")<0&&(p=p+"@"+(m?m.state.name:""));var $={name:p,state:null};s.data("$uiView",$);var b=function(){if(!r){r=!0;try{d(!0)}catch(e){throw r=!1,e}r=!1}};c.$on("$stateChangeSuccess",b),c.$on("$viewContentLoading",b),d(!1)}}};return a}]).directive("navClear",["$ionicViewService",function(e){return{restrict:"AC",link:function(t,n){n.bind("click",function(){e.nextViewOptions({disableAnimate:!0,disableBack:!0})})}}}])}(),angular.module("ionic.ui.scroll").service("$ionicScrollDelegate",e(["resize","scrollTop","scrollBottom","scrollTo","anchorScroll","rememberScrollPosition","forgetScrollPosition","scrollToRememberedPosition"])).factory("$$scrollValueCache",function(){return{}}).controller("$ionicScroll",["$scope","scrollViewOptions","$timeout","$window","$$scrollValueCache","$location","$rootScope","$document","$ionicScrollDelegate",function(e,t,n,i,o,r,a,c,s){var l=this;this._scrollViewOptions=t;var u=this.element=t.el,d=this.$element=angular.element(u),f=this.scrollView=new ionic.views.Scroll(t);(d.parent().length?d.parent():d).data("$$ionicScrollController",this);var v=s._registerInstance(this,t.delegateHandle);angular.isDefined(t.bouncing)||ionic.Platform.ready(function(){f.options.bouncing=!ionic.Platform.isAndroid()});var p=angular.bind(f,f.resize);ionic.on("resize",p,i);var g=angular.noop;e.$on("$destroy",function(){v(),ionic.off("resize",p,i),i.removeEventListener("resize",p),g(),l._rememberScrollId&&(o[l._rememberScrollId]=f.getValues())}),d.on("scroll",function(t){var n=(t.originalEvent||t).detail||{};e.$onScroll&&e.$onScroll({event:t,scrollTop:n.scrollTop||0,scrollLeft:n.scrollLeft||0})}),e.$on("$viewContentLoaded",function(e,t){if(!e.defaultPrevented){e.preventDefault();var n=t&&t.viewId;n&&(l.rememberScrollPosition(n),l.scrollToRememberedPosition(),g=a.$on("$viewHistory.viewBack",function(e,t){n===t&&l.forgetScrollPosition()}))}}),n(function(){f.run()}),this._rememberScrollId=null,this.resize=function(){return n(p)},this.scrollTop=function(e){this.resize().then(function(){f.scrollTo(0,0,!!e)})},this.scrollBottom=function(e){this.resize().then(function(){var t=f.getScrollMax();f.scrollTo(t.left,t.top,!!e)})},this.scrollTo=function(e,t,n){this.resize().then(function(){f.scrollTo(e,t,!!n)})},this.anchorScroll=function(e){this.resize().then(function(){var t=r.hash(),n=t&&c[0].getElementById(t);if(t&&n){var i=ionic.DomUtil.getPositionInParent(n,l.$element);f.scrollTo(i.left,i.top,!!e)}else f.scrollTo(0,0,!!e)})},this.rememberScrollPosition=function(e){if(!e)throw new Error("Must supply an id to remember the scroll by!");this._rememberScrollId=e},this.forgetScrollPosition=function(){delete o[this._rememberScrollId],this._rememberScrollId=null},this.scrollToRememberedPosition=function(e){var t=o[this._rememberScrollId];t&&this.resize().then(function(){f.scrollTo(+t.left,+t.top,e)})},this._setRefresher=function(e,t){var n=this.refresher=t,i=l.refresher.clientHeight||0;f.activatePullToRefresh(i,function(){n.classList.add("active"),e.$onPulling()},function(){n.classList.remove("refreshing"),n.classList.remove("active")},function(){n.classList.add("refreshing"),e.$onRefresh()})}}])}();
/*
 AngularJS v1.2.12
 (c) 2010-2014 Google, Inc. http://angularjs.org
 License: MIT
*/

(function(H,a,A){'use strict';function D(p,g){g=g||{};a.forEach(g,function(a,c){delete g[c]});for(var c in p)!p.hasOwnProperty(c)||"$"===c.charAt(0)&&"$"===c.charAt(1)||(g[c]=p[c]);return g}var v=a.$$minErr("$resource"),C=/^(\.[a-zA-Z_$][0-9a-zA-Z_$]*)+$/;a.module("ngResource",["ng"]).factory("$resource",["$http","$q",function(p,g){function c(a,c){this.template=a;this.defaults=c||{};this.urlParams={}}function t(n,w,l){function r(h,d){var e={};d=x({},w,d);s(d,function(b,d){u(b)&&(b=b());var k;if(b&&
b.charAt&&"@"==b.charAt(0)){k=h;var a=b.substr(1);if(null==a||""===a||"hasOwnProperty"===a||!C.test("."+a))throw v("badmember",a);for(var a=a.split("."),f=0,c=a.length;f<c&&k!==A;f++){var g=a[f];k=null!==k?k[g]:A}}else k=b;e[d]=k});return e}function e(a){return a.resource}function f(a){D(a||{},this)}var F=new c(n);l=x({},B,l);s(l,function(h,d){var c=/^(POST|PUT|PATCH)$/i.test(h.method);f[d]=function(b,d,k,w){var q={},n,l,y;switch(arguments.length){case 4:y=w,l=k;case 3:case 2:if(u(d)){if(u(b)){l=
b;y=d;break}l=d;y=k}else{q=b;n=d;l=k;break}case 1:u(b)?l=b:c?n=b:q=b;break;case 0:break;default:throw v("badargs",arguments.length);}var t=this instanceof f,m=t?n:h.isArray?[]:new f(n),z={},B=h.interceptor&&h.interceptor.response||e,C=h.interceptor&&h.interceptor.responseError||A;s(h,function(a,b){"params"!=b&&("isArray"!=b&&"interceptor"!=b)&&(z[b]=G(a))});c&&(z.data=n);F.setUrlParams(z,x({},r(n,h.params||{}),q),h.url);q=p(z).then(function(b){var d=b.data,k=m.$promise;if(d){if(a.isArray(d)!==!!h.isArray)throw v("badcfg",
h.isArray?"array":"object",a.isArray(d)?"array":"object");h.isArray?(m.length=0,s(d,function(b){m.push(new f(b))})):(D(d,m),m.$promise=k)}m.$resolved=!0;b.resource=m;return b},function(b){m.$resolved=!0;(y||E)(b);return g.reject(b)});q=q.then(function(b){var a=B(b);(l||E)(a,b.headers);return a},C);return t?q:(m.$promise=q,m.$resolved=!1,m)};f.prototype["$"+d]=function(b,a,k){u(b)&&(k=a,a=b,b={});b=f[d].call(this,b,this,a,k);return b.$promise||b}});f.bind=function(a){return t(n,x({},w,a),l)};return f}
var B={get:{method:"GET"},save:{method:"POST"},query:{method:"GET",isArray:!0},remove:{method:"DELETE"},"delete":{method:"DELETE"}},E=a.noop,s=a.forEach,x=a.extend,G=a.copy,u=a.isFunction;c.prototype={setUrlParams:function(c,g,l){var r=this,e=l||r.template,f,p,h=r.urlParams={};s(e.split(/\W/),function(a){if("hasOwnProperty"===a)throw v("badname");!/^\d+$/.test(a)&&(a&&RegExp("(^|[^\\\\]):"+a+"(\\W|$)").test(e))&&(h[a]=!0)});e=e.replace(/\\:/g,":");g=g||{};s(r.urlParams,function(d,c){f=g.hasOwnProperty(c)?
g[c]:r.defaults[c];a.isDefined(f)&&null!==f?(p=encodeURIComponent(f).replace(/%40/gi,"@").replace(/%3A/gi,":").replace(/%24/g,"$").replace(/%2C/gi,",").replace(/%20/g,"%20").replace(/%26/gi,"&").replace(/%3D/gi,"=").replace(/%2B/gi,"+"),e=e.replace(RegExp(":"+c+"(\\W|$)","g"),function(a,c){return p+c})):e=e.replace(RegExp("(/?):"+c+"(\\W|$)","g"),function(a,c,d){return"/"==d.charAt(0)?d:c+d})});e=e.replace(/\/+$/,"")||"/";e=e.replace(/\/\.(?=\w+($|\?))/,".");c.url=e.replace(/\/\\\./,"/.");s(g,function(a,
e){r.urlParams[e]||(c.params=c.params||{},c.params[e]=a)})}};return t}])})(window,window.angular);
//# sourceMappingURL=angular-resource.min.js.map
;
/*
 AngularJS v1.2.12
 (c) 2010-2014 Google, Inc. http://angularjs.org
 License: MIT
 */

(function(h,e,A){'use strict';function u(w,q,k){return{restrict:"ECA",terminal:!0,priority:400,transclude:"element",link:function(a,c,b,f,n){function y(){l&&(l.$destroy(),l=null);g&&(k.leave(g),g=null)}function v(){var b=w.current&&w.current.locals;if(e.isDefined(b&&b.$template)){var b=a.$new(),f=w.current;g=n(b,function(d){k.enter(d,null,g||c,function(){!e.isDefined(t)||t&&!a.$eval(t)||q()});y()});l=f.scope=b;l.$emit("$viewContentLoaded");l.$eval(h)}else y()}var l,g,t=b.autoscroll,h=b.onload||"";
    a.$on("$routeChangeSuccess",v);v()}}}function z(e,h,k){return{restrict:"ECA",priority:-400,link:function(a,c){var b=k.current,f=b.locals;c.html(f.$template);var n=e(c.contents());b.controller&&(f.$scope=a,f=h(b.controller,f),b.controllerAs&&(a[b.controllerAs]=f),c.data("$ngControllerController",f),c.children().data("$ngControllerController",f));n(a)}}}h=e.module("ngRoute",["ng"]).provider("$route",function(){function h(a,c){return e.extend(new (e.extend(function(){},{prototype:a})),c)}function q(a,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         e){var b=e.caseInsensitiveMatch,f={originalPath:a,regexp:a},h=f.keys=[];a=a.replace(/([().])/g,"\\$1").replace(/(\/)?:(\w+)([\?\*])?/g,function(a,e,b,c){a="?"===c?c:null;c="*"===c?c:null;h.push({name:b,optional:!!a});e=e||"";return""+(a?"":e)+"(?:"+(a?e:"")+(c&&"(.+?)"||"([^/]+)")+(a||"")+")"+(a||"")}).replace(/([\/$\*])/g,"\\$1");f.regexp=RegExp("^"+a+"$",b?"i":"");return f}var k={};this.when=function(a,c){k[a]=e.extend({reloadOnSearch:!0},c,a&&q(a,c));if(a){var b="/"==a[a.length-1]?a.substr(0,a.length-
    1):a+"/";k[b]=e.extend({redirectTo:a},q(b,c))}return this};this.otherwise=function(a){this.when(null,a);return this};this.$get=["$rootScope","$location","$routeParams","$q","$injector","$http","$templateCache","$sce",function(a,c,b,f,n,q,v,l){function g(){var d=t(),m=r.current;if(d&&m&&d.$$route===m.$$route&&e.equals(d.pathParams,m.pathParams)&&!d.reloadOnSearch&&!x)m.params=d.params,e.copy(m.params,b),a.$broadcast("$routeUpdate",m);else if(d||m)x=!1,a.$broadcast("$routeChangeStart",d,m),(r.current=
    d)&&d.redirectTo&&(e.isString(d.redirectTo)?c.path(u(d.redirectTo,d.params)).search(d.params).replace():c.url(d.redirectTo(d.pathParams,c.path(),c.search())).replace()),f.when(d).then(function(){if(d){var a=e.extend({},d.resolve),c,b;e.forEach(a,function(d,c){a[c]=e.isString(d)?n.get(d):n.invoke(d)});e.isDefined(c=d.template)?e.isFunction(c)&&(c=c(d.params)):e.isDefined(b=d.templateUrl)&&(e.isFunction(b)&&(b=b(d.params)),b=l.getTrustedResourceUrl(b),e.isDefined(b)&&(d.loadedTemplateUrl=b,c=q.get(b,
    {cache:v}).then(function(a){return a.data})));e.isDefined(c)&&(a.$template=c);return f.all(a)}}).then(function(c){d==r.current&&(d&&(d.locals=c,e.copy(d.params,b)),a.$broadcast("$routeChangeSuccess",d,m))},function(c){d==r.current&&a.$broadcast("$routeChangeError",d,m,c)})}function t(){var a,b;e.forEach(k,function(f,k){var p;if(p=!b){var s=c.path();p=f.keys;var l={};if(f.regexp)if(s=f.regexp.exec(s)){for(var g=1,q=s.length;g<q;++g){var n=p[g-1],r="string"==typeof s[g]?decodeURIComponent(s[g]):s[g];
    n&&r&&(l[n.name]=r)}p=l}else p=null;else p=null;p=a=p}p&&(b=h(f,{params:e.extend({},c.search(),a),pathParams:a}),b.$$route=f)});return b||k[null]&&h(k[null],{params:{},pathParams:{}})}function u(a,c){var b=[];e.forEach((a||"").split(":"),function(a,d){if(0===d)b.push(a);else{var e=a.match(/(\w+)(.*)/),f=e[1];b.push(c[f]);b.push(e[2]||"");delete c[f]}});return b.join("")}var x=!1,r={routes:k,reload:function(){x=!0;a.$evalAsync(g)}};a.$on("$locationChangeSuccess",g);return r}]});h.provider("$routeParams",
    function(){this.$get=function(){return{}}});h.directive("ngView",u);h.directive("ngView",z);u.$inject=["$route","$anchorScroll","$animate"];z.$inject=["$compile","$controller","$route"]})(window,window.angular);
//# sourceMappingURL=angular-route.min.js.map
;
/**
 * Checklist-model
 * AngularJS directive for list of checkboxes
 */


angular.module('checklist-model', [])
    .directive('checklistModel', ['$parse', '$compile', function ($parse, $compile) {
        // contains
        function contains(arr, item) {
            return typeof(arr[item]) !== 'undefined' ? arr[item] : false;
        }

        // add
        function add(arr, item) {
            arr[item] = true;
            return arr;
        }

        // remove
        function remove(arr, item) {
            arr[item] = false;
            return arr;
        }

        // http://stackoverflow.com/a/19228302/1458162
        function postLinkFn(scope, elem, attrs) {
            // compile with `ng-model` pointing to `checked`
            $compile(elem)(scope);

            // getter / setter for original model
            var getter = $parse(attrs.checklistModel);
            var setter = getter.assign;

            // value added to list
            var value = $parse(attrs.checklistValue)(scope.$parent);

            // watch UI checked change
            scope.$watch('checked', function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }

                var current = getter(scope.$parent) || {},
                    val
                    ;

                if (newValue === true) {
                    val = add(current, value);
                } else {
                    val = remove(current, value);
                }

                setter(scope.$parent, val);
            });

            // watch original model change
            scope.$parent.$watch(attrs.checklistModel, function (newArr, oldArr) {
                scope.checked = contains(newArr, value);
            }, true);
        }

        return {
            restrict: 'A',
            priority: 1000,
            terminal: true,
            scope: true,
            compile: function (tElement, tAttrs) {
                if (tElement[0].tagName !== 'INPUT' || !tElement.attr('type', 'checkbox')) {
                    throw 'checklist-model should be applied to `input[type="checkbox"]`.';
                }

                if (!tAttrs.checklistValue) {
                    throw 'You should provide `checklist-value`.';
                }

                // exclude recursion
                tElement.removeAttr('checklist-model');

                // local scope var storing individual checkbox model
                tElement.attr('ng-model', 'checked');

                return postLinkFn;
            }
        };
    }]);
/*! yaMap 17-03-2014 */

"use strict";angular.module("yaMap",[]).constant("GEOMETRY_TYPES",{POINT:"Point",LINESTRING:"LineString",RECTANGLE:"Rectangle",POLYGON:"Polygon",CIRCLE:"Circle"}).value("yaMapSettings",{lang:"ru-RU",order:"longlat"}).factory("mapApiLoad",["yaMapSettings",function(a){var b=!1,c=[],d=function(){b=!0;for(var a;c.length;)a=c.splice(0,1),a[0]()},e="http://api-maps.yandex.ru/2.0/?load=package.full&lang="+(a.lang||"ru-RU")+"&coordorder="+(a.order||"longlat"),f=!1,g=function(a,c){if(!f){f=!0;var d=document.createElement("script");d.type="text/javascript",d.readyState?d.onreadystatechange=function(){("loaded"==d.readyState||"complete"==d.readyState)&&(d.onreadystatechange=null,f=!1,b=!0,c())}:d.onload=function(){f=!1,b=!0,c()},d.src=a,document.getElementsByTagName("head")[0].appendChild(d)}};return function(a){c.push(a),b?d():f||g(e,function(){ymaps.ready(function(){d()})})}}]).service("yaLayer",[function(){this.create=function(a,b){return new ymaps.Layer(a,b)}}]).service("yaMapType",[function(){this.create=function(a,b){return new ymaps.MapType(a,b)}}]).service("layerStorage",["mapApiLoad",function(a){this.get=function(b){if(this._storage)b(this._storage);else{var c=this;a(function(){c._storage=ymaps.layer.storage,b(c._storage)})}}}]).service("mapTypeStorage",["mapApiLoad",function(a){this.get=function(b){if(this._storage)b(this._storage);else{var c=this;a(function(){c._storage=ymaps.mapType.storage,b(c._storage)})}}}]).service("yaSubscriber",function(){var a=/^yaEvent(\w*)?([A-Z]{1}[a-z]+)$/;this.subscribe=function(b,c,d,e){var f=a.exec(d),g=f[2].toLowerCase(),h=f[1]?f[1][1].toLowerCase()+f[1].substring(1):void 0;e[d]=function(a){return c(e.$parent||e,a)};var i=h?b[h].events:b.events;i.add(g,function(a){setTimeout(function(){e.$apply(function(){e[d]({$event:a})})})})}}).service("templateLayoutFactory",[function(){this._cache={},this.get=function(a){return this._cache[a]},this.create=function(a,b,c){this._cache[a]||(this._cache[a]=ymaps.templateLayoutFactory.createClass(b,c))}}]).directive("yaTemplateLayout",["templateLayoutFactory",function(a){return{restrict:"E",priority:1001,scope:{overrides:"=yaOverrides"},compile:function(b){var c=b.html();return b.html(""),function(b,d,e){if(!e.yaKey)throw new Error('not require attribute "key"');var f=e.yaKey;a.create(f,c,b.overrides)}}}}]).controller("YaMapCtrl",["$scope","mapApiLoad",function(a,b){var c=this;b(function(){c.addGeoObjects=function(b){a.map.geoObjects.add(b)},c.removeGeoObjects=function(b){a.map.geoObjects.remove(b)},c.addControl=function(b,c){a.map.controls.add(b,c)},c.getMap=function(){return a.map},c.addImageLayer=function(b,c){var d=new ymaps.Layer(b,c);a.map.layers.add(d)},c.addHotspotLayer=function(b,c,d){var e=new ymaps.hotspot.ObjectSource(b,c),f=new ymaps.hotspot.Layer(e,d);a.map.layers.add(f)},c.addToolbar=function(b){a.map.controls.add(b)}})}]).directive("yaMap",["$compile","mapApiLoad","yaMapSettings","$window","yaSubscriber","$parse","$q","$timeout",function(a,b,c,d,e,f,g,h){return{restrict:"E",scope:{yaCenter:"@",yaType:"@",yaBeforeInit:"&",yaAfterInit:"&"},compile:function(d){var i=d.children(),j=null;return d.html(""),function(d,k,l){for(var m,n=function(a){try{return d.$eval(a)}catch(b){return a}},o=function(a){j&&j.reject(),j=g.defer();var e;return a?angular.isArray(a)?h(function(){j.resolve(a)}):angular.isString(a)&&b(function(){ymaps.geocode(a,{results:1}).then(function(a){var b=a.geoObjects.get(0);e=b.geometry.getCoordinates(),d.$apply(function(){j.resolve(e)})},function(a){d.$apply(function(){j.reject(a)})})}):b(function(){e="longlat"===c.order?[ymaps.geolocation.longitude,ymaps.geolocation.latitude]:[ymaps.geolocation.latitude,ymaps.geolocation.longitude],h(function(){j.resolve(e)})}),j.promise},p=Number(l.yaZoom),q=l.yaBehaviors?l.yaBehaviors.split(" "):["default"],r=[],s=[],t=0,u=q.length;u>t;t++)m=q[t],"-"===m[0]?r.push(m.substring(1)):s.push(m);0>p?p=0:p>23&&(p=23);var v,w=function(c){var h=g.defer();return b(function(){d.yaBeforeInit();var b=l.yaOptions?d.$eval(l.yaOptions):void 0;b&&b.projection&&(b.projection=new ymaps.projection[b.projection.type](b.projection.bounds)),d.map=new ymaps.Map(k[0],{center:c,zoom:p,type:l.yaType||"yandex#map",behaviors:s},b),d.map.behaviors.disable(r);for(var g in l)if(0===g.indexOf("yaEvent")){var j=f(l[g]);e.subscribe(d.map,j,g,d)}h.resolve(d.map),d.yaAfterInit({$target:d.map}),k.append(i),setTimeout(function(){d.$apply(function(){a(i)(d.$parent)})})}),h.promise};d.$watch("yaCenter",function(a){var b=n(a);o(b).then(function(a){if(!v){v=w(a);var b=!0}v.then(function(c){b||c.setCenter(a)})})}),d.$watch("yaType",function(a){a&&v&&v.then(function(b){b.setType(a)})}),d.$on("$destroy",function(){d.map&&d.map.destroy()})}},controller:"YaMapCtrl"}}]).controller("MapToolbarCtrl",["$scope",function(a){this.add=function(b){a.toolbar.add(b)}}]).directive("yaToolbar",["$compile","$parse","yaSubscriber",function(a,b,c){return{require:"^yaMap",restrict:"E",scope:{yaAfterInit:"&"},compile:function(d){var e=d.contents();return d.html(""),function(d,f,g,h){if(!g.yaName)throw new Error('not pass attribute "name"');var i=g.yaOptions?d.$eval(g.yaOptions):void 0,j=g.yaParams?d.$eval(g.yaParams):void 0,k=g.yaName[0].toUpperCase()+g.yaName.substring(1);d.toolbar=new ymaps.control[k](j);for(var l in g)if(0===l.indexOf("yaEvent")){var m=b(g[l]);c.subscribe(d.toolbar,m,l,d)}h.addControl(d.toolbar,i),d.yaAfterInit({$target:d.toolbar}),f.append(e),a(e)(d.$parent)}},controller:"MapToolbarCtrl"}}]).directive("yaControl",["yaSubscriber","templateLayoutFactory","$parse",function(a,b,c){return{restrict:"E",require:"^yaToolbar",scope:{yaAfterInit:"&"},link:function(d,e,f,g){var h=f.yaType[0].toUpperCase()+f.yaType.substring(1),i=function(a){try{return d.$eval(a)}catch(b){return a}},j=i(f.yaParams),k=f.yaOptions?d.$eval(f.yaOptions):void 0;k&&k.layout&&(k.layout=b.get(k.layout)),k&&k.itemLayout&&(k.itemLayout=b.get(k.itemLayout));var l,m=["SearchControl","SmallZoomControl","ScaleLine","ZoomControl"];if(m.indexOf(h)>-1)l=new ymaps.control[h](k);else{if(j&&j.items){for(var n,o=[],p=0,q=j.items.length;q>p;p++)n=j.items[p],o.push(new ymaps.control.ListBoxItem(n));j.items=o}l=new ymaps.control[h](j,k)}for(var r in f)if(0===r.indexOf("yaEvent")){var s=c(f[r]);a.subscribe(l,s,r,d)}g.add(l),d.yaAfterInit({$target:l})}}}]).controller("CollectionCtrl",["$scope",function(a){this.addGeoObjects=function(b){a.collection.add(b)},this.removeGeoObjects=function(b){a.collection.remove(b)}}]).directive("yaCollection",["$compile","yaMapSettings","$timeout","yaSubscriber","$parse",function(a,b,c,d,e){return{require:"^yaMap",restrict:"E",scope:{yaAfterInit:"&"},compile:function(b){var f=b.contents();return b.html(""),function(b,g,h,i){var j=h.yaOptions?b.$eval(h.yaOptions):{},k=angular.isDefined(h.showAll)&&"false"!=h.showAll;if(k){var l,m=i.getMap(),n=function(){l&&c.cancel(l),l=c(function(){m.geoObjects.events.remove("add",n);var a=m.geoObjects.getBounds();a&&m.setBounds(a)},300)};m.geoObjects.events.add("add",n)}b.collection=new ymaps.GeoObjectCollection({},j);for(var o in h)if(0===o.indexOf("yaEvent")){var p=e(h[o]);d.subscribe(b.collection,p,o,b)}i.addGeoObjects(b.collection),b.yaAfterInit({$target:b.collection}),b.$on("$destroy",function(){b.collection&&i.removeGeoObjects(b.collection)}),g.append(f),a(f)(b.$parent)}},controller:"CollectionCtrl"}}]).directive("yaCluster",["yaMapSettings","yaSubscriber","$compile","templateLayoutFactory","$parse",function(a,b,c,d,e){return{require:"^yaMap",restrict:"E",scope:{yaAfterInit:"&"},compile:function(a){var f=a.contents();return a.html(""),function(a,g,h,i){var j=h.yaOptions?a.$eval(h.yaOptions):{};j&&j.clusterBalloonMainContentLayout&&(j.clusterBalloonMainContentLayout=d.get(j.clusterBalloonMainContentLayout)),j&&j.clusterBalloonSidebarItemLayout&&(j.clusterBalloonSidebarItemLayout=d.get(j.clusterBalloonSidebarItemLayout)),j&&j.clusterBalloonContentItemLayout&&(j.clusterBalloonContentItemLayout=d.get(j.clusterBalloonContentItemLayout)),j&&j.clusterBalloonAccordionItemContentLayout&&(j.clusterBalloonAccordionItemContentLayout=d.get(j.clusterBalloonAccordionItemContentLayout)),a.collection=new ymaps.Clusterer(j);for(var k in h)if(0===k.indexOf("yaEvent")){var l=e(h[k]);b.subscribe(a.collection,l,k,a)}i.addGeoObjects(a.collection),a.yaAfterInit({$target:a.collection}),a.$on("$destroy",function(){a.collection&&i.removeGeoObjects(a.collection)}),g.append(f),c(f)(a.$parent)}},controller:"CollectionCtrl"}}]).directive("yaGeoObject",["GEOMETRY_TYPES","yaSubscriber","templateLayoutFactory","$parse",function(a,b,c,d){return{restrict:"E",require:["^yaMap","?^yaCollection","?^yaCluster"],scope:{yaSource:"=",yaShowBalloon:"=",yaAfterInit:"&"},link:function(e,f,g,h){var i,j=h[2]||h[1]||h[0],k=g.yaOptions?e.$eval(g.yaOptions):void 0;k&&k.balloonContentLayout&&(k.balloonContentLayout=c.get(k.balloonContentLayout));var l=function(a,c){i=new ymaps.GeoObject(a,c);for(var f in g)if(0===f.indexOf("yaEvent")){var h=d(g[f]);b.subscribe(i,h,f,e)}j.addGeoObjects(i),e.yaAfterInit({$target:i}),m(g.yaEdit),n(g.yaDraw),o(e.yaShowBalloon)};e.$watch("yaSource",function(b){if(b)if(i){i.geometry.setCoordinates(b.geometry.coordinates),i.geometry.getType()===a.CIRCLE&&i.geometry.setRadius(b.geometry.radius);var c=b.properties;for(var d in c)c.hasOwnProperty(d)&&i.properties.set(d,c[d])}else l(b,k);else i&&j.removeGeoObjects(i)},angular.equals);var m=function(a){angular.isDefined(a)&&"false"!==a?i&&i.editor.startEditing():angular.isDefined(a)&&i&&i.editor.stopEditing()},n=function(a){angular.isDefined(a)&&"false"!==a?i&&i.editor.startDrawing():angular.isDefined(a)&&i&&i.editor.stopDrawing()},o=function(a){a?i&&i.balloon.open():i&&i.balloon.close()};g.$observe("yaEdit",m),g.$observe("yaDraw",n),e.$watch("yaShowBalloon",o),e.$on("$destroy",function(){i&&j.removeGeoObjects(i)})}}}]).directive("yaHotspotLayer",[function(){return{restrict:"E",require:"^yaMap",link:function(a,b,c,d){if(!c.yaUrlTemplate)throw new Error('not exists required attribute "url-template"');if(!c.yaKeyTemplate)throw new Error('not exists required attribute "key-template"');var e=c.yaOptions?a.$eval(c.yaOptions):void 0;d.addHotspotLayer(c.yaUrlTemplate,c.yaKeyTemplate,e)}}}]).directive("yaImageLayer",[function(){return{restrict:"E",require:"^yaMap",link:function(a,b,c,d){if(!c.yaUrlTemplate)throw new Error('not exists required attribute "url-template"');var e=c.yaOptions?a.$eval(c.yaOptions):void 0;d.addImageLayer(c.yaUrlTemplate,e)}}}]);
//! moment.js
//! version : 2.5.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com
(function(a){function b(){return{empty:!1,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:!1,invalidMonth:null,invalidFormat:!1,userInvalidated:!1,iso:!1}}function c(a,b){return function(c){return k(a.call(this,c),b)}}function d(a,b){return function(c){return this.lang().ordinal(a.call(this,c),b)}}function e(){}function f(a){w(a),h(this,a)}function g(a){var b=q(a),c=b.year||0,d=b.month||0,e=b.week||0,f=b.day||0,g=b.hour||0,h=b.minute||0,i=b.second||0,j=b.millisecond||0;this._milliseconds=+j+1e3*i+6e4*h+36e5*g,this._days=+f+7*e,this._months=+d+12*c,this._data={},this._bubble()}function h(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c]);return b.hasOwnProperty("toString")&&(a.toString=b.toString),b.hasOwnProperty("valueOf")&&(a.valueOf=b.valueOf),a}function i(a){var b,c={};for(b in a)a.hasOwnProperty(b)&&qb.hasOwnProperty(b)&&(c[b]=a[b]);return c}function j(a){return 0>a?Math.ceil(a):Math.floor(a)}function k(a,b,c){for(var d=""+Math.abs(a),e=a>=0;d.length<b;)d="0"+d;return(e?c?"+":"":"-")+d}function l(a,b,c,d){var e,f,g=b._milliseconds,h=b._days,i=b._months;g&&a._d.setTime(+a._d+g*c),(h||i)&&(e=a.minute(),f=a.hour()),h&&a.date(a.date()+h*c),i&&a.month(a.month()+i*c),g&&!d&&db.updateOffset(a),(h||i)&&(a.minute(e),a.hour(f))}function m(a){return"[object Array]"===Object.prototype.toString.call(a)}function n(a){return"[object Date]"===Object.prototype.toString.call(a)||a instanceof Date}function o(a,b,c){var d,e=Math.min(a.length,b.length),f=Math.abs(a.length-b.length),g=0;for(d=0;e>d;d++)(c&&a[d]!==b[d]||!c&&s(a[d])!==s(b[d]))&&g++;return g+f}function p(a){if(a){var b=a.toLowerCase().replace(/(.)s$/,"$1");a=Tb[a]||Ub[b]||b}return a}function q(a){var b,c,d={};for(c in a)a.hasOwnProperty(c)&&(b=p(c),b&&(d[b]=a[c]));return d}function r(b){var c,d;if(0===b.indexOf("week"))c=7,d="day";else{if(0!==b.indexOf("month"))return;c=12,d="month"}db[b]=function(e,f){var g,h,i=db.fn._lang[b],j=[];if("number"==typeof e&&(f=e,e=a),h=function(a){var b=db().utc().set(d,a);return i.call(db.fn._lang,b,e||"")},null!=f)return h(f);for(g=0;c>g;g++)j.push(h(g));return j}}function s(a){var b=+a,c=0;return 0!==b&&isFinite(b)&&(c=b>=0?Math.floor(b):Math.ceil(b)),c}function t(a,b){return new Date(Date.UTC(a,b+1,0)).getUTCDate()}function u(a){return v(a)?366:365}function v(a){return a%4===0&&a%100!==0||a%400===0}function w(a){var b;a._a&&-2===a._pf.overflow&&(b=a._a[jb]<0||a._a[jb]>11?jb:a._a[kb]<1||a._a[kb]>t(a._a[ib],a._a[jb])?kb:a._a[lb]<0||a._a[lb]>23?lb:a._a[mb]<0||a._a[mb]>59?mb:a._a[nb]<0||a._a[nb]>59?nb:a._a[ob]<0||a._a[ob]>999?ob:-1,a._pf._overflowDayOfYear&&(ib>b||b>kb)&&(b=kb),a._pf.overflow=b)}function x(a){return null==a._isValid&&(a._isValid=!isNaN(a._d.getTime())&&a._pf.overflow<0&&!a._pf.empty&&!a._pf.invalidMonth&&!a._pf.nullInput&&!a._pf.invalidFormat&&!a._pf.userInvalidated,a._strict&&(a._isValid=a._isValid&&0===a._pf.charsLeftOver&&0===a._pf.unusedTokens.length)),a._isValid}function y(a){return a?a.toLowerCase().replace("_","-"):a}function z(a,b){return b._isUTC?db(a).zone(b._offset||0):db(a).local()}function A(a,b){return b.abbr=a,pb[a]||(pb[a]=new e),pb[a].set(b),pb[a]}function B(a){delete pb[a]}function C(a){var b,c,d,e,f=0,g=function(a){if(!pb[a]&&rb)try{require("./lang/"+a)}catch(b){}return pb[a]};if(!a)return db.fn._lang;if(!m(a)){if(c=g(a))return c;a=[a]}for(;f<a.length;){for(e=y(a[f]).split("-"),b=e.length,d=y(a[f+1]),d=d?d.split("-"):null;b>0;){if(c=g(e.slice(0,b).join("-")))return c;if(d&&d.length>=b&&o(e,d,!0)>=b-1)break;b--}f++}return db.fn._lang}function D(a){return a.match(/\[[\s\S]/)?a.replace(/^\[|\]$/g,""):a.replace(/\\/g,"")}function E(a){var b,c,d=a.match(vb);for(b=0,c=d.length;c>b;b++)d[b]=Yb[d[b]]?Yb[d[b]]:D(d[b]);return function(e){var f="";for(b=0;c>b;b++)f+=d[b]instanceof Function?d[b].call(e,a):d[b];return f}}function F(a,b){return a.isValid()?(b=G(b,a.lang()),Vb[b]||(Vb[b]=E(b)),Vb[b](a)):a.lang().invalidDate()}function G(a,b){function c(a){return b.longDateFormat(a)||a}var d=5;for(wb.lastIndex=0;d>=0&&wb.test(a);)a=a.replace(wb,c),wb.lastIndex=0,d-=1;return a}function H(a,b){var c,d=b._strict;switch(a){case"DDDD":return Ib;case"YYYY":case"GGGG":case"gggg":return d?Jb:zb;case"Y":case"G":case"g":return Lb;case"YYYYYY":case"YYYYY":case"GGGGG":case"ggggg":return d?Kb:Ab;case"S":if(d)return Gb;case"SS":if(d)return Hb;case"SSS":if(d)return Ib;case"DDD":return yb;case"MMM":case"MMMM":case"dd":case"ddd":case"dddd":return Cb;case"a":case"A":return C(b._l)._meridiemParse;case"X":return Fb;case"Z":case"ZZ":return Db;case"T":return Eb;case"SSSS":return Bb;case"MM":case"DD":case"YY":case"GG":case"gg":case"HH":case"hh":case"mm":case"ss":case"ww":case"WW":return d?Hb:xb;case"M":case"D":case"d":case"H":case"h":case"m":case"s":case"w":case"W":case"e":case"E":return xb;default:return c=new RegExp(P(O(a.replace("\\","")),"i"))}}function I(a){a=a||"";var b=a.match(Db)||[],c=b[b.length-1]||[],d=(c+"").match(Qb)||["-",0,0],e=+(60*d[1])+s(d[2]);return"+"===d[0]?-e:e}function J(a,b,c){var d,e=c._a;switch(a){case"M":case"MM":null!=b&&(e[jb]=s(b)-1);break;case"MMM":case"MMMM":d=C(c._l).monthsParse(b),null!=d?e[jb]=d:c._pf.invalidMonth=b;break;case"D":case"DD":null!=b&&(e[kb]=s(b));break;case"DDD":case"DDDD":null!=b&&(c._dayOfYear=s(b));break;case"YY":e[ib]=s(b)+(s(b)>68?1900:2e3);break;case"YYYY":case"YYYYY":case"YYYYYY":e[ib]=s(b);break;case"a":case"A":c._isPm=C(c._l).isPM(b);break;case"H":case"HH":case"h":case"hh":e[lb]=s(b);break;case"m":case"mm":e[mb]=s(b);break;case"s":case"ss":e[nb]=s(b);break;case"S":case"SS":case"SSS":case"SSSS":e[ob]=s(1e3*("0."+b));break;case"X":c._d=new Date(1e3*parseFloat(b));break;case"Z":case"ZZ":c._useUTC=!0,c._tzm=I(b);break;case"w":case"ww":case"W":case"WW":case"d":case"dd":case"ddd":case"dddd":case"e":case"E":a=a.substr(0,1);case"gg":case"gggg":case"GG":case"GGGG":case"GGGGG":a=a.substr(0,2),b&&(c._w=c._w||{},c._w[a]=b)}}function K(a){var b,c,d,e,f,g,h,i,j,k,l=[];if(!a._d){for(d=M(a),a._w&&null==a._a[kb]&&null==a._a[jb]&&(f=function(b){var c=parseInt(b,10);return b?b.length<3?c>68?1900+c:2e3+c:c:null==a._a[ib]?db().weekYear():a._a[ib]},g=a._w,null!=g.GG||null!=g.W||null!=g.E?h=Z(f(g.GG),g.W||1,g.E,4,1):(i=C(a._l),j=null!=g.d?V(g.d,i):null!=g.e?parseInt(g.e,10)+i._week.dow:0,k=parseInt(g.w,10)||1,null!=g.d&&j<i._week.dow&&k++,h=Z(f(g.gg),k,j,i._week.doy,i._week.dow)),a._a[ib]=h.year,a._dayOfYear=h.dayOfYear),a._dayOfYear&&(e=null==a._a[ib]?d[ib]:a._a[ib],a._dayOfYear>u(e)&&(a._pf._overflowDayOfYear=!0),c=U(e,0,a._dayOfYear),a._a[jb]=c.getUTCMonth(),a._a[kb]=c.getUTCDate()),b=0;3>b&&null==a._a[b];++b)a._a[b]=l[b]=d[b];for(;7>b;b++)a._a[b]=l[b]=null==a._a[b]?2===b?1:0:a._a[b];l[lb]+=s((a._tzm||0)/60),l[mb]+=s((a._tzm||0)%60),a._d=(a._useUTC?U:T).apply(null,l)}}function L(a){var b;a._d||(b=q(a._i),a._a=[b.year,b.month,b.day,b.hour,b.minute,b.second,b.millisecond],K(a))}function M(a){var b=new Date;return a._useUTC?[b.getUTCFullYear(),b.getUTCMonth(),b.getUTCDate()]:[b.getFullYear(),b.getMonth(),b.getDate()]}function N(a){a._a=[],a._pf.empty=!0;var b,c,d,e,f,g=C(a._l),h=""+a._i,i=h.length,j=0;for(d=G(a._f,g).match(vb)||[],b=0;b<d.length;b++)e=d[b],c=(h.match(H(e,a))||[])[0],c&&(f=h.substr(0,h.indexOf(c)),f.length>0&&a._pf.unusedInput.push(f),h=h.slice(h.indexOf(c)+c.length),j+=c.length),Yb[e]?(c?a._pf.empty=!1:a._pf.unusedTokens.push(e),J(e,c,a)):a._strict&&!c&&a._pf.unusedTokens.push(e);a._pf.charsLeftOver=i-j,h.length>0&&a._pf.unusedInput.push(h),a._isPm&&a._a[lb]<12&&(a._a[lb]+=12),a._isPm===!1&&12===a._a[lb]&&(a._a[lb]=0),K(a),w(a)}function O(a){return a.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(a,b,c,d,e){return b||c||d||e})}function P(a){return a.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&")}function Q(a){var c,d,e,f,g;if(0===a._f.length)return a._pf.invalidFormat=!0,a._d=new Date(0/0),void 0;for(f=0;f<a._f.length;f++)g=0,c=h({},a),c._pf=b(),c._f=a._f[f],N(c),x(c)&&(g+=c._pf.charsLeftOver,g+=10*c._pf.unusedTokens.length,c._pf.score=g,(null==e||e>g)&&(e=g,d=c));h(a,d||c)}function R(a){var b,c,d=a._i,e=Mb.exec(d);if(e){for(a._pf.iso=!0,b=0,c=Ob.length;c>b;b++)if(Ob[b][1].exec(d)){a._f=Ob[b][0]+(e[6]||" ");break}for(b=0,c=Pb.length;c>b;b++)if(Pb[b][1].exec(d)){a._f+=Pb[b][0];break}d.match(Db)&&(a._f+="Z"),N(a)}else a._d=new Date(d)}function S(b){var c=b._i,d=sb.exec(c);c===a?b._d=new Date:d?b._d=new Date(+d[1]):"string"==typeof c?R(b):m(c)?(b._a=c.slice(0),K(b)):n(c)?b._d=new Date(+c):"object"==typeof c?L(b):b._d=new Date(c)}function T(a,b,c,d,e,f,g){var h=new Date(a,b,c,d,e,f,g);return 1970>a&&h.setFullYear(a),h}function U(a){var b=new Date(Date.UTC.apply(null,arguments));return 1970>a&&b.setUTCFullYear(a),b}function V(a,b){if("string"==typeof a)if(isNaN(a)){if(a=b.weekdaysParse(a),"number"!=typeof a)return null}else a=parseInt(a,10);return a}function W(a,b,c,d,e){return e.relativeTime(b||1,!!c,a,d)}function X(a,b,c){var d=hb(Math.abs(a)/1e3),e=hb(d/60),f=hb(e/60),g=hb(f/24),h=hb(g/365),i=45>d&&["s",d]||1===e&&["m"]||45>e&&["mm",e]||1===f&&["h"]||22>f&&["hh",f]||1===g&&["d"]||25>=g&&["dd",g]||45>=g&&["M"]||345>g&&["MM",hb(g/30)]||1===h&&["y"]||["yy",h];return i[2]=b,i[3]=a>0,i[4]=c,W.apply({},i)}function Y(a,b,c){var d,e=c-b,f=c-a.day();return f>e&&(f-=7),e-7>f&&(f+=7),d=db(a).add("d",f),{week:Math.ceil(d.dayOfYear()/7),year:d.year()}}function Z(a,b,c,d,e){var f,g,h=U(a,0,1).getUTCDay();return c=null!=c?c:e,f=e-h+(h>d?7:0)-(e>h?7:0),g=7*(b-1)+(c-e)+f+1,{year:g>0?a:a-1,dayOfYear:g>0?g:u(a-1)+g}}function $(a){var b=a._i,c=a._f;return null===b?db.invalid({nullInput:!0}):("string"==typeof b&&(a._i=b=C().preparse(b)),db.isMoment(b)?(a=i(b),a._d=new Date(+b._d)):c?m(c)?Q(a):N(a):S(a),new f(a))}function _(a,b){db.fn[a]=db.fn[a+"s"]=function(a){var c=this._isUTC?"UTC":"";return null!=a?(this._d["set"+c+b](a),db.updateOffset(this),this):this._d["get"+c+b]()}}function ab(a){db.duration.fn[a]=function(){return this._data[a]}}function bb(a,b){db.duration.fn["as"+a]=function(){return+this/b}}function cb(a){var b=!1,c=db;"undefined"==typeof ender&&(a?(gb.moment=function(){return!b&&console&&console.warn&&(b=!0,console.warn("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.")),c.apply(null,arguments)},h(gb.moment,c)):gb.moment=db)}for(var db,eb,fb="2.5.1",gb=this,hb=Math.round,ib=0,jb=1,kb=2,lb=3,mb=4,nb=5,ob=6,pb={},qb={_isAMomentObject:null,_i:null,_f:null,_l:null,_strict:null,_isUTC:null,_offset:null,_pf:null,_lang:null},rb="undefined"!=typeof module&&module.exports&&"undefined"!=typeof require,sb=/^\/?Date\((\-?\d+)/i,tb=/(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,ub=/^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/,vb=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|X|zz?|ZZ?|.)/g,wb=/(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g,xb=/\d\d?/,yb=/\d{1,3}/,zb=/\d{1,4}/,Ab=/[+\-]?\d{1,6}/,Bb=/\d+/,Cb=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,Db=/Z|[\+\-]\d\d:?\d\d/gi,Eb=/T/i,Fb=/[\+\-]?\d+(\.\d{1,3})?/,Gb=/\d/,Hb=/\d\d/,Ib=/\d{3}/,Jb=/\d{4}/,Kb=/[+-]?\d{6}/,Lb=/[+-]?\d+/,Mb=/^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,Nb="YYYY-MM-DDTHH:mm:ssZ",Ob=[["YYYYYY-MM-DD",/[+-]\d{6}-\d{2}-\d{2}/],["YYYY-MM-DD",/\d{4}-\d{2}-\d{2}/],["GGGG-[W]WW-E",/\d{4}-W\d{2}-\d/],["GGGG-[W]WW",/\d{4}-W\d{2}/],["YYYY-DDD",/\d{4}-\d{3}/]],Pb=[["HH:mm:ss.SSSS",/(T| )\d\d:\d\d:\d\d\.\d{1,3}/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],Qb=/([\+\-]|\d\d)/gi,Rb="Date|Hours|Minutes|Seconds|Milliseconds".split("|"),Sb={Milliseconds:1,Seconds:1e3,Minutes:6e4,Hours:36e5,Days:864e5,Months:2592e6,Years:31536e6},Tb={ms:"millisecond",s:"second",m:"minute",h:"hour",d:"day",D:"date",w:"week",W:"isoWeek",M:"month",y:"year",DDD:"dayOfYear",e:"weekday",E:"isoWeekday",gg:"weekYear",GG:"isoWeekYear"},Ub={dayofyear:"dayOfYear",isoweekday:"isoWeekday",isoweek:"isoWeek",weekyear:"weekYear",isoweekyear:"isoWeekYear"},Vb={},Wb="DDD w W M D d".split(" "),Xb="M D H h m s w W".split(" "),Yb={M:function(){return this.month()+1},MMM:function(a){return this.lang().monthsShort(this,a)},MMMM:function(a){return this.lang().months(this,a)},D:function(){return this.date()},DDD:function(){return this.dayOfYear()},d:function(){return this.day()},dd:function(a){return this.lang().weekdaysMin(this,a)},ddd:function(a){return this.lang().weekdaysShort(this,a)},dddd:function(a){return this.lang().weekdays(this,a)},w:function(){return this.week()},W:function(){return this.isoWeek()},YY:function(){return k(this.year()%100,2)},YYYY:function(){return k(this.year(),4)},YYYYY:function(){return k(this.year(),5)},YYYYYY:function(){var a=this.year(),b=a>=0?"+":"-";return b+k(Math.abs(a),6)},gg:function(){return k(this.weekYear()%100,2)},gggg:function(){return k(this.weekYear(),4)},ggggg:function(){return k(this.weekYear(),5)},GG:function(){return k(this.isoWeekYear()%100,2)},GGGG:function(){return k(this.isoWeekYear(),4)},GGGGG:function(){return k(this.isoWeekYear(),5)},e:function(){return this.weekday()},E:function(){return this.isoWeekday()},a:function(){return this.lang().meridiem(this.hours(),this.minutes(),!0)},A:function(){return this.lang().meridiem(this.hours(),this.minutes(),!1)},H:function(){return this.hours()},h:function(){return this.hours()%12||12},m:function(){return this.minutes()},s:function(){return this.seconds()},S:function(){return s(this.milliseconds()/100)},SS:function(){return k(s(this.milliseconds()/10),2)},SSS:function(){return k(this.milliseconds(),3)},SSSS:function(){return k(this.milliseconds(),3)},Z:function(){var a=-this.zone(),b="+";return 0>a&&(a=-a,b="-"),b+k(s(a/60),2)+":"+k(s(a)%60,2)},ZZ:function(){var a=-this.zone(),b="+";return 0>a&&(a=-a,b="-"),b+k(s(a/60),2)+k(s(a)%60,2)},z:function(){return this.zoneAbbr()},zz:function(){return this.zoneName()},X:function(){return this.unix()},Q:function(){return this.quarter()}},Zb=["months","monthsShort","weekdays","weekdaysShort","weekdaysMin"];Wb.length;)eb=Wb.pop(),Yb[eb+"o"]=d(Yb[eb],eb);for(;Xb.length;)eb=Xb.pop(),Yb[eb+eb]=c(Yb[eb],2);for(Yb.DDDD=c(Yb.DDD,3),h(e.prototype,{set:function(a){var b,c;for(c in a)b=a[c],"function"==typeof b?this[c]=b:this["_"+c]=b},_months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),months:function(a){return this._months[a.month()]},_monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),monthsShort:function(a){return this._monthsShort[a.month()]},monthsParse:function(a){var b,c,d;for(this._monthsParse||(this._monthsParse=[]),b=0;12>b;b++)if(this._monthsParse[b]||(c=db.utc([2e3,b]),d="^"+this.months(c,"")+"|^"+this.monthsShort(c,""),this._monthsParse[b]=new RegExp(d.replace(".",""),"i")),this._monthsParse[b].test(a))return b},_weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdays:function(a){return this._weekdays[a.day()]},_weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysShort:function(a){return this._weekdaysShort[a.day()]},_weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),weekdaysMin:function(a){return this._weekdaysMin[a.day()]},weekdaysParse:function(a){var b,c,d;for(this._weekdaysParse||(this._weekdaysParse=[]),b=0;7>b;b++)if(this._weekdaysParse[b]||(c=db([2e3,1]).day(b),d="^"+this.weekdays(c,"")+"|^"+this.weekdaysShort(c,"")+"|^"+this.weekdaysMin(c,""),this._weekdaysParse[b]=new RegExp(d.replace(".",""),"i")),this._weekdaysParse[b].test(a))return b},_longDateFormat:{LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D YYYY",LLL:"MMMM D YYYY LT",LLLL:"dddd, MMMM D YYYY LT"},longDateFormat:function(a){var b=this._longDateFormat[a];return!b&&this._longDateFormat[a.toUpperCase()]&&(b=this._longDateFormat[a.toUpperCase()].replace(/MMMM|MM|DD|dddd/g,function(a){return a.slice(1)}),this._longDateFormat[a]=b),b},isPM:function(a){return"p"===(a+"").toLowerCase().charAt(0)},_meridiemParse:/[ap]\.?m?\.?/i,meridiem:function(a,b,c){return a>11?c?"pm":"PM":c?"am":"AM"},_calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},calendar:function(a,b){var c=this._calendar[a];return"function"==typeof c?c.apply(b):c},_relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},relativeTime:function(a,b,c,d){var e=this._relativeTime[c];return"function"==typeof e?e(a,b,c,d):e.replace(/%d/i,a)},pastFuture:function(a,b){var c=this._relativeTime[a>0?"future":"past"];return"function"==typeof c?c(b):c.replace(/%s/i,b)},ordinal:function(a){return this._ordinal.replace("%d",a)},_ordinal:"%d",preparse:function(a){return a},postformat:function(a){return a},week:function(a){return Y(a,this._week.dow,this._week.doy).week},_week:{dow:0,doy:6},_invalidDate:"Invalid date",invalidDate:function(){return this._invalidDate}}),db=function(c,d,e,f){var g;return"boolean"==typeof e&&(f=e,e=a),g={},g._isAMomentObject=!0,g._i=c,g._f=d,g._l=e,g._strict=f,g._isUTC=!1,g._pf=b(),$(g)},db.utc=function(c,d,e,f){var g;return"boolean"==typeof e&&(f=e,e=a),g={},g._isAMomentObject=!0,g._useUTC=!0,g._isUTC=!0,g._l=e,g._i=c,g._f=d,g._strict=f,g._pf=b(),$(g).utc()},db.unix=function(a){return db(1e3*a)},db.duration=function(a,b){var c,d,e,f=a,h=null;return db.isDuration(a)?f={ms:a._milliseconds,d:a._days,M:a._months}:"number"==typeof a?(f={},b?f[b]=a:f.milliseconds=a):(h=tb.exec(a))?(c="-"===h[1]?-1:1,f={y:0,d:s(h[kb])*c,h:s(h[lb])*c,m:s(h[mb])*c,s:s(h[nb])*c,ms:s(h[ob])*c}):(h=ub.exec(a))&&(c="-"===h[1]?-1:1,e=function(a){var b=a&&parseFloat(a.replace(",","."));return(isNaN(b)?0:b)*c},f={y:e(h[2]),M:e(h[3]),d:e(h[4]),h:e(h[5]),m:e(h[6]),s:e(h[7]),w:e(h[8])}),d=new g(f),db.isDuration(a)&&a.hasOwnProperty("_lang")&&(d._lang=a._lang),d},db.version=fb,db.defaultFormat=Nb,db.updateOffset=function(){},db.lang=function(a,b){var c;return a?(b?A(y(a),b):null===b?(B(a),a="en"):pb[a]||C(a),c=db.duration.fn._lang=db.fn._lang=C(a),c._abbr):db.fn._lang._abbr},db.langData=function(a){return a&&a._lang&&a._lang._abbr&&(a=a._lang._abbr),C(a)},db.isMoment=function(a){return a instanceof f||null!=a&&a.hasOwnProperty("_isAMomentObject")},db.isDuration=function(a){return a instanceof g},eb=Zb.length-1;eb>=0;--eb)r(Zb[eb]);for(db.normalizeUnits=function(a){return p(a)},db.invalid=function(a){var b=db.utc(0/0);return null!=a?h(b._pf,a):b._pf.userInvalidated=!0,b},db.parseZone=function(a){return db(a).parseZone()},h(db.fn=f.prototype,{clone:function(){return db(this)},valueOf:function(){return+this._d+6e4*(this._offset||0)},unix:function(){return Math.floor(+this/1e3)},toString:function(){return this.clone().lang("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")},toDate:function(){return this._offset?new Date(+this):this._d},toISOString:function(){var a=db(this).utc();return 0<a.year()&&a.year()<=9999?F(a,"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]"):F(a,"YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")},toArray:function(){var a=this;return[a.year(),a.month(),a.date(),a.hours(),a.minutes(),a.seconds(),a.milliseconds()]},isValid:function(){return x(this)},isDSTShifted:function(){return this._a?this.isValid()&&o(this._a,(this._isUTC?db.utc(this._a):db(this._a)).toArray())>0:!1},parsingFlags:function(){return h({},this._pf)},invalidAt:function(){return this._pf.overflow},utc:function(){return this.zone(0)},local:function(){return this.zone(0),this._isUTC=!1,this},format:function(a){var b=F(this,a||db.defaultFormat);return this.lang().postformat(b)},add:function(a,b){var c;return c="string"==typeof a?db.duration(+b,a):db.duration(a,b),l(this,c,1),this},subtract:function(a,b){var c;return c="string"==typeof a?db.duration(+b,a):db.duration(a,b),l(this,c,-1),this},diff:function(a,b,c){var d,e,f=z(a,this),g=6e4*(this.zone()-f.zone());return b=p(b),"year"===b||"month"===b?(d=432e5*(this.daysInMonth()+f.daysInMonth()),e=12*(this.year()-f.year())+(this.month()-f.month()),e+=(this-db(this).startOf("month")-(f-db(f).startOf("month")))/d,e-=6e4*(this.zone()-db(this).startOf("month").zone()-(f.zone()-db(f).startOf("month").zone()))/d,"year"===b&&(e/=12)):(d=this-f,e="second"===b?d/1e3:"minute"===b?d/6e4:"hour"===b?d/36e5:"day"===b?(d-g)/864e5:"week"===b?(d-g)/6048e5:d),c?e:j(e)},from:function(a,b){return db.duration(this.diff(a)).lang(this.lang()._abbr).humanize(!b)},fromNow:function(a){return this.from(db(),a)},calendar:function(){var a=z(db(),this).startOf("day"),b=this.diff(a,"days",!0),c=-6>b?"sameElse":-1>b?"lastWeek":0>b?"lastDay":1>b?"sameDay":2>b?"nextDay":7>b?"nextWeek":"sameElse";return this.format(this.lang().calendar(c,this))},isLeapYear:function(){return v(this.year())},isDST:function(){return this.zone()<this.clone().month(0).zone()||this.zone()<this.clone().month(5).zone()},day:function(a){var b=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=a?(a=V(a,this.lang()),this.add({d:a-b})):b},month:function(a){var b,c=this._isUTC?"UTC":"";return null!=a?"string"==typeof a&&(a=this.lang().monthsParse(a),"number"!=typeof a)?this:(b=this.date(),this.date(1),this._d["set"+c+"Month"](a),this.date(Math.min(b,this.daysInMonth())),db.updateOffset(this),this):this._d["get"+c+"Month"]()},startOf:function(a){switch(a=p(a)){case"year":this.month(0);case"month":this.date(1);case"week":case"isoWeek":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===a?this.weekday(0):"isoWeek"===a&&this.isoWeekday(1),this},endOf:function(a){return a=p(a),this.startOf(a).add("isoWeek"===a?"week":a,1).subtract("ms",1)},isAfter:function(a,b){return b="undefined"!=typeof b?b:"millisecond",+this.clone().startOf(b)>+db(a).startOf(b)},isBefore:function(a,b){return b="undefined"!=typeof b?b:"millisecond",+this.clone().startOf(b)<+db(a).startOf(b)},isSame:function(a,b){return b=b||"ms",+this.clone().startOf(b)===+z(a,this).startOf(b)},min:function(a){return a=db.apply(null,arguments),this>a?this:a},max:function(a){return a=db.apply(null,arguments),a>this?this:a},zone:function(a){var b=this._offset||0;return null==a?this._isUTC?b:this._d.getTimezoneOffset():("string"==typeof a&&(a=I(a)),Math.abs(a)<16&&(a=60*a),this._offset=a,this._isUTC=!0,b!==a&&l(this,db.duration(b-a,"m"),1,!0),this)},zoneAbbr:function(){return this._isUTC?"UTC":""},zoneName:function(){return this._isUTC?"Coordinated Universal Time":""},parseZone:function(){return this._tzm?this.zone(this._tzm):"string"==typeof this._i&&this.zone(this._i),this},hasAlignedHourOffset:function(a){return a=a?db(a).zone():0,(this.zone()-a)%60===0},daysInMonth:function(){return t(this.year(),this.month())},dayOfYear:function(a){var b=hb((db(this).startOf("day")-db(this).startOf("year"))/864e5)+1;return null==a?b:this.add("d",a-b)},quarter:function(){return Math.ceil((this.month()+1)/3)},weekYear:function(a){var b=Y(this,this.lang()._week.dow,this.lang()._week.doy).year;return null==a?b:this.add("y",a-b)},isoWeekYear:function(a){var b=Y(this,1,4).year;return null==a?b:this.add("y",a-b)},week:function(a){var b=this.lang().week(this);return null==a?b:this.add("d",7*(a-b))},isoWeek:function(a){var b=Y(this,1,4).week;return null==a?b:this.add("d",7*(a-b))},weekday:function(a){var b=(this.day()+7-this.lang()._week.dow)%7;return null==a?b:this.add("d",a-b)},isoWeekday:function(a){return null==a?this.day()||7:this.day(this.day()%7?a:a-7)},get:function(a){return a=p(a),this[a]()},set:function(a,b){return a=p(a),"function"==typeof this[a]&&this[a](b),this},lang:function(b){return b===a?this._lang:(this._lang=C(b),this)}}),eb=0;eb<Rb.length;eb++)_(Rb[eb].toLowerCase().replace(/s$/,""),Rb[eb]);_("year","FullYear"),db.fn.days=db.fn.day,db.fn.months=db.fn.month,db.fn.weeks=db.fn.week,db.fn.isoWeeks=db.fn.isoWeek,db.fn.toJSON=db.fn.toISOString,h(db.duration.fn=g.prototype,{_bubble:function(){var a,b,c,d,e=this._milliseconds,f=this._days,g=this._months,h=this._data;h.milliseconds=e%1e3,a=j(e/1e3),h.seconds=a%60,b=j(a/60),h.minutes=b%60,c=j(b/60),h.hours=c%24,f+=j(c/24),h.days=f%30,g+=j(f/30),h.months=g%12,d=j(g/12),h.years=d},weeks:function(){return j(this.days()/7)},valueOf:function(){return this._milliseconds+864e5*this._days+this._months%12*2592e6+31536e6*s(this._months/12)},humanize:function(a){var b=+this,c=X(b,!a,this.lang());return a&&(c=this.lang().pastFuture(b,c)),this.lang().postformat(c)},add:function(a,b){var c=db.duration(a,b);return this._milliseconds+=c._milliseconds,this._days+=c._days,this._months+=c._months,this._bubble(),this},subtract:function(a,b){var c=db.duration(a,b);return this._milliseconds-=c._milliseconds,this._days-=c._days,this._months-=c._months,this._bubble(),this},get:function(a){return a=p(a),this[a.toLowerCase()+"s"]()},as:function(a){return a=p(a),this["as"+a.charAt(0).toUpperCase()+a.slice(1)+"s"]()},lang:db.fn.lang,toIsoString:function(){var a=Math.abs(this.years()),b=Math.abs(this.months()),c=Math.abs(this.days()),d=Math.abs(this.hours()),e=Math.abs(this.minutes()),f=Math.abs(this.seconds()+this.milliseconds()/1e3);return this.asSeconds()?(this.asSeconds()<0?"-":"")+"P"+(a?a+"Y":"")+(b?b+"M":"")+(c?c+"D":"")+(d||e||f?"T":"")+(d?d+"H":"")+(e?e+"M":"")+(f?f+"S":""):"P0D"}});for(eb in Sb)Sb.hasOwnProperty(eb)&&(bb(eb,Sb[eb]),ab(eb.toLowerCase()));bb("Weeks",6048e5),db.duration.fn.asMonths=function(){return(+this-31536e6*this.years())/2592e6+12*this.years()},db.lang("en",{ordinal:function(a){var b=a%10,c=1===s(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c}}),rb?(module.exports=db,cb(!0)):"function"==typeof define&&define.amd?define("moment",function(b,c,d){return d.config&&d.config()&&d.config().noGlobal!==!0&&cb(d.config().noGlobal===a),db}):cb()}).call(this);
/*!
 Autosize v1.18.1 - 2013-11-05
 Automatically adjust textarea height based on user input.
 (c) 2013 Jack Moore - http://www.jacklmoore.com/autosize
 license: http://www.opensource.org/licenses/mit-license.php
 */

(function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);
/**
 * Copyright (c) 2010 Zef Hemel <zef@zef.me>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


if (typeof exports !== 'undefined') {
    exports.createPersistence = function() {
        return initPersistence({})
    }
    var singleton;
    if (typeof (exports.__defineGetter__) === 'function') {
        exports.__defineGetter__("persistence", function () {
            if (!singleton)
                singleton = exports.createPersistence();
            return singleton;
        });
    } else {
        Object.defineProperty(exports, "persistence", {
            get: function () {
                if (!singleton)
                    singleton = exports.createPersistence();
                return singleton;
            },
            enumerable: true, configurable: true
        });
    }

}
else {
    window = window || {};
    window.persistence = initPersistence(window.persistence || {});
}


function initPersistence(persistence) {
    if (persistence.isImmutable) // already initialized
        return persistence;

    /**
     * Check for immutable fields
     */
    persistence.isImmutable = function(fieldName) {
        return (fieldName == "id");
    };

    /**
     * Default implementation for entity-property
     */
    persistence.defineProp = function(scope, field, setterCallback, getterCallback) {
        if (typeof (scope.__defineSetter__) === 'function' && typeof (scope.__defineGetter__) === 'function') {
            scope.__defineSetter__(field, function (value) {
                setterCallback(value);
            });
            scope.__defineGetter__(field, function () {
                return getterCallback();
            });
        } else {
            Object.defineProperty(scope, field, {
                get: getterCallback,
                set: function (value) {
                    setterCallback(value);
                },
                enumerable: true, configurable: true
            });
        }
    };

    /**
     * Default implementation for entity-property setter
     */
    persistence.set = function(scope, fieldName, value) {
        if (persistence.isImmutable(fieldName)) throw new Error("immutable field: "+fieldName);
        scope[fieldName] = value;
    };

    /**
     * Default implementation for entity-property getter
     */
    persistence.get = function(arg1, arg2) {
        return (arguments.length == 1) ? arg1 : arg1[arg2];
    };


    (function () {
        var entityMeta = {};
        var entityClassCache = {};
        persistence.getEntityMeta = function() { return entityMeta; }

        // Per-session data
        persistence.trackedObjects = {};
        persistence.objectsToRemove = {};
        persistence.objectsRemoved = []; // {id: ..., type: ...}
        persistence.globalPropertyListeners = {}; // EntityType__prop -> QueryColleciton obj
        persistence.queryCollectionCache = {}; // entityName -> uniqueString -> QueryCollection

        persistence.getObjectsToRemove = function() { return this.objectsToRemove; };
        persistence.getTrackedObjects = function() { return this.trackedObjects; };

        // Public Extension hooks
        persistence.entityDecoratorHooks = [];
        persistence.flushHooks = [];
        persistence.schemaSyncHooks = [];

        // Enable debugging (display queries using console.log etc)
        persistence.debug = true;

        persistence.subscribeToGlobalPropertyListener = function(coll, entityName, property) {
            var key = entityName + '__' + property;

            if(key in this.globalPropertyListeners) {
                var listeners = this.globalPropertyListeners[key];
                for(var i = 0; i < listeners.length; i++) {
                    if(listeners[i] === coll) {
                        return;
                    }
                }
                this.globalPropertyListeners[key].push(coll);
            } else {
                this.globalPropertyListeners[key] = [coll];
            }
        }

        persistence.unsubscribeFromGlobalPropertyListener = function(coll, entityName, property) {
            var key = entityName + '__' + property;
            var listeners = this.globalPropertyListeners[key];
            for(var i = 0; i < listeners.length; i++) {
                if(listeners[i] === coll) {
                    listeners.splice(i, 1);
                    return;
                }
            }
        }

        persistence.propertyChanged = function(obj, property, oldValue, newValue) {
            if(!this.trackedObjects[obj.id]) return; // not yet added, ignore for now

            var entityName = obj._type;
            var key = entityName + '__' + property;

            if(key in this.globalPropertyListeners) {
                var listeners = this.globalPropertyListeners[key];
                for(var i = 0; i < listeners.length; i++) {
                    var coll = listeners[i];
                    var dummyObj = obj._data;
                    dummyObj[property] = oldValue;
                    var matchedBefore = coll._filter.match(dummyObj);
                    dummyObj[property] = newValue;
                    var matchedAfter = coll._filter.match(dummyObj);
                    if(matchedBefore != matchedAfter) {
                        coll.triggerEvent('change', coll, obj);
                    }
                }
            }
        }

        persistence.objectRemoved = function(obj) {
            var entityName = obj._type;
            if(this.queryCollectionCache[entityName]) {
                var colls = this.queryCollectionCache[entityName];
                for(var key in colls) {
                    if(colls.hasOwnProperty(key)) {
                        var coll = colls[key];
                        if(coll._filter.match(obj)) { // matched the filter -> was part of collection
                            coll.triggerEvent('change', coll, obj);
                        }
                    }
                }
            }
        }

        /**
         * Retrieves metadata about entity, mostly for internal use
         */
        function getMeta(entityName) {
            return entityMeta[entityName];
        }

        persistence.getMeta = getMeta;


        /**
         * A database session
         */
        function Session(conn) {
            this.trackedObjects = {};
            this.objectsToRemove = {};
            this.objectsRemoved = [];
            this.globalPropertyListeners = {}; // EntityType__prop -> QueryColleciton obj
            this.queryCollectionCache = {}; // entityName -> uniqueString -> QueryCollection
            this.conn = conn;
        }

        Session.prototype = persistence; // Inherit everything from the root persistence object

        persistence.Session = Session;

        /**
         * Define an entity
         *
         * @param entityName
         *            the name of the entity (also the table name in the database)
         * @param fields
         *            an object with property names as keys and SQLite types as
         *            values, e.g. {name: "TEXT", age: "INT"}
         * @return the entity's constructor
         */
        persistence.define = function (entityName, fields) {
            if (entityMeta[entityName]) { // Already defined, ignore
                return getEntity(entityName);
            }
            var meta = {
                name: entityName,
                fields: fields,
                isMixin: false,
                indexes: [],
                hasMany: {},
                hasOne: {}
            };
            entityMeta[entityName] = meta;
            return getEntity(entityName);
        };

        /**
         * Checks whether an entity exists
         *
         * @param entityName
         *            the name of the entity (also the table name in the database)
         * @return `true` if the entity exists, otherwise `false`
         */
        persistence.isDefined = function (entityName) {
            return !!entityMeta[entityName];
        }

        /**
         * Define a mixin
         *
         * @param mixinName
         *            the name of the mixin
         * @param fields
         *            an object with property names as keys and SQLite types as
         *            values, e.g. {name: "TEXT", age: "INT"}
         * @return the entity's constructor
         */
        persistence.defineMixin = function (mixinName, fields) {
            var Entity = this.define(mixinName, fields);
            Entity.meta.isMixin = true;
            return Entity;
        };

        persistence.isTransaction = function(obj) {
            return !obj || (obj && obj.executeSql);
        };

        persistence.isSession = function(obj) {
            return !obj || (obj && obj.schemaSync);
        };

        /**
         * Adds the object to tracked entities to be persisted
         *
         * @param obj
         *            the object to be tracked
         */
        persistence.add = function (obj) {
            if(!obj) return;
            if (!this.trackedObjects[obj.id]) {
                this.trackedObjects[obj.id] = obj;
                if(obj._new) {
                    for(var p in obj._data) {
                        if(obj._data.hasOwnProperty(p)) {
                            this.propertyChanged(obj, p, undefined, obj._data[p]);
                        }
                    }
                }
            }
            return this;
        };

        /**
         * Marks the object to be removed (on next flush)
         * @param obj object to be removed
         */
        persistence.remove = function(obj) {
            if (obj._new) {
                delete this.trackedObjects[obj.id];
            } else {
                if (!this.objectsToRemove[obj.id]) {
                    this.objectsToRemove[obj.id] = obj;
                }
                this.objectsRemoved.push({id: obj.id, entity: obj._type});
            }
            this.objectRemoved(obj);
            return this;
        };


        /**
         * Clean the persistence context of cached entities and such.
         */
        persistence.clean = function () {
            console.error("CLEANUP!");
            this.trackedObjects = {};
            this.objectsToRemove = {};
            this.objectsRemoved = [];
            this.globalPropertyListeners = {};
            this.queryCollectionCache = {};
        };

        /**
         * asynchronous sequential version of Array.prototype.forEach
         * @param array the array to iterate over
         * @param fn the function to apply to each item in the array, function
         *        has two argument, the first is the item value, the second a
         *        callback function
         * @param callback the function to call when the forEach has ended
         */
        persistence.asyncForEach = function(array, fn, callback) {
            array = array.slice(0); // Just to be sure
            function processOne() {
                var item = array.pop();
                fn(item, function(result, err) {
                    if(array.length > 0) {
                        processOne();
                    } else {
                        callback(result, err);
                    }
                });
            }
            if(array.length > 0) {
                processOne();
            } else {
                callback();
            }
        };

        /**
         * asynchronous parallel version of Array.prototype.forEach
         * @param array the array to iterate over
         * @param fn the function to apply to each item in the array, function
         *        has two argument, the first is the item value, the second a
         *        callback function
         * @param callback the function to call when the forEach has ended
         */
        persistence.asyncParForEach = function(array, fn, callback) {
            var completed = 0;
            var arLength = array.length;
            if(arLength === 0) {
                callback();
            }
            for(var i = 0; i < arLength; i++) {
                fn(array[i], function(result, err) {
                    completed++;
                    if(completed === arLength) {
                        callback(result, err);
                    }
                });
            }
        };

        /**
         * Retrieves or creates an entity constructor function for a given
         * entity name
         * @return the entity constructor function to be invoked with `new fn()`
         */
        function getEntity(entityName) {
            if (entityClassCache[entityName]) {
                return entityClassCache[entityName];
            }
            var meta = entityMeta[entityName];

            /**
             * @constructor
             */
            function Entity (session, obj, noEvents) {
                var args = argspec.getArgs(arguments, [
                    { name: "session", optional: true, check: persistence.isSession, defaultValue: persistence },
                    { name: "obj", optional: true, check: function(obj) { return obj; }, defaultValue: {} }
                ]);
                if (meta.isMixin)
                    throw new Error("Cannot instantiate mixin");
                session = args.session;
                obj = args.obj;

                var that = this;
                this.id = obj.id || persistence.createUUID();
                this._new = true;
                this._type = entityName;
                this._dirtyProperties = {};
                this._data = {};
                this._data_obj = {}; // references to objects
                this._session = session || persistence;
                this.subscribers = {}; // observable

                for ( var field in meta.fields) {
                    (function () {
                        if (meta.fields.hasOwnProperty(field)) {
                            var f = field; // Javascript scopes/closures SUCK
                            persistence.defineProp(that, f, function(val) {
                                // setterCallback
                                var oldValue = that._data[f];
                                if(oldValue !== val || (oldValue && val && oldValue.getTime && val.getTime)) { // Don't mark properties as dirty and trigger events unnecessarily
                                    that._data[f] = val;
                                    that._dirtyProperties[f] = oldValue;
                                    that.triggerEvent('set', that, f, val);
                                    that.triggerEvent('change', that, f, val);
                                    session.propertyChanged(that, f, oldValue, val);
                                }
                            }, function() {
                                // getterCallback
                                return that._data[f];
                            });
                            that._data[field] = defaultValue(meta.fields[field]);
                        }
                    }());
                }

                for ( var it in meta.hasOne) {
                    if (meta.hasOne.hasOwnProperty(it)) {
                        (function () {
                            var ref = it;
                            var mixinClass = meta.hasOne[it].type.meta.isMixin ? ref + '_class' : null;
                            persistence.defineProp(that, ref, function(val) {
                                // setterCallback
                                var oldValue = that._data[ref];
                                var oldValueObj = that._data_obj[ref] || session.trackedObjects[that._data[ref]];
                                if (val == null) {
                                    that._data[ref] = null;
                                    that._data_obj[ref] = undefined;
                                    if (mixinClass)
                                        that[mixinClass] = '';
                                } else if (val.id) {
                                    that._data[ref] = val.id;
                                    that._data_obj[ref] = val;
                                    if (mixinClass)
                                        that[mixinClass] = val._type;
                                    session.add(val);
                                    session.add(that);
                                } else { // let's assume it's an id
                                    that._data[ref] = val;
                                }
                                that._dirtyProperties[ref] = oldValue;
                                that.triggerEvent('set', that, ref, val);
                                that.triggerEvent('change', that, ref, val);
                                // Inverse

                                if(meta.hasOne[ref].inverseProperty) {
                                    var newVal = that[ref];
                                    if(newVal) {
                                        var inverse = newVal[meta.hasOne[ref].inverseProperty];
                                        if(inverse.list && inverse._filter) {
                                            inverse.triggerEvent('change', that, ref, val);
                                        }
                                    }
                                    if(oldValueObj) {
                                        var inverse = oldValueObj[meta.hasOne[ref].inverseProperty];
                                        if(inverse.list && inverse._filter) {
                                            inverse.triggerEvent('change', that, ref, val);
                                        }
                                    }
                                }
                            }, function() {
                                // getterCallback
                                if (!that._data[ref]) {
                                    return null;
                                } else if(that._data_obj[ref] !== undefined) {
                                    return that._data_obj[ref];
                                } else if(that._data[ref] && session.trackedObjects[that._data[ref]]) {
                                    that._data_obj[ref] = session.trackedObjects[that._data[ref]];
                                    return that._data_obj[ref];
                                } else {
                                    throw new Error("Property '" + ref + "' of '" + meta.name + "' with id: " + that._data[ref] + " not fetched, either prefetch it or fetch it manually.");
                                }
                            });
                        }());
                    }
                }

                for ( var it in meta.hasMany) {
                    if (meta.hasMany.hasOwnProperty(it)) {
                        (function () {
                            var coll = it;
                            if (meta.hasMany[coll].manyToMany) {
                                persistence.defineProp(that, coll, function(val) {
                                    // setterCallback
                                    if(val && val._items) {
                                        // Local query collection, just add each item
                                        // TODO: this is technically not correct, should clear out existing items too
                                        var items = val._items;
                                        for(var i = 0; i < items.length; i++) {
                                            persistence.get(that, coll).add(items[i]);
                                        }
                                    } else {
                                        throw new Error("Not yet supported.");
                                    }
                                }, function() {
                                    // getterCallback
                                    if (that._data[coll]) {
                                        return that._data[coll];
                                    } else {
                                        var rel = meta.hasMany[coll];
                                        var inverseMeta = rel.type.meta;
                                        var inv = inverseMeta.hasMany[rel.inverseProperty];
                                        var direct = rel.mixin ? rel.mixin.meta.name : meta.name;
                                        var inverse = inv.mixin ? inv.mixin.meta.name : inverseMeta.name;

                                        var queryColl = new persistence.ManyToManyDbQueryCollection(session, inverseMeta.name);
                                        queryColl.initManyToMany(that, coll);
                                        queryColl._manyToManyFetch = {
                                            table: rel.tableName,
                                            prop: direct + '_' + coll,
                                            inverseProp: inverse + '_' + rel.inverseProperty,
                                            id: that.id
                                        };
                                        that._data[coll] = queryColl;
                                        return session.uniqueQueryCollection(queryColl);
                                    }
                                });
                            } else { // one to many
                                persistence.defineProp(that, coll, function(val) {
                                    // setterCallback
                                    if(val && val._items) {
                                        // Local query collection, just add each item
                                        // TODO: this is technically not correct, should clear out existing items too
                                        var items = val._items;
                                        for(var i = 0; i < items.length; i++) {
                                            persistence.get(that, coll).add(items[i]);
                                        }
                                    } else {
                                        throw new Error("Not yet supported.");
                                    }
                                }, function() {
                                    // getterCallback
                                    if (that._data[coll]) {
                                        return that._data[coll];
                                    } else {
                                        var queryColl = session.uniqueQueryCollection(new persistence.DbQueryCollection(session, meta.hasMany[coll].type.meta.name).filter(meta.hasMany[coll].inverseProperty, '=', that));
                                        that._data[coll] = queryColl;
                                        return queryColl;
                                    }
                                });
                            }
                        }());
                    }
                }

                if(this.initialize) {
                    this.initialize();
                }

                for ( var f in obj) {
                    if (obj.hasOwnProperty(f)) {
                        if(f !== 'id') {
                            persistence.set(that, f, obj[f]);
                        }
                    }
                }
            } // Entity

            Entity.prototype = new Observable();

            Entity.meta = meta;

            Entity.prototype.equals = function(other) {
                return this.id == other.id;
            };

            Entity.prototype.toJSON = function() {
                var json = {id: this.id};
                for(var p in this._data) {
                    if(this._data.hasOwnProperty(p)) {
                        if (typeof this._data[p] == "object" && this._data[p] != null) {
                            if (this._data[p].toJSON != undefined) {
                                json[p] = this._data[p].toJSON();
                            }
                        } else {
                            json[p] = this._data[p];
                        }
                    }
                }
                return json;
            };


            /**
             * Select a subset of data as a JSON structure (Javascript object)
             *
             * A property specification is passed that selects the
             * properties to be part of the resulting JSON object. Examples:
             *    ['id', 'name'] -> Will return an object with the id and name property of this entity
             *    ['*'] -> Will return an object with all the properties of this entity, not recursive
             *    ['project.name'] -> will return an object with a project property which has a name
             *                        property containing the project name (hasOne relationship)
             *    ['project.[id, name]'] -> will return an object with a project property which has an
             *                              id and name property containing the project name
             *                              (hasOne relationship)
             *    ['tags.name'] -> will return an object with an array `tags` property containing
             *                     objects each with a single property: name
             *
             * @param tx database transaction to use, leave out to start a new one
             * @param props a property specification
             * @param callback(result)
             */
            Entity.prototype.selectJSON = function(tx, props, callback) {
                var that = this;
                var args = argspec.getArgs(arguments, [
                    { name: "tx", optional: true, check: persistence.isTransaction, defaultValue: null },
                    { name: "props", optional: false },
                    { name: "callback", optional: false }
                ]);
                tx = args.tx;
                props = args.props;
                callback = args.callback;

                if(!tx) {
                    this._session.transaction(function(tx) {
                        that.selectJSON(tx, props, callback);
                    });
                    return;
                }
                var includeProperties = {};
                props.forEach(function(prop) {
                    var current = includeProperties;
                    var parts = prop.split('.');
                    for(var i = 0; i < parts.length; i++) {
                        var part = parts[i];
                        if(i === parts.length-1) {
                            if(part === '*') {
                                current.id = true;
                                for(var p in meta.fields) {
                                    if(meta.fields.hasOwnProperty(p)) {
                                        current[p] = true;
                                    }
                                }
                                for(var p in meta.hasOne) {
                                    if(meta.hasOne.hasOwnProperty(p)) {
                                        current[p] = true;
                                    }
                                }
                                for(var p in meta.hasMany) {
                                    if(meta.hasMany.hasOwnProperty(p)) {
                                        current[p] = true;
                                    }
                                }
                            } else if(part[0] === '[') {
                                part = part.substring(1, part.length-1);
                                var propList = part.split(/,\s*/);
                                propList.forEach(function(prop) {
                                    current[prop] = true;
                                });
                            } else {
                                current[part] = true;
                            }
                        } else {
                            current[part] = current[part] || {};
                            current = current[part];
                        }
                    }
                });
                buildJSON(this, tx, includeProperties, callback);
            };

            function buildJSON(that, tx, includeProperties, callback) {
                var session = that._session;
                var properties = [];
                var meta = getMeta(that._type);
                var fieldSpec = meta.fields;

                for(var p in includeProperties) {
                    if(includeProperties.hasOwnProperty(p)) {
                        properties.push(p);
                    }
                }

                var cheapProperties = [];
                var expensiveProperties = [];

                properties.forEach(function(p) {
                    if(includeProperties[p] === true && !meta.hasMany[p]) { // simple, loaded field
                        cheapProperties.push(p);
                    } else {
                        expensiveProperties.push(p);
                    }
                });

                var itemData = that._data;
                var item = {};

                cheapProperties.forEach(function(p) {
                    if(p === 'id') {
                        item.id = that.id;
                    } else if(meta.hasOne[p]) {
                        item[p] = itemData[p] ? {id: itemData[p]} : null;
                    } else {
                        item[p] = persistence.entityValToJson(itemData[p], fieldSpec[p]);
                    }
                });
                properties = expensiveProperties.slice();

                persistence.asyncForEach(properties, function(p, callback) {
                    if(meta.hasOne[p]) {
                        that.fetch(tx, p, function(obj) {
                            if(obj) {
                                buildJSON(obj, tx, includeProperties[p], function(result) {
                                    item[p] = result;
                                    callback();
                                });
                            } else {
                                item[p] = null;
                                callback();
                            }
                        });
                    } else if(meta.hasMany[p]) {
                        persistence.get(that, p).list(function(objs) {
                            item[p] = [];
                            persistence.asyncForEach(objs, function(obj, callback) {
                                var obj = objs.pop();
                                if(includeProperties[p] === true) {
                                    item[p].push({id: obj.id});
                                    callback();
                                } else {
                                    buildJSON(obj, tx, includeProperties[p], function(result) {
                                        item[p].push(result);
                                        callback();
                                    });
                                }
                            }, callback);
                        });
                    }
                }, function() {
                    callback(item);
                });
            }; // End of buildJson

            Entity.prototype.fetch = function(tx, rel, callback) {
                var args = argspec.getArgs(arguments, [
                    { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                    { name: 'rel', optional: false, check: argspec.hasType('string') },
                    { name: 'callback', optional: false, check: argspec.isCallback() }
                ]);
                tx = args.tx;
                rel = args.rel;
                callback = args.callback;

                var that = this;
                var session = this._session;

                if(!tx) {
                    session.transaction(function(tx) {
                        that.fetch(tx, rel, callback);
                    });
                    return;
                }
                if(!this._data[rel]) { // null
                    if(callback) {
                        callback(null);
                    }
                } else if(this._data_obj[rel]) { // already loaded
                    if(callback) {
                        callback(this._data_obj[rel]);
                    }
                } else {
                    var type = meta.hasOne[rel].type;
                    if (type.meta.isMixin) {
                        type = getEntity(this._data[rel + '_class']);
                    }
                    type.load(session, tx, this._data[rel], function(obj) {
                        that._data_obj[rel] = obj;
                        if(callback) {
                            callback(obj);
                        }
                    });
                }
            };

            /**
             * Currently this is only required when changing JSON properties
             */
            Entity.prototype.markDirty = function(prop) {
                this._dirtyProperties[prop] = true;
            };

            /**
             * Returns a QueryCollection implementation matching all instances
             * of this entity in the database
             */
            Entity.all = function(session) {
                var args = argspec.getArgs(arguments, [
                    { name: 'session', optional: true, check: persistence.isSession, defaultValue: persistence }
                ]);
                session = args.session;
                return session.uniqueQueryCollection(new AllDbQueryCollection(session, entityName));
            };

            Entity.fromSelectJSON = function(session, tx, jsonObj, callback) {
                var args = argspec.getArgs(arguments, [
                    { name: 'session', optional: true, check: persistence.isSession, defaultValue: persistence },
                    { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                    { name: 'jsonObj', optional: false },
                    { name: 'callback', optional: false, check: argspec.isCallback() }
                ]);
                session = args.session;
                tx = args.tx;
                jsonObj = args.jsonObj;
                callback = args.callback;

                if(!tx) {
                    session.transaction(function(tx) {
                        Entity.fromSelectJSON(session, tx, jsonObj, callback);
                    });
                    return;
                }

                if(typeof jsonObj === 'string') {
                    jsonObj = JSON.parse(jsonObj);
                }

                if(!jsonObj) {
                    callback(null);
                    return;
                }

                function loadedObj(obj) {
                    if(!obj) {
                        obj = new Entity(session);
                        if(jsonObj.id) {
                            obj.id = jsonObj.id;
                        }
                    }
                    session.add(obj);
                    var expensiveProperties = [];
                    for(var p in jsonObj) {
                        if(jsonObj.hasOwnProperty(p)) {
                            if(p === 'id') {
                                continue;
                            } else if(meta.fields[p]) { // regular field
                                persistence.set(obj, p, persistence.jsonToEntityVal(jsonObj[p], meta.fields[p]));
                            } else if(meta.hasOne[p] || meta.hasMany[p]){
                                expensiveProperties.push(p);
                            }
                        }
                    }
                    persistence.asyncForEach(expensiveProperties, function(p, callback) {
                        if(meta.hasOne[p]) {
                            meta.hasOne[p].type.fromSelectJSON(session, tx, jsonObj[p], function(result) {
                                persistence.set(obj, p, result);
                                callback();
                            });
                        } else if(meta.hasMany[p]) {
                            var coll = persistence.get(obj, p);
                            var ar = jsonObj[p].slice(0);
                            var PropertyEntity = meta.hasMany[p].type;
                            // get all current items
                            coll.list(tx, function(currentItems) {
                                persistence.asyncForEach(ar, function(item, callback) {
                                    PropertyEntity.fromSelectJSON(session, tx, item, function(result) {
                                        // Check if not already in collection
                                        for(var i = 0; i < currentItems.length; i++) {
                                            if(currentItems[i].id === result.id) {
                                                callback();
                                                return;
                                            }
                                        }
                                        coll.add(result);
                                        callback();
                                    });
                                }, function() {
                                    callback();
                                });
                            });
                        }
                    }, function() {
                        callback(obj);
                    });
                }
                if(jsonObj.id) {
                    Entity.load(session, tx, jsonObj.id, loadedObj);
                } else {
                    loadedObj(new Entity(session));
                }
            };

            Entity.load = function(session, tx, id, callback) {
                var args = argspec.getArgs(arguments, [
                    { name: 'session', optional: true, check: persistence.isSession, defaultValue: persistence },
                    { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                    { name: 'id', optional: false, check: argspec.hasType('string') },
                    { name: 'callback', optional: true, check: argspec.isCallback(), defaultValue: function(){} }
                ]);
                Entity.findBy(args.session, args.tx, "id", args.id, args.callback);
            };

            Entity.findBy = function(session, tx, property, value, callback) {
                var args = argspec.getArgs(arguments, [
                    { name: 'session', optional: true, check: persistence.isSession, defaultValue: persistence },
                    { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                    { name: 'property', optional: false, check: argspec.hasType('string') },
                    { name: 'value', optional: false },
                    { name: 'callback', optional: true, check: argspec.isCallback(), defaultValue: function(){} }
                ]);
                session = args.session;
                tx = args.tx;
                property = args.property;
                value = args.value;
                callback = args.callback;

                if(property === 'id' && value in session.trackedObjects) {
                    callback(session.trackedObjects[value]);
                    return;
                }
                if(!tx) {
                    session.transaction(function(tx) {
                        Entity.findBy(session, tx, property, value, callback);
                    });
                    return;
                }
                Entity.all(session).filter(property, "=", value).one(tx, function(obj) {
                    callback(obj);
                });
            }


            Entity.index = function(cols,options) {
                var opts = options || {};
                if (typeof cols=="string") {
                    cols = [cols];
                }
                opts.columns = cols;
                meta.indexes.push(opts);
            };

            /**
             * Declares a one-to-many or many-to-many relationship to another entity
             * Whether 1:N or N:M is chosed depends on the inverse declaration
             * @param collName the name of the collection (becomes a property of
             *   Entity instances
             * @param otherEntity the constructor function of the entity to define
             *   the relation to
             * @param inverseRel the name of the inverse property (to be) defined on otherEntity
             */
            Entity.hasMany = function (collName, otherEntity, invRel) {
                var otherMeta = otherEntity.meta;
                if (otherMeta.hasMany[invRel]) {
                    // other side has declared it as a one-to-many relation too -> it's in
                    // fact many-to-many
                    var tableName = meta.name + "_" + collName + "_" + otherMeta.name;
                    var inverseTableName = otherMeta.name + '_' + invRel + '_' + meta.name;

                    if (tableName > inverseTableName) {
                        // Some arbitrary way to deterministically decide which table to generate
                        tableName = inverseTableName;
                    }
                    meta.hasMany[collName] = {
                        type: otherEntity,
                        inverseProperty: invRel,
                        manyToMany: true,
                        tableName: tableName
                    };
                    otherMeta.hasMany[invRel] = {
                        type: Entity,
                        inverseProperty: collName,
                        manyToMany: true,
                        tableName: tableName
                    };
                    delete meta.hasOne[collName];
                    delete meta.fields[collName + "_class"]; // in case it existed
                } else {
                    meta.hasMany[collName] = {
                        type: otherEntity,
                        inverseProperty: invRel
                    };
                    otherMeta.hasOne[invRel] = {
                        type: Entity,
                        inverseProperty: collName
                    };
                    if (meta.isMixin)
                        otherMeta.fields[invRel + "_class"] = persistence.typeMapper ? persistence.typeMapper.classNameType : "TEXT";
                }
            }

            Entity.hasOne = function (refName, otherEntity, inverseProperty, field) {
                meta.hasOne[refName] = {
                    type: otherEntity,
                    field: field,
                    inverseProperty: inverseProperty
                };
                if (otherEntity.meta.isMixin)
                    meta.fields[refName + "_class"] = persistence.typeMapper ? persistence.typeMapper.classNameType : "TEXT";
            };

            Entity.is = function(mixin){
                var mixinMeta = mixin.meta;
                if (!mixinMeta.isMixin)
                    throw new Error("not a mixin: " + mixin);

                mixin.meta.mixedIns = mixin.meta.mixedIns || [];
                mixin.meta.mixedIns.push(meta);

                for (var field in mixinMeta.fields) {
                    if (mixinMeta.fields.hasOwnProperty(field))
                        meta.fields[field] = mixinMeta.fields[field];
                }
                for (var it in mixinMeta.hasOne) {
                    if (mixinMeta.hasOne.hasOwnProperty(it))
                        meta.hasOne[it] = mixinMeta.hasOne[it];
                }
                for (var it in mixinMeta.hasMany) {
                    if (mixinMeta.hasMany.hasOwnProperty(it)) {
                        mixinMeta.hasMany[it].mixin = mixin;
                        meta.hasMany[it] = mixinMeta.hasMany[it];
                    }
                }
            }

            // Allow decorator functions to add more stuff
            var fns = persistence.entityDecoratorHooks;
            for(var i = 0; i < fns.length; i++) {
                fns[i](Entity);
            }

            entityClassCache[entityName] = Entity;
            return Entity;
        }

        persistence.jsonToEntityVal = function(value, type) {
            if(type) {
                switch(type) {
                    case 'DATE':
                        if(typeof value === 'number') {
                            if (value > 1000000000000) {
                                // it's in milliseconds
                                return new Date(value);
                            } else {
                                return new Date(value * 1000);
                            }
                        } else {
                            return null;
                        }
                        break;
                    default:
                        return value;
                }
            } else {
                return value;
            }
        };

        persistence.entityValToJson = function(value, type) {
            if(type) {
                switch(type) {
                    case 'DATE':
                        if(value) {
                            value = new Date(value);
                            return Math.round(value.getTime() / 1000);
                        } else {
                            return null;
                        }
                        break;
                    default:
                        return value;
                }
            } else {
                return value;
            }
        };

        /**
         * Dumps the entire database into an object (that can be serialized to JSON for instance)
         * @param tx transaction to use, use `null` to start a new one
         * @param entities a list of entity constructor functions to serialize, use `null` for all
         * @param callback (object) the callback function called with the results.
         */
        persistence.dump = function(tx, entities, callback) {
            var args = argspec.getArgs(arguments, [
                { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                { name: 'entities', optional: true, check: function(obj) { return !obj || (obj && obj.length && !obj.apply); }, defaultValue: null },
                { name: 'callback', optional: false, check: argspec.isCallback(), defaultValue: function(){} }
            ]);
            tx = args.tx;
            entities = args.entities;
            callback = args.callback;

            if(!entities) { // Default: all entity types
                entities = [];
                for(var e in entityClassCache) {
                    if(entityClassCache.hasOwnProperty(e)) {
                        entities.push(entityClassCache[e]);
                    }
                }
            }

            var result = {};
            persistence.asyncParForEach(entities, function(Entity, callback) {
                Entity.all().list(tx, function(all) {
                    var items = [];
                    persistence.asyncParForEach(all, function(e, callback) {
                        var rec = {};
                        var fields = Entity.meta.fields;
                        for(var f in fields) {
                            if(fields.hasOwnProperty(f)) {
                                rec[f] = persistence.entityValToJson(e._data[f], fields[f]);
                            }
                        }
                        var refs = Entity.meta.hasOne;
                        for(var r in refs) {
                            if(refs.hasOwnProperty(r)) {
                                rec[r] = e._data[r];
                            }
                        }
                        var colls = Entity.meta.hasMany;
                        var collArray = [];
                        for(var coll in colls) {
                            if(colls.hasOwnProperty(coll)) {
                                collArray.push(coll);
                            }
                        }
                        persistence.asyncParForEach(collArray, function(collP, callback) {
                            var coll = persistence.get(e, collP);
                            coll.list(tx, function(results) {
                                rec[collP] = results.map(function(r) { return r.id; });
                                callback();
                            });
                        }, function() {
                            rec.id = e.id;
                            items.push(rec);
                            callback();
                        });
                    }, function() {
                        result[Entity.meta.name] = items;
                        callback();
                    });
                });
            }, function() {
                callback(result);
            });
        };

        /**
         * Loads a set of entities from a dump object
         * @param tx transaction to use, use `null` to start a new one
         * @param dump the dump object
         * @param callback the callback function called when done.
         */
        persistence.load = function(tx, dump, callback) {
            var args = argspec.getArgs(arguments, [
                { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                { name: 'dump', optional: false },
                { name: 'callback', optional: true, check: argspec.isCallback(), defaultValue: function(){} }
            ]);
            tx = args.tx;
            dump = args.dump;
            callback = args.callback;

            var finishedCount = 0;
            var collItemsToAdd = [];
            var session = this;
            for(var entityName in dump) {
                if(dump.hasOwnProperty(entityName)) {
                    var Entity = getEntity(entityName);
                    var fields = Entity.meta.fields;
                    var instances = dump[entityName];
                    for(var i = 0; i < instances.length; i++) {
                        var instance = instances[i];
                        var ent = new Entity();
                        ent.id = instance.id;
                        for(var p in instance) {
                            if(instance.hasOwnProperty(p)) {
                                if (persistence.isImmutable(p)) {
                                    ent[p] = instance[p];
                                } else if(Entity.meta.hasMany[p]) { // collection
                                    var many = Entity.meta.hasMany[p];
                                    if(many.manyToMany && Entity.meta.name < many.type.meta.name) { // Arbitrary way to avoid double adding
                                        continue;
                                    }
                                    var coll = persistence.get(ent, p);
                                    if(instance[p].length > 0) {
                                        instance[p].forEach(function(it) {
                                            collItemsToAdd.push({Entity: Entity, coll: coll, id: it});
                                        });
                                    }
                                } else {
                                    persistence.set(ent, p, persistence.jsonToEntityVal(instance[p], fields[p]));
                                }
                            }
                        }
                        this.add(ent);
                    }
                }
            }
            session.flush(tx, function() {
                persistence.asyncForEach(collItemsToAdd, function(collItem, callback) {
                    collItem.Entity.load(session, tx, collItem.id, function(obj) {
                        collItem.coll.add(obj);
                        callback();
                    });
                }, function() {
                    session.flush(tx, callback);
                });
            });
        };

        /**
         * Dumps the entire database to a JSON string
         * @param tx transaction to use, use `null` to start a new one
         * @param entities a list of entity constructor functions to serialize, use `null` for all
         * @param callback (jsonDump) the callback function called with the results.
         */
        persistence.dumpToJson = function(tx, entities, callback) {
            var args = argspec.getArgs(arguments, [
                { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                { name: 'entities', optional: true, check: function(obj) { return obj && obj.length && !obj.apply; }, defaultValue: null },
                { name: 'callback', optional: false, check: argspec.isCallback(), defaultValue: function(){} }
            ]);
            tx = args.tx;
            entities = args.entities;
            callback = args.callback;
            this.dump(tx, entities, function(obj) {
                callback(JSON.stringify(obj));
            });
        };

        /**
         * Loads data from a JSON string (as dumped by `dumpToJson`)
         * @param tx transaction to use, use `null` to start a new one
         * @param jsonDump JSON string
         * @param callback the callback function called when done.
         */
        persistence.loadFromJson = function(tx, jsonDump, callback) {
            var args = argspec.getArgs(arguments, [
                { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                { name: 'jsonDump', optional: false },
                { name: 'callback', optional: true, check: argspec.isCallback(), defaultValue: function(){} }
            ]);
            tx = args.tx;
            jsonDump = args.jsonDump;
            callback = args.callback;
            this.load(tx, JSON.parse(jsonDump), callback);
        };


        /**
         * Generates a UUID according to http://www.ietf.org/rfc/rfc4122.txt
         */
        function createUUID () {
            if(persistence.typeMapper && persistence.typeMapper.newUuid) {
                return persistence.typeMapper.newUuid();
            }
            var s = [];
            var hexDigits = "0123456789ABCDEF";
            for ( var i = 0; i < 32; i++) {
                s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            s[12] = "4";
            s[16] = hexDigits.substr((s[16] & 0x3) | 0x8, 1);

            var uuid = s.join("");
            return uuid;
        }

        persistence.createUUID = createUUID;


        function defaultValue(type) {
            if(persistence.typeMapper && persistence.typeMapper.defaultValue) {
                return persistence.typeMapper.defaultValue(type);
            }
            switch(type) {
                case "TEXT": return "";
                case "BOOL": return false;
                default:
                    if(type.indexOf("INT") !== -1) {
                        return 0;
                    } else if(type.indexOf("CHAR") !== -1) {
                        return "";
                    } else {
                        return null;
                    }
            }
        }

        function arrayContains(ar, item) {
            var l = ar.length;
            for(var i = 0; i < l; i++) {
                var el = ar[i];
                if(el.equals && el.equals(item)) {
                    return true;
                } else if(el === item) {
                    return true;
                }
            }
            return false;
        }

        function arrayRemove(ar, item) {
            var l = ar.length;
            for(var i = 0; i < l; i++) {
                var el = ar[i];
                if(el.equals && el.equals(item)) {
                    ar.splice(i, 1);
                    return;
                } else if(el === item) {
                    ar.splice(i, 1);
                    return;
                }
            }
        }

        ////////////////// QUERY COLLECTIONS \\\\\\\\\\\\\\\\\\\\\\\

        function Subscription(obj, eventType, fn) {
            this.obj = obj;
            this.eventType = eventType;
            this.fn = fn;
        }

        Subscription.prototype.unsubscribe = function() {
            this.obj.removeEventListener(this.eventType, this.fn);
        };

        /**
         * Simple observable function constructor
         * @constructor
         */
        function Observable() {
            this.subscribers = {};
        }

        Observable.prototype.addEventListener = function (eventType, fn) {
            if (!this.subscribers[eventType]) {
                this.subscribers[eventType] = [];
            }
            this.subscribers[eventType].push(fn);
            return new Subscription(this, eventType, fn);
        };

        Observable.prototype.removeEventListener = function(eventType, fn) {
            var subscribers = this.subscribers[eventType];
            for ( var i = 0; i < subscribers.length; i++) {
                if(subscribers[i] == fn) {
                    this.subscribers[eventType].splice(i, 1);
                    return true;
                }
            }
            return false;
        };

        Observable.prototype.triggerEvent = function (eventType) {
            if (!this.subscribers[eventType]) { // No subscribers to this event type
                return;
            }
            var subscribers = this.subscribers[eventType].slice(0);
            for(var i = 0; i < subscribers.length; i++) {
                subscribers[i].apply(null, arguments);
            }
        };

        /*
         * Each filter has 4 methods:
         * - sql(prefix, values) -- returns a SQL representation of this filter,
         *     possibly pushing additional query arguments to `values` if ?'s are used
         *     in the query
         * - match(o) -- returns whether the filter matches the object o.
         * - makeFit(o) -- attempts to adapt the object o in such a way that it matches
         *     this filter.
         * - makeNotFit(o) -- the oppositive of makeFit, makes the object o NOT match
         *     this filter
         */

        /**
         * Default filter that does not filter on anything
         * currently it generates a 1=1 SQL query, which is kind of ugly
         */
        function NullFilter () {
        }

        NullFilter.prototype.match = function (o) {
            return true;
        };

        NullFilter.prototype.makeFit = function(o) {
        };

        NullFilter.prototype.makeNotFit = function(o) {
        };

        NullFilter.prototype.toUniqueString = function() {
            return "NULL";
        };

        NullFilter.prototype.subscribeGlobally = function() { };

        NullFilter.prototype.unsubscribeGlobally = function() { };

        /**
         * Filter that makes sure that both its left and right filter match
         * @param left left-hand filter object
         * @param right right-hand filter object
         */
        function AndFilter (left, right) {
            this.left = left;
            this.right = right;
        }

        AndFilter.prototype.match = function (o) {
            return this.left.match(o) && this.right.match(o);
        };

        AndFilter.prototype.makeFit = function(o) {
            this.left.makeFit(o);
            this.right.makeFit(o);
        };

        AndFilter.prototype.makeNotFit = function(o) {
            this.left.makeNotFit(o);
            this.right.makeNotFit(o);
        };

        AndFilter.prototype.toUniqueString = function() {
            return this.left.toUniqueString() + " AND " + this.right.toUniqueString();
        };

        AndFilter.prototype.subscribeGlobally = function(coll, entityName) {
            this.left.subscribeGlobally(coll, entityName);
            this.right.subscribeGlobally(coll, entityName);
        };

        AndFilter.prototype.unsubscribeGlobally = function(coll, entityName) {
            this.left.unsubscribeGlobally(coll, entityName);
            this.right.unsubscribeGlobally(coll, entityName);
        };

        /**
         * Filter that makes sure that either its left and right filter match
         * @param left left-hand filter object
         * @param right right-hand filter object
         */
        function OrFilter (left, right) {
            this.left = left;
            this.right = right;
        }

        OrFilter.prototype.match = function (o) {
            return this.left.match(o) || this.right.match(o);
        };

        OrFilter.prototype.makeFit = function(o) {
            this.left.makeFit(o);
            this.right.makeFit(o);
        };

        OrFilter.prototype.makeNotFit = function(o) {
            this.left.makeNotFit(o);
            this.right.makeNotFit(o);
        };

        OrFilter.prototype.toUniqueString = function() {
            return this.left.toUniqueString() + " OR " + this.right.toUniqueString();
        };

        OrFilter.prototype.subscribeGlobally = function(coll, entityName) {
            this.left.subscribeGlobally(coll, entityName);
            this.right.subscribeGlobally(coll, entityName);
        };

        OrFilter.prototype.unsubscribeGlobally = function(coll, entityName) {
            this.left.unsubscribeGlobally(coll, entityName);
            this.right.unsubscribeGlobally(coll, entityName);
        };

        /**
         * Filter that checks whether a certain property matches some value, based on an
         * operator. Supported operators are '=', '!=', '<', '<=', '>' and '>='.
         * @param property the property name
         * @param operator the operator to compare with
         * @param value the literal value to compare to
         */
        function PropertyFilter (property, operator, value) {
            this.property = property;
            this.operator = operator.toLowerCase();
            this.value = value;
        }

        PropertyFilter.prototype.match = function (o) {
            var value = this.value;
            var propValue = persistence.get(o, this.property);
            if(value && value.getTime) { // DATE
                // TODO: Deal with arrays of dates for 'in' and 'not in'
                value = Math.round(value.getTime() / 1000) * 1000; // Deal with precision
                if(propValue && propValue.getTime) { // DATE
                    propValue = Math.round(propValue.getTime() / 1000) * 1000; // Deal with precision
                }
            }
            switch (this.operator) {
                case '=':
                    return propValue === value;
                    break;
                case '!=':
                    return propValue !== value;
                    break;
                case '<':
                    return propValue < value;
                    break;
                case '<=':
                    return propValue <= value;
                    break;
                case '>':
                    return propValue > value;
                    break;
                case '>=':
                    return propValue >= value;
                    break;
                case 'in':
                    return arrayContains(value, propValue);
                    break;
                case 'not in':
                    return !arrayContains(value, propValue);
                    break;
            }
        };

        PropertyFilter.prototype.makeFit = function(o) {
            if(this.operator === '=') {
                persistence.set(o, this.property, this.value);
            } else {
                throw new Error("Sorry, can't perform makeFit for other filters than =");
            }
        };

        PropertyFilter.prototype.makeNotFit = function(o) {
            if(this.operator === '=') {
                persistence.set(o, this.property, null);
            } else {
                throw new Error("Sorry, can't perform makeNotFit for other filters than =");
            }
        };

        PropertyFilter.prototype.subscribeGlobally = function(coll, entityName) {
            persistence.subscribeToGlobalPropertyListener(coll, entityName, this.property);
        };

        PropertyFilter.prototype.unsubscribeGlobally = function(coll, entityName) {
            persistence.unsubscribeFromGlobalPropertyListener(coll, entityName, this.property);
        };

        PropertyFilter.prototype.toUniqueString = function() {
            var val = this.value;
            if(val && val._type) {
                val = val.id;
            }
            return this.property + this.operator + val;
        };

        persistence.NullFilter = NullFilter;
        persistence.AndFilter = AndFilter;
        persistence.OrFilter = OrFilter;
        persistence.PropertyFilter = PropertyFilter;

        /**
         * Ensure global uniqueness of query collection object
         */
        persistence.uniqueQueryCollection = function(coll) {
            var entityName = coll._entityName;
            if(coll._items) { // LocalQueryCollection
                return coll;
            }
            if(!this.queryCollectionCache[entityName]) {
                this.queryCollectionCache[entityName] = {};
            }
            var uniqueString = coll.toUniqueString();
            if(!this.queryCollectionCache[entityName][uniqueString]) {
                this.queryCollectionCache[entityName][uniqueString] = coll;
            }
            return this.queryCollectionCache[entityName][uniqueString];
        }

        /**
         * The constructor function of the _abstract_ QueryCollection
         * DO NOT INSTANTIATE THIS
         * @constructor
         */
        function QueryCollection () {
        }

        QueryCollection.prototype = new Observable();

        QueryCollection.prototype.oldAddEventListener = QueryCollection.prototype.addEventListener;

        QueryCollection.prototype.setupSubscriptions = function() {
            this._filter.subscribeGlobally(this, this._entityName);
        };

        QueryCollection.prototype.teardownSubscriptions = function() {
            this._filter.unsubscribeGlobally(this, this._entityName);
        };

        QueryCollection.prototype.addEventListener = function(eventType, fn) {
            var that = this;
            var subscription = this.oldAddEventListener(eventType, fn);
            if(this.subscribers[eventType].length === 1) { // first subscriber
                this.setupSubscriptions();
            }
            subscription.oldUnsubscribe = subscription.unsubscribe;
            subscription.unsubscribe = function() {
                this.oldUnsubscribe();

                if(that.subscribers[eventType].length === 0) { // last subscriber
                    that.teardownSubscriptions();
                }
            };
            return subscription;
        };

        /**
         * Function called when session is flushed, returns list of SQL queries to execute
         * (as [query, arg] tuples)
         */
        QueryCollection.prototype.persistQueries = function() { return []; };

        /**
         * Invoked by sub-classes to initialize the query collection
         */
        QueryCollection.prototype.init = function (session, entityName, constructor) {
            this._filter = new NullFilter();
            this._orderColumns = []; // tuples of [column, ascending]
            this._prefetchFields = [];
            this._entityName = entityName;
            this._constructor = constructor;
            this._limit = -1;
            this._skip = 0;
            this._reverse = false;
            this._session = session || persistence;
            // For observable
            this.subscribers = {};
        }

        QueryCollection.prototype.toUniqueString = function() {
            var s = this._constructor.name + ": " + this._entityName;
            s += '|Filter:';
            var values = [];
            s += this._filter.toUniqueString();
            s += '|Values:';
            for(var i = 0; i < values.length; i++) {
                s += values + "|^|";
            }
            s += '|Order:';
            for(var i = 0; i < this._orderColumns.length; i++) {
                var col = this._orderColumns[i];
                s += col[0] + ", " + col[1] + ", " + col[2];
            }
            s += '|Prefetch:';
            for(var i = 0; i < this._prefetchFields.length; i++) {
                s += this._prefetchFields[i];
            }
            s += '|Limit:';
            s += this._limit;
            s += '|Skip:';
            s += this._skip;
            s += '|Reverse:';
            s += this._reverse;
            return s;
        };

        /**
         * Creates a clone of this query collection
         * @return a clone of the collection
         */
        QueryCollection.prototype.clone = function (cloneSubscribers) {
            var c = new (this._constructor)(this._session, this._entityName);
            c._filter = this._filter;
            c._prefetchFields = this._prefetchFields.slice(0); // clone
            c._orderColumns = this._orderColumns.slice(0);
            c._limit = this._limit;
            c._skip = this._skip;
            c._reverse = this._reverse;
            if(cloneSubscribers) {
                var subscribers = {};
                for(var eventType in this.subscribers) {
                    if(this.subscribers.hasOwnProperty(eventType)) {
                        subscribers[eventType] = this.subscribers[eventType].slice(0);
                    }
                }
                c.subscribers = subscribers; //this.subscribers;
            } else {
                c.subscribers = this.subscribers;
            }
            return c;
        };

        /**
         * Returns a new query collection with a property filter condition added
         * @param property the property to filter on
         * @param operator the operator to use
         * @param value the literal value that the property should match
         * @return the query collection with the filter added
         */
        QueryCollection.prototype.filter = function (property, operator, value) {
            var c = this.clone(true);
            c._filter = new AndFilter(this._filter, new PropertyFilter(property,
                operator, value));
            // Add global listener (TODO: memory leak waiting to happen!)
            var session = this._session;
            c = session.uniqueQueryCollection(c);
            //session.subscribeToGlobalPropertyListener(c, this._entityName, property);
            return session.uniqueQueryCollection(c);
        };

        /**
         * Returns a new query collection with an OR condition between the
         * current filter and the filter specified as argument
         * @param filter the other filter
         * @return the new query collection
         */
        QueryCollection.prototype.or = function (filter) {
            var c = this.clone(true);
            c._filter = new OrFilter(this._filter, filter);
            return this._session.uniqueQueryCollection(c);
        };

        /**
         * Returns a new query collection with an AND condition between the
         * current filter and the filter specified as argument
         * @param filter the other filter
         * @return the new query collection
         */
        QueryCollection.prototype.and = function (filter) {
            var c = this.clone(true);
            c._filter = new AndFilter(this._filter, filter);
            return this._session.uniqueQueryCollection(c);
        };

        /**
         * Returns a new query collection with an ordering imposed on the collection
         * @param property the property to sort on
         * @param ascending should the order be ascending (= true) or descending (= false)
         * @param caseSensitive should the order be case sensitive (= true) or case insensitive (= false)
         *        note: using case insensitive ordering for anything other than TEXT fields yields
         *        undefinded behavior
         * @return the query collection with imposed ordering
         */
        QueryCollection.prototype.order = function (property, ascending, caseSensitive) {
            ascending = ascending === undefined ? true : ascending;
            caseSensitive = caseSensitive === undefined ? true : caseSensitive;
            var c = this.clone();
            c._orderColumns.push( [ property, ascending, caseSensitive ]);
            return this._session.uniqueQueryCollection(c);
        };

        /**
         * Returns a new query collection will limit its size to n items
         * @param n the number of items to limit it to
         * @return the limited query collection
         */
        QueryCollection.prototype.limit = function(n) {
            var c = this.clone();
            c._limit = n;
            return this._session.uniqueQueryCollection(c);
        };

        /**
         * Returns a new query collection which will skip the first n results
         * @param n the number of results to skip
         * @return the query collection that will skip n items
         */
        QueryCollection.prototype.skip = function(n) {
            var c = this.clone();
            c._skip = n;
            return this._session.uniqueQueryCollection(c);
        };

        /**
         * Returns a new query collection which reverse the order of the result set
         * @return the query collection that will reverse its items
         */
        QueryCollection.prototype.reverse = function() {
            var c = this.clone();
            c._reverse = true;
            return this._session.uniqueQueryCollection(c);
        };

        /**
         * Returns a new query collection which will prefetch a certain object relationship.
         * Only works with 1:1 and N:1 relations.
         * Relation must target an entity, not a mix-in.
         * @param rel the relation name of the relation to prefetch
         * @return the query collection prefetching `rel`
         */
        QueryCollection.prototype.prefetch = function (rel) {
            var c = this.clone();
            c._prefetchFields.push(rel);
            return this._session.uniqueQueryCollection(c);
        };


        /**
         * Select a subset of data, represented by this query collection as a JSON
         * structure (Javascript object)
         *
         * @param tx database transaction to use, leave out to start a new one
         * @param props a property specification
         * @param callback(result)
         */
        QueryCollection.prototype.selectJSON = function(tx, props, callback) {
            var args = argspec.getArgs(arguments, [
                { name: "tx", optional: true, check: persistence.isTransaction, defaultValue: null },
                { name: "props", optional: false },
                { name: "callback", optional: false }
            ]);
            var session = this._session;
            var that = this;
            tx = args.tx;
            props = args.props;
            callback = args.callback;

            if(!tx) {
                session.transaction(function(tx) {
                    that.selectJSON(tx, props, callback);
                });
                return;
            }
            var Entity = getEntity(this._entityName);
            // TODO: This could do some clever prefetching to make it more efficient
            this.list(function(items) {
                var resultArray = [];
                persistence.asyncForEach(items, function(item, callback) {
                    item.selectJSON(tx, props, function(obj) {
                        resultArray.push(obj);
                        callback();
                    });
                }, function() {
                    callback(resultArray);
                });
            });
        };

        /**
         * Adds an object to a collection
         * @param obj the object to add
         */
        QueryCollection.prototype.add = function(obj) {
            if(!obj.id || !obj._type) {
                throw new Error("Cannot add object of non-entity type onto collection.");
            }
            this._session.add(obj);
            this._filter.makeFit(obj);
            this.triggerEvent('add', this, obj);
            this.triggerEvent('change', this, obj);
        }

        /**
         * Adds an an array of objects to a collection
         * @param obj the object to add
         */
        QueryCollection.prototype.addAll = function(objs) {
            for(var i = 0; i < objs.length; i++) {
                var obj = objs[i];
                this._session.add(obj);
                this._filter.makeFit(obj);
                this.triggerEvent('add', this, obj);
            }
            this.triggerEvent('change', this);
        }

        /**
         * Removes an object from a collection
         * @param obj the object to remove from the collection
         */
        QueryCollection.prototype.remove = function(obj) {
            if(!obj.id || !obj._type) {
                throw new Error("Cannot remove object of non-entity type from collection.");
            }
            this._filter.makeNotFit(obj);
            this.triggerEvent('remove', this, obj);
            this.triggerEvent('change', this, obj);
        }


        /**
         * A database implementation of the QueryCollection
         * @param entityName the name of the entity to create the collection for
         * @constructor
         */
        function DbQueryCollection (session, entityName) {
            this.init(session, entityName, DbQueryCollection);
        }

        /**
         * Execute a function for each item in the list
         * @param tx the transaction to use (or null to open a new one)
         * @param eachFn (elem) the function to be executed for each item
         */
        QueryCollection.prototype.each = function (tx, eachFn) {
            var args = argspec.getArgs(arguments, [
                { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                { name: 'eachFn', optional: true, check: argspec.isCallback() }
            ]);
            tx = args.tx;
            eachFn = args.eachFn;

            this.list(tx, function(results) {
                for(var i = 0; i < results.length; i++) {
                    eachFn(results[i]);
                }
            });
        }

        // Alias
        QueryCollection.prototype.forEach = QueryCollection.prototype.each;

        QueryCollection.prototype.one = function (tx, oneFn) {
            var args = argspec.getArgs(arguments, [
                { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                { name: 'oneFn', optional: false, check: argspec.isCallback() }
            ]);
            tx = args.tx;
            oneFn = args.oneFn;

            var that = this;

            this.limit(1).list(tx, function(results) {
                if(results.length === 0) {
                    oneFn(null);
                } else {
                    oneFn(results[0]);
                }
            });
        }

        DbQueryCollection.prototype = new QueryCollection();


        /**
         * An implementation of QueryCollection, that is used
         * to represent all instances of an entity type
         * @constructor
         */
        function AllDbQueryCollection (session, entityName) {
            this.init(session, entityName, AllDbQueryCollection);
        }

        AllDbQueryCollection.prototype = new DbQueryCollection();

        AllDbQueryCollection.prototype.add = function(obj) {
            this._session.add(obj);
            this.triggerEvent('add', this, obj);
            this.triggerEvent('change', this, obj);
        };

        AllDbQueryCollection.prototype.remove = function(obj) {
            this._session.remove(obj);
            this.triggerEvent('remove', this, obj);
            this.triggerEvent('change', this, obj);
        };

        /**
         * A ManyToMany implementation of QueryCollection
         * @constructor
         */
        function ManyToManyDbQueryCollection (session, entityName) {
            this.init(session, entityName, persistence.ManyToManyDbQueryCollection);
            this._localAdded = [];
            this._localRemoved = [];
        }

        ManyToManyDbQueryCollection.prototype = new DbQueryCollection();

        ManyToManyDbQueryCollection.prototype.initManyToMany = function(obj, coll) {
            this._obj = obj;
            this._coll = coll;
        };

        ManyToManyDbQueryCollection.prototype.add = function(obj) {
            if(!arrayContains(this._localAdded, obj)) {
                this._session.add(obj);
                this._localAdded.push(obj);
                this.triggerEvent('add', this, obj);
                this.triggerEvent('change', this, obj);
            }
        };

        ManyToManyDbQueryCollection.prototype.addAll = function(objs) {
            for(var i = 0; i < objs.length; i++) {
                var obj = objs[i];
                if(!arrayContains(this._localAdded, obj)) {
                    this._session.add(obj);
                    this._localAdded.push(obj);
                    this.triggerEvent('add', this, obj);
                }
            }
            this.triggerEvent('change', this);
        }

        ManyToManyDbQueryCollection.prototype.clone = function() {
            var c = DbQueryCollection.prototype.clone.call(this);
            c._localAdded = this._localAdded;
            c._localRemoved = this._localRemoved;
            c._obj = this._obj;
            c._coll = this._coll;
            return c;
        };

        ManyToManyDbQueryCollection.prototype.remove = function(obj) {
            if(arrayContains(this._localAdded, obj)) { // added locally, can just remove it from there
                arrayRemove(this._localAdded, obj);
            } else if(!arrayContains(this._localRemoved, obj)) {
                this._localRemoved.push(obj);
            }
            this.triggerEvent('remove', this, obj);
            this.triggerEvent('change', this, obj);
        };

        ////////// Local implementation of QueryCollection \\\\\\\\\\\\\\\\

        function LocalQueryCollection(initialArray) {
            this.init(persistence, null, LocalQueryCollection);
            this._items = initialArray || [];
        }

        LocalQueryCollection.prototype = new QueryCollection();

        LocalQueryCollection.prototype.clone = function() {
            var c = DbQueryCollection.prototype.clone.call(this);
            c._items = this._items;
            return c;
        };

        LocalQueryCollection.prototype.add = function(obj) {
            if(!arrayContains(this._items, obj)) {
                this._session.add(obj);
                this._items.push(obj);
                this.triggerEvent('add', this, obj);
                this.triggerEvent('change', this, obj);
            }
        };

        LocalQueryCollection.prototype.addAll = function(objs) {
            for(var i = 0; i < objs.length; i++) {
                var obj = objs[i];
                if(!arrayContains(this._items, obj)) {
                    this._session.add(obj);
                    this._items.push(obj);
                    this.triggerEvent('add', this, obj);
                }
            }
            this.triggerEvent('change', this);
        }

        LocalQueryCollection.prototype.remove = function(obj) {
            var items = this._items;
            for(var i = 0; i < items.length; i++) {
                if(items[i] === obj) {
                    this._items.splice(i, 1);
                    this.triggerEvent('remove', this, obj);
                    this.triggerEvent('change', this, obj);
                }
            }
        };

        LocalQueryCollection.prototype.list = function(tx, callback) {
            var args = argspec.getArgs(arguments, [
                { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                { name: 'callback', optional: true, check: argspec.isCallback() }
            ]);
            callback = args.callback;

            if(!callback || callback.executeSql) { // first argument is transaction
                callback = arguments[1]; // set to second argument
            }
            var array = this._items.slice(0);
            var that = this;
            var results = [];
            for(var i = 0; i < array.length; i++) {
                if(this._filter.match(array[i])) {
                    results.push(array[i]);
                }
            }
            results.sort(function(a, b) {
                for(var i = 0; i < that._orderColumns.length; i++) {
                    var col = that._orderColumns[i][0];
                    var asc = that._orderColumns[i][1];
                    var sens = that._orderColumns[i][2];
                    var aVal = persistence.get(a, col);
                    var bVal = persistence.get(b, col);
                    if (!sens) {
                        aVal = aVal.toLowerCase();
                        bVal = bVal.toLowerCase();
                    }
                    if(aVal < bVal) {
                        return asc ? -1 : 1;
                    } else if(aVal > bVal) {
                        return asc ? 1 : -1;
                    }
                }
                return 0;
            });
            if(this._skip) {
                results.splice(0, this._skip);
            }
            if(this._limit > -1) {
                results = results.slice(0, this._limit);
            }
            if(this._reverse) {
                results.reverse();
            }
            if(callback) {
                callback(results);
            } else {
                return results;
            }
        };

        LocalQueryCollection.prototype.destroyAll = function(callback) {
            if(!callback || callback.executeSql) { // first argument is transaction
                callback = arguments[1]; // set to second argument
            }
            this._items = [];
            this.triggerEvent('change', this);
            if(callback) callback();
        };

        LocalQueryCollection.prototype.count = function(tx, callback) {
            var args = argspec.getArgs(arguments, [
                { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
                { name: 'callback', optional: true, check: argspec.isCallback() }
            ]);
            tx = args.tx;
            callback = args.callback;

            var result = this.list();

            if(callback) {
                callback(result.length);
            } else {
                return result.length;
            }
        };

        persistence.QueryCollection             = QueryCollection;
        persistence.DbQueryCollection           = DbQueryCollection;
        persistence.ManyToManyDbQueryCollection = ManyToManyDbQueryCollection;
        persistence.LocalQueryCollection        = LocalQueryCollection;
        persistence.Observable                  = Observable;
        persistence.Subscription                = Subscription;
        persistence.AndFilter                   = AndFilter;
        persistence.OrFilter                    = OrFilter;
        persistence.PropertyFilter              = PropertyFilter;
    }());

// ArgSpec.js library: http://github.com/zefhemel/argspecjs
    var argspec = {};

    (function() {
        argspec.getArgs = function(args, specs) {
            var argIdx = 0;
            var specIdx = 0;
            var argObj = {};
            while(specIdx < specs.length) {
                var s = specs[specIdx];
                var a = args[argIdx];
                if(s.optional) {
                    if(a !== undefined && s.check(a)) {
                        argObj[s.name] = a;
                        argIdx++;
                        specIdx++;
                    } else {
                        if(s.defaultValue !== undefined) {
                            argObj[s.name] = s.defaultValue;
                        }
                        specIdx++;
                    }
                } else {
                    if(s.check && !s.check(a)) {
                        throw new Error("Invalid value for argument: " + s.name + " Value: " + a);
                    }
                    argObj[s.name] = a;
                    specIdx++;
                    argIdx++;
                }
            }
            return argObj;
        }

        argspec.hasProperty = function(name) {
            return function(obj) {
                return obj && obj[name] !== undefined;
            };
        }

        argspec.hasType = function(type) {
            return function(obj) {
                return typeof obj === type;
            };
        }

        argspec.isCallback = function() {
            return function(obj) {
                return obj && obj.apply;
            };
        }
    }());

    persistence.argspec = argspec;

    return persistence;
} // end of createPersistence



// JSON2 library, source: http://www.JSON.org/js.html
// Most modern browsers already support this natively, but mobile
// browsers often don't, hence this implementation
// Relevant APIs:
//    JSON.stringify(value, replacer, space)
//    JSON.parse(text, reviver)

if(typeof JSON === 'undefined') {
    JSON = {};
}
//var JSON = typeof JSON === 'undefined' ? window.JSON : {};
if (!JSON.stringify) {
    (function () {
        function f(n) {
            return n < 10 ? '0' + n : n;
        }
        if (typeof Date.prototype.toJSON !== 'function') {

            Date.prototype.toJSON = function (key) {

                return isFinite(this.valueOf()) ?
                    this.getUTCFullYear()   + '-' +
                        f(this.getUTCMonth() + 1) + '-' +
                        f(this.getUTCDate())      + 'T' +
                        f(this.getUTCHours())     + ':' +
                        f(this.getUTCMinutes())   + ':' +
                        f(this.getUTCSeconds())   + 'Z' : null;
            };

            String.prototype.toJSON =
                Number.prototype.toJSON =
                    Boolean.prototype.toJSON = function (key) {
                        return this.valueOf();
                    };
        }

        var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            gap, indent,
            meta = {
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"' : '\\"',
                '\\': '\\\\'
            },
            rep;

        function quote(string) {
            escapable.lastIndex = 0;
            return escapable.test(string) ?
                '"' + string.replace(escapable, function (a) {
                    var c = meta[a];
                    return typeof c === 'string' ? c :
                        '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                }) + '"' :
                '"' + string + '"';
        }


        function str(key, holder) {
            var i, k, v, length, mind = gap, partial, value = holder[key];

            if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
                value = value.toJSON(key);
            }

            if (typeof rep === 'function') {
                value = rep.call(holder, key, value);
            }

            switch (typeof value) {
                case 'string':
                    return quote(value);
                case 'number':
                    return isFinite(value) ? String(value) : 'null';
                case 'boolean':
                case 'null':
                    return String(value);
                case 'object':
                    if (!value) {
                        return 'null';
                    }

                    gap += indent;
                    partial = [];

                    if (Object.prototype.toString.apply(value) === '[object Array]') {
                        length = value.length;
                        for (i = 0; i < length; i += 1) {
                            partial[i] = str(i, value) || 'null';
                        }

                        v = partial.length === 0 ? '[]' :
                            gap ? '[\n' + gap +
                                partial.join(',\n' + gap) + '\n' +
                                mind + ']' :
                                '[' + partial.join(',') + ']';
                        gap = mind;
                        return v;
                    }

                    if (rep && typeof rep === 'object') {
                        length = rep.length;
                        for (i = 0; i < length; i += 1) {
                            k = rep[i];
                            if (typeof k === 'string') {
                                v = str(k, value);
                                if (v) {
                                    partial.push(quote(k) + (gap ? ': ' : ':') + v);
                                }
                            }
                        }
                    } else {
                        for (k in value) {
                            if (Object.hasOwnProperty.call(value, k)) {
                                v = str(k, value);
                                if (v) {
                                    partial.push(quote(k) + (gap ? ': ' : ':') + v);
                                }
                            }
                        }
                    }

                    v = partial.length === 0 ? '{}' :
                        gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                            mind + '}' : '{' + partial.join(',') + '}';
                    gap = mind;
                    return v;
            }
        }

        if (typeof JSON.stringify !== 'function') {
            JSON.stringify = function (value, replacer, space) {
                var i;
                gap = '';
                indent = '';
                if (typeof space === 'number') {
                    for (i = 0; i < space; i += 1) {
                        indent += ' ';
                    }
                } else if (typeof space === 'string') {
                    indent = space;
                }

                rep = replacer;
                if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                        typeof replacer.length !== 'number')) {
                    throw new Error('JSON.stringify');
                }

                return str('', {'': value});
            };
        }

        if (typeof JSON.parse !== 'function') {
            JSON.parse = function (text, reviver) {
                var j;
                function walk(holder, key) {
                    var k, v, value = holder[key];
                    if (value && typeof value === 'object') {
                        for (k in value) {
                            if (Object.hasOwnProperty.call(value, k)) {
                                v = walk(value, k);
                                if (v !== undefined) {
                                    value[k] = v;
                                } else {
                                    delete value[k];
                                }
                            }
                        }
                    }
                    return reviver.call(holder, key, value);
                }

                cx.lastIndex = 0;
                if (cx.test(text)) {
                    text = text.replace(cx, function (a) {
                        return '\\u' +
                            ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                    });
                }

                if (/^[\],:{}\s]*$/.
                    test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
                        replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
                        replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                    j = eval('(' + text + ')');
                    return typeof reviver === 'function' ?
                        walk({'': j}, '') : j;
                }
                throw new SyntaxError('JSON.parse');
            };
        }
    }());
}
;
/**
 * Default type mapper. Override to support more types or type options.
 */

var defaultTypeMapper = {
    /**
     * SQL type for ids
     */
    idType: "VARCHAR(32)",

    /**
     * SQL type for class names (used by mixins)
     */
    classNameType: "TEXT",

    /**
     * Returns SQL type for column definition
     */
    columnType: function(type){
        switch(type) {
            case 'JSON': return 'TEXT';
            case 'BOOL': return 'INT';
            case 'DATE': return 'INT';
            default: return type;
        }
    },

    inVar: function(str, type){
        return str;
    },
    outVar: function(str, type){
        return str;
    },
    outId: function(str){
        return "'" + str + "'";
    },
    /**
     * Converts a value from the database to a value suitable for the entity
     * (also does type conversions, if necessary)
     */
    dbValToEntityVal: function(val, type){
        if (val === null || val === undefined) {
            return val;
        }
        switch (type) {
            case 'DATE':
                // SQL is in seconds and JS in miliseconds
                if (val > 1000000000000) {
                    // usually in seconds, but sometimes it's milliseconds
                    return new Date(parseInt(val, 10));
                } else {
                    return new Date(parseInt(val, 10) * 1000);
                }
            case 'BOOL':
                return val === 1 || val === '1';
                break;
            case 'INT':
                return +val;
                break;
            case 'BIGINT':
                return +val;
                break;
            case 'JSON':
                if (val) {
                    return JSON.parse(val);
                }
                else {
                    return val;
                }
                break;
            default:
                return val;
        }
    },

    /**
     * Converts an entity value to a database value, inverse of
     *   dbValToEntityVal
     */
    entityValToDbVal: function(val, type){
        if (val === undefined || val === null) {
            return null;
        }
        else if (type === 'JSON' && val) {
            return JSON.stringify(val);
        }
        else if (val.id) {
            return val.id;
        }
        else if (type === 'BOOL') {
            return (val === 'false') ? 0 : (val ? 1 : 0);
        }
        else if (type === 'DATE' || val.getTime) {
            // In order to make SQLite Date/Time functions work we should store
            // values in seconds and not as miliseconds as JS Date.getTime()
            val = new Date(val);
            return Math.round(val.getTime() / 1000);
        }
        else if(type === 'VARCHAR(32)'){
            return val.toString();
        }
        else {
            return val;
        }
    },
    /**
     * Shortcut for inVar when type is id -- no need to override
     */
    inIdVar: function(str){
        return this.inVar(str, this.idType);
    },
    /**
     * Shortcut for outVar when type is id -- no need to override
     */
    outIdVar: function(str){
        return this.outVar(str, this.idType);
    },
    /**
     * Shortcut for entityValToDbVal when type is id -- no need to override
     */
    entityIdToDbId: function(id){
        return this.entityValToDbVal(id, this.idType);
    }
}

function config(persistence, dialect) {
    var argspec = persistence.argspec;

    persistence.typeMapper = dialect.typeMapper || defaultTypeMapper;

    persistence.generatedTables = {}; // set

    /**
     * Synchronize the data model with the database, creates table that had not
     * been defined before
     *
     * @param tx
     *            transaction object to use (optional)
     * @param callback
     *            function to be called when synchronization has completed,
     *            takes started transaction as argument
     */
    persistence.schemaSync = function (tx, callback, emulate) {
        var args = argspec.getArgs(arguments, [
            { name: "tx", optional: true, check: persistence.isTransaction, defaultValue: null },
            { name: "callback", optional: true, check: argspec.isCallback(), defaultValue: function(){} },
            { name: "emulate", optional: true, check: argspec.hasType('boolean') }
        ]);
        tx = args.tx;
        callback = args.callback;
        emulate = args.emulate;

        if(!tx) {
            var session = this;
            this.transaction(function(tx) { session.schemaSync(tx, callback, emulate); });
            return;
        }
        var queries = [], meta, colDefs, otherMeta, tableName;

        var tm = persistence.typeMapper;
        var entityMeta = persistence.getEntityMeta();
        for (var entityName in entityMeta) {
            if (entityMeta.hasOwnProperty(entityName)) {
                meta = entityMeta[entityName];
                if (!meta.isMixin) {
                    colDefs = [];
                    for (var prop in meta.fields) {
                        if (meta.fields.hasOwnProperty(prop)) {
                            colDefs.push([prop, meta.fields[prop]]);
                        }
                    }
                    for (var rel in meta.hasOne) {
                        if (meta.hasOne.hasOwnProperty(rel)) {
                            otherMeta = meta.hasOne[rel].type.meta;
                            colDefs.push([rel, tm.idType]);
                            queries.push([dialect.createIndex(meta.name, [rel]), null]);
                        }
                    }
                    for (var i = 0; i < meta.indexes.length; i++) {
                        queries.push([dialect.createIndex(meta.name, meta.indexes[i].columns, meta.indexes[i]), null]);
                    }
                }
                for (var rel in meta.hasMany) {
                    if (meta.hasMany.hasOwnProperty(rel) && meta.hasMany[rel].manyToMany) {
                        tableName = meta.hasMany[rel].tableName;
                        if (!persistence.generatedTables[tableName]) {
                            var otherMeta = meta.hasMany[rel].type.meta;
                            var inv = meta.hasMany[rel].inverseProperty;
                            // following test ensures that mixin mtm tables get created with the mixin itself
                            // it seems superfluous because mixin will be processed before entitites that use it
                            // but better be safe than sorry.
                            if (otherMeta.hasMany[inv].type.meta != meta)
                                continue;
                            var p1 = meta.name + "_" + rel;
                            var p2 = otherMeta.name + "_" + inv;
                            queries.push([dialect.createIndex(tableName, [p1]), null]);
                            queries.push([dialect.createIndex(tableName, [p2]), null]);
                            var columns = [[p1, tm.idType], [p2, tm.idType]];
                            if (meta.isMixin)
                                columns.push([p1 + "_class", tm.classNameType])
                            if (otherMeta.isMixin)
                                columns.push([p2 + "_class", tm.classNameType])
                            queries.push([dialect.createTable(tableName, columns), null]);
                            persistence.generatedTables[tableName] = true;
                        }
                    }
                }
                if (!meta.isMixin) {
                    colDefs.push(["id", tm.idType, "PRIMARY KEY"]);
                    persistence.generatedTables[meta.name] = true;
                    queries.push([dialect.createTable(meta.name, colDefs), null]);
                }
            }
        }
        var fns = persistence.schemaSyncHooks;
        for(var i = 0; i < fns.length; i++) {
            fns[i](tx);
        }
        if(emulate) {
            // Done
            callback(tx);
        } else {
            executeQueriesSeq(tx, queries, function(_, err) {
                callback(tx, err);
            });
        }
    };

    /**
     * Persists all changes to the database transaction
     *
     * @param tx
     *            transaction to use
     * @param callback
     *            function to be called when done
     */
    persistence.flush = function (tx, callback) {
        var args = argspec.getArgs(arguments, [
            { name: "tx", optional: true, check: persistence.isTransaction },
            { name: "callback", optional: true, check: argspec.isCallback(), defaultValue: null }
        ]);
        tx = args.tx;
        callback = args.callback;

        var session = this;
        if(!tx) {
            this.transaction(function(tx) { session.flush(tx, callback); });
            return;
        }
        var fns = persistence.flushHooks;
        persistence.asyncForEach(fns, function(fn, callback) {
            fn(session, tx, callback);
        }, function() {
            // After applying the hooks
            var persistObjArray = [];

            for (var id in session.trackedObjects) {
                if (session.trackedObjects.hasOwnProperty(id)) {
                    persistObjArray.push(session.trackedObjects[id]);
                }
            }
            var removeObjArray = [];
            for (var id in session.objectsToRemove) {
                if (session.objectsToRemove.hasOwnProperty(id)) {
                    removeObjArray.push(session.objectsToRemove[id]);
                    delete session.trackedObjects[id]; // Stop tracking
                }
            }
            session.objectsToRemove = {};
            if(callback) {
                persistence.asyncParForEach(removeObjArray, function(obj, callback) {
                    remove(obj, tx, callback);
                }, function(result, err) {
                    if (err) return callback(result, err);
                    persistence.asyncParForEach(persistObjArray, function(obj, callback) {
                        save(obj, tx, callback);
                    }, callback);
                });
            } else { // More efficient
                for(var i = 0; i < persistObjArray.length; i++) {
                    save(persistObjArray[i], tx);
                }
                for(var i = 0; i < removeObjArray.length; i++) {
                    remove(removeObjArray[i], tx);
                }
            }
        });
    };

    /**
     * Remove all tables in the database (as defined by the model)
     */
    persistence.reset = function (tx, callback) {
        var args = argspec.getArgs(arguments, [
            { name: "tx", optional: true, check: persistence.isTransaction, defaultValue: null },
            { name: "callback", optional: true, check: argspec.isCallback(), defaultValue: function(){} }
        ]);
        tx = args.tx;
        callback = args.callback;

        var session = this;
        if(!tx) {
            session.transaction(function(tx) { session.reset(tx, callback); });
            return;
        }
        // First emulate syncing the schema (to know which tables were created)
        this.schemaSync(tx, function() {
            var tableArray = [];
            for (var p in persistence.generatedTables) {
                if (persistence.generatedTables.hasOwnProperty(p)) {
                    tableArray.push(p);
                }
            }
            function dropOneTable () {
                var tableName = tableArray.pop();
                tx.executeSql("DROP TABLE IF EXISTS `" + tableName + "`", null, function () {
                    if (tableArray.length > 0) {
                        dropOneTable();
                    } else {
                        cb();
                    }
                }, cb);
            }
            if(tableArray.length > 0) {
                dropOneTable();
            } else {
                cb();
            }

            function cb(result, err) {
                session.clean();
                persistence.generatedTables = {};
                if (callback) callback(result, err);
            }
        }, true);
    };

    /**
     * Converts a database row into an entity object
     */
    function rowToEntity(session, entityName, row, prefix) {
        prefix = prefix || '';
        if (session.trackedObjects[row[prefix + "id"]]) { // Cached version
            return session.trackedObjects[row[prefix + "id"]];
        }
        var tm = persistence.typeMapper;
        var rowMeta = persistence.getMeta(entityName);
        var ent = persistence.define(entityName); // Get entity
        if(!row[prefix+'id']) { // null value, no entity found
            return null;
        }
        var o = new ent(session, undefined, true);
        o.id = tm.dbValToEntityVal(row[prefix + 'id'], tm.idType);
        o._new = false;
        for ( var p in row) {
            if (row.hasOwnProperty(p)) {
                if (p.substring(0, prefix.length) === prefix) {
                    var prop = p.substring(prefix.length);
                    if (prop != 'id') {
                        o._data[prop] = tm.dbValToEntityVal(row[p], rowMeta.fields[prop] || tm.idType);
                    }
                }
            }
        }
        return o;
    }

    /**
     * Internal function to persist an object to the database
     * this function is invoked by persistence.flush()
     */
    function save(obj, tx, callback) {
        var meta = persistence.getMeta(obj._type);
        var tm = persistence.typeMapper;
        var properties = [];
        var values = [];
        var qs = [];
        var propertyPairs = [];
        if(obj._new) { // Mark all properties dirty
            for (var p in meta.fields) {
                if(meta.fields.hasOwnProperty(p)) {
                    obj._dirtyProperties[p] = true;
                }
            }
        }
        for ( var p in obj._dirtyProperties) {
            if (obj._dirtyProperties.hasOwnProperty(p)) {
                properties.push("`" + p + "`");
                var type = meta.fields[p] || tm.idType;
                values.push(tm.entityValToDbVal(obj._data[p], type));
                qs.push(tm.outVar("?", type));
                propertyPairs.push("`" + p + "` = " + tm.outVar("?", type));
            }
        }
        var additionalQueries = [];
        for(var p in meta.hasMany) {
            if(meta.hasMany.hasOwnProperty(p)) {
                additionalQueries = additionalQueries.concat(persistence.get(obj, p).persistQueries());
            }
        }
        executeQueriesSeq(tx, additionalQueries, function() {
            if (!obj._new && properties.length === 0) { // Nothing changed and not new
                if(callback) callback();
                return;
            }
            obj._dirtyProperties = {};
            if (obj._new) {
                properties.push('id');
                values.push(tm.entityIdToDbId(obj.id));
                qs.push(tm.outIdVar('?'));
                var sql = "INSERT INTO `" + obj._type + "` (" + properties.join(", ") + ") VALUES (" + qs.join(', ') + ")";
                obj._new = false;
                tx.executeSql(sql, values, callback, callback);
            } else {
                var sql = "UPDATE `" + obj._type + "` SET " + propertyPairs.join(',') + " WHERE id = " + tm.outId(obj.id);
                tx.executeSql(sql, values, callback, callback);
            }
        });
    }

    persistence.save = save;

    function remove (obj, tx, callback) {
        var meta = persistence.getMeta(obj._type);
        var tm = persistence.typeMapper;
        var queries = [["DELETE FROM `" + obj._type + "` WHERE id = " + tm.outId(obj.id), null]];
        for (var rel in meta.hasMany) {
            if (meta.hasMany.hasOwnProperty(rel) && meta.hasMany[rel].manyToMany) {
                var tableName = meta.hasMany[rel].tableName;
                //var inverseProperty = meta.hasMany[rel].inverseProperty;
                queries.push(["DELETE FROM `" + tableName + "` WHERE `" + meta.name + '_' + rel + "` = " + tm.outId(obj.id), null]);
            }
        }
        executeQueriesSeq(tx, queries, callback);
    }

    /**
     * Utility function to execute a series of queries in an asynchronous way
     * @param tx the transaction to execute the queries on
     * @param queries an array of [query, args] tuples
     * @param callback the function to call when all queries have been executed
     */
    function executeQueriesSeq (tx, queries, callback) {
        // queries.reverse();
        var callbackArgs = [];
        for ( var i = 3; i < arguments.length; i++) {
            callbackArgs.push(arguments[i]);
        }
        persistence.asyncForEach(queries, function(queryTuple, callback) {
            tx.executeSql(queryTuple[0], queryTuple[1], callback, function(_, err) {
                console.log(err.message);
                callback(_, err);
            });
        }, function(result, err) {
            if (err && callback) {
                callback(result, err);
                return;
            }
            if(callback) callback.apply(null, callbackArgs);
        });
    }

    persistence.executeQueriesSeq = executeQueriesSeq;

    /////////////////////////// QueryCollection patches to work in SQL environment

    /**
     * Function called when session is flushed, returns list of SQL queries to execute
     * (as [query, arg] tuples)
     */
    persistence.QueryCollection.prototype.persistQueries = function() { return []; };

    var oldQCClone = persistence.QueryCollection.prototype.clone;

    persistence.QueryCollection.prototype.clone = function (cloneSubscribers) {
        var c = oldQCClone.call(this, cloneSubscribers);
        c._additionalJoinSqls = this._additionalJoinSqls.slice(0);
        c._additionalWhereSqls = this._additionalWhereSqls.slice(0);
        c._additionalGroupSqls = this._additionalGroupSqls.slice(0);
        c._manyToManyFetch = this._manyToManyFetch;
        return c;
    };

    var oldQCInit = persistence.QueryCollection.prototype.init;

    persistence.QueryCollection.prototype.init = function(session, entityName, constructor) {
        oldQCInit.call(this, session, entityName, constructor);
        this._manyToManyFetch = null;
        this._additionalJoinSqls = [];
        this._additionalWhereSqls = [];
        this._additionalGroupSqls = [];
    };

    var oldQCToUniqueString = persistence.QueryCollection.prototype.toUniqueString;

    persistence.QueryCollection.prototype.toUniqueString = function() {
        var s = oldQCToUniqueString.call(this);
        s += '|JoinSQLs:';
        for(var i = 0; i < this._additionalJoinSqls.length; i++) {
            s += this._additionalJoinSqls[i];
        }
        s += '|WhereSQLs:';
        for(var i = 0; i < this._additionalWhereSqls.length; i++) {
            s += this._additionalWhereSqls[i];
        }
        s += '|GroupSQLs:';
        for(var i = 0; i < this._additionalGroupSqls.length; i++) {
            s += this._additionalGroupSqls[i];
        }
        if(this._manyToManyFetch) {
            s += '|ManyToManyFetch:';
            s += JSON.stringify(this._manyToManyFetch); // TODO: Do something more efficient
        }
        return s;
    };

    persistence.NullFilter.prototype.sql = function (meta, alias, values) {
        return "1=1";
    };

    persistence.AndFilter.prototype.sql = function (meta, alias, values) {
        return "(" + this.left.sql(meta, alias, values) + " AND "
            + this.right.sql(meta, alias, values) + ")";
    };

    persistence.OrFilter.prototype.sql = function (meta, alias, values) {
        return "(" + this.left.sql(meta, alias, values) + " OR "
            + this.right.sql(meta, alias, values) + ")";
    };

    persistence.PropertyFilter.prototype.sql = function (meta, alias, values) {
        var tm = persistence.typeMapper;
        var aliasPrefix = alias ? "`" + alias + "`." : "";
        var sqlType = meta.fields[this.property] || tm.idType;
        if (this.operator === '=' && this.value === null) {
            return aliasPrefix + '`' + this.property + "` IS NULL";
        } else if (this.operator === '!=' && this.value === null) {
            return aliasPrefix + '`' + this.property + "` IS NOT NULL";
        } else if (this.operator === 'in') {
            var vals = this.value;
            var qs = [];
            for(var i = 0; i < vals.length; i++) {
                qs.push('?');
                values.push(tm.entityValToDbVal(vals[i], sqlType));
            }
            if(vals.length === 0) {
                // Optimize this a little
                return "1 = 0";
            } else {
                return aliasPrefix + '`' + this.property + "` IN (" + qs.join(', ') + ")";
            }
        } else if (this.operator === 'not in') {
            var vals = this.value;
            var qs = [];
            for(var i = 0; i < vals.length; i++) {
                qs.push('?');
                values.push(tm.entityValToDbVal(vals[i], sqlType));
            }

            if(vals.length === 0) {
                // Optimize this a little
                return "1 = 1";
            } else {
                return aliasPrefix + '`' + this.property + "` NOT IN (" + qs.join(', ') + ")";
            }
        }
        //TODO push to github
        else if (this.operator === 'not in (?)') {
            var subquery = this.value;
            return aliasPrefix + '`' + this.property + "` NOT IN (" + subquery + ")";
        }
        //TODO push to github
        else if (this.operator === 'in (?)') {
            var subquery = this.value;
            return aliasPrefix + '`' + this.property + "` IN (" + subquery + ")";
        }
        else {
            var value = this.value;
            if(value === true || value === false) {
                value = value ? 1 : 0;
            }
            values.push(tm.entityValToDbVal(value, sqlType));
            return aliasPrefix + '`' + this.property + "` " + this.operator + " " + tm.outVar("?", sqlType);
        }
    };

    // QueryColleciton's list

    /**
     * Asynchronous call to actually fetch the items in the collection
     * @param tx transaction to use
     * @param callback function to be called taking an array with
     *   result objects as argument
     */
    persistence.DbQueryCollection.prototype.list = function (tx, callback) {
        var args = argspec.getArgs(arguments, [
            { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
            { name: 'callback', optional: false, check: argspec.isCallback() }
        ]);
        tx = args.tx;
        callback = args.callback;

        var that = this;
        var session = this._session;
        if(!tx) { // no transaction supplied
            session.transaction(function(tx) {
                that.list(tx, callback);
            });
            return;
        }
        var entityName = this._entityName;
        var meta = persistence.getMeta(entityName);
        var tm = persistence.typeMapper;

        // handles mixin case -- this logic is generic and could be in persistence.
        if (meta.isMixin) {
            var result = [];
            persistence.asyncForEach(meta.mixedIns, function(realMeta, next) {
                var query = that.clone();
                query._entityName = realMeta.name;
                query.list(tx, function(array) {
                    result = result.concat(array);
                    next();
                });
            }, function() {
                var query = new persistence.LocalQueryCollection(result);
                query._orderColumns = that._orderColumns;
                query._reverse = that._reverse;
                // TODO: handle skip and limit -- do we really want to do it?
                query.list(null, callback);
            });
            return;
        }

        function selectAll (meta, tableAlias, prefix) {
            var selectFields = [ tm.inIdVar("`" + tableAlias + "`.id") + " AS " + prefix + "id" ];

            for ( var p in meta.fields) {
                if (meta.fields.hasOwnProperty(p)) {
                    selectFields.push(tm.inVar("`" + tableAlias + "`.`" + p + "`", meta.fields[p]) + " AS `"
                        + prefix + p + "`");
                }
            }

            for ( var p in meta.hasOne) {

                if (meta.hasOne.hasOwnProperty(p)) {
                    //TODO push to github
                    var field = meta.hasOne[p].field || p;
                    selectFields.push(tm.inIdVar("`" + tableAlias + "`.`" + field + "`") + " AS `"
                        + prefix + p + "`");
                }
            }

            return selectFields;
        }
        var args = [];
        var mainPrefix = entityName + "_";

        var mainAlias = 'root';
        var selectFields = selectAll(meta, mainAlias, mainPrefix);

        var joinSql = '';
        var additionalWhereSqls = this._additionalWhereSqls.slice(0);
        var mtm = this._manyToManyFetch;
        if(mtm) {
            joinSql += "LEFT JOIN `" + mtm.table + "` AS mtm ON mtm.`" + mtm.inverseProp + "` = `root`.`id` ";
            additionalWhereSqls.push("mtm.`" + mtm.prop + "` = " + tm.outId(mtm.id));
        }

        joinSql += this._additionalJoinSqls.join(' ');

        for ( var i = 0; i < this._prefetchFields.length; i++) {
            var prefetchFieldParts = this._prefetchFields[i].split('.');
            var prefetchField = prefetchFieldParts[0];
            var eName = entityName;
            if(prefetchFieldParts.length > 1){
                prefetchField = prefetchFieldParts[1];
                eName = prefetchFieldParts[0];
            }
            var theMeta = persistence.getMeta(eName);
            var thisMeta = theMeta.hasOne[prefetchField].type.meta;
            if (thisMeta.isMixin)
                throw new Error("cannot prefetch a mixin");
            var tableAlias = thisMeta.name + '_' + prefetchField + "_tbl";
            var PrefetchFrom = mainAlias;
            if(prefetchFieldParts.length > 1){
                PrefetchFrom = eName + '_' + eName + "_tbl";;
            }
            selectFields = selectFields.concat(selectAll(thisMeta, tableAlias,
                prefetchField + "_"));
            joinSql += "LEFT JOIN `" + thisMeta.name + "` AS `" + tableAlias
                + "` ON `" + tableAlias + "`.`id` = `" + PrefetchFrom + '`.`' + prefetchField + "` ";

        }

        var whereSql = "WHERE "
            + [ this._filter.sql(meta, mainAlias, args) ].concat(additionalWhereSqls).join(' AND ');

        var sql = "SELECT " + selectFields.join(", ") + " FROM `" + entityName
            + "` AS `" + mainAlias + "` " + joinSql + " " + whereSql;

        if(this._additionalGroupSqls.length > 0) {
            sql += this._additionalGroupSqls.join(' ');
        }

        if(this._orderColumns.length > 0) {
            sql += " ORDER BY "
                + this._orderColumns.map(
                function (c) {
                    return (c[2] ? "`" : "LOWER(`") + mainPrefix + c[0] + (c[2] ? "` " : "`) ")
                        + (c[1] ? "ASC" : "DESC");
                }).join(", ");
        }
        if(this._limit >= 0) {
            sql += " LIMIT " + this._limit;
        }
        if(this._skip > 0) {
            sql += " OFFSET " + this._skip;
        }
        session.flush(tx, function () {
            tx.executeSql(sql, args, function (rows) {
                var results = [];
                if(that._reverse) {
                    rows.reverse();
                }
                for ( var i = 0; i < rows.length; i++) {
                    var r = rows[i];
                    var e = rowToEntity(session, entityName, r, mainPrefix);
                    for ( var j = 0; j < that._prefetchFields.length; j++) {

                        var prefetchFieldParts = that._prefetchFields[j].split('.');
                        var prefetchField = prefetchFieldParts[0];
                        var eName = entityName;
                        if(prefetchFieldParts.length > 1){
                            prefetchField = prefetchFieldParts[1];
                            eName = prefetchFieldParts[0];
                        }
                        var theMeta = persistence.getMeta(eName);
                        var thisMeta = theMeta.hasOne[prefetchField].type.meta;

                        e._data_obj[prefetchField] = rowToEntity(session, thisMeta.name, r, prefetchField + '_');
                        session.add(e._data_obj[prefetchField]);
                    }
                    results.push(e);
                    session.add(e);
                }
                callback(results);
                that.triggerEvent('list', that, results);
            });
        });
    };

    /**
     * Asynchronous call to remove all the items in the collection.
     * Note: does not only remove the items from the collection, but
     * the items themselves.
     * @param tx transaction to use
     * @param callback function to be called when clearing has completed
     */
    persistence.DbQueryCollection.prototype.destroyAll = function (tx, callback) {
        var args = argspec.getArgs(arguments, [
            { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
            { name: 'callback', optional: true, check: argspec.isCallback(), defaultValue: function(){} }
        ]);
        tx = args.tx;
        callback = args.callback;

        var that = this;
        var session = this._session;
        if(!tx) { // no transaction supplied
            session.transaction(function(tx) {
                that.destroyAll(tx, callback);
            });
            return;
        }
        var entityName = this._entityName;
        var meta = persistence.getMeta(entityName);
        var tm = persistence.typeMapper;

        // handles mixin case -- this logic is generic and could be in persistence.
        if (meta.isMixin) {
            persistence.asyncForEach(meta.mixedIns, function(realMeta, next) {
                var query = that.clone();
                query._entityName = realMeta.name;
                query.destroyAll(tx, callback);
            }, callback);
            return;
        }

        var joinSql = '';
        var additionalWhereSqls = this._additionalWhereSqls.slice(0);
        var mtm = this._manyToManyFetch;
        if(mtm) {
            joinSql += "LEFT JOIN `" + mtm.table + "` AS mtm ON mtm.`" + mtm.inverseProp + "` = `root`.`id` ";
            additionalWhereSqls.push("mtm.`" + mtm.prop + "` = " + tm.outId(mtm.id));
        }

        joinSql += this._additionalJoinSqls.join(' ');

        var args = [];
        var whereSql = "WHERE "
            + [ this._filter.sql(meta, null, args) ].concat(additionalWhereSqls).join(' AND ');

        var selectSql = "SELECT id FROM `" + entityName + "` " + joinSql + ' ' + whereSql;
        var deleteSql = "DELETE FROM `" + entityName + "` " + joinSql + ' ' + whereSql;
        var args2 = args.slice(0);

        session.flush(tx, function () {
            tx.executeSql(selectSql, args, function(results) {
                for(var i = 0; i < results.length; i++) {
                    delete session.trackedObjects[results[i].id];
                    session.objectsRemoved.push({id: results[i].id, entity: entityName});
                }
                that.triggerEvent('change', that);
                tx.executeSql(deleteSql, args2, callback, callback);
            }, callback);
        });
    };

    /**
     * Asynchronous call to count the number of items in the collection.
     * @param tx transaction to use
     * @param callback function to be called when clearing has completed
     */
    persistence.DbQueryCollection.prototype.count = function (tx, callback) {
        var args = argspec.getArgs(arguments, [
            { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
            { name: 'callback', optional: false, check: argspec.isCallback() }
        ]);
        tx = args.tx;
        callback = args.callback;

        var that = this;
        var session = this._session;
        if(tx && !tx.executeSql) { // provided callback as first argument
            callback = tx;
            tx = null;
        }
        if(!tx) { // no transaction supplied
            session.transaction(function(tx) {
                that.count(tx, callback);
            });
            return;
        }
        var entityName = this._entityName;
        var meta = persistence.getMeta(entityName);
        var tm = persistence.typeMapper;

        // handles mixin case -- this logic is generic and could be in persistence.
        if (meta.isMixin) {
            var result = 0;
            persistence.asyncForEach(meta.mixedIns, function(realMeta, next) {
                var query = that.clone();
                query._entityName = realMeta.name;
                query.count(tx, function(count) {
                    result += count;
                    next();
                });
            }, function() {
                callback(result);
            });
            return;
        }

        var joinSql = '';
        var additionalWhereSqls = this._additionalWhereSqls.slice(0);
        var mtm = this._manyToManyFetch;
        if(mtm) {
            joinSql += "LEFT JOIN `" + mtm.table + "` AS mtm ON mtm.`" + mtm.inverseProp + "` = `root`.`id` ";
            additionalWhereSqls.push("mtm.`" + mtm.prop + "` = " + tm.outId(mtm.id));
        }

        joinSql += this._additionalJoinSqls.join(' ');
        var args = [];
        var whereSql = "WHERE " + [ this._filter.sql(meta, "root", args) ].concat(additionalWhereSqls).join(' AND ');

        var sql = "SELECT COUNT(*) AS cnt FROM `" + entityName + "` AS `root` " + joinSql + " " + whereSql;

        session.flush(tx, function () {
            tx.executeSql(sql, args, function(results) {
                callback(parseInt(results[0].cnt, 10));
            });
        });
    };

    persistence.ManyToManyDbQueryCollection.prototype.persistQueries = function() {
        var queries = [];
        var meta = persistence.getMeta(this._obj._type);
        var inverseMeta = meta.hasMany[this._coll].type.meta;
        var tm = persistence.typeMapper;
        var rel = meta.hasMany[this._coll];
        var inv = inverseMeta.hasMany[rel.inverseProperty];
        var direct = rel.mixin ? rel.mixin.meta.name : meta.name;
        var inverse = inv.mixin ? inv.mixin.meta.name : inverseMeta.name;

        // Added
        for(var i = 0; i < this._localAdded.length; i++) {
            var columns = [direct + "_" + this._coll, inverse + '_' + rel.inverseProperty];
            var vars = [tm.outIdVar("?"), tm.outIdVar("?")];
            var args = [tm.entityIdToDbId(this._obj.id), tm.entityIdToDbId(this._localAdded[i].id)];
            if (rel.mixin) {
                columns.push(direct + "_" + this._coll + "_class");
                vars.push("?");
                args.push(meta.name);
            }
            if (inv.mixin) {
                columns.push(inverse + "_" + rel.inverseProperty + "_class");
                vars.push("?");
                args.push(inverseMeta.name);
            }
            queries.push(["INSERT INTO " + rel.tableName +
                " (`" + columns.join("`, `") + "`) VALUES (" + vars.join(",") + ")", args]);
        }
        this._localAdded = [];
        // Removed
        for(var i = 0; i < this._localRemoved.length; i++) {
            queries.push(["DELETE FROM  " + rel.tableName +
                " WHERE `" + direct + "_" + this._coll + "` = " + tm.outIdVar("?") + " AND `" +
                inverse + '_' + rel.inverseProperty +
                "` = " + tm.outIdVar("?"), [tm.entityIdToDbId(this._obj.id), tm.entityIdToDbId(this._localRemoved[i].id)]]);
        }
        this._localRemoved = [];
        return queries;
    };
};

if (typeof exports !== 'undefined') {
    exports.defaultTypeMapper = defaultTypeMapper;
    exports.config = config;
}
else {
    window = window || {};
    window.persistence = window.persistence || persistence || {};
    window.persistence.store = window.persistence.store || {};
    window.persistence.store.sql = {
        defaultTypeMapper: defaultTypeMapper,
        config: config
    };
}
;
try {
    if(!window) {
        window = {};
        //exports.console = console;
    }
} catch(e) {
    window = {};
    exports.console = console;
}

var persistence = (window && window.persistence) ? window.persistence : {};

if(!persistence.store) {
    persistence.store = {};
}

persistence.store.websql = {};


persistence.store.websql.config = function(persistence, dbname, description, size) {
    var conn = null;

    /**
     * Create a transaction
     *
     * @param callback,
     *            the callback function to be invoked when the transaction
     *            starts, taking the transaction object as argument
     */
    persistence.transaction = function (callback) {
        if(!conn) {
            throw new Error("No ongoing database connection, please connect first.");
        } else {
            conn.transaction(callback);
        }
    };

    ////////// Low-level database interface, abstracting from HTML5 and Gears databases \\\\
    persistence.db = persistence.db || {};

    persistence.db.implementation = "unsupported";
    persistence.db.conn = null;

    // window object does not exist on Qt Declarative UI (http://doc.trolltech.org/4.7-snapshot/declarativeui.html)
    if (window && window.openDatabase) {
        persistence.db.implementation = "html5";
    } else if (window && window.google && google.gears) {
        persistence.db.implementation = "gears";
    } else {
        try {
            if (openDatabaseSync) {
                // TODO: find a browser that implements openDatabaseSync and check out if
                //       it is attached to the window or some other object
                persistence.db.implementation = "html5-sync";
            }
        } catch(e) {
        }
    }

    persistence.db.html5 = {};

    persistence.db.html5.connect = function (dbname, description, size) {
        var that = {};
        var conn = openDatabase(dbname, '1.0', description, size);

        that.transaction = function (fn) {
            return conn.transaction(function (sqlt) {
                return fn(persistence.db.html5.transaction(sqlt));
            });
        };
        return that;
    };

    persistence.db.html5.transaction = function (t) {
        var that = {};
        that.executeSql = function (query, args, successFn, errorFn) {
            if(persistence.debug) {
                console.log(query, args);
            }

            t.executeSql(query, args, function (_, result) {
                if (successFn) {
                    var results = [];
                    for ( var i = 0; i < result.rows.length; i++) {
                        results.push(result.rows.item(i));
                    }
                    successFn(results);
                    if (persistence.debug) console.log('SUCCESS QUERY: ', query, args);
                }
            }, function() {
                if (persistence.debug) console.log('QUERY FAIL: ', query, args);
                if (errorFn) errorFn();
            });
        };
        return that;
    };

    persistence.db.html5Sync = {};

    persistence.db.html5Sync.connect = function (dbname, description, size) {
        var that = {};
        var conn = openDatabaseSync(dbname, '1.0', description, size);

        that.transaction = function (fn) {
            return conn.transaction(function (sqlt) {
                return fn(persistence.db.html5Sync.transaction(sqlt));
            });
        };
        return that;
    };

    persistence.db.html5Sync.transaction = function (t) {
        var that = {};
        that.executeSql = function (query, args, successFn, errorFn) {
            if (args == null) args = [];

            if(persistence.debug) {
                console.log(query, args);
            }

            var result = t.executeSql(query, args);
            if (result) {
                if (successFn) {
                    var results = [];
                    for ( var i = 0; i < result.rows.length; i++) {
                        results.push(result.rows.item(i));
                    }
                    successFn(results);
                }
            }
        };
        return that;
    };

    persistence.db.gears = {};

    persistence.db.gears.connect = function (dbname) {
        var that = {};
        var conn = google.gears.factory.create('beta.database');
        conn.open(dbname);

        that.transaction = function (fn) {
            fn(persistence.db.gears.transaction(conn));
        };
        return that;
    };

    persistence.db.gears.transaction = function (conn) {
        var that = {};
        that.executeSql = function (query, args, successFn, errorFn) {
            if(persistence.debug) {
                console.log(query, args);
            }
            var rs = conn.execute(query, args);
            if (successFn) {
                var results = [];
                while (rs.isValidRow()) {
                    var result = {};
                    for ( var i = 0; i < rs.fieldCount(); i++) {
                        result[rs.fieldName(i)] = rs.field(i);
                    }
                    results.push(result);
                    rs.next();
                }
                successFn(results);
            }
        };
        return that;
    };

    persistence.db.connect = function (dbname, description, size) {
        if (persistence.db.implementation == "html5") {
            return persistence.db.html5.connect(dbname, description, size);
        } else if (persistence.db.implementation == "html5-sync") {
            return persistence.db.html5Sync.connect(dbname, description, size);
        } else if (persistence.db.implementation == "gears") {
            return persistence.db.gears.connect(dbname);
        }
    };

    ///////////////////////// SQLite dialect

    persistence.store.websql.sqliteDialect = {
        // columns is an array of arrays, e.g.
        // [["id", "VARCHAR(32)", "PRIMARY KEY"], ["name", "TEXT"]]
        createTable: function(tableName, columns) {
            var tm = persistence.typeMapper;
            var sql = "CREATE TABLE IF NOT EXISTS `" + tableName + "` (";
            var defs = [];
            for(var i = 0; i < columns.length; i++) {
                var column = columns[i];
                defs.push("`" + column[0] + "` " + tm.columnType(column[1]) + (column[2] ? " " + column[2] : ""));
            }
            sql += defs.join(", ");
            sql += ')';
            return sql;
        },

        // columns is array of column names, e.g.
        // ["id"]
        createIndex: function(tableName, columns, options) {
            options = options || {};
            return "CREATE "+(options.unique?"UNIQUE ":"")+"INDEX IF NOT EXISTS `" + tableName + "__" + columns.join("_") +
                "` ON `" + tableName + "` (" +
                columns.map(function(col) { return "`" + col + "`"; }).join(", ") + ")";
        }
    };

    // Configure persistence for generic sql persistence, using sqliteDialect
    persistence.store.sql.config(persistence, persistence.store.websql.sqliteDialect);

    // Make the connection
    conn = persistence.db.connect(dbname, description, size);
    if(!conn) {
        throw new Error("No supported database found in this browser.");
    }
};

try {
    exports.persistence = persistence;
} catch(e) {}
;
try {
    if (!window) {
        window = {};
        //exports.console = console;
    }
} catch (e) {
    window = {};
    exports.console = console;
}

var persistence = (window && window.persistence) ? window.persistence : {};

if (!persistence.store) {
    persistence.store = {};
}

persistence.store.cordovasql = {};

/**
 * Configure the database connection (either sqliteplugin or websql)
 *
 * @param persistence
 * @param dbname
 * @param dbversion
 * @param description
 * @param size
 * @param backgroundProcessing
 */
persistence.store.cordovasql.config = function (persistence, dbname, dbversion, description, size, backgroundProcessing) {
    var conn = null;

    /**
     * Create a transaction
     *
     * @param callback
     *            the callback function to be invoked when the transaction
     *            starts, taking the transaction object as argument
     */
    persistence.transaction = function (callback) {
        if (!conn) {
            throw new Error("No ongoing database connection, please connect first.");
        } else {
            conn.transaction(callback);
        }
    };

    persistence.db = persistence.db || {};
    persistence.db.implementation = "unsupported";
    persistence.db.conn = null;

    /* Find out if sqliteplugin is loaded. Otherwise, we'll fall back to WebSql */
    if (window && 'sqlitePlugin' in window) {
        persistence.db.implementation = 'sqliteplugin';
    } else if (window && window.openDatabase) {
        persistence.db.implementation = "websql";
    } else {
        // Well, we are stuck!
    }

    /*
     * Cordova SqlitePlugin
     */
    persistence.db.sqliteplugin = {};

    /**
     * Connect to Sqlite plugin database
     *
     * @param dbname
     * @param backgroundProcessing
     * @returns {{}}
     */
    persistence.db.sqliteplugin.connect = function (dbname, backgroundProcessing) {
        var that = {};
        var conn = window.sqlitePlugin.openDatabase({name: dbname, bgType: backgroundProcessing});

        that.transaction = function (fn) {
            return conn.transaction(function (sqlt) {
                return fn(persistence.db.websql.transaction(sqlt));
            });
        };
        return that;
    };

    /**
     * Run transaction on Sqlite plugin database
     *
     * @param t
     * @returns {{}}
     */
    persistence.db.sqliteplugin.transaction = function (t) {
        var that = {};
        that.executeSql = function (query, args, successFn, errorFn) {
            if (persistence.debug) {
                console.log(query, args);
            }
            t.executeSql(query, args, function (_, result) {
                if (successFn) {
                    var results = [];
                    for (var i = 0; i < result.rows.length; i++) {
                        results.push(result.rows.item(i));
                    }
                    successFn(results);
                }
            }, errorFn);
        };
        return that;
    };

    /*
     * WebSQL
     */
    persistence.db.websql = {};

    /**
     * Connect to the default WebSQL database
     *
     * @param dbname
     * @param dbversion
     * @param description
     * @param size
     * @returns {{}}
     */
    persistence.db.websql.connect = function (dbname, dbversion, description, size) {
        var that = {};
        var conn = openDatabase(dbname, dbversion, description, size);

        that.transaction = function (fn) {
            return conn.transaction(function (sqlt) {
                return fn(persistence.db.websql.transaction(sqlt));
            });
        };
        return that;
    };

    /**
     * Run transaction on WebSQL database
     *
     * @param t
     * @returns {{}}
     */
    persistence.db.websql.transaction = function (t) {
        var that = {};
        that.executeSql = function (query, args, successFn, errorFn) {
            if (persistence.debug) {
                console.log(query, args);
            }
            t.executeSql(query, args, function (_, result) {
                if (successFn) {
                    var results = [];
                    for (var i = 0; i < result.rows.length; i++) {
                        results.push(result.rows.item(i));
                    }
                    successFn(results);
                }
            }, errorFn);
        };
        return that;
    };

    /**
     * Connect() wrapper
     *
     * @param dbname
     * @param dbversion
     * @param description
     * @param size
     * @param backgroundProcessing
     * @returns {*}
     */
    persistence.db.connect = function (dbname, dbversion, description, size, backgroundProcessing) {
        if (persistence.db.implementation == "sqliteplugin") {
            return persistence.db.sqliteplugin.connect(dbname, backgroundProcessing);
        } else if (persistence.db.implementation == "websql") {
            return persistence.db.websql.connect(dbname, dbversion, description, size);
        }

        return null;
    };

    /**
     * Set the sqlite dialect
     *
     * @type {{createTable: createTable, createIndex: createIndex}}
     */
    persistence.store.cordovasql.sqliteDialect = {

        /**
         * columns is an array of arrays, e.g. [["id", "VARCHAR(32)", "PRIMARY KEY"], ["name", "TEXT"]]
         *
         * @param tableName
         * @param columns
         * @returns {string}
         */
        createTable: function (tableName, columns) {
            var tm = persistence.typeMapper;
            var sql = "CREATE TABLE IF NOT EXISTS `" + tableName + "` (";
            var defs = [];
            for (var i = 0; i < columns.length; i++) {
                var column = columns[i];
                defs.push("`" + column[0] + "` " + tm.columnType(column[1]) + (column[2] ? " " + column[2] : ""));
            }
            sql += defs.join(", ");
            sql += ')';
            return sql;
        },

        /**
         * columns is array of column names, e.g. ["id"]
         * @param tableName
         * @param columns
         * @param options
         * @returns {string}
         */
        createIndex: function (tableName, columns, options) {
            options = options || {};
            return "CREATE " + (options.unique ? "UNIQUE " : "") + "INDEX IF NOT EXISTS `" + tableName + "__" + columns.join("_") +
                "` ON `" + tableName + "` (" +
                columns.map(function (col) {
                    return "`" + col + "`";
                }).join(", ") + ")";
        }
    };

    // Configure persistence for generic sql persistence, using sqliteDialect
    persistence.store.sql.config(persistence, persistence.store.cordovasql.sqliteDialect);

    // Make the connection
    conn = persistence.db.connect(dbname, dbversion, description, size, backgroundProcessing);
    if (!conn) {
        throw new Error("No supported database found in this browser.");
    }
};

try {
    exports.persistence = persistence;
} catch (e) {
}
;
/**
 * @license
 * Copyright (c) 2010 Fábio Rehm <fgrehm@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


if(!window.persistence) { // persistence.js not loaded!
    throw new Error("persistence.js should be loaded before persistence.migrations.js");
}

(function() {

    var Migrator = {
        migrations: [],

        version: function(callback) {
            persistence.transaction(function(t){
                t.executeSql('SELECT current_version FROM schema_version', null, function(result){
                    if (result.length == 0) {
                        t.executeSql('INSERT INTO schema_version VALUES (0)', null, function(){
                            callback(0);
                        });
                    } else {
                        callback(result[0].current_version);
                    }
                });
            });
        },

        setVersion: function(v, callback) {
            persistence.transaction(function(t){
                t.executeSql('UPDATE schema_version SET current_version = ?', [v], function(){
                    Migrator._version = v;
                    if (callback) callback();
                });
            });
        },

        setup: function(callback) {
            persistence.transaction(function(t){
                t.executeSql('CREATE TABLE IF NOT EXISTS schema_version (current_version INTEGER)', null, function(){
                    // Creates a dummy migration just to force setting schema version when cleaning DB
                    Migrator.migration(0, { up: function() { }, down: function() { } });
                    if (callback) callback();
                });
            });
        },

        // Method should only be used for testing
        reset: function(callback) {
            // Creates a dummy migration just to force setting schema version when cleaning DB
            Migrator.migrations = [];
            Migrator.migration(0, { up: function() { }, down: function() { } });
            Migrator.setVersion(0, callback);
        },

        migration: function(version, actions) {
            Migrator.migrations[version] = new Migration(version, actions);
            return Migrator.migrations[version];
        },

        migrateUpTo: function(version, callback) {
            var migrationsToRun = [];

            function migrateOne() {
                var migration = migrationsToRun.pop();

                if (!migration) callback();

                migration.up(function(){
                    if (migrationsToRun.length > 0) {
                        migrateOne();
                    } else if (callback) {
                        callback();
                    }
                });
            }

            this.version(function(currentVersion){
                for (var v = currentVersion+1; v <= version; v++)
                    migrationsToRun.unshift(Migrator.migrations[v]);

                if (migrationsToRun.length > 0) {
                    migrateOne();
                } else if (callback) {
                    callback();
                }
            });
        },

        migrateDownTo: function(version, callback) {
            var migrationsToRun = [];

            function migrateOne() {
                var migration = migrationsToRun.pop();

                if (!migration) callback();

                migration.down(function(){
                    if (migrationsToRun.length > 0) {
                        migrateOne();
                    } else if (callback) {
                        callback();
                    }
                });
            }

            this.version(function(currentVersion){
                for (var v = currentVersion; v > version; v--)
                    migrationsToRun.unshift(Migrator.migrations[v]);

                if (migrationsToRun.length > 0) {
                    migrateOne();
                } else if (callback) {
                    callback();
                }
            });
        },

        migrate: function(version, callback) {
            if ( arguments.length === 1 ) {
                callback = version;
                version = this.migrations.length-1;
            }

            this.version(function(curVersion){
                if (curVersion < version)
                    Migrator.migrateUpTo(version, callback);
                else if (curVersion > version)
                    Migrator.migrateDownTo(version, callback);
                else
                    callback();
            });
        }
    }

    var Migration = function(version, body) {
        this.version = version;
        // TODO check if actions contains up and down methods
        this.body = body;
        this.actions = [];
    };

    Migration.prototype.executeActions = function(callback, customVersion) {
        var actionsToRun = this.actions;
        var version = (customVersion!==undefined) ? customVersion : this.version;

        persistence.transaction(function(tx){
            function nextAction() {
                if (actionsToRun.length == 0)
                    Migrator.setVersion(version, callback);
                else {
                    var action = actionsToRun.pop();
                    action(tx, nextAction);
                }
            }

            nextAction();
        });
    }

    Migration.prototype.up = function(callback) {
        if (this.body.up) this.body.up.apply(this);
        this.executeActions(callback);
    }

    Migration.prototype.down = function(callback) {
        if (this.body.down) this.body.down.apply(this);
        this.executeActions(callback, this.version-1);
    }

    Migration.prototype.createTable = function(tableName, callback) {
        var table = new ColumnsHelper();

        if (callback) callback(table);

        var column;
        var sql = 'CREATE TABLE ' + tableName + ' (id VARCHAR(32) PRIMARY KEY';
        while (column = table.columns.pop())
            sql += ', ' + column;

        this.executeSql(sql + ')');
    }

    Migration.prototype.dropTable = function(tableName) {
        var sql = 'DROP TABLE ' + tableName;
        this.executeSql(sql);
    }

    Migration.prototype.addColumn = function(tableName, columnName, columnType) {
        var sql = 'ALTER TABLE ' + tableName + ' ADD ' + columnName + ' ' + columnType;
        this.executeSql(sql);
    }

    Migration.prototype.removeColumn = function(tableName, columnName) {
        this.action(function(tx, nextCommand){
            var sql = 'select sql from sqlite_master where type = "table" and name == "'+tableName+'"';
            tx.executeSql(sql, null, function(result){
                var columns = new RegExp("CREATE TABLE `\\w+` |\\w+ \\((.+)\\)").exec(result[0].sql)[1].split(', ');
                var selectColumns = [];
                var columnsSql = [];

                for (var i = 0; i < columns.length; i++) {
                    var colName = new RegExp("((`\\w+`)|(\\w+)) .+").exec(columns[i])[1];
                    if (colName == columnName) continue;

                    columnsSql.push(columns[i]);
                    selectColumns.push(colName);
                }
                columnsSql = columnsSql.join(', ');
                selectColumns = selectColumns.join(', ');

                var queries = [];
                queries.unshift(["ALTER TABLE " + tableName + " RENAME TO " + tableName + "_bkp;", null]);
                queries.unshift(["CREATE TABLE " + tableName + " (" + columnsSql + ");", null]);
                queries.unshift(["INSERT INTO " + tableName + " SELECT " + selectColumns + " FROM " + tableName + "_bkp;", null]);
                queries.unshift(["DROP TABLE " + tableName + "_bkp;", null]);

                persistence.executeQueriesSeq(tx, queries, nextCommand);
            });
        });
    }

    Migration.prototype.addIndex = function(tableName, columnName, unique) {
        var sql = 'CREATE ' + (unique === true ? 'UNIQUE' : '') + ' INDEX ' + tableName + '_' + columnName + ' ON ' + tableName + ' (' + columnName + ')';
        this.executeSql(sql);
    }

    Migration.prototype.removeIndex = function(tableName, columnName) {
        var sql = 'DROP INDEX ' + tableName + '_' + columnName;
        this.executeSql(sql);
    }

    Migration.prototype.executeSql = function(sql, args) {
        this.action(function(tx, nextCommand){
            tx.executeSql(sql, args, nextCommand);
        });
    }

    Migration.prototype.action = function(callback) {
        this.actions.unshift(callback);
    }

    var ColumnsHelper = function() {
        this.columns = [];
    }

    ColumnsHelper.prototype.text = function(columnName) {
        this.columns.unshift(columnName + ' TEXT');
    }

    ColumnsHelper.prototype.integer = function(columnName) {
        this.columns.unshift(columnName + ' INT');
    }

    ColumnsHelper.prototype.real = function(columnName) {
        this.columns.unshift(columnName + ' REAL');
    }

    ColumnsHelper.prototype['boolean'] = function(columnName) {
        this.columns.unshift(columnName + ' BOOL');
    }

    ColumnsHelper.prototype.date = function(columnName) {
        this.columns.unshift(columnName + ' DATE');
    }

    ColumnsHelper.prototype.json = function(columnName) {
        this.columns.unshift(columnName + ' TEXT');
    }

    // Makes Migrator and Migration available to tests
    persistence.migrations = {};
    persistence.migrations.Migrator = Migrator;
    persistence.migrations.Migration = Migration;
    persistence.migrations.init = function() { Migrator.setup.apply(Migrator, Array.prototype.slice.call(arguments, 0))};

    persistence.migrate = function() { Migrator.migrate.apply(Migrator, Array.prototype.slice.call(arguments, 0))};
    persistence.defineMigration = function() { Migrator.migration.apply(Migrator, Array.prototype.slice.call(arguments, 0))};

}());
/*
 Masked Input plugin for jQuery
 Copyright (c) 2007-2013 Josh Bush (digitalbush.com)
 Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
 Version: 1.3.1
 */

(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);




















