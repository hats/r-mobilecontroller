require "rvm/capistrano"

set :rvm_ruby_string, :local # use the same ruby as used locally for deployment
set :rvm_autolibs_flag, "read-only" # more info: rvm help autolibs

before 'deploy:setup', 'rvm:install_rvm' # install RVM
before 'deploy:setup', 'rvm:install_ruby'

set :stages, %w(development production)
set :default_stage, 'development'

require 'capistrano/ext/multistage'

set(:unicorn_env) { rails_env }

set :application, 'MobileController'
set :repository, 'git@bitbucket.org:revoltion/mobilecontroller.git'
set :deploy_via, :remote_cache


set :user, 'deploy'
set :use_sudo, false
set :project_path, "/var/www/mobilecontroller/"
set (:deploy_to) { "/var/www/mobilecontroller/#{stage_name}/" }
set (:ts) {
  d = DateTime.now
  d.strftime("%Y%m%d%H%M%S")
}

set :whenever_command, 'rvm use 2.0.0-p247 do bundle exec whenever'

set :scm, :git
set :ssh_options, {:forward_agent => true}

set :keep_releases, 5
after "deploy:restart", "deploy:cleanup"

require 'bundler/capistrano'
                             #require "thinking_sphinx/deploy/capistrano"
                             #require "whenever/capistrano"

before 'deploy:finalize_update', :copy_database_config #, :copy_email_config

task :copy_database_config, roles => :app do
  db_config = "#{shared_path}/database.yml"
  run "cp #{db_config} #{release_path}/config/database.yml"
end

#task :copy_email_config, roles => :app do
#  mail_config = "#{shared_path}/email.yml"
#  run "cp #{mail_config} #{release_path}/config/email.yml"
#end

set (:unicorn_conf) { "./config/unicorn/#{stage_name}.rb" }
set (:unicorn_pid) { "#{shared_path}/pids/unicorn.pid" }
set (:unicorn_start_cmd) { "(cd #{deploy_to}current; rvm use 2.0.0-p247 do bundle exec unicorn_rails -Dc #{unicorn_conf} -E #{unicorn_env})" }

set :bundle_cmd, 'rvm use 2.0.0-p247 do bundle'


# - for unicorn - #
namespace :deploy do
  desc 'Start application'
  task :start, :roles => :app do
    run unicorn_start_cmd
  end

  desc "reload the database with seed data"
  task :seed do
    run "cd #{current_path}; RAILS_ENV=#{rails_env} bundle exec rake db:seed"
  end

  desc "drop the database"
  task :db_drop do
    run "cd #{current_path}; RAILS_ENV=#{rails_env} bundle exec rake db:drop"
  end

  desc "create the database"
  task :db_create do
    run "cd #{current_path}; RAILS_ENV=#{rails_env} bundle exec rake db:create"
  end

  desc "show routes"
  task :routes do
    run "cd #{current_path}/public; bundle exec rake routes RAILS_ENV=#{rails_env}"
  end

  desc 'Stop application'
  task :stop, :roles => :app do
    run "[ -f #{unicorn_pid} ] && kill -QUIT `cat #{unicorn_pid}`"
  end

  desc 'Restart Application'
  task :restart, :roles => :app do
    run "[ -f #{unicorn_pid} ] && kill -USR2 `cat #{unicorn_pid}` || #{unicorn_start_cmd}"
  end

  desc 'Refresh images'
end


set :max_asset_age, 2 ## Set asset age in minutes to test modified date against.

namespace :deploy do
  namespace :assets do

    desc 'Figure out modified assets.'
    task :determine_modified_assets, :roles => assets_role, :except => {:no_release => true} do
      set :updated_assets, capture("find #{latest_release}/app/assets -type d -name .git -prune -o -mmin -#{max_asset_age} -type f -print", :except => {:no_release => true}).split
    end

    desc 'Remove callback for asset precompiling unless assets were updated in most recent git commit.'
    task :conditionally_precompile, :roles => assets_role, :except => {:no_release => true} do
      if (updated_assets.empty?)
        callback = callbacks[:after].find { |c| c.source == 'deploy:assets:precompile' }
        callbacks[:after].delete(callback)
        logger.info('Skipping asset precompiling, no updated assets.')
      else
        logger.info("#{updated_assets.length} updated assets. Will precompile.")
      end
    end
  end

  namespace :paperclip do
    desc "rebuild images"
    task :reload, :roles => :app do
      run "cd #{deploy_to}current; RAILS_ENV=#{rails_env} bundle exec rake paperclip:refresh CLASS=Good"
      run "cd #{deploy_to}current; RAILS_ENV=#{rails_env} bundle exec rake paperclip:refresh CLASS=Employee"
    end
  end

  namespace :xls do
    desc "load xls"
    task :auditions, :roles => :app do
      run "cd #{deploy_to}current; RAILS_ENV=#{rails_env} bundle exec rake xls:auditions"
    end

    namespace :msk do
      task :tpc, :roles => :app do
        run "cd #{deploy_to}current; RAILS_ENV=#{rails_env} bundle exec rake xls:msk:tpc"
      end
    end

    namespace :vld do
      task :tpc, :roles => :app do
        run "cd #{deploy_to}current; RAILS_ENV=#{rails_env} bundle exec rake xls:vld:tpc"
      end
    end

    namespace :cf do
      task :kvi_bb, :roles => :app do
        run "cd #{deploy_to}current; RAILS_ENV=#{rails_env} bundle exec rake xls:cf:kvi_bb"
      end
    end

    namespace :szf do
      task :kvi_bb, :roles => :app do
        run "cd #{deploy_to}current; RAILS_ENV=#{rails_env} bundle exec rake xls:szf:kvi_bb"
      end

      task :tpc, :roles => :app do
        run "cd #{deploy_to}current; RAILS_ENV=#{rails_env} bundle exec rake xls:szf:tpc"
      end
    end
  end

  namespace :goods do
    task :available, :roles => :app do
      run "cd #{deploy_to}current; RAILS_ENV=#{rails_env} bundle exec rake goods:available"
    end

    task :measures, :roles => :app do
      run "cd #{deploy_to}current; RAILS_ENV=#{rails_env} bundle exec rake goods:measures"
    end
  end

end

after 'deploy:finalize_update', 'deploy:assets:determine_modified_assets', 'deploy:assets:conditionally_precompile'