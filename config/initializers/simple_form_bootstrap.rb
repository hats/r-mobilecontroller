inputs = %w[
  CollectionSelectInput
  DateTimeInput
  FileInput
  GroupedCollectionSelectInput
  NumericInput
  PasswordInput
  RangeInput
  StringInput
  HtmlInput
  TextInput
]

my_inputs = %w(HtmlInput DatePickerInput)

(inputs + my_inputs).each do |input_type|
  if my_inputs.include?(input_type)
    superclass = input_type.constantize
  else
    superclass = "SimpleForm::Inputs::#{input_type}".constantize
  end

  new_class = Class.new(superclass) do
    def input_html_classes
      super.push('form-control')
    end
  end

  Object.const_set(input_type, new_class)
end

# Use this setup block to configure all options available in SimpleForm.
SimpleForm.setup do |config|

  config.wrappers :checkbox, tag: :div, class: "checkbox", error_class: "has-error" do |b|

    # Form extensions
    b.use :html5

    # Form components
    b.wrapper tag: :label do |ba|
      ba.use :input
      ba.use :label_text
    end

    b.use :hint,  wrap_with: { tag: :p, class: "help-block" }
    b.use :error, wrap_with: { tag: :span, class: "help-block text-danger" }
  end

  config.wrappers :bootstrap, tag: 'div',
                  class: 'form-group',
                  error_class: 'has-error',
                  defaults: { input_html: { class: 'default_class' } } do |b|

    #config.input_class  = "form-control"
    config.label_class  = "col-sm-2 control-label"
    config.button_class = "btn btn-default"

    b.use :html5

    b.use :min_max
    b.use :maxlength
    b.use :placeholder
    b.optional :pattern
    b.optional :readonly

    b.use :label
    b.wrapper tag: 'div', class: 'col-sm-10' do |input|
      input.use :input
      input.use :hint,  wrap_with: { tag: :div, class: 'help-block' }
      input.use :error, wrap_with: { tag: 'span', class: 'help-block' }
    end
  end

  config.wrappers :group, tag: 'div', class: "form-group", error_class: 'error' do |b|
    b.use :html5
    b.use :placeholder
    b.use :label
    b.use :input, wrap_with: { class: "input-group" }
    b.use :hint,  wrap_with: { tag: 'span', class: 'help-block' }
    b.use :error, wrap_with: { tag: 'span', class: 'help-inline' }
  end

  config.wrappers :prepend, tag: 'div', class: "control-group", error_class: 'error' do |b|
    b.use :html5
    b.use :placeholder
    b.use :label
    b.wrapper tag: 'div', class: 'controls' do |input|
      input.wrapper tag: 'div', class: 'input-prepend' do |prepend|
        prepend.use :input
      end
      input.use :hint,  wrap_with: { tag: 'span', class: 'help-block' }
      input.use :error, wrap_with: { tag: 'span', class: 'help-inline' }
    end
  end

  config.wrappers :append, tag: 'div', class: "control-group", error_class: 'error' do |b|
    b.use :html5
    b.use :placeholder
    b.use :label
    b.wrapper tag: 'div', class: 'controls' do |input|
      input.wrapper tag: 'div', class: 'input-append' do |append|
        append.use :input
      end
      input.use :hint,  wrap_with: { tag: 'span', class: 'help-block' }
      input.use :error, wrap_with: { tag: 'span', class: 'help-inline' }
    end
  end

  config.wrappers :checkbox, tag: :div, class: 'checkbox', error_class: 'has-error' do |b|

    # Form extensions
    b.use :html5

    # Form components
    b.wrapper tag: :label do |ba|
      ba.use :input, class: nil
      ba.use :label_text
    end

    b.use :hint,  wrap_with: { tag: :p, class: 'help-block' }
    b.use :error, wrap_with: { tag: :span, class: 'help-block text-danger' }
  end

  # Wrappers for forms and inputs using the Twitter Bootstrap toolkit.
  # Check the Bootstrap docs (http://twitter.github.com/bootstrap)
  # to learn about the different styles for forms and inputs,
  # buttons and other elements.
  config.default_wrapper = :bootstrap
end