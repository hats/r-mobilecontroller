role :web, 'mobilecontroller.ru' # Your HTTP server, Apache/etc
role :app, 'mobilecontroller.ru' # This may be the same as your `Web` server
role :db,  'mobilecontroller.ru', :primary => true        # This is where Rails migrations will run

set :branch, 			      'master'
set :repository_branch, 'master'
set :stage_name, 		'development'
set :unicorn_env,   'development'
set :rails_env, 		'development'