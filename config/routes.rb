MobileController::Application.routes.draw do
  mount RedactorRails::Engine => 'redactor_rails'

  scope :admin do
    devise_for :admin,
               path: '',
               path_names: {
                   sign_in: 'login',
                   sign_out: 'logout'
               },
               controllers: {
                   sessions: 'auth/sessions'
               },
               class_name: 'User::Admin'
  end

  namespace :admin do
    authenticated :admin do
      root to: 'welcome#index'

      resources :clients do
        member do
          get :projects
        end
      end

      resources :tasks do
        resources :steps, only: [:new, :create] do
          post :sort, on: :collection
        end

        resources :fields, except: :all do
          resources :variants do
            post :sort, on: :collection
          end
        end
      end

      resources :steps do
        resources :blocks, only: [:new, :create] do
          post :sort, on: :collection
        end
      end

      resources :blocks do
        resources :fields, only: [:new, :create] do
          post :sort, on: :collection
        end
      end

      resources :fields do
        resources :options, only: [:new, :create] do
          post :sort, on: :collection
        end
      end

      resources :operators, :audits, :admins, :task_types, except: [:show]

      resources :projects, except: [:show] do
        resources :targets, except: [:show] do
          get :batch, on: :collection
          post :batch, on: :collection, action: :batch_update
        end

        resources :tasks, only: [:index, :new, :create]
      end
    end

    unauthenticated :admin do
      root to: redirect('admin/login'), as: :noauth
      match '*not_found', to: redirect('admin/login'), via: :all
    end
  end

  scope :client do
    devise_for :client,
               path: '',
               path_names: {
                   sign_in: 'login',
                   sign_out: 'logout'
               },
               controllers: {
                   sessions: 'auth/sessions'
               },
               class_name: 'User::Client'
  end

  namespace :client do
    authenticated :client do
      root to: redirect('client/projects')
      resources :projects, only: [:index, :show]
      resources :archives, path: :archive, only: [:index]
      resources :comings, path: :coming, only: [:index]
      resources :feedbacks, path: :feedback, except: [:edit, :update] do
        resources :messages, only: [:create, :destroy]
      end

    end

    unauthenticated :client do
      root to: redirect('client/login'), as: :noauth
      match '*not_found', to: redirect('client/login'), via: :all
    end
  end

  scope :operator do
    devise_for :operator,
               path: '',
               path_names: {
                   sign_in: 'login',
                   sign_out: 'logout'
               },
               controllers: {
                   sessions: 'auth/sessions'
               },
               class_name: 'User::Operator'
  end

  namespace :operator do
    authenticated :operator do
      root to: 'welcome#index'

      resources :tasks, only: [:index, :show, :update]
      resources :feedbacks, path: :feedback, only: [:index, :show] do
        patch '', action: :accept, on: :member
        resources :messages, only: [:create, :destroy]
      end
    end

    unauthenticated :operator do
      root to: redirect('operator/login'), as: :noauth
      match '*not_found', to: redirect('operator/login'), via: :all
    end
  end

  scope :agent do
    devise_for :agent,
               path: 'profile',
               skip: :all,
               class_name: 'User::Audit'
  end

  namespace :api do

    root to: 'welcome#index'
    post :user, to: 'user#login'

    #authenticated :agent do
      delete :user, to: 'user#logout'
      #post :missions, to: 'missions#empty'

      post :sync, to: 'sync#sync'
      post :upload, to: 'upload#recieve'

      resources :missions, only: [:update]

      resources :user_tasks, path: :tasks, only: [:destroy, :update]
      resource :geolocation
      resource :map
    #end

    #unauthenticated :agent do
    #  root to: redirect('agent/profile/login'), as: :noauth
    #  match '*not_found', to: redirect('agent/profile/login'), via: :all
    #end
  end


  namespace :agent do
    root to: 'welcome#index'


    # registration and confirmation
    authenticated :agent do
      get 'dashboard', to: 'dashboard#show'

      resources :user_tasks, path: :tasks
      resources :feedbacks, path: :feedback, except: [:edit, :update] do
        resources :messages, only: [:create, :destroy]
      end

      resource :profile, except: :all, controller: :profile, as: :profile do
        get :edit, action: :edit, on: :member
        post :edit, to: :update
      end

      get :logout, as: :logout, to: 'profile#logout'
    end

    scope :profile, controller: :profile do
      get :login
      post :login, action: :do_login

      get :recover
      post :recover, action: :do_recover

      get :confirm
      post :confirm, action: :do_confirm

      get :change
      post :change, action: :do_change

      get :register
      post :register, action: :do_register
    end
  end

#unauthenticated do
#  root to: redirect('login'), as: :noauth
#  match '*not_found', to: redirect('login'), via: :all
#end

  root to: 'welcome#index'
end
