#TODO refactor all the class to get optimised queries!!!!
#TODO get all tasks from active projects!
#TODO project end date!

class TaskAggregator
  def initialize(user)
    @user = user
    @user_tasks = false
  end

  def missions
    targeted + important + normal
  end

  def targeted(coords=nil)
    coords = @user.coords unless coords.present?
    coords = [55.7500, 37.6167] unless coords.present?

    Target.nearest(coords, 100).active.includes(:tasks).map { |target| target.tasks.map { |task| Mission.new(@user, task, target) } }.flatten
  end

  def important
    #Task.active.important.unaccepted.without_targets.includes(:project).map{ |task| Mission.new(@user, task)}
    Task.active.important.without_targets.includes(:project).map{ |task| Mission.new(@user, task)}
  end

  def normal
    #Task.active.normal.unaccepted.without_targets.includes(:project).map{ |task| Mission.new(@user, task)}
    Task.active.normal.without_targets.includes(:project).map{ |task| Mission.new(@user, task)}
  end

  def all
    user_tasks
  end

  # текущие задания
  def current
    user_tasks.current
  end

  # завершенные задания
  def done
    user_tasks.done
  end

  def approved
    user_tasks.approved
  end

  private
  def missionify(array)
    array.map { |user_task| user_task.missionify }
  end

  def user_tasks
    @user_tasks = @user.user_tasks if @user_tasks == false
    @user_tasks
  end
end