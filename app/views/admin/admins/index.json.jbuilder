json.array!(@users) do |user|
  json.extract! user, :id
  json.url admin_client_url(user, format: :json)
end
