json.extract! @user, :rating, :balance, :email, :full_name
json.phone @user.phone(true)
json.done @user.done_tasks
json.approved @user.approved_tasks