json.extract! @user, :full_name, :phone, :email, :rating, :balance, :latitude, :longitude
json.done @user.done_tasks
json.approved @user.approved_tasks
json.auth_token @user.authentication_token