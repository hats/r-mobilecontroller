json.center @audit.coords.reverse!
json.zoom 10.to_s

json.location do
    json.geometry do
      json.type 'Point'
      json.coordinates @audit.coords.reverse!
    end
end

json.geoObjects do
    json.array!(@aggregator.targeted) do |mission|
        json.geometry do
          json.type 'Point'
          json.coordinates mission.coords.reverse!
        end

        json.properties do
          json.extract! mission, :id, :name, :address, :rating, :price, :description
          json.baloonLayout 'targetBaloon'
        end
    end
end
