json.current do
    json.array!(@aggregator.current) do |user_task|
        json.extract! user_task, :id, :name, :address, :time_to_complete, :time_left
        json.price user_task.penalty_price
        json.is_current true
        json.type 'sensus'
        json.rating user_task.penalty_rating
    end
end

json.approved do
    json.array!(@aggregator.approved) do |user_task|
        json.extract! user_task, :id, :name, :address, :time_to_complete, :time_left
        json.is_approved true
        json.price user_task.penalty_price
        json.rating user_task.penalty_rating
    end
end

json.done do
    json.array!(@aggregator.done) do |user_task|
        json.extract! user_task, :id, :name, :address, :time_to_complete, :time_left
        json.price user_task.penalty_price
        json.is_done true
        json.rating user_task.penalty_rating
    end
end