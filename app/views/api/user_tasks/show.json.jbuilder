json.extract! @user_task, :id, :name, :address, :penalty, :project_id
json.description @user_task.description.cleanup_html

json.price  @user_task.penalty_price
json.rating @user_task.penalty_rating

json.is_done @user_task.is_done?
json.is_approved @user_task.is_approved?
json.is_current @user_task.is_current?

json.step do
    json.extract! @step, :id, :name
    json.description @step.description.cleanup_html
    json.last @step.last?
end

json.blocks do
    json.array!(@step.blocks) do |block|
        json.extract! block, :id, :name
        json.description block.description.cleanup_html

        json.fields do
            json.array!(block.fields) do |field|
                json.extract! field, :id, :name, :field_type, :hint, :description, :required
                json.value field.answer(@user_task, true)
                if field.with_variants?
                    json.variants do
                        json.array!(field.variants) do |variant|
                            json.extract! variant, :id, :label
                            json.checked variant.checked?(@user_task)
                        end
                    end
                end
            end
        end
    end
end

json.required @step.required

json.mission do
    json.extract! @mission, :id, :project_id, :task_id, :name, :address, :rating, :price, :time_to_complete
    json.description @mission.description.cleanup_html
    json.inactive true

    if @mission.target.present?
        json.geoObjects do
            json.location do
                json.geometry do
                  json.type 'Point'
                  json.coordinates @user.coords.reverse!
                end
            end

            json.target do
                json.geometry do
                  json.type 'Point'
                  json.coordinates @mission.coords.reverse!
                end
            end
        end
    end
end
