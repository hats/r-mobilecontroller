json.updates do
    json.array! @aggregator.missions do |json, mission|
        json.extract! mission,
            :project_id,
            :task_id,
            :target_id,

            :name,
            :address,
            :type,

            :rating,
            :price,

            :time_left,
            :time_to_complete

        json.distance mission.distance(@user)
        json.important mission.important?
        json.mission_id mission.id
        json.description mission.description.cleanup_html
    end
end