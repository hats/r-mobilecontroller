json.extract! @mission, :id, :project_id, :task_id, :name, :address, :rating, :price, :time_to_complete
json.description @mission.description.cleanup_html

if @mission.target.present?
    json.geoObjects do
        json.location do
            json.geometry do
              json.type 'Point'
              json.coordinates @audit.coords.reverse!
            end
        end

        json.target do
            json.geometry do
              json.type 'Point'
              json.coordinates @mission.coords.reverse!
            end
        end
    end
end
