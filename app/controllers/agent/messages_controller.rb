class Agent::MessagesController < Agent::BaseController
  before_filter :authenticate_agent!

  before_action :set_feedback
  before_action :set_message, only: [:destroy]

  def create
    @message = Message.new(message_params)
    @message.feedback = @feedback
    @message.user = current_agent

    respond_to do |format|
      if @message.save
        format.html { redirect_to agent_feedback_path(@feedback), notice: 'Сообщение добавлено!' }
        format.json { render action: 'show', status: :created, location: @feedback }
      else
        format.html { redirect_to agent_feedback_path(@feedback), error: 'Произошла ошибка!' }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if @message.is_deleteable_by?(current_agent)
      @message.destroy
      redirect_to agent_feedback_path(@feedback), notice: 'Сообщение удалено из переписки.'
    else
      redirect_to agent_feedback_path(@feedback), notice: 'Нельзя удалить это сообщение!'
    end
  end

  private

  def set_message
    @message = Message.find(params[:id])
  end

  def set_feedback
    @feedback = Feedback.find(params[:feedback_id])
  end

  def message_params
    params.require(:message).permit(:content)
  end
end