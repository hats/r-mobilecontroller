class Agent::UserTasksController < Agent::BaseController
  before_filter :authenticate_agent!

  def index
    @aggregator = TaskAggregator.new(current_agent)
  end

  def show
  end
end