class Agent::DashboardController < Agent::BaseController
  before_filter :authenticate_agent!

  def show
  end
end