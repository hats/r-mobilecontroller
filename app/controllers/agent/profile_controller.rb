class Agent::ProfileController < Agent::BaseController
  before_action :set_agent_from_session, only: [:confirm, :do_confirm, :change, :do_change]
  before_action :set_agent, only: [:edit, :update]

  before_filter :authenticate_agent!, only: [:edit, :update]

  prepend_before_filter :allow_params_authentication!, only: [:do_login]
  prepend_before_filter only: [ :do_login, :destroy ] { request.env["devise.skip_timeout"] = true }

  def edit
  end

  def update
    if @agent.profile.update(profile_params)
      redirect_to agent_dashboard_path, notice: 'Данные сохранены успешно'
    else
      render 'edit'
    end
  end

  def register
    @agent = User::Audit.new
  end

  def do_register
    @agent = User::Audit.new(register_params)
    @agent.password = User::Audit.generate

    if @agent.save
      AgentSender.new(@agent).password
      redirect_to agent_login_path, notice: 'Пароль отправлен в SMS'
    else
      render action: 'register'
    end
  end

  def login
    @agent = User::Audit.new
  end

  def do_login
    @resource = warden.authenticate!(scope: :agent, recall: 'agent/profile#login')
    set_flash_message(:notice, :signed_in) if is_flashing_format?
    sign_in(:agent, @resource)
    yield @resource if block_given?
    redirect_to agent_dashboard_path
  end


  def recover
    @agent = User::Audit.new
  end

  def do_recover
    @agent = User::Audit.find_first_by_auth_conditions(recover_params)

    if @agent.present?
      session[:recover_agent_id] = @agent.id
      @agent.update(confirmation_token: User::Audit.generate)
      AgentSender.new(@agent).confirm

      redirect_to agent_confirm_path
    else
      @agent = User::Audit.new(recover_params)
      @agent.errors[:login] = 'Телефон не найден'

      render 'recover'
    end
  end

  def confirm
  end

  def do_confirm
    if @agent.update(confirm_params)
      redirect_to agent_change_path
    else
      render action: 'confirm'
    end
  end

  def do_change
    if @agent.update(change_params)
      redirect_to agent_login_path, notice: 'Пароль изменен успешно!'
    else
      @agent.password = change_params[:password]
      render action: 'change'
    end
  end

  def logout
    scope = Devise::Mapping.find_scope!(current_agent)
    Devise.sign_out_all_scopes ? sign_out : sign_out(:agent)
    redirect_to agent_root_path, notice: 'Вы вышли из системы.'
  end

  private
  def set_agent
    @agent = current_agent
  end

  def set_agent_from_session
    begin
      @agent = User::Audit.find(session[:recover_agent_id])
    rescue
      redirect_to agent_recover_path, alert: 'Произошел сбой'
    end
  end

  def register_params
    params.require(:agent).permit(:login, profile_attributes: [:first_name, :last_name, :address, :sex, :agreement, :birth_date, :email])
  end

  def login_params
    params.require(:agent).permit(:login, :password)
  end

  def recover_params
    params.require(:agent).permit(:login)
  end

  def confirm_params
    params.require(:agent).permit(:confirmation_token_confirmation)
  end

  def change_params
    params.require(:agent).permit(:password, :password_confirmation)
  end

  def profile_params
    params.require(:profile).permit(:first_name, :last_name, :address, :sex, :agreement, :birth_date, :email)
  end






  ###
  ## DEVISE SMS AUTH
  ###
  def auth_options
    { scope: :agent, recall: 'agent/profile#login' }
  end

  def set_flash_message(key, kind, options = {})
    message = find_message(kind, options)
    flash[key] = message if message.present?
  end

  def find_message(kind, options = {})
    options[:scope] = "devise.sessions.agent"
    options[:default] = Array(options[:default]).unshift(kind.to_sym)
    options = devise_i18n_options(options)
    I18n.t("#{options[:agent]}.#{kind}", options)
  end

  def devise_i18n_options(options)
    options
  end
end