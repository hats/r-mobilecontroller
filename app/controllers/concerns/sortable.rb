module Sortable
  extend ActiveSupport::Concern

  included do
    def sorting_order_params
      params.require(:order)
    end

    def sort
      errors = []

      @entity = controller_name.classify.constantize

      if sorting_order_params
        sorting_order_params.each_with_index do |id, sorting_order|
          item = @entity.find id
          item.sorting_order = sorting_order

          unless item.save :validate => false
            errors << item.errors
          end
        end
      end

      respond_to do |format|
        if errors.empty?
          format.json { head :no_content }
        else
          format.json { render json: errors, status: :unprocessable_entity }
        end
      end
    end
  end

end