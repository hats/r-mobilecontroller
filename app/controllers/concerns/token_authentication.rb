module TokenAuthentication
  extend ActiveSupport::Concern

  # Please see https://gist.github.com/josevalim/fb706b1e933ef01e4fb6
  # before editing this file, the discussion is very interesting.

  included do
    private :authenticate_agent_from_token!
    # This is our new function that comes before Devise's one
    before_filter :authenticate_agent_from_token!
    # This is Devise's authentication
    before_filter :authenticate_agent!
  end

  # For this example, we are simply using token authentication
  # via parameters. However, anyone could use Rails's token
  # authentication features to get the token from a header.
  def authenticate_agent_from_token!
    # Set the authentication params if not already present
    if auth_token = params[:auth_token].blank? && request.headers["X-Auth-Token"]
      params[:auth_token] = auth_token
    end
    if auth_login = params[:auth_login].blank? && request.headers["X-Auth-Login"]
      params[:auth_login] = auth_login
    end

    auth_login = params[:auth_login].presence
    agent = auth_login && User::Audit.find_first_by_auth_conditions(login: auth_login)

    # Notice how we use Devise.secure_compare to compare the token
    # in the database with the token given in the params, mitigating
    # timing attacks.
    if agent && Devise.secure_compare(agent.authentication_token, params[:auth_token])
      # Notice we are passing store false, so the user is not
      # actually stored in the session and a token is needed
      # for every request. If you want the token to work as a
      # sign in token, you can simply remove store: false.
      sign_in agent, store: false
      @user = agent
    end
  end

  module ClassMethods
    # nop
  end
end