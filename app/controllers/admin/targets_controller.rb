class Admin::TargetsController < Admin::BaseController
  before_action :set_target, only: [:show, :edit, :update, :destroy]
  before_action :set_project

  # GET /admin/targets
  # GET /admin/targets.json
  def index
    @targets = @project.targets.all
  end

  # GET /admin/targets/1
  # GET /admin/targets/1.json
  def show
  end

  # GET /admin/targets/new
  def new
    @target = Target.new
    @target.project = @project
  end

  # GET /admin/targets/1/edit
  def edit
  end

  def batch_update
    importer = TargetImporter.new(@project)
    importer.import(batch_params[:file])

    respond_to do |format|
      if @project.save
        format.html { redirect_to admin_project_targets_path(@project), notice: ['Список точек загружен успешно!', ['Обработано: ', importer.parsed].join(''), ['Добавлено: ', importer.added].join('')].join(' ') }
      else
        abort @project.client.inspect
        #abort @project.errors.inspect
        format.html { redirect_to batch_admin_project_targets_path(@project), alert: 'При загрузке точек произошла ошибка!' }
      end


    end
  end

  def batch
    if params[:template]
      send_file File.join(Rails.root, 'lib', 'downloads', 'project-targets.xls'),
                filename: [[@project.name, 'Точки'].join('_'), 'xls'].join('.'),
                type: 'application/xls',
                x_sendfile: true

    end
  end

  # POST /admin/targets
  # POST /admin/targets.json
  def create
    @target = Target.new(admin_target_params)
    @target.project = @project

    respond_to do |format|
      if @target.save
        format.html { redirect_to admin_project_targets_path(@project), notice: 'Точка создана успешно' }
        format.json { render action: 'show', status: :created, location: @target }
      else
        format.html { render action: 'new' }
        format.json { render json: @target.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/targets/1
  # PATCH/PUT /admin/targets/1.json
  def update
    respond_to do |format|
      if @target.update(admin_target_params)
        format.html { redirect_to admin_project_targets_path(@project), notice: 'Точка сохранена успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @target.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/targets/1
  # DELETE /admin/targets/1.json
  def destroy
    @target.destroy
    respond_to do |format|
      format.html { redirect_to admin_project_targets_path(@project), notice: 'Точка удалена успешно' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_target
    @target = Target.find(params[:id])
  end

  def set_project
    @project = Project.find(params[:project_id])
  end

  def batch_params
    params.require(:batch).permit(:file)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_target_params
    #TODO safe params
    params.require(:target).permit!
  end
end
