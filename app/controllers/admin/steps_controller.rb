class Admin::StepsController < Admin::BaseController
  include Sortable
  before_action :set_step, only: [:show, :edit, :update, :destroy]
  before_action :set_task, only: [:index, :new, :create]

  # GET /admin/steps
  # GET /admin/steps.json
  def index
    @steps = Step.all
  end

  # GET /admin/steps/1
  # GET /admin/steps/1.json
  def show
  end

  # GET /admin/steps/new
  def new
    @step = Step.new
  end

  # GET /admin/steps/1/edit
  def edit
  end

  # POST /admin/steps
  # POST /admin/steps.json
  def create
    @step = Step.new(admin_step_params)
    @step.task = @task

    respond_to do |format|
      if @step.save
        format.html { redirect_to admin_task_path(@task), notice: 'Шаг создан успешно' }
        format.json { render action: 'show', status: :created, location: @step }
      else
        format.html { render action: 'new' }
        format.json { render json: @step.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/steps/1
  # PATCH/PUT /admin/steps/1.json
  def update
    respond_to do |format|
      if @step.update(admin_step_params)
        format.html { redirect_to admin_task_path(@task), notice: 'Шаг сохранен успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @step.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/steps/1
  # DELETE /admin/steps/1.json
  def destroy
    @step.destroy
    respond_to do |format|
      format.html { redirect_to admin_task_path(@task), notice: 'Шаг удален успешно' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_step
    @step = Step.find(params[:id])
    @task = @step.task
  end

  def set_task
    @task = Task.find(params[:task_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_step_params
    #TODO safe params
    params.require(:step).permit!
  end
end
