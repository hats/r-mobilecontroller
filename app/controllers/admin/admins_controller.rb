class Admin::AdminsController < Admin::BaseController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /admin/admins
  # GET /admin/admins.json
  def index
    @users = User::Admin.all
  end

  # GET /admin/admins/1
  # GET /admin/admins/1.json
  def show
  end

  # GET /admin/admins/new
  def new
    @user = User::Admin.new
  end

  # GET /admin/admins/1/edit
  def edit
  end

  # POST /admin/admins
  # POST /admin/admins.json
  def create
    @user = User::Admin.new(admin_admin_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_admins_path, notice: 'Админ создан успешно' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/admins/1
  # PATCH/PUT /admin/admins/1.json
  def update
    respond_to do |format|
      #abort admin_admin_params.inspect
      if @user.update(admin_admin_params)
        format.html { redirect_to admin_admins_path, notice: 'Админ сохранен успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/admins/1
  # DELETE /admin/admins/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_admins_url, notice: 'Админ удален успешно' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User::Admin.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_admin_params
    strong_params = params.require(:user).permit!

    unless strong_params[:password].present?
      strong_params.delete(:password)
    end

    strong_params
  end
end
