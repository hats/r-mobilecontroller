class Admin::FieldsController < Admin::BaseController
  include Sortable

  before_action :set_field, only: [:show, :edit, :update, :destroy, :variants]
  before_action :set_block, only: [:index, :new, :create]
  before_action :set_step, except: [:sort]
  before_action :set_task, except: [:sort]

  # GET /admin/fields
  # GET /admin/fields.json
  def index
    @fields = Field.all
  end

  # GET /admin/fields/1
  # GET /admin/fields/1.json
  def show
  end

  # GET /admin/fields/new
  def new
    @field = Field.new
  end

  def variants
  end

  # GET /admin/fields/1/edit
  def edit
  end

  # POST /admin/fields
  # POST /admin/fields.json
  def create
    @field = Field.new(admin_field_params)
    @field.block = @block

    respond_to do |format|
      if @field.save
        if %w(Field::Checkbox Field::Radio Field::Selectbox).include?(@field.class.name)
          redirect_path = edit_admin_field_path(@field, anchor: 'variants')
        else
          redirect_path = admin_task_path(@task)
        end

        format.html { redirect_to redirect_path, notice: 'Поле создано успешно' }
        format.json { render action: 'show', status: :created, location: @field }
      else
        format.html { render action: 'new' }
        format.json { render json: @field.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fields/1
  # PATCH/PUT /admin/fields/1.json
  def update
    respond_to do |format|
      if @field.update(admin_field_params)
        format.html { redirect_to admin_task_path(@task), notice: 'Поле сохранено успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @field.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fields/1
  # DELETE /admin/fields/1.json
  def destroy
    @field.destroy
    respond_to do |format|
      format.html { redirect_to admin_task_path(@task), notice: 'Поле удалено успешно' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_field
    @field = Field.find(params[:id])
    @block = @field.block
  end

  def set_block
    @block = Block.find(params[:block_id])
  end

  def set_step
    @step = @block.step
  end

  def set_task
    @task = @step.task
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_field_params
    #TODO safe params
    params.require(:field).permit!
  end
end
