class Admin::VariantsController < Admin::BaseController
  include Sortable
  before_action :set_variant, only: [:show, :edit, :update, :destroy]
  before_action :set_task
  before_action :set_field


  # GET /admin/variants
  # GET /admin/variants.json
  def index
    @variants = @field.variants.all
  end

  # GET /admin/variants/1
  # GET /admin/variants/1.json
  def show
  end

  # GET /admin/variants/new
  def new
    @variant = @field.variants.new
  end

  # GET /admin/variants/1/edit
  def edit
  end

  # POST /admin/variants
  # POST /admin/variants.json
  def create
    @variant = @field.variants.new(variant_params)

    respond_to do |format|
      if @variant.save
        format.html { redirect_to edit_admin_task_field_path(@task, @field, anchor: 'variants'), notice: 'Вариант ответа создан успешно' }
        format.json { render action: 'show', status: :created, location: @variant }
      else
        format.html { render action: 'new' }
        format.json { render json: @variant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/variants/1
  # PATCH/PUT /admin/variants/1.json
  def update
    respond_to do |format|
      if @variant.update(variant_params)
        format.html { redirect_to edit_admin_task_field_path(@task, @field, anchor: 'variants'), notice: 'Вариант ответа сохранен успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @variant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/variants/1
  # DELETE /admin/variants/1.json
  def destroy
    @variant.destroy
    respond_to do |format|
      format.html { redirect_to admin_variants_url, notice: 'Вариант ответа удален успешно' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_variant
    @variant = Variant.find(params[:id])
  end

  def set_task
    @task = Task.find(params[:task_id])
  end

  def set_field
    @field = Field.find(params[:field_id])
  end



  # Never trust parameters from the scary internet, only allow the white list through.
  def variant_params
    #TODO safe params
    params.require(:variant).permit!
  end
end
