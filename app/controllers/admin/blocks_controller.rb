class Admin::BlocksController < Admin::BaseController
  include Sortable

  before_action :set_block, only: [:show, :edit, :update, :destroy]
  before_action :set_step, only: [:index, :new, :create]
  before_action :set_task, except: [:sort]

  # GET /admin/blocks
  # GET /admin/blocks.json
  def index
    @blocks = Block.all
  end

  # GET /admin/blocks/1
  # GET /admin/blocks/1.json
  def show
  end

  # GET /admin/blocks/new
  def new
    @block = Block.new
  end

  # GET /admin/blocks/1/edit
  def edit
  end

  # POST /admin/blocks
  # POST /admin/blocks.json
  def create
    @block = Block.new(admin_block_params)
    @block.step = @step

    respond_to do |format|
      if @block.save
        format.html { redirect_to admin_task_path(@task), notice: 'Блок создан успешно' }
        format.json { render action: 'show', status: :created, location: @block }
      else
        format.html { render action: 'new' }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/blocks/1
  # PATCH/PUT /admin/blocks/1.json
  def update
    respond_to do |format|
      if @block.update(admin_block_params)
        format.html { redirect_to admin_task_path(@task), notice: 'Блок сохранен успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/blocks/1
  # DELETE /admin/blocks/1.json
  def destroy
    @block.destroy
    respond_to do |format|
      format.html { redirect_to admin_task_path(@task), notice: 'Блок удален успешно' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_block
    @block = Block.find(params[:id])
    @step = @block.step
  end

  def set_step
    @step = Step.find(params[:step_id])
  end

  def set_task
    @task = @step.task
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_block_params
    #TODO safe params
    params.require(:block).permit!
  end
end
