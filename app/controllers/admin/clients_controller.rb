class Admin::ClientsController < Admin::BaseController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :projects]

  # GET /admin/clients
  # GET /admin/clients.json
  def index
    @users = User::Client.all
  end

  # GET /admin/clients/1
  # GET /admin/clients/1.json
  def show
  end

  # GET /admin/clients/new
  def new
    @user = User::Client.new
  end

  # GET /admin/clients/1/edit
  def edit
  end

  def projects
    @projects = @user.projects
  end

  # POST /admin/clients
  # POST /admin/clients.json
  def create
    @user = User::Client.new(admin_client_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_clients_path, notice: 'Клиент создан успешно' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/clients/1
  # PATCH/PUT /admin/clients/1.json
  def update
    respond_to do |format|
      #abort admin_client_params.inspect
      if @user.update(admin_client_params)
        format.html { redirect_to admin_clients_path, notice: 'Клиент сохранен успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/clients/1
  # DELETE /admin/clients/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_clients_url, notice: 'Клиент удален успешно' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User::Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_client_params
      strong_params = params.require(:user).permit!

      unless strong_params[:password].present?
        strong_params.delete(:password)
      end

      strong_params
    end
end
