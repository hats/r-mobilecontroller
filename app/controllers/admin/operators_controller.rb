class Admin::OperatorsController < Admin::BaseController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :projects]

  # GET /admin/operators
  # GET /admin/operators.json
  def index
    @users = User::Operator.all
  end

  # GET /admin/operators/1
  # GET /admin/operators/1.json
  def show
  end

  # GET /admin/operators/new
  def new
    @user = User::Operator.new
  end

  # GET /admin/operators/1/edit
  def edit
  end

  # POST /admin/operators
  # POST /admin/operators.json
  def create
    @user = User::Operator.new(admin_operator_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_operators_path, notice: 'Оператор создан успешно' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/operators/1
  # PATCH/PUT /admin/operators/1.json
  def update
    respond_to do |format|
      #abort admin_operator_params.inspect
      if @user.update(admin_operator_params)
        format.html { redirect_to admin_operators_path, notice: 'Оператор сохранен успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/operators/1
  # DELETE /admin/operators/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_operators_url, notice: 'Оператор удален успешно' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User::Operator.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_operator_params
      strong_params = params.require(:user).permit!

      unless strong_params[:password].present?
        strong_params.delete(:password)
      end

      strong_params
    end
end
