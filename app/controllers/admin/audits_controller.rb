class Admin::AuditsController < Admin::BaseController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :projects]

  # GET /admin/audits
  # GET /admin/audits.json
  def index
    @users = User::Audit.all
  end

  # GET /admin/audits/1
  # GET /admin/audits/1.json
  def show
  end

  # GET /admin/audits/new
  def new
    @user = User::Audit.new
  end

  # GET /admin/audits/1/edit
  def edit
  end

  # POST /admin/audits
  # POST /admin/audits.json
  def create
    @user = User::Audit.new(admin_audit_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_audits_path, notice: 'Аудитор создан успешно' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/audits/1
  # PATCH/PUT /admin/audits/1.json
  def update
    respond_to do |format|
      #abort admin_audit_params.inspect
      if @user.update(admin_audit_params)
        format.html { redirect_to admin_audits_path, notice: 'Аудитор сохранен успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/audits/1
  # DELETE /admin/audits/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_audits_url, notice: 'Аудитор удален успешно' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User::Audit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_audit_params
      strong_params = params.require(:user).permit!

      unless strong_params[:password].present?
        strong_params.delete(:password)
      end

      strong_params
    end
end
