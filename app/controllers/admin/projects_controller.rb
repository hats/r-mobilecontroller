class Admin::ProjectsController < Admin::BaseController
  before_action :set_project, only: [:show, :edit, :update, :destroy, :tasks]

  # GET /admin/projects
  # GET /admin/projects.json
  def index
    @projects = Project.all
  end

  # GET /admin/projects/1
  # GET /admin/projects/1.json
  def show
  end

  # GET /admin/projects/new
  def new
    @project = Project.new
  end

  # GET /admin/projects/1/edit
  def edit
  end

  def tasks
    @tasks = @project.tasks
  end

  # POST /admin/projects
  # POST /admin/projects.json
  def create
    @project = Project.new(admin_project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to admin_projects_path, notice: 'Проект создан успешно' }
        format.json { render action: 'show', status: :created, location: @project }
      else
        format.html { render action: 'new' }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/projects/1
  # PATCH/PUT /admin/projects/1.json
  def update
    respond_to do |format|
      if @project.update(admin_project_params)
        format.html { redirect_to admin_projects_path, notice: 'Проект сохранен успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/projects/1
  # DELETE /admin/projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to admin_projects_url, notice: 'Проект удален успешно' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_project
    @project = Project.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_project_params
    #TODO safe params
    params.require(:project).permit!
  end
end
