class Admin::TasksController < Admin::BaseController
  before_action :set_task, only: [:show, :edit, :update, :destroy]
  before_action :set_project, only: [:index, :new, :create]

  # GET /admin/tasks
  # GET /admin/tasks.json
  def index
    @tasks = @project.tasks.sorted
  end

  # GET /admin/tasks/1
  # GET /admin/tasks/1.json
  def show
  end

  # GET /admin/tasks/new
  def new
    @task = Task.new
    @task.project = @project
  end

  # GET /admin/tasks/1/edit
  def edit
  end

  # POST /admin/tasks
  # POST /admin/tasks.json
  def create
    @task = Task.new(admin_task_params)
    @task.project = @project

    respond_to do |format|
      if @task.save
        format.html { redirect_to admin_project_tasks_path(@project), notice: 'Задача создана успешно' }
        format.json { render action: 'show', status: :created, location: @task }
      else
        format.html { render action: 'new' }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/tasks/1
  # PATCH/PUT /admin/tasks/1.json
  def update
    respond_to do |format|
      if @task.update(admin_task_params)
        format.html { redirect_to admin_project_tasks_path(@project), notice: 'Задача сохранена успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/tasks/1
  # DELETE /admin/tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to admin_project_tasks_path(@project), notice: 'Задача удалена успешно' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_task
    @task = Task.find(params[:id])
    @project = @task.project
  end

  def set_project
    @project = Project.find(params[:project_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_task_params
    #TODO safe params
    params.require(:task).permit!
  end
end
