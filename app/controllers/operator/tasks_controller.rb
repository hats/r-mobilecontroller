class Operator::TasksController < Operator::BaseController
  before_action :set_task, only: [:show, :update]

  def index
    @user_tasks = UserTask.where(task_status: TaskStatus::Done.first)
  end

  def show
  end

  def update
    @user_task.approve(current_operator)

    if @user_task.update(approve_params)
      redirect_to operator_tasks_path, notice: 'Задание подтвеждено успешно'
    else
      render :show, error: 'Произошла ошибка'
    end
  end

  private
  def set_task
    @user_task = UserTask.find(params[:id])
  end

  def approve_params
    params.require(:user_task).permit(:penalty)
  end
end