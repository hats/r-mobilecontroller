class Operator::BaseController < ApplicationController
  before_filter :authenticate_operator!
  layout 'operator'
end