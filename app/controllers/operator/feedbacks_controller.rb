class Operator::FeedbacksController < Operator::BaseController
  before_action :set_feedback, only: [:show, :accept]

  # GET /admin/feedbacks
  # GET /admin/feedbacks.json
  def index
    @feedbacks = Feedback.unaccepted
  end

  # GET /admin/feedbacks/1
  # GET /admin/feedbacks/1.json
  def show
  end

  def accept
    if @feedback.operator.nil? and @feedback.update(operator: current_operator)
      redirect_to operator_feedback_path(@feedback), notice: 'Запрос на обработку принят успешно!'
    else
      redirect_to operator_feedbacks_path, error: 'Произошла ошибка'
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_feedback
    @feedback = Feedback.find(params[:id])
  end
end
