class Client::ComingsController < Client::BaseController
  def index
    @projects = current_client.projects.coming
  end
end