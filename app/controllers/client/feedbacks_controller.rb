class Client::FeedbacksController < Client::BaseController
  before_action :set_feedback, only: [:show, :edit, :update, :destroy]

  # GET /admin/feedbacks
  # GET /admin/feedbacks.json
  def index
    @feedbacks = current_client.feedbacks
  end

  # GET /admin/feedbacks/1
  # GET /admin/feedbacks/1.json
  def show
  end

  # GET /admin/feedbacks/new
  def new
    @feedback = current_client.feedbacks.build
  end

  # GET /admin/feedbacks/1/edit
  def edit
  end

  # POST /admin/feedbacks
  # POST /admin/feedbacks.json
  def create
    @feedback = current_client.feedbacks.build(client_feedback_params)

    respond_to do |format|
      if @feedback.save
        format.html { redirect_to client_feedbacks_path, notice: 'Запрос обратной связи создан успешно' }
        format.json { render action: 'show', status: :created, location: @feedback }
      else
        format.html { render action: 'new' }
        format.json { render json: @feedback.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/feedbacks/1
  # PATCH/PUT /admin/feedbacks/1.json
  def update
    respond_to do |format|
      if @feedback.update(client_feedback_params)
        format.html { redirect_to client_feedbacks_path, notice: 'Запрос обратной связи сохранен успешно' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @feedback.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/feedbacks/1
  # DELETE /admin/feedbacks/1.json
  def destroy
    @feedback.destroy
    respond_to do |format|
      format.html { redirect_to client_feedbacks_url, notice: 'Запрос обратной связи удален успешно' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_feedback
    @feedback = current_client.feedbacks.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def client_feedback_params
    params.require(:feedback).permit(:content)
  end
end
