class Client::ProjectsController < Client::BaseController
  before_action :set_project, only: [:show]

  def show
  end

  def index
    @projects = current_client.projects
  end

  private
  def set_project
    @project = current_client.projects.find(params[:id])
  end
end