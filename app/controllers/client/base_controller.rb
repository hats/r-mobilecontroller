class Client::BaseController < ApplicationController
  before_filter :authenticate_client!
  layout 'client'
end