class Client::ArchivesController < Client::BaseController
  def index
    @projects = current_client.projects.archive
  end
end