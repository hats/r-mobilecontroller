class Api::MissionsController < Api::BaseController
  include Timestamps
  include TokenAuthentication

  before_action :set_mission, only: [:update]
  protect_from_forgery except: [:update]

  def update
    #TODO sensus
    #TODO check if target exists
    #TODO check if task exists
    #TODO check if project exists
    #TODO check if expired

    @user_task = @user.user_tasks.find_or_create_by(task: @mission.task,
                                                    target_id: @mission.target_id,
                                                    deleted_at: nil)

    if @user_task.save
      render json: {
          task: @user_task.json_attributes.merge(login: @user.login)
      }
    else

      render json: @user_task.errors, status: :unprocessable_entity

      #TODO if remove from local
      #render json: {remove: true}, status: :unprocessable_entity
    end
  end

  private

  def set_mission
    @mission = Mission.get(@user, params[:id])
  end
end