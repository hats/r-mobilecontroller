class Api::BaseController < ApplicationController
  protect_from_forgery except: :all
  layout 'api'
end