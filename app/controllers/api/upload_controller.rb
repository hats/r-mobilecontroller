class Api::UploadController < Api::BaseController
  include Timestamps
  include TokenAuthentication

  protect_from_forgery except: :all
  before_action :set_user_task, only: [:recieve]

  def recieve
    user_task_answer = @user_task.user_task_answers.find_or_initialize_by field_id: params[:fieldId]
    user_task_answer.answer = params[:file]

    if user_task_answer.save
      render json: true, status: 200
    else
      render json: false, status: 500
    end
  end

  private
  def set_user_task
    @user_task = UserTask.find_by(user: @user, token: params[:taskId])
  end
end