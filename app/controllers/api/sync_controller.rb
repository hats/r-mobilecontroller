class Api::SyncController < Api::BaseController
  include Timestamps
  include TokenAuthentication

  protect_from_forgery except: :all

  def sync
    since = from_unix_ms_timestamp params[:since]

    # TODO where updated_at < since
    aggregator = TaskAggregator.new(@user)

    # all available missions
    missions = aggregator.missions
    missions.each { |mission| puts "-- Mission sent: #{mission.id}" }

    # all user tasks
    user_tasks = aggregator.all
    user_tasks.each { |user_task| puts "-- User task sent: #{user_task.id}" }

    #TODO refactor
    steps = missions.map(&:task).map { |task| task.steps }.flatten.index_by(&:id).values
    steps = steps.map(&:json_attributes)

    now = to_unix_ms_timestamp Time.now
    render json: {
        now: now,
        missions: missions.map(&:attributes).map{ |a| a.merge({login: @user.login})},
        tasks: user_tasks.map(&:json_attributes).map{ |a| a.merge({login: @user.login})},
        steps: steps
    }
  end
end