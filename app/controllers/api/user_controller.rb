class Api::UserController < Api::BaseController
  #before_filter :authenticate_agent!, except: [:login]

  #prepend_before_filter :allow_params_authentication!, only: [:login]
  #prepend_before_filter only: [:login, :destroy] { request.env["devise.skip_timeout"] = true }

  def login
    login = login_params[:login]
    password = login_params[:password]

    if request.format != :json
      render status: 406,
             json: { message: 'Ошибка соединения с сервером!' }
      return
    end

    if !login.present? or !password.present?
      render status: 400,
             json: { message: 'Введите логин и пароль!' }
      return
    end

    invalidMsg = 'Вы ввели неверный логин и пароль!'

    @user = User::Audit.find_first_by_auth_conditions(login: login)

    unless @user.present?
      logger.info("User #{login} failed signin, user cannot be found.")
      render status: 401, json: {message: invalidMsg}
      return
    end

    # http://rdoc.info/github/plataformatec/devise/master/Devise/Models/TokenAuthenticatable
    @user.ensure_authentication_token!

    unless @user.valid_password?(password)
      logger.info("User #{login} failed signin, invalid password")

      render status: 401,
             json: { message: invalidMsg }
    else
      render status: 200
    end
  end

  def logout
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(:agent))
    set_flash_message :notice, :signed_out if signed_out && is_flashing_format?
    yield if block_given?

    respond_to do |format|
      format.all { head :no_content }
    end
  end

  private
  def set_flash_message(type, message)
  end

  def login_params
    params.require(:agent).permit(:login, :password)
  end
end