class Api::MapsController < Api::BaseController
  include TokenAuthentication

  def show
    @aggregator = TaskAggregator.new(@user)
  end
end