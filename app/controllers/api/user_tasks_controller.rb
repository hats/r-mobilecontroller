class Api::UserTasksController < Api::BaseController
  include TokenAuthentication

  before_action :set_user_task, only: [:show, :update]

  def index
    @aggregator = TaskAggregator.new(@user)
  end

  def show
    @step = @user_task.step
    @mission = @user_task.missionify
  end

  def update
    @user_task.update_values(fields_params) if fields_params.present?
    @user_task.finalize

    if @user_task.save
      render json: @user.tasks_info
    else
      render json: @user_task.errors, status: :unprocessable_entity
    end
  end

  def destroy
    #begin
    #
    #rescue
    #  # DO NUTHING WITHOUT TASK
    #end

    @user_task = @user.user_tasks.find_by(token: params[:id])
    @user_task.destroy if @user_task.present?

    render json: @user.tasks_info
  end

  private
  def fields_params
    params.require(:answers) if params[:answers].present?
  end

  def set_user_task
    @user_task = @user.user_tasks.find_by(token: params[:id])
  end
end