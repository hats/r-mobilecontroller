class Api::GeolocationsController < Api::BaseController
  include TokenAuthentication

  def update
    @user.locate(geolocation_params)
    render :nothing => true
  end

  private
  def geolocation_params
    params.require(:geolocation).permit(:latitude, :longitude)
  end
end