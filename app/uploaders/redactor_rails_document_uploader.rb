# encoding: utf-8
class RedactorRailsDocumentUploader < CarrierWave::Uploader::Base
  include RedactorRails::Backend::CarrierWave

  # storage :fog
  storage :file

  def store_dir
    "system/apetail/documents/#{model.id}"
  end

  def extension_white_list
    %w(png jpg jpeg doc zip docx xls xlsx)
  end
end
