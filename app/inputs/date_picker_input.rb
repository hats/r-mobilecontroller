# календарь для библиотеки SimpleForm
class DatePickerInput < SimpleForm::Inputs::StringInput
  def input_html_options
    value = object.send(attribute_name)
    options = {
        value: value.nil?? nil : I18n.localize(value),
        class: 'form-control datepicker',
        data: { behaviour: 'datepicker' }
    }
    super.merge options
  end
end