require 'net/http'

class AgentSender

  @@login = '9039746968'
  @@password = 'ber13dss'
  @@key = 'aacab455-a490-add4-4974-fee7d95ec978'
  @@url = URI.parse("http://sms.ru/sms/send")

  def initialize(agent)
    @agent = agent
    @to = agent.login
  end

  def password
    send("Ваш пароль: #{@agent.password}")
  end

  def confirm
    send("Код подтверждения: #{@agent.confirmation_token}")
  end

  private
  def send(text)
    params = {
        api_id: @@key,
        to: @to,
        text: text,
        from: 'MController'
    }

    req = Net::HTTP.get('sms.ru', @@url.path + "?#{params.to_query}")
  end
end