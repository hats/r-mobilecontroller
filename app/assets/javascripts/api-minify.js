//= require shared/modernizr
//= require alertify
//= require jquery

//= require ionic/ionic.bundle.min
//= require ionic/angular/angular-resource.min
//= require ionic/angular/angular-route.min
//= require ionic/angular/checklist

//= require shared/ya-maps.min
//= require shared/moment.min
//= require shared/autosize

//= require shared/persistance/persistance
//= require shared/persistance/store.sql
//= require shared/persistance/store.websql
//= require shared/persistance/store.cordovasql
//= require shared/persistance/migrations

//= require shared/input-mask