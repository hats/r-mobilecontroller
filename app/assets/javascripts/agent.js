//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require bootstrap-datepicker/core
//= require bootstrap-datepicker/locales/bootstrap-datepicker.ru.js

//= require alertify
//= require shared/autosize
//= require shared/message
//= require shared/datepicker
//= require shared/input-mask
//= require shared/jquery.validate
//= require shared/persistent-tabs
//= require_tree ./agent/
