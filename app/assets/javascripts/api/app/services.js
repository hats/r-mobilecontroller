var DOMAIN = 'http://mobilecontroller-2014.herokuapp.com/';

angular.module('MC.services', ['ngResource'])

    .factory('MissionsService', function ($resource, $http) {
        var resource = $resource(DOMAIN + '/api/missions/:id.json', {id: "@id"}, {
            query: { method: 'GET' },
            get: { method: 'GET' },
            start: { method: 'PATCH', isArray: false }
        });

        return {
            start: function (id, onSuccess, onError) {
                MissionModel.load(id, function (mission) {
                    resource.start({id: id, lastSync: Math.random()})
                        .$promise
                        .then(
                        function (result) {
                            var task = TasksMapper.data(result.task);
                            Sync(TaskModel, [result.task], TasksMapper, function () {
                                onSuccess(task);
                            }, false);
                        },
                        function (error) {
                            if (onError) onError(error);
                            else alertify.error('Ошибка запроса');
                        }
                    )
                    ;
                })
            }
        }
    })

    .factory('UserService', function ($resource) {
        var user = null;
        var resource = $resource(DOMAIN + '/api/user.json', {}, {
            login: { method: 'POST' },
            logout: { method: 'DELETE' }
        })

        return {
            login: function (data, onSuccess, onError) {
                resource.login({agent: data, dt: new Date().getTime()})
                    .$promise
                    .then(function (result) {
                        user = UserMapper.data(result);
                        console.log(user);
                        onSuccess();
                    }, function (result) {
                        onError(result.data.message);
                    })
                ;
            },
            logout: function (onSuccess) {
                resource.logout().$promise.then(function (result) {
                    user = null;
                    onSuccess();
                });
            },
            currentUser: function () {
                return user;
            }
        };
    })

    .factory('TasksService', function ($resource, $http, $ionicLoading) {
        return {
            cancel: function (taskId, onSuccess, onError) {
                $http({method: 'DELETE', url: DOMAIN + '/api/tasks/' + taskId, data: {}})
                    .success(function () {
                        if (onSuccess) onSuccess();
                    })
                    .error(function () {
                        alertify.error('Ошибка запроса');
                        if (onError) onError();
                    })
                ;
            },
            send: function ($ionicLoading, scope, taskId, files, answers, onSuccess, onError) {

                if (files.length)
                    scope.loading = $ionicLoading.show({
                        content: 'Отправка выполненного задания...',
                        maxWidth: 200
                    });

                persistence.asyncForEach(files,
                    function(file, next) {
                        SendImage(
                            taskId,
                            file.field_id,
                            file.url,
                            scope.currentUser,
                            function() {
                                next();
                            },
                            function() {
                                onError('Фотография не отправлена!');
                            }
                        );
                    },
                    function() {
                        scope.loading = $ionicLoading.show({
                            content: 'Отправка введенных данных...',
                            maxWidth: 200
                        });

                        $http({method: 'PATCH', url: DOMAIN + '/api/tasks/' + taskId, data: { answers: answers }})
                            .success(function (response) {
                                if (onSuccess) onSuccess(response);
                            })
                            .error(function (response) {
                                if (onError) onError(response);
                            })
                        ;
                    }
                );

                return;

            }
        }
    })
    .factory('GeolocationService', function ($resource, $state, $location, $ionicLoading, $rootScope) {
        var showLoading = function (scope) {
            scope.loading = $ionicLoading.show({
                content: 'Определение вашего местоположения...',
                maxWidth: 200
            });
        };

        var hideLoading = function (scope) {
            scope.loading.hide();
        };

        var resource = $resource(DOMAIN + '/api/geolocation', {}, {
                update: {
                    method: 'PATCH'
                }
            }
        );

        var gotoRoot = function () {
            $state.go('tabs.missions');
        };

        var errorHandler = function (scope, showLoading) {
            alertify.error('Не удалось определить ваше местоположение...');
            if (showLoading) {
                gotoRoot();
                hideLoading(scope);
            }
        }

        var getCoordinates = function (scope) {
            var showLoading = typeof(scope) !== 'undefined' ? true : false;
            if (showLoading) showLoading(scope);

            Geolocator.position(
                function (coords, timestamp) {
                    resource
                        .update({latitude: coords.latitude, longitude: coords.longitude})
                        .$promise
                        .then(
                        function (response) {
                            if ($rootScope.currentUser)
                            {
                                var update = UserMapper.data({latitude: coords.latitude, longitude: coords.longitude})
                                $rootScope.currentUser = $.extend($rootScope.currentUser, update);
                            };

                            if (showLoading) {
                                hideLoading(scope);
                                gotoRoot();
                            }
                        },
                        function () {
                            errorHandler(scope, showLoading);
                        }
                    )
                },
                function () {
                    if (showLoading) hideLoading(scope);
                    errorHandler(scope)
                }
            );
        }

        return {
            update: function (scope, showLoading) {
                if (showLoading === true) getCoordinates(scope, showLoading);
                else getCoordinates();
            }
        }
    })
;

var SendImage = function (taskId, fieldId, fileURL, currentUser, onSuccess, onError) {
    console.log('SEND IMAGE: ', currentUser);

//    var win = function (fileUrl) {
//        alert('uploaded!', fileUrl);
//    }
//
//    var fail = function (error) {
//        alert("An error has occurred: Code = " + error.code);
//    }

    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";

    var params = {};
    params.fieldId = fieldId;
    params.taskId = taskId;
    params.auth_login = currentUser.phone;
    params.auth_token = currentUser.auth_token;
    options.params = params;

    var ft = new FileTransfer();
    ft.upload(fileURL, encodeURI(DOMAIN + "/api/upload"), onSuccess, onError, options);
}

//var FileUploadOptions = function() {
//    return {};
//}
//
//var FileTransfer = function() {
//    this.upload = function(fileUrl, uploadTo, onSuccess, onError, options) {
//        if (fileUrl == 'error') onError({code: 404});
//        else onSuccess(fileUrl);
//    };
//}