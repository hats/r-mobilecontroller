var TaskBuilder = function (entity, $scope) {
    var task = TasksMapper.data(entity),
        completed = function () {
            return task.steps
        },

        self = this,
        current = false
    awaiting = []
    ;

    this.getSteps = function (callback) {
        StepModel
            .all()
            .filter('task_id', '=', task.task_id)
            .filter('id', 'not in', completed())
            .order('sorting_order')
            .list(function (results) {
                awaiting = results;

                if (results.length === 0) self.done(callback);
                else
                    for (var i in results) {
                        var step = results[i];
                        break;
                    }

                if (step) {
                    step = StepsMapper.data(step);
                    callback(step);
                }
            });
    };

    //TODO ACCEPT CURRENT STEP & GO TO NEXT
    this.currentStep = function (callback) {
        this.getSteps(
            function (step) {
                current = step;
                self.buildStep(step, callback);
            }
        );
    };

    this.canGoBack = function() {
        return (completed().length > 0 && task.status != 'done') ? true : false;
    }

    this.lastStep = function() {
        return awaiting.length === 1 ? true : false;
    }

    this.buildStep = function (step, callback) {
        task.step = step;

        AnswerModel
            .all()
            .filter('task_id', '=', task.id)
            .filter('step_id', '=', step.id)
            .list(function (results) {
                results.forEach(function (row) {
                    $scope.answers[row.field_id / 1] = row.value;
                });

                callback(task);
            })
        ;
    };

    this.done = function (callback) {
        task.status = 'done';
        entity.status = 'done';

//        persistence.add(entity);
        persistence.flush(function () {
            callback(task);
        });
    }

    this.nextStep = function (callback) {
        var _completed = completed();
        _completed.push(current.id);

        entity.steps = _completed.join(',')

//        persistence.add(entity);
        persistence.flush(function () {
            self.currentStep(callback);
        });
    };

    this.stepBack = function (callback) {
        var _completed = completed();
        _completed.pop();

        entity.steps = _completed.join(',')

        persistence.flush(function () {
            self.currentStep(callback);
        });
    };
}