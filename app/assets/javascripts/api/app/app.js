var startPath = "/missions";

angular.module('MC', ['ionic', 'ngResource', 'ngRoute', 'MC.controllers', 'MC.services', 'MC.directives', 'yaMap', 'checklist-model'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
//        $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');

        $stateProvider
            .state('login', {
                url: "/login",
                templateUrl: "login.html",
                controller: 'LoginCtrl'
            })

            .state('recover', {
                url: "/recover",
                templateUrl: "recover.html",
                controller: 'RecoverCtrl'
            })

            .state('tabs', {
                url: "",
                abstract: true,
                templateUrl: "tabs.html"
            })

            .state('tabs.missions', {
                url: "/missions",
                views: {
                    'missions-tab': {
                        templateUrl: 'missions.html',
                        controller: 'MissionsCtrl'
                    }
                }
            })
            .state('tabs.mission', {
                url: "/missions/:missionId",
                views: {
                    'missions-tab': {
                        templateUrl: 'mission.html',
                        controller: 'MissionCtrl'
                    }
                }
            })

            .state('tabs.tasks', {
                url: "/tasks",
                views: {
                    'tasks-tab': {
                        templateUrl: "tasks.html",
                        controller: 'TasksCtrl'
                    }
                }
            })
            .state('tabs.task', {
                url: "/tasks/:taskId",
                views: {
                    'tasks-tab': {
                        templateUrl: "task.html",
                        controller: 'TaskCtrl'
                    }
                }
            })

            .state('tabs.map', {
                url: "/map",
                views: {
                    'map-tab': {
                        templateUrl: "map.html",
                        controller: 'MapTabCtrl'
                    }
                }
            })
        ;

        $urlRouterProvider.otherwise('/missions');
    })

    .run(function ($location, $rootScope, GeolocationService) {
        // TODO get agent data on login and update
//        $rootScope.currentUser = {
//            location: {
//                geometry: {
//                    type: 'Point',
//                    coordinates: [37.6167,55.7500]
//                }
//            }
//        };

        $rootScope.hideTabs = function() {
            $rootScope.$broadcast('tabs.hide');
        };

        $rootScope.showTabs = function() {
            $rootScope.$broadcast('tabs.show');
        };

        $rootScope.seenTimeout = null;

        $rootScope.badges = {
            missions: 0,
            targets: 0,
            tasks: 0
        };

        $rootScope.updateBadges = function() {
            MissionModel
                .all()
                .filter('mission_id', 'not in (?)', 'select mission_id from tasks')
                .filter('login', '=', $rootScope.currentUser.phone)
                .filter('seen', '=', false)
                .list(function(results) {
                    $rootScope.badges.missions = results.length;
                    $rootScope.$apply();
                }
            );

            TaskModel
                .all()
                .filter('mission_id', 'in (?)', 'select id from missions')
                .filter('login', '=', $rootScope.currentUser.phone)
                .filter('seen', '=', false)
                .filter('removed', '=', false)
                .list(function(results) {
                    $rootScope.badges.tasks = results.length;
                    $rootScope.$apply();
                });

            MissionModel
                .all()
                .filter('mission_id', 'not in (?)', 'select mission_id from tasks')
                .filter('seen', '=', false)
                .filter('login', '=', $rootScope.currentUser.phone)
                .filter('target_id', 'is not', 0)
                .list(function(results) {
                    $rootScope.badges.targets = results.length;
                    $rootScope.$apply();
                }
            );
        };

        $rootScope.locateTimeout = null;

        $rootScope.locationUpdater = function() {
            console.log('UPDATING LOCATION');
            GeolocationService.update();

            $rootScope.locateTimeout = setTimeout(function() {
                $rootScope.locationUpdater();
            }, 1000 * 60 * 5);
        };

        $rootScope.stopLocationUpdater = function() {
            clearTimeout($rootScope.locateTimeout);
        };

//        TODO locate after login
        if ($rootScope.currentUser)
        {
            $rootScope.updateBadges();

            var path = $location.path();
            $rootScope.startPath = path == '/locate' ? startPath : path;
            $location.path('/locate');
        }
        else
        {
            $location.path('/login');
        }
    });