angular.module('MC.directives', ['ngResource'])
    .directive('missionStatus', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'mission_status.html'
        }
    })
    .directive('missionInfo', function () {
        return {
            restrict: 'E',
            templateUrl: 'mission_info.html'
        };
    })
    .directive('formField', function () {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                scope.$watch('taskForm.field' + scope.field.id + '.$valid', function () {
//                    scope.$broadcast('scroll.refreshComplete');
                });

                if (scope.field.type == 'checkbox' || scope.field.type == 'location') {
                    var val,
                        values = scope.answers[scope.field.id]
                        ;

                    try {
                        val = JSON.parse(values);
                    }
                    catch (e) {
                        val = scope.field.type == 'location' ? null : {};
                    }

                    scope.answers[scope.field.id] = val;
                }

                if (scope.field.type == 'location' && scope.field.required) {
                    ngModel.$setValidity('valid', false);
                    ngModel.$setValidity('required', true);
                }

                scope.$watch('answers.' + scope.field.id, function (newValue, oldValue) {
                    if (scope.field.type == 'checkbox') {
                        var answers = scope.answers[scope.field.id],
                            valid = false
                            ;

                        for (var i in answers) {
                            if (answers[i] === true) {
                                valid = true;
                                break;
                            }
                        }

                        ngModel.$setValidity('required', valid);
                    }

                    else if (scope.field.type == 'location') {
                        var answers = scope.answers[scope.field.id],
                            valid = answers && typeof(answers.latitude) != 'undefined' && typeof(answers.longitude) != 'undefined'
                            ;

                        ngModel.$setValidity('valid', valid);
                    }

                    scope.update(scope.field);
                }, true);
            }
        }
    })
    .directive('formCheck', function () {
        return {
            restrict: 'A',
            link: function (scope) {
                if (scope.field.type == 'checkbox') {
                    scope.isChecked = function (answers, variant) {
                        if (typeof(answers[variant.id]) == 'undefined')
                            answers[variant.id] = variant.default;

                        return answers[variant.id];
                    };
                }
                else if (scope.field.type == 'radio') {
                    scope.isChecked = function (answers, variant) {
                        if (typeof(answers) == 'undefined') {
                            if (variant.default) scope.answers[scope.field.id] = variant.id;
                            return variant.default;
                        }
                        else {
                            return answers == variant.id;
                        }
                    };
                }
            }
        }
    })

    .directive('distance', function () {
        return {
            restrict: 'E',
            templateUrl: 'distance.html',
            link: function (scope, element, attrs) {
                var distance = scope.distance;

                if (distance > 1000) {
                    distance = Math.round(distance / 10) / 100 + 'км';
                }
                else {
                    distance = distance + 'м';
                }

                scope.distance = distance;
            }
        };
    })

    .directive('locateButton', function () {
        return {
            restrict: 'E',
            templateUrl: 'locate_button.html',
            link: function (scope, element, attrs) {
                var answers = scope.answers[scope.field.id];

                scope.locating = false;
                scope.located = answers && typeof(answers.latitude) !== 'undefined' && typeof(answers.longitude) !== 'undefined';
                scope.locationError = false;

                scope.getCurrentLocation = function () {
                    scope.locating = true;
                    scope.located = false;
                    scope.answers[scope.field.id] = null;

                    Geolocator.position(
                        function (coords, timestamp) {
                            scope.locating = false;
                            scope.located = true;

                            var location = {
                                longitude: null,
                                latitude: null,
                                accuracy: null
                            };

                            location.longitude = coords.longitude;
                            location.latitude = coords.latitude;
                            location.accuracy = coords.accuracy;

                            scope.answers[scope.field.id] = location;
                            scope.$apply();
                        },
                        function () {
                            scope.locationError = true;
                            scope.locating = false;
                            scope.located = false;

                            scope.answers[scope.field.id] = null;
                            scope.$apply();
                        }
                    );
                }
            }
        };
    })

    .directive('audioButton', function () {
        return {
            restrict: 'E',
            templateUrl: 'audio_button.html',
            link: function (scope, element, attrs) {

            }
        };
    })

    .directive('imageButton', function () {
        return {
            restrict: 'E',
            templateUrl: 'image_button.html',
            link: function (scope, element, attrs) {
                scope.processing = false;
                scope.url = false;

                var onSuccess = function (imageURI) {
                    scope.answers[scope.field.id] = imageURI;
                    scope.url = imageURI;
                    scope.processing = false;

                    setTimeout(function () {
                        scope.$apply();
                    })
                };

                var onFail = function (message) {
                    scope.url = false;
                    scope.processing = false;

                    setTimeout(function () {
                        scope.$apply();
                    })
                }

                scope.takePicture = function () {
                    scope.processing = true;
                    setTimeout(function() {
                        scope.$apply();
                    });

                    navigator.camera.getPicture(
                        onSuccess,
                        onFail,
                        {
                            quality: 51,
                            allowEdit: true,
                            correctOrientation: true,
                            encodingType: Camera.EncodingType.JPEG,
                            destinationType: Camera.DestinationType.FILE_URI
                        }
                    );
                }

            }
        };
    })

    .directive('videoButton', function () {
        return {
            restrict: 'E',
            templateUrl: 'video_button.html',
            link: function (scope, element, attrs) {

            }
        };
    })

    .directive('loading', function () {
        return {
            restrict: 'E',
            templateUrl: 'loading.html',
            scope: {},
            link: function (scope, element, attrs) {
                var element = angular.element(element);

                scope.$on('loading.start', function () {
                    element.removeClass('fade-out');
                });

                scope.$on('loading.complete', function () {
                    element.addClass('fade-out');
                });
            }
        }
    })
    .directive('taskBlock', function () {
        return {
            restrict: 'E',
            templateUrl: 'task_block.html'
        };
    })
    .directive('taskField', function () {
        return {
            restrict: 'E',
            templateUrl: 'task_field.html'
        };
    })
    .directive('taskCurrent', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'task_current.html'
        };
    })
    .directive('taskNotFound', function () {
        return {
            restrict: 'E',
            templateUrl: 'task_not_found.html'
        };
    })
    .directive('taskDone', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'task_done.html'
        };
    })
    .directive('taskSynced', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'task_synced.html'
        };
    })
    .directive('taskApproved', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'task_approved.html'
        };
    })

    .directive('footerAcceptMission', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_accept_mission.html'
        };
    })

    .directive('footerTask', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_task.html'
        };
    })

    .directive('footerEmpty', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_empty.html'
        };
    })

    .directive('footerTaskInfo', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_task_info.html'
        };
    })


    .directive('footerLogout', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_logout.html'
        };
    })

    .directive('footerCloseModal', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'footer_close_modal.html'
        };
    })

    .directive('missionListItem', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'mission_list_item.html'
        };
    })

    // TODO if element seen
    .directive('missionSeen', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var timeout;
                var interval = element.attr('mission-seen') ? 500 : 5000;

                scope.$on('$destroy', function () {
                    clearTimeout(timeout);
                });

                timeout = setTimeout(function () {
                    if (scope.mission) {
                        Seen(scope.mission.mission_id, MissionModel);
                        scope.updateBadges();
                    }
                }, interval);
            }
        }
    })

    .directive('taskSeen', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var timeout;
                var interval = element.attr('task-seen') ? 500 : 5000;

                scope.$on('$destroy', function () {
                    clearTimeout(timeout);
                });

                timeout = setTimeout(function () {
                    Seen(scope.task.id + '', TaskModel);
                    scope.updateBadges();
                }, interval);
            }
        }
    })

    .directive('taskListItem', function () {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'task_list_item.html',
            link: function (scope, element) {
                if (scope.task && scope.task.status == '') scope.task.status = 'current';
            }

        };
    })
    .directive('autosizeable', ['$document', function ($document) {
        return function (scope, element, attr) {
            element.autosize();
        };
    }])

    .directive('phonify', ['$document', function ($document) {
        return function (scope, element, attr) {
//            TODO angular ui-mask
            $(element).mask("+7 (999) 999-99-99");
        };
    }])

    .directive('timer', function () {
        return {
            restrict: 'E',
            templateUrl: 'timer.html',
            link: function (scope, element, attrs) {
                var task = scope.task || scope.mission;
                new Timer(element, task.time_left);
            }
        };
    })

    .directive('appTabs', function () {
        return {
            restrict: 'A',
            scope: {},
            link: function (scope, element, attrs) {
                element.removeAttr('app-tabs');

                scope.$on('tabs.hide', function () {
                    element.find('.tabs').addClass('tabs-item-hide');
                    $('.has-tabs').removeClass('has-tabs').addClass('_has-tabs');
                });

                scope.$on('tabs.show', function () {
                    element.find('.bar-footer').hide();

                    element.find('.tabs').removeClass('tabs-item-hide');
                    $('._has-tabs').removeClass('_has-tabs').addClass('has-tabs');
                });
            }
        }
    })

    .directive('time', function () {
        return {
            restrict: 'E',
            templateUrl: 'time.html',
            link: function (scope, element, attrs) {
                var task = scope.task || scope.mission;
                new Time(element, task.time_left);
            }
        };
    })

    .directive('noTabs', function ($timeout) {
        return {
            restrict: 'E',
            link: function (scope, element, attrs) {
                element.find('.bar-footer').hide();

                $timeout(function () {
                    element.find('.has-tabs').removeClass('has-tabs').addClass('_has-tabs');
                    var view = element.find('.has-footer').removeClass('has-footer').addClass('_has-footer');

                    element.find('.bar-footer').slideDown(200, function () {
                        view.removeClass('_has-footer').addClass('has-footer');
                    });
                });
            }
        };
    })

    .directive('dynamicName', function ($compile, $parse) {
        return {
            restrict: 'A',
            terminal: true,
            priority: 100000,
            link: function (scope, elem) {
                var name = $parse(elem.attr('dynamic-name'))(scope);

                elem.removeAttr('dynamic-name');
                elem.attr('name', name);
                $compile(elem)(scope);
            }
        };
    })
;