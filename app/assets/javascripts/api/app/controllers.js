angular.module('MC.controllers', [])
    .controller('RecoverCtrl', function ($http, $scope, $ionicModal, $rootScope, $state, $ionicLoading, $ionicViewService, UserService) {
        //TODO save login

        var isRecovering = false
            ;

        $scope.user = { login: '' };

        $scope.isRecovering = function () {
            console.log('isRecovering');
            return isRecovering;
        };

        $rootScope.hideTabs();

        $scope.recover = function () {
            isRecovering = true;

//            UserService.login($scope.user,
//                function () {
//                    //TODO get token
//                    $rootScope.currentUser = UserService.currentUser();
//                    $http.defaults.headers.common["X-Auth-Token"] = $rootScope.currentUser.auth_token;
//                    $http.defaults.headers.common["X-Auth-Login"] = $rootScope.currentUser.phone;
//
//                    $rootScope.locationUpdater();
//                    $rootScope.updateBadges();
//                    $state.go('tabs.missions');
//
//                    $scope.$broadcast('login.enable');
//                    isLoggingIn = false;
//                },
//                function (message) {
//                    alertify.error(message);
//                    $scope.$broadcast('login.enable');
//                    isLoggingIn = false;
//                }
//            );
        }

        $scope.recover = function (form) {
            if (form.$valid) $scope.recover();
        }
    })

    .controller('LoginCtrl', function ($http, $scope, $ionicModal, $rootScope, $state, $ionicLoading, $ionicViewService, UserService) {
        //TODO save login
        $scope.user = {login: '', password: ''};
        var isLoggingIn = false;

        $scope.isLogingIn = function () {
            return isLoggingIn;
        };

        $rootScope.hideTabs();

        $scope.login = function () {
            isLoggingIn = true;

            UserService.login($scope.user,
                function () {
                    //TODO get token
                    $rootScope.currentUser = UserService.currentUser();
                    $http.defaults.headers.common["X-Auth-Token"] = $rootScope.currentUser.auth_token;
                    $http.defaults.headers.common["X-Auth-Login"] = $rootScope.currentUser.phone;

                    $rootScope.locationUpdater();
                    $rootScope.updateBadges();
                    $state.go('tabs.missions');

                    $scope.$broadcast('login.enable');
                    isLoggingIn = false;
                },
                function (message) {
                    alertify.error(message);
                    $scope.$broadcast('login.enable');
                    isLoggingIn = false;
                }
            );
        }

        $scope.checkAndGo = function (form) {
            if (form.$valid) $scope.login();
        }
    })


    .controller('MissionsCtrl', function ($scope, $timeout, $state, $http, $rootScope, $ionicModal, $ionicLoading, $ionicViewService, MissionsService, UserService) {
        $ionicViewService.clearHistory();
        $rootScope.showTabs();

        profileModal($scope, $rootScope, $state, $ionicModal, UserService);

        var load = function (callback) {
            MissionModel
                .all()
                .filter('login', '=', $rootScope.currentUser.phone)
                .filter('mission_id', 'not in (?)', 'select mission_id from tasks')
                .and(new persistence.PropertyFilter('type', '!=', 'sensus'))
                .or(new persistence.PropertyFilter('type', '=', 'sensus'))
//
                .order('distance')
                .list(function (results) {
                    var missions = [];
                    results.forEach(function (mission) {
                        missions.push(MissionsMapper.data(mission));
                    });

                    $scope.missions = MissionsMapper.categories(missions);

                    if (callback) callback();
                })
            ;
        };

        $scope.onRefresh = function () {
            new SyncAll($http, function () {
                $rootScope.updateBadges();

                load(function () {
                    $scope.$apply();
                    $scope.$broadcast('scroll.refreshComplete');
                });

            });
        };

        $scope.$broadcast('loading.start');
        load(function () {
            setTimeout(function () {
                $scope.$apply();
                $scope.$broadcast('loading.complete');
            })

        });
    })
    .controller('TaskCtrl', function ($scope, $state, $ionicModal, $rootScope, $ionicViewService, $stateParams, $location, $ionicScrollDelegate, $ionicLoading, TasksService, templateLayoutFactory) {
        $rootScope.hideTabs();
        var taskId = $stateParams.taskId;

        taskInfoModal($scope, $ionicModal, templateLayoutFactory);

        $scope.goBack = function () {
            $state.go('tabs.tasks');
        };

        $scope.answers = {};

        var showLoading = function () {
            $scope.loading = $ionicLoading.show({
                content: 'Загрузка задания...',
                maxWidth: 200
            });
        };

        var hideLoading = function (scope) {
            $scope.loading.hide();
        };

        var load = function (callback) {
            TaskModel.load(taskId, function (entity) {
                if (entity) {
                    $scope.task = TasksMapper.data(entity);
                    console.log($scope.task);

                    MissionModel.load(entity.mission_id, function (mission) {
                        $scope.mission = MissionsMapper.data(mission);
                    });

                    if ($scope.task.status == 'current' || $scope.task.status == '') {
                        var builder = new TaskBuilder(entity, $scope);

                        builder.currentStep(function (entity, step_id) {
                            $scope.task = entity;
                            $scope.step_id = step_id;
                            callback();
                        });

                        $scope.lastStep = function () {
                            return builder.lastStep();
                        };

                        $scope.canGoBack = function () {
                            return builder.canGoBack();
                        };

                        $scope.backClicked = function () {
                            showLoading();
                            builder.stepBack(function (entity, step_id) {
                                $scope.task = entity;
                                $scope.step_id = step_id;
                                callback();

                                hideLoading();
                            });
                        };

                        $scope.next = function () {
                            showLoading();
                            builder.nextStep(function (entity, step_id) {
                                $scope.task = entity;
                                $scope.step_id = step_id;
                                callback();
                                hideLoading();
                            });
                        };
                    }
                    else {
                        callback();
                    }
                }
                else {
                    $scope.notFound = true;
                }
            })
        }

        $scope.$broadcast('loading.start');

        load(function () {
            setTimeout(function () {
                $scope.$apply();
                $scope.$broadcast('loading.complete');
                $ionicScrollDelegate.scrollTop(false);
            });
        });

        // ACCEPT CURRENT STEP, MOVE TO NEXT STEP
        $scope.nextClicked = function (form) {
            if (form.$valid) $scope.next();
        };

        // UPLOAD TASK
        $scope.upload = function () {
            //TODO update user data
            TaskSend($scope, $state, TasksService, $ionicLoading);
        };

        // SAVE FIELD VALUE
        $scope.update = function (field) {
            var fieldId = field.id,
                taskId = $scope.task.id,
                stepId = $scope.task.step.id,
                value = $scope.answers[fieldId],
                removeAnswer = false
                ;

            if (!stepId)
            {
                console.log('NO STEP LOADED');
                return;
            }

            if (value == undefined) removeAnswer = true;

            value = field.type == 'checkbox' || field.type == 'location' ? JSON.stringify(value) : value;

            var values = removeAnswer
                    ? {id: [taskId, fieldId].join('@'), _removed: true}
                    : {id: [taskId, fieldId].join('@'), field_id: fieldId, step_id: stepId, task_id: taskId, value: value, type: field.type}
                ;

            new Sync(
                AnswerModel,
                [values],
                AnswersMapper,
                function () {
//                    console.log('answer.saved!');
                },
                false
            );
        };

        // CANCEL TASK
        $scope.cancel = function () {
            //TODO update user data
            var task = $scope.task;

            //TODO confirm
            if (confirm('Вы действительно хотите отменить это задание?')) {
                TaskModel.load($scope.task.id + '', function (row) {
                    TasksService.cancel($scope.task.id, function () {
                        persistence.remove(row);
                        persistence.flush(function () {
                            AnswerModel
                                .all()
                                .filter('task_id', '=', $scope.task.id)
                                .destroyAll();

                            $rootScope.updateBadges();
                            $state.go('tabs.tasks');
                        });
                    });
                });
            }
        };

        $scope.remove = function () {
            var task = $scope.task;
            //TODO update user data

            //TODO confirm
            if (confirm('Вы действительно хотите удалить это задание из списка?')) {
                TaskModel.load($scope.task.id + '', function (row) {
                    row.removed = true;

                    persistence.flush(function () {
                        AnswerModel
                            .all()
                            .filter('task_id', '=', $scope.task.id)
                            .destroyAll()
                        ;

                        $rootScope.updateBadges();
                        $state.go('tabs.tasks');
                    });
                });
            }
        };
    })
    .controller('MissionCtrl', function ($scope, $rootScope, $state, $ionicViewService, $stateParams, $location, $ionicLoading, MissionsService, templateLayoutFactory) {
        $rootScope.hideTabs();
        var missionId = $stateParams.missionId;

        $scope.goBack = function () {
            $state.go('tabs.missions');
        }

        missionMap($scope, templateLayoutFactory);

        var showLoading = function () {
            $scope.loading = $ionicLoading.show({
                content: 'Загрузка задания...',
                maxWidth: 200
            });
        };

        var hideLoading = function (scope) {
            $scope.loading.hide();
        };

        $scope.start = function () {
            showLoading();
            MissionsService.start(
                missionId,
                function (task) {
                    setTimeout(function () {
                        $rootScope.updateBadges();
                        $state.go('tabs.task', {taskId: task.id})
                    });

                    hideLoading();
                },
                function (result) {
                    if (result.remove === true) {
                        MissionModel.load(missionId, function (mission) {
                            persistence.remove(mission);
                            //TODO текст для удаленного задания: типа.. задание более недоступно, удаляем из списка!
                            alertify.error('Задание недоступно!');
                            $state.go('tabs.missions');
                        })
                    }
                    else {
                        alertify.error('Произошла ошибка!');
                    }

                    hideLoading();
                }
            );
        };

        var load = function (callback) {
            MissionModel.load(missionId, function (mission) {
                if (mission)
                    $scope.mission = MissionsMapper.data(mission);
                else
                    $scope.notFound = true;

                if (callback) callback();
            })
        }

        $scope.$broadcast('loading.start');

        load(function () {
            setTimeout(function () {
                $scope.showMap = true;
                $scope.$apply();
                $scope.$broadcast('loading.complete');
            });
        })
    })
    .controller('MapTabCtrl', function ($scope, $state, $ionicModal, $rootScope, $location, templateLayoutFactory, UserService) {
        profileModal($scope, $rootScope, $state, $ionicModal, UserService);

        $rootScope.showTabs();
        $scope.$broadcast('loading.start');

        //TODO from currentUser
        $scope.map = {
            center: $rootScope.currentUser.location.geometry.coordinates,
            zoom: 10,
            location: $rootScope.currentUser.location
        };

        var load = function (callback) {
            MissionModel
                .all()
                .filter('target_id', 'is not', 'null')
                .filter('id', 'not in (?)', 'select mission_id from tasks')
                .filter('login', '=', $rootScope.currentUser.phone)
                .list(function (results) {
                    var targets = [];

                    results.forEach(function (target) {
                        targets.push(MapMapper.data(target));
                    });

                    $scope.map = $.extend($scope.map, {targets: targets});

                    if (callback()) callback();
                })
            ;
        }

        load(function () {
            setTimeout(function () {
                $scope.$apply();
            });
        })

        var setSize = function (container) {
            var mapEl = $('#tasks-map'),
                viewport = mapEl.closest('.scroll-content')
                ;

            mapEl.css({width: viewport.width(), height: viewport.height(), display: 'block'});
        }

        $scope.beforeMapInit = function () {
            setSize();
        };

        $scope.afterMapInit = function (map) {
            YMap(map, $scope, templateLayoutFactory, $rootScope);
            $scope.$broadcast('loading.complete');
        };
    })
    .controller('TasksCtrl', function ($scope, $rootScope, $rootScope, $state, $http, $ionicModal, UserService, $ionicViewService) {
        $ionicViewService.clearHistory();
        profileModal($scope, $rootScope, $state, $ionicModal, UserService);

        $rootScope.$broadcast('tabs.show');
        $scope.$broadcast('loading.start');

        var load = function (callback) {
            TaskModel
                .all()
                .filter('mission_id', 'in (?)', 'select id from missions')
                .filter('removed', '=', false)
                .filter('login', '=', $rootScope.currentUser.phone)
                .list(function (results) {
                    var tasks = [];

                    results.forEach(function (task) {
                        tasks.push(task.toJSON());
                    });

                    $scope.tasks = TasksMapper.categories(tasks);

                    if (callback()) callback();
                })
            ;
        }

        load(function () {
            setTimeout(function () {
                $scope.$apply();
                $scope.$broadcast('loading.complete');
            });
        })

        $scope.onRefresh = function () {
            new SyncAll($http, function () {
                $rootScope.updateBadges();

                load(function () {
                    $scope.$apply();
                    $scope.$broadcast('scroll.refreshComplete');
                });
            });
        };
    })
    .controller('ProfileCtrl', function ($scope) {

    });
;

var profileModal = function ($scope, $rootScope, $state, $ionicModal, UserService) {
    $ionicModal.fromTemplateUrl(
        'profile.html',
        function (modal) {
            $scope.modal = modal;
        },
        {
            scope: $scope,
            animation: 'slide-in-up'
        }
    );

    $scope.logout = function () {
        //TODO logout
        UserService.logout(function () {
            $state.go('login');
        });
    }

    $scope.openModal = function () {
        $scope.profile = $rootScope.currentUser;
        $scope.modal.show();
    };

    $scope.closeModal = function () {
        $scope.modal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });

    if ($scope.rightButtons == undefined)
        $scope.rightButtons = [];

    $scope.rightButtons.push(
        {
            type: 'button-clear',
            content: '<i class="icon ion-ios7-person"></i>',
            tap: function (e) {
                $scope.openModal();
            }
        }
    );
}

var taskInfoModal = function ($scope, $ionicModal, templateLayoutFactory) {
    var target;

    missionMap($scope, templateLayoutFactory);

    $ionicModal.fromTemplateUrl(
        'task_info.html',
        function (modal) {
            $scope.modal = modal;
        },
        {
            scope: $scope,
            animation: 'slide-in-up'
        }
    );

    $scope.openModal = function () {
        $scope.modal.show();
        $scope.$broadcast('map.reload');
    };

    $scope.closeModal = function () {
        $scope.modal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });


    if ($scope.rightButtons == undefined)
        $scope.rightButtons = [];

    $scope.rightButtons.push(
        {
            type: 'button-clear',
            content: '<i class="icon ion-ios7-information"></i>',
            tap: function (e) {
                $scope.openModal();
            }
        }
    );
}

var missionMap = function ($scope, templateLayoutFactory) {
    var $target;

    $scope.$on('map.reload', function () {
        if ($target) {
            $target.container.fitToViewport();
            $scope.afterMapInit($target);
        }
    });

    $scope.afterMapInit = function (map) {
        $target = map;

        var map_el = $(map.container.getElement()).parent(),
            viewport = map_el.closest('.mission-map')
            ;

        YMap(map, $scope, templateLayoutFactory);

        if ($scope.mission.target) {
            var objects = [], center = [];

            if ($scope.mission.target) {
                objects.push($scope.mission.target);
                center.push($scope.mission.target.geometry.coordinates);
            }

            if ($scope.currentUser.location) {
                objects.push($scope.currentUser.location);
                center.push($scope.currentUser.location.geometry.coordinates);
            }

            if (map_el.size()) {

                if (center[0][0] > center[1][0])
                    center = center.reverse();

                var params = ymaps.util.bounds.getCenterAndZoom(
                    center,
                    [map_el.width(), map_el.height()]
                );

                $scope.mission.center = params.center;
                $scope.mission.zoom = params.zoom;
                map.setZoom(params.zoom - 1);
            }
        }
    };
}

var TaskSend = function (scope, state, TaskService, $ionicLoading) {
    AnswerModel
        .all()
        .filter('task_id', '=', scope.task.id)
        .list(function (results) {
            var answers = {},
                files = []
            ;

            results.forEach(function (row) {
                if (row.type == 'image')
                    files.push({field_id: row.field_id, url: row.value});
                else
                    answers[row.field_id] = row.value;
            });

            TaskService.send($ionicLoading, scope, scope.task.id, files, answers, function (update) {
                TaskModel.load(scope.task.id, function (task) {
                    task.status = 'synced';
                    persistence.add(task);

                    persistence.flush(function () {
                        $.extend(scope.currentUser, update.user);
                        scope.loading.hide();
                        state.go('tabs.tasks');
                    });
                });
            }, function (message) {
                alert('Ошибка выгрузки задачи: ' + message);
                scope.loading.hide();
            });
        });
}