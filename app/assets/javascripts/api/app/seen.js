var Seen = function(id, entity) {
    if (!id) return;

    entity.load(id, function(item) {
        item.seen = true;
        persistence.add(item);
        persistence.flush();
    });
};