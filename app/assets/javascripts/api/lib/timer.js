var Timer = function (el, seconds) {

    var now = new Date().getTime() / 1000;

    var countdown = function () {
        var passed = seconds + Math.round(now - new Date().getTime() / 1000);


        el.toggleClass('time-warning', passed < 3600);
        el.toggleClass('time-danger', passed < 60);

        el.html(moment.duration(passed, "seconds").humanize());

        setTimeout(function () {
            if (passed > 0) countdown();
        }, 1000);
    }

    countdown();
}

var toHHMMSS = function (_seconds) {
    var sec_num = parseInt(_seconds, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    var time = [];

    time.push(hours);
    time.push(minutes);
    time.push(seconds);

    return time.join(':');
}