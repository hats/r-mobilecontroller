var Geolocator = {
    position: function(onSuccess, onError) {
        var onError = onError || function() { alertify.error('Служба геолокации недоступна...') }
        var onSuccess = onSuccess || function() { alertify.notice('Положение определено успешно!') };;

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    onSuccess(position.coords, position.timestamp)
                },
                function() {
                    onError();
                }
            );
        }
        else {
            onError();
        }
    }
}