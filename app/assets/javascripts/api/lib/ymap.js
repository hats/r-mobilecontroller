var YMap = function(map, $scope, templateLayoutFactory, $rootScope) {
    $scope.overrides={
        build: function () {
            var BalloonContentLayout = templateLayoutFactory.get('targetBaloon');
            BalloonContentLayout.superclass.build.call(this);
            angular.element($('[data-mission]')).bind('click', this.onMissionClick);

            var missionId = $('[data-mission]').first().data('mission');

            if (missionId) {
                Seen(missionId, MissionModel);
                $scope.updateBadges();
            }
        },

        onMissionClick: function() {
            var mission = $('[data-mission]').first().data('mission');
            window.location.href="#/missions/" + mission;
        },

        clear: function () {
            angular.element($('[data-mission]')).unbind('click', this.onMissionClick)
            var BalloonContentLayout = templateLayoutFactory.get('targetBaloon');
            BalloonContentLayout.superclass.clear.call(this);
        }
    };
}