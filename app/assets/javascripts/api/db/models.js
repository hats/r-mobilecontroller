var MissionModel = persistence.define('Missions', {
    mission_id: 'TEXT',
    project_id: 'INTEGER',
    task_id: 'INTEGER',
    target_id: 'INTEGER',

    seen: 'BOOL',
    login: 'TEXT',

    name: "TEXT",
    type: "TEXT",
    address: "TEXT",
    description: "TEXT",
    done: "TEXT",

    distance: 'TEXT',
    important: "BOOL",

    rating: "INTEGER",
    price: "INTEGER",

    time_to_complete: "INTEGER",
    time_left: "INTEGER",

    latitude: "TEXT",
    longitude: "TEXT"
});

var TaskModel = persistence.define('Tasks', {
    mission_id: 'TEXT',
    task_id: 'INTEGER',
    steps: 'TEXT',

    seen: 'BOOL',
    login: 'TEXT',

    name: "TEXT",
    type: "TEXT",
    address: "TEXT",

    rating: "INTEGER",
    price: "INTEGER",

    status: "TEXT",

    time_to_complete: "INTEGER",
    time_left: "INTEGER",
    removed: 'BOOL'
});

var StepModel = persistence.define('Steps', {
    task_id: "INTEGER",
    name: "TEXT",
    description: "TEXT",
    sorting_order: "INTEGER",
    content: "TEXT"
});

var AnswerModel = persistence.define('Answers', {
    type: "TEXT",
    field_id: "INTEGER",
    step_id: "TEXT",
    task_id: "TEXT",
    value: "TEXT"
});