persistence.debug = false;

var DB = {
    name: 'mc',
    version: '1.0',
    description: 'MobileController DB',
    size: 100 * 1024 * 1024,

    init: function (callback) {
//        persistence.store.cordovasql.config(
//            persistence,
//            DB.name,
//            DB.version,
//            DB.description,
//            DB.size,
//            0
//        );
        persistence.store.websql.config(
            persistence,
            DB.name,
            DB.version,
            DB.description,
            DB.size
        );

        // before migrations init
        persistence.migrations.init(function() {
            console.log('MIGRATIONS STARTED');
        });

        // after migrate
        persistence.migrate(function() {
            console.log('MIGRATIONS COMPLETED');
            callback();
        });

//        persistence.store.cordovasql.config(
//            persistence,
//            DB.name,
//            DB.version,
//            DB.description,             // DB version
//            DB.size,
//            0                       // SQLitePlugin Background processing disabled
//        );
    }
}


