var Prefetch = function ($rootScope, onSuccess) {
    var db = {
            steps: null
        },
        self = this
    ;


    this.getSteps = function(results, callback) {
        var steps = {
            items: [],
            byTask: {},
            ids: {}
        }

        results.forEach(function (row) {
            var step = StepsMapper.data(row);
            steps.items.push(step);
            steps.ids[step.id] = step;

            if (undefined === steps.byTask[step.task_id]) steps.byTask[step.task_id] = { items: [], ids: {}};
            steps.byTask[step.task_id].items.push(step);
            steps.byTask[step.task_id].ids[step.id] = step;
        });

        db.steps = steps;
        callback();
    };

    persistence.asyncForEach([StepModel],
        function(entity, next) {
            var name = entity.meta.name;

            entity.all().order('sorting_order').list(function (results) {
                self['get' + name](results, function() {
                    next();
                });
            });
        },
        function() {
            $rootScope.db = db;
            onSuccess();
        }
    );

//    MissionModel.all().list(function (results) {
//        var missions = {},
//            targets = []
//            ;
//
//        results.forEach(function (row) {
//            var mission = MissionsMapper.data(row);
//            missions[mission.mission_id] = mission;
//
//            if (mission.target_id) {
//                var target = MapMapper.data(row);
//                targets.push(target);
//            }
//        });
//
//        db.missions = missions;
//        db.targets = targets;
//        ready.missions = true;
//        callback();
//    });
//
//    TaskModel.all().list(function (results) {
//        var tasks = {};
//
//        results.forEach(function (task) {
//            var task = TasksMapper.data(task);
//            tasks[task.id] = task;
//        });
//
//        db.tasks = tasks;
//        ready.tasks = true;
//        callback();
//    });
}