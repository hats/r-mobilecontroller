var MissionsMapper = {
    categories: function (missions) {
        var missionCategories =
        {
            important: [],
            targeted: [],
            common: []
        }

        for (var missionId in missions) {
            var mission = missions[missionId],
                noTask = true
                ;

            if (mission.target_id)
                missionCategories.targeted.push(mission);
            else if (mission.important === 'true' || mission.important === true)
                missionCategories.important.push(mission);
            else
                missionCategories.common.push(mission);

        }
        ;

        return missionCategories;
    },

    // to DB
    row: function (data) {
        var data = data || {};

        var attributes = {
            id: data.id,
            mission_id: data.id,
            project_id: data.project_id,
            task_id: data.task_id,

            type: data.type,

            name: data.name,
            description: data.description,
            done: data.done,

            important: data.important,
            login: data.login,

            time_to_complete: data.time_to_complete,
            time_left: data.time_left,

            rating: data.rating,
            price: data.price
        };

        if (data.target_id) {
            attributes.target_id = data.target_id;
            attributes.address = data.address;
            attributes.distance = data.distance;
            attributes.longitude = data.longitude;
            attributes.latitude = data.latitude;
        }

        return attributes;
    },
    data: function (row) {
        var row = row || {};

        var target = null,
            attributes = {
                mission_id: row.mission_id,
                project_id: row.project_id,
                task_id: row.task_id,

                seen: row.seen,

                type: row.type,

                name: row.name,
                description: row.description,
                done: row.done,
                important: row.important,
                login: row.login,


                time_to_complete: row.time_to_complete,
                time_left: row.time_left,

                rating: row.rating,
                price: row.price
            }
            ;

        if (row.target_id / 1) {
            var add = {
                target_id: row.target_id,
                address: row.address,
                distance: row.distance,
                target: {
                    geometry: {
                        type: 'Point',
                        coordinates: [row.longitude / 1, row.latitude / 1]
                    }
                }
            };

            attributes = $.extend(attributes, add);
        }

        return attributes;
    }
};

var MapMapper = {
    data: function (row) {
        var row = row || {};
        return {
            mission_id: row.mission_id,
            properties: {
                mission_id: row.mission_id,
                name: row.name,
                address: row.address,
                rating: row.rating,
                price: row.price,
                description: row.description,
                baloonLayout: 'targetBaloon'
            },

            geometry: {
                type: 'Point',
                coordinates: [row.longitude / 1, row.latitude / 1]
            }
        }
    }
};

var UserMapper = {
    data: function (row) {
        var row = row || {},
            attributes = {}
            ;

        var keys = ['full_name', 'approved', 'done', 'balance', 'rating', 'email', 'phone', 'auth_token']

        for (var key in keys)
        {
            var attribute = keys[key],
                value = row[attribute];

            if (value != undefined) attributes[attribute] = value;
        }


        if (row.latitude && row.longitude)
            attributes.location = {
                geometry: {
                    type: 'Point',
                    coordinates: [row.longitude / 1, row.latitude / 1]
                }
            }

        return attributes;
    }
};

var TasksMapper = {
    categories: function (tasks) {
        var taskCategories = {
            current: [],
            done: [],
            synced: [],
            approved: []
        };

        for (var taskId in tasks) {
            var task = tasks[taskId];

            if (task.status == 'done')
                taskCategories.done.push(task);
            else if (task.status == 'synced')
                taskCategories.synced.push(task);
            else if (task.status == 'approved')
                taskCategories.approved.push(task);
            else
                taskCategories.current.push(task);
        }
        ;

        return taskCategories;

    },
    row: function (data) {
        var data = data || {};

        var properties = {
            id: data.id,
            mission_id: data.mission_id,
            task_id: data.task_id,

            type: data.type,
            name: data.name,

            time_to_complete: data.time_to_complete,
            time_left: data.time_left,
            login: data.login,

            rating: data.rating,
            price: data.price
        };

        if (data.status) properties.status = data.status;
        if (data.status != 'approved' && data.status != 'synced')
            properties.removed = 0;

        if (data.status != '' && data.status != 'current')
            properties.seen = 1;

        return properties;
    },

    data: function (row) {
        var row = row || {};

        var steps = [];
        if (row.steps) steps = row.steps.split(',');

        return {
            id: row.id,
            mission_id: row.mission_id,
            task_id: row.task_id,
            steps: steps,

            seen: row.seen,
            removed: row.removed,

            type: row.type,
            name: row.name,
            status: row.status,
            login: row.login,

            completed_for: row.completed_for,

            time_to_complete: row.time_to_complete,
            time_left: row.time_left,

            rating: row.rating,
            price: row.price
        }
    }
};


var StepsMapper = {
    // to db
    row: function (data) {
        var data = data || {};

        return {
            id: data.id,
            task_id: data.task_id,
            name: data.name,
            description: data.description,
            content: JSON.stringify(data.content),
            sorting_order: data.sorting_order
        }

    },

    // to template
    data: function (row) {
        var row = row || {},
            blocks = row.content
            ;

        try {
            blocks = JSON.parse(row.content)
        }
        catch (e) {

        }

        return {
            id: row.id,
            task_id: row.task_id,
            name: row.name,
            description: row.description,
            blocks: blocks,
            sorting_order: row.sorting_order
        }
    }
};

var AnswersMapper = {
    row: function (data) {
        var data = data || {}
            ;

        return {
            id: data.id,
            task_id: data.task_id,
            step_id: data.step_id,
            type: data.type,
            field_id: data.field_id,
            value: data.value
        }
    },
    data: function (row) {
        var row = row || {}
            ;

        return {
            id: row.id,
            task_id: row.task_id,
            step_id: row.step_id,
            type: row.type,
            field_id: row.field_id,
            value: null
        }
    }
};