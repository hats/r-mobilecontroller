var Sync = function (entity, results, mapper, callback, syncRemote) {
    var toRemove = [],
        toInsert = [],
        ids = [],
        lookup = {},
        syncRemote = syncRemote == undefined ? true : syncRemote,
        meta = entity.meta,
        fieldSpec = meta.fields
        ;

    results.forEach(function (item) {
        if (item._removed)
            toRemove.push(item.id);
        else {
            ids.push(item.id);
            lookup[item.id] = item;
        }
    });

    entity.all().list(function (results) {
        var updatesToPush = []
            ;

        results.forEach(function (row) {
            if (row) {
                var remoteData = mapper.row(lookup[row.id]),
                    updates;

                if (lookup[row.id]) {

                    delete lookup[row.id];

                    for (var p in fieldSpec) {

                        if (remoteData[p] != undefined && remoteData[p] != row[p]) {
                            var update = {
                                property: p,
                                value: remoteData[p]
                            };

                            if (!updates) {
                                updates = {
                                    row: row,
                                    changes: []
                                };
                            }
                            updates.changes.push(update);
                        }
                    }

                    if (updates) updatesToPush.push(updates);
                }
                else if (syncRemote) {
                    toRemove.push(row.id);
                }
            }
        });

        for (var id in lookup)
            if (!lookup[id]._removed)
                toInsert.push(id);


        entity.all().filter('id', 'in', toRemove).destroyAll(function() {
            persistence.asyncForEach(toInsert, function (id, next) {
                var item = new entity(mapper.row(lookup[id]));
                persistence.add(item).flush(function () {
                    next();
                });
            }, function () {
//                console.log('UPDATES: ', updatesToPush);

                updatesToPush.forEach(function (item) {
                    item.changes.forEach(function (change) {
                        var property = change.property,
                            value = change.value
                            ;

                        item.row[property] = value;
                    })

                    persistence.add(item.row);
                });

                persistence.flush(function () {
                    callback();
                })
            });
        });

    })
};