persistence.defineMigration(1, {
    up: function () {
        this.createTable('missions', function (table) {
            table.text('mission_id');
            table.integer('project_id');
            table.integer('task_id');
            table.integer('target_id');

            table.integer('seen');
            table.text('login');

            table.text('name');
            table.boolean('important');
            table.text('type');

            table.text('address');
            table.text('description');
            table.text('done');

            table.integer('rating');
            table.integer('price');

            table.integer('time_to_complete');
            table.integer('time_left');

            table.integer('distance');

            table.text('latitude');
            table.text('longitude');
        });
    }
});

persistence.defineMigration(2, {
    up: function () {
        this.createTable('tasks', function (table) {
            table.text('mission_id');
            table.integer('task_id');

            table.integer('seen');
            table.text('login');
            table.integer('removed');

            table.text('name');
            table.text('type');
            table.text('address');

            table.text('status');
            table.text('steps');

            table.integer('rating');
            table.integer('price');

            table.integer('time_to_complete');
            table.integer('time_left');
        });
    }
});

persistence.defineMigration(3, {
    up: function () {
        this.createTable('steps', function (table) {
            table.integer('task_id');

            table.text('name');
            table.text('content');

            table.text('description');
            table.integer('sorting_order');
        });
    }
});

persistence.defineMigration(4, {
    up: function () {
        this.createTable('answers', function (table) {
            table.integer('field_id');
            table.text('type');
            table.text('task_id');
            table.text('step_id');
            table.text('value');
        });
    }
});