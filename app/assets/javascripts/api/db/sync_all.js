var SyncAll = function ($http, onSyncComplete) {
    var toSync = ['missions', 'tasks', 'steps'];
    var ready = {};

    var complete = function () {
        for (var scope in ready)
            if (ready[scope] === false) return;

        console.log('SYNC COMPLETE');
        onSyncComplete();
    }

    $http({method: 'POST', url: DOMAIN + '/api/sync', data: {lastSync: Math.random()}})
        .success(function (results) {
            var syncs = [];

            for (var key in results)
                if (toSync.indexOf(key) >= 0) {
                    ready[key] = false;
                    syncs.push({key: key, items: results[key]});
                }

            persistence.asyncForEach(syncs, function (item, next) {
                var key = item.key,
                    items = item.items,
                    entity = window[[key.capitalize().replace(/s$/, ''), 'Model'].join('')],
                    mapper = window[[key.capitalize(), 'Mapper'].join('')]
                    ;

                new Sync(
                    entity,
                    items,
                    mapper,
                    function () {
                        ready[key] = true;
                        next();
                    }
                );

            }, function () {
                complete();
            })
        })
        .error(function () {
            alertify.error('Ошибка запроса');
        })
    ;
}