var PhoneInput = function(el) {
    el.mask("+7 (999) 999-99-99");
}

$(document).on('page:load ready', function() {
    $('[data-type=phone]').each(function() {
        new PhoneInput($(this));
    });
});