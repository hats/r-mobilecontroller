//= require jquery
//= require jquery_ujs
//= require jquery.ui.draggable
//= require alertify
//= require bootstrap
//= require bootstrap-datepicker/core
//= require bootstrap-datepicker/locales/bootstrap-datepicker.ru.js
//= require fullcalendar
//= require select2
//= require shared/message
//= require shared/autosize
//= require shared/persistent-tabs
//= require_tree ./client/

