var Message = function(form) {
    var input = form.find('[data-message=input]');

    input.on('keydown', function(e) {
        if (!(e.ctrlKey || e.altKey) && e.keyCode == 13)
        {
            e.preventDefault();
            input.trigger('blur');
            form.submit();

            return false;
        }
    });
};

$(function() {
    $('[data-type=message]').each(function() {
        new Message($(this));
    });
});