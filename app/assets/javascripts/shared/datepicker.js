var Datepicker = function(el) {
    el.datepicker({
        language: "ru-RU",
        format: "dd.mm.yyyy"
    });
};

$(function() {
    $("[data-behaviour=datepicker]").each(function() {
        new Datepicker($(this));
    });
});