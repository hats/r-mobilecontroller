$(function () {
    var tab_name = ['lastTab'];

    if (_rails_route) tab_name.push(_rails_route);
    var tabKey = tab_name.join(':');

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        localStorage.setItem(tabKey, $(e.target).attr('href'));
    });

    var lastTab = localStorage.getItem(tabKey);

    var activateTab = function (href) {
        var elements = $('a[data-toggle="tab"][href=' + href + ']');
        if (elements.size() > 0)elements.tab('show');
        return elements;
    };

    if (location.hash) {
        if (activateTab(location.hash).size() == 0) activateTab(lastTab);
        else location.hash = '';
    }
    else activateTab(lastTab)

});