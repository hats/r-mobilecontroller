var Tree = function(el) {


    var key = el.data('tree'),
        storage = JSON.parse(localStorage.getItem('trees:' + key)) || []
    ;

    var toggle = function(handle, immediate) {
        var children = handle.closest('li.parent_li').find('> ul'),
            children_li = children.find('> li'),
            immediate = undefined != immediate ? immediate : false,
            key = handle.data('key')
            ;

        if (children.is(":visible"))
        {
            storage = jQuery.grep(storage, function(value) {
                return value != key;
            });

            if (immediate)
            {
                children.hide();
                children_li.hide();
            }
            else
            {
                children_li.hide('fast', function() {
                    children.hide();
                });
            }

            handle
                .addClass('glyphicon-folder-close')
                .removeClass('glyphicon-folder-open');
        }
        else
        {
            storage.push(key);

            if (immediate)
            {
                children.show();
                children_li.show();
            }
            else {
                children.show();
                children_li.show('fast');
            }

            handle
                .addClass('glyphicon-folder-open')
                .removeClass('glyphicon-folder-close');
        }
    };

    el.find('li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');

    el.find('li.parent_li [data-tree=handle]').on('click', function (e) {
        toggle($(this));
        e.stopPropagation();
        localStorage.setItem('trees:' + key, JSON.stringify(storage));
    });

    $.each(storage, function(i) {
        toggle(el.find('li.parent_li [data-tree=handle][data-key=' + storage[i] + ']'), true);
    });
};


$(function () {
    $('[data-type=tree]').each(function() {
        new Tree($(this));
    });
});