var Select = function (el) {
    el.select2();
};

$(document).on('ready page:load', function () {
    $('select').each(function () {
        new Select($(this));
    })
})