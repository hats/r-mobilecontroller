var Nested = function(el) {
    var items_query = [
        '> tbody[data-nested=item]',
        '> tbody > tr[data-nested=item]',
        '> [data-nested=item]'
    ],
        handle = el.find(items_query).find('[data-nested=handle]')
    ;

    var fixHelper = function(e, ui) {
        var children = ui.children();

        if (ui.is('tbody'))
        {
            children = ui.children('tr').children('td');
//            ui = ui.children('tr:first-child');
        }

        children.each(function() {
            $(this).width($(this).width());
        });

        return ui;
    };

    var options = {
        items: items_query.join(','),
        axis: 'y',
        placeholder: "placeholder",
        // connectWith: '[data-type=nested]',
        helper: fixHelper,
        forcePlaceholderSize: true,
        update: function(e, ui) {
            var items = [];
            el.find(items_query.join(',')).each(function() {
                items.push($(this).data('nested-id'));
            });

            console.log(items);

            $.post(el.data('nested-url'), {order: items});
        },
        start: function(e, ui) {
            ui.item.find('> td, > tr > td').each(function() {
                $(this).width($(this).width());
            });
        }
    };

    if (handle.size() > 0) options.handle = handle;

    el.sortable(options).disableSelection();
};


$(function() {
    $('[data-type=nested]').each(function() {
        new Nested($(this));
    });
});