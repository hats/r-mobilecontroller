var Redactor = function (el) {
    var buttons = [],
        csrf_token = $('meta[name=csrf-token]').attr('content'),
        csrf_param = $('meta[name=csrf-param]').attr('content'),
        params
        ;

    if (csrf_param !== undefined && csrf_token !== undefined) {
        params = csrf_param + "=" + encodeURIComponent(csrf_token);
    }

    var options = {
        wym: true,
        buttonsAdd: buttons,
        imageUpload: REDACTOR.urls.pictures + "?" + params,
        imageGetJson: REDACTOR.urls.pictures,
        fileUpload: REDACTOR.urls.documents + "?" + params,
        fileGetJson: REDACTOR.urls.documents,
        path: "/assets/redactor-rails"
    };

    if (el.data('minheight'))
        options['minHeight'] = el.data('minheight')

    el.redactor(options);
}

$(function () {
    $('[data-action=redactor]').each(function () {
        new Redactor($(this));
    })
});