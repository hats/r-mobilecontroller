module ApplicationHelper
  def save_or_update(entity)
    entity.new_record? ? 'Добавить' : 'Сохранить'
  end

  def priority(priority)
    if priority.weight == 0
      badge_class = 'danger'
    elsif priority.weight == 10
      badge_class = 'warning'
    elsif priority.weight == 100
      badge_class = 'default'
    end

    badge_label :label, priority.name, badge_class
  end

  def route_name
    Rails.application.routes.router.recognize(request) do |route, _|
      return route.name.to_sym if route.present? and route.name.present?
    end
  end

  def js_route_name
    content_tag :script do
      "var _rails_route = '#{route_name}'".html_safe
    end
  end
end
