module BootstrapHelper
  def centered(text)
    content_tag :div, text, class: 'text-center'
  end

  def muted(text='')
    content_tag :div, text, class: 'text-muted'
  end

  def pencil_icon
    content_tag(:i, '', class: 'glyphicon glyphicon-pencil')
  end

  def message_icon
    content_tag(:i, '', class: 'glyphicon glyphicon-comment')
  end

  def checked_icon
    content_tag(:i, '', class: 'glyphicon glyphicon-check')
  end

  def unchecked_icon
    content_tag(:i, '', class: 'glyphicon glyphicon-unchecked')
  end

  def trash_icon
    content_tag(:i, '', class: 'glyphicon glyphicon-trash')
  end

  def list_icon(text='')
    icon = [content_tag(:i, '', class: 'glyphicon glyphicon-list')]
    icon += ['&nbsp;'.html_safe, text.to_s] if text.present?
    icon.join.html_safe
  end

  def ok_icon(text=nil)
    icon = [content_tag(:i, '', class: 'glyphicon glyphicon-ok')]
    icon += ['&nbsp;'.html_safe, text.to_s] if text.present?
    icon.join.html_safe
  end

  def record_icon(text=nil)
    icon = [content_tag(:i, '', class: 'glyphicon glyphicon-record')]
    icon += ['&nbsp;'.html_safe, text.to_s] if text.present?
    icon.join.html_safe
  end

  def cog_icon
    content_tag(:i, '', class: 'glyphicon glyphicon-cog')
  end

  def clone_icon
    content_tag(:i, '', class: 'glyphicon glyphicon-transfer')
  end

  def screenshot_icon
    content_tag(:i, '', class: 'glyphicon glyphicon-screenshot')
  end

  def plus_icon(text=nil)
    icon = [content_tag(:i, '', class: 'glyphicon glyphicon-plus')]
    icon << ['&nbsp;', text.to_s].join if text.present?
    icon.join.html_safe
  end

  def picture_field(form, entity, options={})
    render 'bootstrap/picture_field', form: form, entity: entity, options: options
  end

  def file_field(form, options={})
    options = { key: :file }.merge(options)
    render 'bootstrap/file_field', form: form, options: options
  end
end