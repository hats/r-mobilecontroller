class TargetImporter
  def initialize(project)
    @project = project
    @parsed = 0
    @added = 0
  end

  def import(file)
    tmp = file.tempfile
    file = File.join('tmp', file.original_filename)
    FileUtils.cp(tmp.path, file)

    @xls = Roo::Excel.new(file)
    (2..@xls.last_row).each do |row|
      parse_row(row)
    end

    FileUtils.rm file
  end

  def parsed
    @parsed
  end

  def added
    @added
  end

  private
  def parse_row(row)
    name = @xls.cell('A', row).squish
    address = @xls.cell('B', row).squish

    target = Target.find_or_initialize_by name: name, address: address
    @project.targets << target

    @parsed += 1
    @added += 1 if target.new_record?
  end
end