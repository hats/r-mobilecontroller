class Field::Location < Field
  def self.field
    'Гео-точка'
  end

  def self.field_type
    'location'
  end
end