class Field::Text < Field
  def self.field
    'Текст'
  end

  def self.field_type
    'text'
  end

end