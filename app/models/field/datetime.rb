class Field::Datetime < Field
  def self.field
    'Время и дата'
  end

  def self.field_type
    'date_time'
  end
end