class Field::Checkbox < Field
  include WithVariants

  def self.field
    'Один или несколько из списка'
  end

  def self.field_type
    'checkbox'
  end
end