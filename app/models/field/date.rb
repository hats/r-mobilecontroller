class Field::Date < Field
  def self.field
    'Дата'
  end

  def self.field_type
    'date'
  end
end