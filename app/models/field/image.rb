class Field::Image < Field
  def self.field
    'Изображение'
  end

  def self.field_type
    'image'
  end
end