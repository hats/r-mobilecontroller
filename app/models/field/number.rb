class Field::Number < Field
  def self.field
    'Число'
  end

  def self.field_type
    'number'
  end
end