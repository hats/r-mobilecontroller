class Field::String < Field
  def self.field
    'Строка'
  end

  def self.field_type
    'string'
  end
end