class Field::Video < Field
  def self.field
    'Видео'
  end

  def self.field_type
    'video'
  end
end