class Field::Audio < Field
  def self.field
    'Аудио'
  end

  def self.field_type
    'audio'
  end
end