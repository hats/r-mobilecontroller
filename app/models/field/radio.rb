class Field::Radio < Field
  include WithVariants

  def self.field
    'Один из списка'
  end

  def self.field_type
    'radio'
  end
end