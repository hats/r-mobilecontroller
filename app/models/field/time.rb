class Field::Time < Field
  def self.field
    'Время'
  end

  def self.field_type
    'time'
  end
end