class Field::Toggle < Field
  def self.field
    'Выключатель'
  end

  def self.field_type
    'toggle'
  end
end