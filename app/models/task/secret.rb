class Task::Secret < Task
  def self.function
    'Тайный покупатель'
  end

  def self.human_type
    'secret'
  end
end