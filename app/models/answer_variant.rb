class AnswerVariant < ActiveRecord::Base
  belongs_to :user_task_answer, :inverse_of => :answer_variants
  belongs_to :variant

  validates :user_task_answer, :variant, presence: true
end