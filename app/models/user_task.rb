class UserTask < ActiveRecord::Base
  acts_as_paranoid
  include Tokenable

  belongs_to :user, class_name: 'User::Audit'
  belongs_to :operator, class_name: 'User::Operator'

  belongs_to :task, class_name: 'Task', foreign_key: :task_id, inverse_of: :user_tasks
  belongs_to :target, class_name: 'Target', foreign_key: :target_id, inverse_of: :user_tasks

  belongs_to :step
  belongs_to :task_status

  alias_attribute :rating, :penalty_rating
  alias_attribute :price, :penalty_price

  has_many :user_task_answers, autosave: true

  validates :user, :task, :task_status, presence: true

  #TODO sensus!
  validates_uniqueness_of :task, scope: [:target, :user, :deleted_at]

  delegate :name, :type, :human_type, :rating, :price, :description, :project_id, :time_to_complete, to: :task, allow_nil: true
  delegate :address, to: :target, allow_nil: true

  delegate :project_name, to: :task, allow_nil: true

  scope :current, -> { joins(:task_status).where(task_status: TaskStatus::New.first) }
  scope :done, -> { joins(:task_status).where(task_status: TaskStatus::Done.first) }
  scope :approved, -> { joins(:task_status).where(task_status: TaskStatus::Approved.first) }

  before_validation(on: :create) do
    self.task_status = TaskStatus::New.first
  end

  #after_initialize do
  #  self.step = self.task.steps.first unless self.step.present?
  #end

  def answers
    user_task_answers.index_by(&:field_id)
  end

  def next_step
    if self.step.last?
      finalize
    else
      self.step = task.next_step(self.step)
    end
  end

  def finalize
    self.task_status = TaskStatus::Done.first
  end

  def is_done?
    self.task_status == TaskStatus::Done.first
  end

  def is_current?
    self.task_status == TaskStatus::New.first
  end

  def is_approved?
    self.task_status == TaskStatus::Approved.first
  end

  def approve(operator)
    operator = operator
    self.task_status = TaskStatus::Approved.first
  end

  def penalty_rating
    (task.rating.to_f * (1 - penalty.to_f/100)).to_i
  end

  def penalty_price
    (task.price.to_f * (1 - penalty.to_f/100)).to_i
  end

  def time_left
    (created_at + time_to_complete.hours - Time.now).seconds if time_to_complete.present?
  end

  def status
    return 'current' if is_current?
    return 'approved' if is_approved?
    return 'synced' if is_done?
  end

  def steps
    task.steps.flatten
  end

  def blocks
    steps.map(&:blocks).flatten
  end

  def fields
    blocks.map(&:fields).flatten
  end

  def json_attributes
    unless destroyed?
      attributes = {
          id: token,
          mission_id: missionify.id,
          task_id: task_id,
          name: name,
          type: human_type,
          price: penalty_price,
          rating: penalty_rating
          #TODO
          #time_left: time_left,
          #time_to_complete: time_to_complete,
      }

      attributes[:status] = status unless is_current?
    else
      attributes = { _removed: true }
    end

    attributes
  end

  def update_values(values)
    values.each_pair do |key, value|
      task_answer = UserTaskAnswer.find_or_initialize_by(user_task: self, field_id: key)
      task_answer.answer = value
      task_answer.save!
    end
  end

  def uuid
    key = ['user_task', task_id]
    key << target_id if target.present?
    Digest::MD5.hexdigest key.join
  end

  def missionify
    Mission.new(user, task, target)
  end
end