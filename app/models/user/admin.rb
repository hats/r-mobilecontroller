class User::Admin < User
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :authentication_keys => [:login]

  validates :login, presence: true, uniqueness: true

  def self.human_type
    'Админ'
  end
end