class User::Operator < User
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :authentication_keys => [:login]

  validates :login, presence: true, uniqueness: true

  has_many :feedbacks

  def self.human_type
    'Оператор'
  end
end