class User::Audit < User
  include TokenAuthenticable

  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         authentication_keys: [:login]

  validates_confirmation_of :confirmation_token

  has_many :user_tasks, foreign_key: :user_id
  has_many :completed_tasks, -> { approved }, class_name: 'UserTask'
  has_many :feedbacks, foreign_key: :user_id

  # TODO telephone number validation
  validates :login, presence: true, uniqueness: true
  validates :password,
            # you only need presence on create
            :presence => {:on => :create},
            # allow_nil for length (presence will handle it on create)
            :length => {:minimum => 6, :allow_nil => true},
            # and use confirmation to ensure they always match
            :confirmation => true

  before_validation :cleanup_login

  def task_aggregator
    @task_aggregator = TaskAggregator.new(self) unless @task_aggregator.present?
    @task_aggregator
  end

  def coords
    [latitude, longitude]
  end

  def locate(params)
    params = params.to_h
    params.symbolize_keys!
    update(params.select { |k, v| [:latitude, :longitude].include?(k) })
  end

  def cleanup_login
    self[:login] = self.class.cleanup_login(self[:login])
  end

  def self.cleanup_login(login)
    login.gsub /\D/, ''
  end

  def email
    profile.email
  end

  def phone(formatted=false)
    if formatted
      ['+', login[0], ' (', login[1, 3], ') ', login[4, 3], '-', login[7, 2], '-', login[9, 2]].join
    else
      login
    end

  end

  def rating
    #TODO refactor
    user_tasks.approved.sum(&:penalty_rating)
  end

  def balance
    #TODO refactor
    user_tasks.approved.sum(&:penalty_price)
  end

  def approved_tasks
    user_tasks.approved.size
  end

  def done_tasks
    tasks_done
  end

  def tasks_done
    user_tasks.done.size
  end



  def tasks_info
    {
        'user' => {
            'done' => task_aggregator.done.size,
            'approved' => task_aggregator.approved.size
        }
    }
  end

  def self.generate(length=6)
    length.times.map { |i| rand(9) }.join
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    conditions[:login] = self.cleanup_login(conditions[:login])
    where(conditions).first
  end

  def self.human_type
    'Агент'
  end
end