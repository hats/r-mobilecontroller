class User::Client < User
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         #:confirmable,
         :trackable,
         :authentication_keys => [:login]

  validates :login, presence: true, uniqueness: true
  has_many :projects
  has_many :feedbacks, foreign_key: :user_id

  def to_s
    full_name
  end

  def email
    profile.email
  end

  def full_name
    self.profile.name || self.login
  end

  def self.human_type
    'Клиент'
  end
end