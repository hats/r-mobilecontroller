class Feedback < ActiveRecord::Base
  acts_as_paranoid

  has_many :messages
  belongs_to :user
  belongs_to :operator, class_name: 'User::Operator', foreign_key: :operator_id

  scope :unaccepted, -> { where(operator_id: nil) }

  validate :user, :content, presence: true

  #TODO order by last message

  def available_for?(for_user)
    true if for_user == operator or for_user == user
  end
end