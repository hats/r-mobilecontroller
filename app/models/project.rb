class Project < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client', foreign_key: :client_id
  validates :name, :client, presence: true

  has_many :targets
  has_many :tasks

  scope :active, -> { where('start_date <= ? and end_date >= ?', Time.now.to_date, Time.now.to_date) }
  scope :archive, -> { where('end_date < ?', Time.now.to_date) }
  scope :coming, -> { where('start_date > ?', Time.now.to_date) }

  def build_project_task(params={})
    @task = Task.new(params)
    @task.project = self
    @task
  end

  def completed_for
    (tasks.sum(&:completed_for)/tasks.size.to_f * 100).to_i
  end
end
