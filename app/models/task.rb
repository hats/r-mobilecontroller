class Task < ActiveRecord::Base
  acts_as_paranoid

  include Sortable
  default_scope { unscoped.order('sorting_order asc') }

  belongs_to :project
  belongs_to :priority

  has_and_belongs_to_many :targets

  has_many :steps, -> { order(:sorting_order) }
  has_many :user_tasks

  scope :sorted, -> { joins(:priority).order('priorities.weight') }
  scope :active, -> { joins(:project).where('projects.start_date <= ? and projects.end_date >= ?', Time.now.to_date, Time.now.to_date) }

  scope :without_targets, -> { where('tasks.id not in (select task_id from targets_tasks)') }
  scope :with_targets, -> { joins(:targets) }

  scope :important, -> { where(priority: Priority::Major.first) }
  scope :normal, -> { where(priority: Priority::Normal.first) }

  #TODO unaccepted tasks
  scope :unaccepted, ->(target=nil) {
    if target.present?
      where('tasks.id not in (select task_id from user_tasks where target_id=?)', target.id)
    else
      where('tasks.id not in (select task_id from user_tasks where target_id is null)')
    end
  }

  delegate :name, to: :project, prefix: true, allow_nil: true

  validates_presence_of :priority, :type

  def function
    self.class.function
  end

  def next_step(step)
    _next = step
    steps.each_cons(2) { |n| _next = n[1] if n[0] == step }
    _next
  end

  def important?
    priority.is_a?(Priority::Major)
  end

  def last_step?(step)
    steps.to_a.last == step
  end

  def self.types
    hash = {}

    %w(Task::Basic Task::Sensus Task::Secret).each do |task|
      hash[task] = task.constantize.function
    end
    hash
  end

  def human_type
    self.class.human_type
  end

  def time_left
    #TODO from time_to_complete
    (project.end_date.to_time - Time.now).seconds if project.present?
  end

  def completed_user_tasks
    user_tasks.where(task_status: TaskStatus::Approved.first)
  end

  def fields
    fields = []

    steps.each do |step|
      step.blocks.each do |block|
        fields += block.fields
      end
    end

    fields
  end

  def completed_for
    if targets.present?
      completed_user_tasks.size.to_f/targets.size.to_f
    else
      completed_user_tasks.size.to_f
    end
  end
end