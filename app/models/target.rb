class Target < ActiveRecord::Base
  has_and_belongs_to_many :tasks
  belongs_to :project

  has_many :user_tasks

  geocoded_by :address, lookup: :yandex   # can also be an IP address
  after_validation :geocode, if: ->(obj){ obj.address.present? and obj.address_changed? }

  #default_scope { order(:name) }
  scope :active, -> { joins(:project).where('projects.start_date <= ? and projects.end_date >= ?', Time.now.to_date, Time.now.to_date) }

  def self.nearest(coords, range=10)
    # ORDER by distance
    near(coords, range, units: :km, order: 'distance')
  end

  def coords
    [latitude, longitude]
  end

  def map_params
    {
        name: name,
        coords: coords
    }.to_json
  end

  def gmaps_link
    ['http://www.google.com/maps/place', address, ('@' + [latitude, longitude, '14z'].join(','))].join '/'
  end
end
