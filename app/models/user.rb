class User < ActiveRecord::Base
  acts_as_paranoid

  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :authentication_keys => [:login]

  has_one :profile
  accepts_nested_attributes_for :profile

  #default_scope { joins('left join profiles on profiles.user_id = users.id').order('profiles.first_name, profiles.last_name') }

  def profile
    super || build_profile(user: self)
  end

  def full_name
    self.profile.full_name
  end

  def human_type
    self.class.human_type
  end

  def self.human_type
    'Юзер'
  end
end
