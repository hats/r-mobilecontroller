class Priority < ActiveRecord::Base
  validates :name, :type, :weight, presence: true

  scope :sorted, -> { order(:weight) }
end