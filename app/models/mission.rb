class Mission
  @@sep = '_'

  delegate :name, :rating, :price, :description, :done, :time_to_complete, :time_left, :destroyed?, to: :task
  delegate :project_id, to: :task

  delegate :id, to: :task, prefix: true, allow_nil: true

  delegate :address, :coords, :latitude, :longitude, to: :target, allow_nil: true
  delegate :id, to: :target, prefix: true, allow_nil: true

  def initialize(user, task, target=nil)
    @user = user
    @task = task
    @target = target
  end

  def task
    @task
  end

  def target
    @target
  end

  def distance(user)
    [(target.distance_from(user.coords, :km) * 100).round].join if target.present?
  end

  def type
    task.human_type
  end

  def _lastChange
    if target.present?
      [task[:updated_at], target[:updated_at]].max
    else
      task[:updated_at]
    end
  end

  # _removed will remove mission from list!
  def attributes
    unless destroyed?
      attributes = {
          id: id,
          distance: distance(@user),
          description: (description.cleanup_html if description.present?),
          done: (done.cleanup_html if done.present?),
          important: important?,
          _removed: destroyed?
      }

      #time_to_complete time_left
      %w(project_id task_id target_id name address type rating price latitude longitude).each do |a|
        attributes[a] = send(a) unless send(a).nil?
      end

      # TODO absolete
    else
      attributes = {_removed: true}
    end

    attributes
  end

  def important?
    task.important?
  end

  def id
    param = [task_id]
    param << target_id if target_id.present?
    param.join @@sep
  end

  def self.get(user, param)
    task, target = param.split @@sep
    task = Task.find task if task.present?
    target = Target.find target if target.present?

    new(user, task, target)
  end

  def to_param
    id
  end
end