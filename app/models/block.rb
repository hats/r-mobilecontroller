class Block < ActiveRecord::Base
  acts_as_paranoid

  include Sortable
  default_scope { unscoped.order('sorting_order asc') }

  belongs_to :step
  has_many :fields

  def json_attributes
    unless destroyed?
      {
          name: name,
          description: description.cleanup_html,
          fields: fields.map(&:json_attributes)
      }
    end
  end
end