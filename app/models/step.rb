class Step < ActiveRecord::Base
  acts_as_paranoid
  include Sortable
  include Tokenable

  default_scope { unscoped.order('sorting_order asc') }

  belongs_to :task
  has_many :blocks

  def last?
    task.last_step?(self)
  end

  def content
    blocks.map(&:json_attributes).flatten
  end

  def json_attributes
    unless destroyed?
    {
        id: token,
        task_id: task_id,
        name: name,
        description: description.cleanup_html,
        content: content,
        sorting_order: sorting_order,
    }
    else
      {
          _removed: true
      }
    end
  end

  def required
    fields = {}

    blocks.each do |block|
      block.fields.each do |field|
        fields[field.id] = field.field_type if field.required?
      end
    end

    fields
  end
end