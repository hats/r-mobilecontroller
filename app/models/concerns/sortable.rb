module Sortable
  extend ActiveSupport::Concern



  included do
    default_scope unscoped.order('sorting_order desc')
    before_save :set_sorting_order

    def set_sorting_order
      if new_record?
        last = self.class.all.last
        sorting_order = last.sorting_order + 1 if last.present?
      end
    end
  end
end