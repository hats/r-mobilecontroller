# Dont forget to add to your model
#   default_scope unscoped.order('sorting_order asc')

module WithVariants
  extend ActiveSupport::Concern

  included do
    has_many :variants, foreign_key: :field_id, autosave: true
  end
end