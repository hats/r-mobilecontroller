class Profile < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :user

  has_attached_file :picture,
                    styles: { medium: '300x300>', thumb: '100x100>', preview: '150x150#' },
                    default_url: '/images/:style/missing.png'

  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/

  validates_confirmation_of :agreement

  validates_presence_of :sex, :agreement, :birth_date, :first_name, :last_name, :email, if: -> { user.is_a? User::Audit }

  def full_name
    full_name = [first_name, last_name]
    full_name.reject!{|c| !c.present? }
    full_name.present? ? (full_name.join ' ') : user.login
  end
end
