class UserTaskAnswer < ActiveRecord::Base
  belongs_to :user_task
  belongs_to :field
  belongs_to :option
  belongs_to :radio_variant, class_name: 'Variant', foreign_key: :radio_answer

  has_many :answer_variants, autosave: true, :inverse_of => :user_task_answer

  validates :user_task, :field, presence: true

  has_attached_file :image_answer, styles: {
      thumb: '100x100#',
      preview: '300x300#'
  }

  validates_attachment_content_type :image_answer, content_type: /\Aimage\/.*\Z/

  attr_accessor :answer

  def to_s
    answer
  end

  def answer(raw=false)
    value = read_attribute [get_key, 'answer'].join('_')

    if field.date?
      return value.strftime '%d.%m.%Y'
    elsif field.toggle?
      if raw
        return value
      else
        return value ? 'Да' : 'Нет'
      end
    elsif field.radio?
      if raw
        radio_variant.id
      else
        return radio_variant.label if radio_variant.present?
      end

    elsif field.variants?
      if raw
        return answer_variants.map(&:variant).map(&:id)
      else
        return answer_variants.map(&:variant).map(&:label).join ', '
      end
    elsif field.image?
      return self
    elsif field.location?
      return {latitude: latitude, longitude: longitude, accuracy: accuracy}
    else
      return value
    end
  end

  def answer=(answer)
    self.send([get_key, '_', 'answer', '='].join, answer)
  end

  def variants_answer=(answer)
    old_answer = answer

    begin
      answer = JSON.parse(answer)
    rescue
      answer = old_answer
    end

    answer.each_pair do |variant_id, value|
      if value == true
        if new_record?
          answer_variants.build(variant_id: variant_id)
        else
          answer_variants.find_or_create_by(variant_id: variant_id)
        end
      end
    end

    answer_variants.where('variant_id not in (?)', answer.keys).destroy_all
  end

  #def image_answer=(answer)
  #  abort answer.inspect
  #end

  def location_answer=(answer)
    old_answer = answer

    begin
      answer = JSON.parse(answer)
      answer.symbolize_keys!
    rescue
      answer = old_answer
    end

    if answer.is_a?(Hash)
      self[:latitude] = answer[:latitude]
      self[:longitude] = answer[:longitude]
      self[:accuracy] = answer[:accuracy]
    end
  end

  private
  def get_key
    if field.string?
      key = 'string'
    elsif field.radio?
      key = 'radio'
    elsif field.location?
      key = 'location'
    elsif field.variants?
      key = 'variants'
    elsif field.toggle?
      key = 'toggle'
    elsif field.date?
      key = 'datetime'
    elsif field.text?
      key = 'text'
    elsif field.image?
      key = 'image'
    else
      key = 'string'
    end

    key
  end

end