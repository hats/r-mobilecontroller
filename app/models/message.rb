class Message < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :user
  belongs_to :feedback

  validate :user, :content, :feedback, presence: true

  default_scope { order(:created_at) }

  def is_deleteable?
    true if Time.now < (self.created_at + 5.minutes)
  end

  def is_deleteable_by?(user)
    true if self.is_deleteable? and self.user == user
  end
end