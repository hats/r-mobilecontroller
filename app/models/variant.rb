class Variant < ActiveRecord::Base
  include Sortable
  default_scope unscoped.order('sorting_order asc')

  belongs_to :field, foreign_key: :field_id
  before_save :check_default, if: -> { field.one_default? and default == true }

  def check_default
    field.variants.where(default: true).reject { |v| v == self }.each { |v| v.update_attribute(:default, false) }
    field.save
  end

  def checked?(user_task=nil)
    if user_task.present?
      if field.variants?
        user_task_answer = user_task.user_task_answers.select { |uta| uta.field == field }.first
        answer_variants = user_task_answer.answer_variants if user_task_answer.present?

        if answer_variants.present?
          answer_variant = answer_variants.map(&:variant).select { |v| v == self }.first
          answer_variant.present?
        else
          default?
        end
      else
        user_task_answer = user_task.user_task_answers.select { |uta| uta.field == field }.first

        if user_task_answer.present? and user_task_answer.answer(true).present?
          user_task_answer.answer(true) == id
        else
          default?
        end
      end
    else
      default?
    end
  end

  def json_attributes
    attributes = {id: id}

    attributes[:label] = label if label.present?
    attributes[:default] = default? if default?

    attributes
  end
end