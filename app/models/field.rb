class Field < ActiveRecord::Base
  acts_as_paranoid

  include Sortable
  default_scope { unscoped.order('sorting_order asc') }

  belongs_to :block
  has_many :user_task_answers

  validates_presence_of :type

  def self.field
    'Поле'
  end

  def field
    self.class.field
  end

  def field_type
    self.class.field_type
  end

  def self.with_variants
    %w(Field::Checkbox Field::Radio)
  end

  def self.medias
    %w(Field::Image Field::Audio Field::Video)
  end

  def self.dates
    %w(Field::Date Field::Time)
  end

  def self.strings
    %w(Field::String)
  end

  def self.texts
    %w(Field::Text)
  end

  def self.numbers
    %w(Field::Number)
  end

  def self.locations
    %w(Field::Location)
  end

  def self.toggles
    %w(Field::Toggle)
  end

  def with_variants?
    self.class.with_variants.include?(self.class.name)
  end

  def number?
    self.class.numbers.include?(self.class.name)
  end

  def toggle?
    self.class.toggles.include?(self.class.name)
  end

  def string?
    self.class.strings.include?(self.class.name)
  end

  def radio?
    self.is_a?(Field::Radio)
  end

  def image?
    self.is_a?(Field::Image)
  end

  def one_default?
    self.is_a?(Field::Radio)
  end

  def text?
    self.class.texts.include?(self.class.name)
  end

  def date?
    self.class.dates.include?(self.class.name)
  end

  def media?
    self.class.medias.include?(self.class.name)
  end

  def variants?
    self.is_a?(Field::Checkbox)
  end

  def location?
    self.is_a?(Field::Location)
  end

  def answer(user_task, raw = false)
    user_task_answer = user_task_answers.find_by(user_task: user_task)
    answer = user_task_answer.answer(raw) if user_task_answer.present?
    answer = false if toggle? and !answer.present?
    answer
  end

  def json_attributes
    attributes = { id: id }
    attributes[:name] = name if name.present?
    attributes[:hint] = hint if hint.present?
    attributes[:type] = field_type if field_type.present?
    attributes[:required] = true if required?
    attributes[:description] = description.cleanup_html if description.present?

    attributes[:variants] = variants.map(&:json_attributes) if with_variants?

    attributes
  end

  def self.types
    hash = {}

    (strings + texts + locations + toggles + numbers + dates + medias + with_variants).each do |task|
      hash[task] = task.constantize.field
    end

    hash
  end
end