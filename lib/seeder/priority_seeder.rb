class PrioritySeeder
  def seed
    priorities = YAML::load_file File.join(Rails.root, 'db', 'seeds', 'priorities.yml')

    priorities.each do |p|
      priority = Priority.find_or_initialize_by type: p['type']

      if priority.update(p)
        STDOUT.puts "Priority #{priority.name} added!".green
      else
        STDOUT.puts "Error adding priority: #{priority.name}".red
        STDOUT.puts priority.errors.inspect
      end
    end
  end
end