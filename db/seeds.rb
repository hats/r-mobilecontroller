require 'colored'
require 'seeder/priority_seeder'

admins = YAML::load_file File.join(Rails.root, 'db', 'seeds', 'admins.yml')

admins.each do |a|
  admin = User::Admin.find_or_initialize_by login: a['login']
  if admin.update(a)
    STDOUT.puts "Admin #{admin.full_name} added!".green
  else
    STDOUT.puts "Error adding admin: #{admin.full_name}".red
    STDOUT.puts admin.errors.inspect
  end
end

if ActiveRecord::Base.connection.table_exists? 'task_statuses'
  task_statuses = YAML::load_file File.join(Rails.root, 'db', 'seeds', 'task_statuses.yml')

  task_statuses.each do |i|
    status = TaskStatus.find_or_initialize_by type: i['type']

    if status.update(i)
      STDOUT.puts "TaskStatus #{status.name} added!".green
    else
      STDOUT.puts "Error adding TaskStatus: #{status.name}".red
      STDOUT.puts admin.errors.inspect
    end
  end
end

if ActiveRecord::Base.connection.table_exists? 'priorities'
  PrioritySeeder.new.seed
end