# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140428031724) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answer_variants", force: true do |t|
    t.integer "variant_id",          null: false
    t.integer "user_task_answer_id", null: false
  end

  add_index "answer_variants", ["user_task_answer_id"], name: "index_answer_variants_on_user_task_answer_id", using: :btree
  add_index "answer_variants", ["variant_id"], name: "index_answer_variants_on_variant_id", using: :btree

  create_table "blocks", force: true do |t|
    t.integer  "step_id"
    t.string   "name",                      null: false
    t.text     "description"
    t.integer  "sorting_order", default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "feedbacks", force: true do |t|
    t.integer  "user_id",     null: false
    t.integer  "operator_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "feedbacks", ["operator_id"], name: "index_feedbacks_on_operator_id", using: :btree
  add_index "feedbacks", ["user_id"], name: "index_feedbacks_on_user_id", using: :btree

  create_table "fields", force: true do |t|
    t.integer  "block_id"
    t.string   "name",                          null: false
    t.string   "type",                          null: false
    t.boolean  "is_global",     default: false, null: false
    t.text     "description"
    t.text     "hint"
    t.integer  "sorting_order", default: 0,     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "required",      default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "messages", force: true do |t|
    t.integer  "feedback_id", null: false
    t.integer  "user_id",     null: false
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "messages", ["feedback_id"], name: "index_messages_on_feedback_id", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "options", force: true do |t|
    t.integer  "field_id"
    t.boolean  "is_default",    default: false, null: false
    t.string   "value",                         null: false
    t.text     "hint"
    t.integer  "sorting_order", default: 0,     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "priorities", force: true do |t|
    t.string  "name"
    t.string  "type"
    t.integer "weight", default: 999, null: false
  end

  create_table "profiles", force: true do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.text     "description"
    t.string   "sex"
    t.string   "address"
    t.date     "birth_date"
    t.boolean  "experience"
    t.boolean  "agreement"
  end

  create_table "project_tasks", force: true do |t|
    t.integer "task_id"
    t.integer "project_id"
    t.integer "price"
  end

  create_table "projects", force: true do |t|
    t.integer  "client_id"
    t.string   "name"
    t.text     "description"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "projects", ["deleted_at"], name: "index_projects_on_deleted_at", using: :btree

  create_table "redactor_assets", force: true do |t|
    t.integer  "user_id"
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "redactor_assets", ["assetable_type", "assetable_id"], name: "idx_redactor_assetable", using: :btree
  add_index "redactor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_redactor_assetable_type", using: :btree

  create_table "steps", force: true do |t|
    t.integer  "task_id"
    t.string   "name",                      null: false
    t.text     "description"
    t.integer  "sorting_order", default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "token"
  end

  create_table "targets", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "geocoded_manually", default: false, null: false
  end

  create_table "targets_tasks", force: true do |t|
    t.integer "target_id"
    t.integer "task_id"
  end

  create_table "task_statuses", force: true do |t|
    t.string   "name",                     null: false
    t.string   "type",                     null: false
    t.integer  "weight",     default: 999, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tasks", force: true do |t|
    t.string   "type"
    t.string   "name"
    t.integer  "price"
    t.text     "description"
    t.datetime "deleted_at"
    t.integer  "sorting_order",    default: 0, null: false
    t.integer  "rating",           default: 0, null: false
    t.integer  "project_id",                   null: false
    t.integer  "priority_id",                  null: false
    t.integer  "time_to_complete"
    t.text     "done"
  end

  add_index "tasks", ["deleted_at"], name: "index_tasks_on_deleted_at", using: :btree
  add_index "tasks", ["priority_id"], name: "index_tasks_on_priority_id", using: :btree

  create_table "user_task_answers", force: true do |t|
    t.integer  "user_task_id",                              null: false
    t.integer  "field_id",                                  null: false
    t.integer  "option_id"
    t.float    "number_answer"
    t.string   "string_answer"
    t.text     "text_answer"
    t.datetime "datetime_answer"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "toggle_answer",             default: false, null: false
    t.integer  "radio_answer"
    t.integer  "selectbox_answer"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "accuracy"
    t.string   "image_answer_file_name"
    t.string   "image_answer_content_type"
    t.integer  "image_answer_file_size"
    t.datetime "image_answer_updated_at"
  end

  create_table "user_tasks", force: true do |t|
    t.integer  "user_id",                    null: false
    t.integer  "task_id",                    null: false
    t.integer  "task_status_id",             null: false
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "step_id"
    t.integer  "penalty",        default: 0, null: false
    t.integer  "operator_id"
    t.integer  "target_id"
    t.string   "token"
  end

  add_index "user_tasks", ["operator_id"], name: "index_user_tasks_on_operator_id", using: :btree
  add_index "user_tasks", ["step_id"], name: "index_user_tasks_on_step_id", using: :btree

  create_table "users", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "type",                                null: false
    t.string   "login"
    t.datetime "deleted_at"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "authentication_token"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "variants", force: true do |t|
    t.integer "field_id",                      null: false
    t.string  "label",                         null: false
    t.boolean "default",       default: false, null: false
    t.integer "sorting_order", default: 999,   null: false
  end

  add_index "variants", ["field_id"], name: "index_variants_on_field_id", using: :btree

end
