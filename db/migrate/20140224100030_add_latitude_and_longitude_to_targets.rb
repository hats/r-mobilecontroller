class AddLatitudeAndLongitudeToTargets < ActiveRecord::Migration
  def change
    add_column :targets, :latitude, :float
    add_column :targets, :longitude, :float

    add_column :targets, :geocoded_manually, :boolean, default: false, null: false
  end
end
