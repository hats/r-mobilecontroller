class AddFieldsToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :sex, :string, default: nil
    add_column :profiles, :address, :string, default: nil
    add_column :profiles, :birth_date, :date, default: nil
    add_column :profiles, :experience, :boolean, default: nil
    add_column :profiles, :agreement, :boolean, default: nil

    Profile.reset_column_information
    add_column :profiles, :email, :string, default: nil unless Profile.column_names.include?('email')


  end
end
