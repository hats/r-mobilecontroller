class ChangeUsersAndProfiles < ActiveRecord::Migration
  def change
    remove_column :users, :name
    remove_column :users, :email
    remove_column :users, :description

    add_column :profiles, :name, :string
    add_column :profiles, :description, :text
  end
end
