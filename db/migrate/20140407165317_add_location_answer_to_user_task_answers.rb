class AddLocationAnswerToUserTaskAnswers < ActiveRecord::Migration
  def change
    add_column :user_task_answers, :latitude, :float
    add_column :user_task_answers, :longitude, :float
    add_column :user_task_answers, :accuracy, :integer
  end
end
