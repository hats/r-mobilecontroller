class AddTokenToStepsTasks < ActiveRecord::Migration
  def change
    add_column :steps, :token, :string
  end
end
