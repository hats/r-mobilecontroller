class UpdateUsersWithProfile < ActiveRecord::Migration
  def change
    remove_column :users, :first_name
    remove_column :users, :last_name
    drop_attached_file :users, :logo
  end
end
