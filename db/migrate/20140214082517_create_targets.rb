class CreateTargets < ActiveRecord::Migration
  def change
    create_table :targets do |t|
      t.string :name
      t.string :address
      t.references :project, index1: true

      t.timestamps
    end
  end
end
