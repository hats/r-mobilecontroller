class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :feedback, index: true, null: false
      t.references :user, index: true, null: false
      t.text :content

      t.timestamps
      t.timestamp :deleted_at
    end
  end
end
