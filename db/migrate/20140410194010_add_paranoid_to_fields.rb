class AddParanoidToFields < ActiveRecord::Migration
  def change
    add_column :fields, :deleted_at, :timestamp
  end
end
