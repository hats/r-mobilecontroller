class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.references :task
      t.string :name, null: false
      t.text :description
      t.integer :sorting_order, default: 0, null: false

      t.timestamps
    end
  end
end
