class CreateBlocks < ActiveRecord::Migration
  def change
    create_table :blocks do |t|
      t.references :step
      t.string :name, null: false
      t.text :description
      t.integer :sorting_order, default: 0, null: false

      t.timestamps
    end
  end
end
