class CreateProjectTasks < ActiveRecord::Migration
  def change
    create_table :project_tasks do |t|
      t.references :task, index1: true
      t.references :project, index1: true
      t.integer :price
    end
  end
end
