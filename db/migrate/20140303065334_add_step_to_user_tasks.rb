class AddStepToUserTasks < ActiveRecord::Migration
  def change
    add_reference :user_tasks, :step, index: true, null: true
  end
end
