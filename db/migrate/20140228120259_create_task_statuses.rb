class CreateTaskStatuses < ActiveRecord::Migration
  def change
    create_table :task_statuses do |t|
      t.string :name, null: false
      t.string :type, null: false
      t.integer :weight, default: 999, null: false

      t.timestamps
    end
  end
end
