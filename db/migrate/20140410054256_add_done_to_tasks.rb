class AddDoneToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :done, :text
  end
end
