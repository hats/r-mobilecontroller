require 'seeder/priority_seeder'

class AddNotNullToPriorityTask < ActiveRecord::Migration
  def up
    PrioritySeeder.new.seed

    priority = Priority::Normal.first
    if priority.present?
      Task.all.each do |t|
        t.update(priority: priority)
      end

      change_column :tasks, :priority_id, :integer, null: false
    else
      abort 'No priorities found. Run db:seed'
    end
  end

  def down
    # nothing to do here
  end
end
