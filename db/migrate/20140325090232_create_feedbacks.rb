class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.references :client, index: true, null: false
      t.references :operator, index: true, null: true, default: nil
      t.text :content

      t.timestamps
    end
  end
end
