class ChangeTaskToUserTaskInTaskAnswers < ActiveRecord::Migration
  def change
    rename_table :task_answers, :user_task_answers
    rename_column :user_task_answers, :task_id, :user_task_id
  end
end
