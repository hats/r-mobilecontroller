class AddSortingOrderToTasks < ActiveRecord::Migration
  def change
    remove_column :tasks, :weight
    add_column :tasks, :sorting_order, :integer, default: 0, null: false
  end
end
