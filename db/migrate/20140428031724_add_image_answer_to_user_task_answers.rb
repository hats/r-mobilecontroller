class AddImageAnswerToUserTaskAnswers < ActiveRecord::Migration
  def change
    add_attachment :user_task_answers, :image_answer
  end
end
