class AddToggleAnswerToAnswers < ActiveRecord::Migration
  def change
    add_column :user_task_answers, :toggle_answer, :boolean, default: false, null: false
  end
end
