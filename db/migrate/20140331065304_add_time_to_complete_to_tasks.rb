class AddTimeToCompleteToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :time_to_complete, :integer, default: nil
  end
end
