class FixTargetIdInUserTasks < ActiveRecord::Migration
  def change
    remove_column :user_tasks, :target_id
    change_table :user_tasks do |t|
      t.belongs_to :target, null: true, default: nil
    end
  end
end
