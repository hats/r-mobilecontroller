class RenameClientIdInFeedbacks < ActiveRecord::Migration
  def change
    rename_column :feedbacks, :client_id, :user_id
  end
end
