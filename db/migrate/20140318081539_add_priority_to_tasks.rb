class AddPriorityToTasks < ActiveRecord::Migration
  def change
    add_reference :tasks, :priority, index: true
  end
end
