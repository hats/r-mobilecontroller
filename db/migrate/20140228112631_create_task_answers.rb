class CreateTaskAnswers < ActiveRecord::Migration
  def change
    create_table :task_answers do |t|
      t.belongs_to :task, null: false
      t.belongs_to :field, null: false
      t.belongs_to :option, null: true, default: nil

      t.float :number_answer, null: true
      t.string :string_answer, null: true
      t.text :text_answer, null: true
      t.timestamp :datetime_answer, null: true

      t.timestamps
    end
  end
end
