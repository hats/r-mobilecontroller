class CreateTargetsTasks < ActiveRecord::Migration
  def change
    create_table :targets_tasks do |t|
      t.belongs_to :target
      t.belongs_to :task
    end
  end
end
