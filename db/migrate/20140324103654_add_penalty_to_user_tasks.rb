class AddPenaltyToUserTasks < ActiveRecord::Migration
  def change
    add_column :user_tasks, :penalty, :integer, default: 0, null: false
  end
end
