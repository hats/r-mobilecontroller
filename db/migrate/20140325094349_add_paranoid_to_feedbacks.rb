class AddParanoidToFeedbacks < ActiveRecord::Migration
  def change

    change_table :feedbacks do |t|
      t.timestamp :deleted_at, null: true, default: nil
    end
  end
end
