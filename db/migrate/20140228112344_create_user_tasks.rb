class CreateUserTasks < ActiveRecord::Migration
  def change
    create_table :user_tasks do |t|
      t.belongs_to :user, null: false
      t.belongs_to :task, null: false
      t.belongs_to :task_status, null: false

      t.timestamp :deleted_at
      t.timestamps
    end
  end
end
