class AddSortingOrderToVariants < ActiveRecord::Migration
  def change
    add_column :variants, :sorting_order, :integer, default: 999, null: false
  end
end
