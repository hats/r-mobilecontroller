class AddOperatorToUserTasks < ActiveRecord::Migration
  def change
    add_reference :user_tasks, :operator, index: true
  end
end
