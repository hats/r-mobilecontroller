class AddAuthTokenToAudit < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :authentication_token, unique: true
    end
  end
end
