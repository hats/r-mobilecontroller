class AddProjectToTasks < ActiveRecord::Migration
  def change
    add_reference :tasks, :project
    project = Project.first
    Task.all.each{|t| t.update(project: project) }

    change_column :tasks, :project_id, :integer, null: false
  end
end
