class CreatePriorities < ActiveRecord::Migration
  def change
    create_table :priorities do |t|
      t.string :name
      t.string :type
      t.integer :weight, default: 999, null: false
    end
  end
end
