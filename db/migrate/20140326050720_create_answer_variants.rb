class CreateAnswerVariants < ActiveRecord::Migration
  def change
    create_table :answer_variants do |t|
      t.references :variant, index: true, null: false
      t.references :user_task_answer, index: true, null: false
    end
  end
end
