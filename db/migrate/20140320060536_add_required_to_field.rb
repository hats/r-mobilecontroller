class AddRequiredToField < ActiveRecord::Migration
  def change
    add_column :fields, :required, :boolean, null: false, default: false
  end
end
