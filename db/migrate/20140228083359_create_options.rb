class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.references :field
      t.boolean :is_default, default: false, null: false
      t.string :value, null: false
      t.text :hint

      t.integer :sorting_order, default: 0, null: false

      t.timestamps
    end
  end
end
