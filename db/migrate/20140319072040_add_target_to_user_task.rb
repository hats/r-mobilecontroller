class AddTargetToUserTask < ActiveRecord::Migration
  def change
    add_reference :user_tasks, :target, index: true
  end
end
