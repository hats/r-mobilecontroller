class CreateVariants < ActiveRecord::Migration
  def change
    create_table :variants do |t|
      t.references :field, index: true, null: false
      t.string :label, null: false
      t.boolean :default, null: false, default: false
    end
  end
end
