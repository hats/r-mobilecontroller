class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|
      t.references :block
      t.string :name, null: false
      t.string :type, null: false
      t.boolean :is_global, default: false, null: false

      t.text :description
      t.text :hint

      t.integer :sorting_order, default: 0, null: false

      t.timestamps
    end
  end
end
