class AddWeightToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :weight, :integer, default: 0, null: false
  end
end
