class AddRadioAndSelectboxAnswersToUserTaskAnswers < ActiveRecord::Migration
  def change
    change_table :user_task_answers do |t|
      t.integer :radio_answer, index: true
      t.integer :selectbox_answer, index: true
    end
  end
end
