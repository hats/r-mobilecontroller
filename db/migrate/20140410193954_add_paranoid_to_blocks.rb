class AddParanoidToBlocks < ActiveRecord::Migration
  def change
    add_column :blocks, :deleted_at, :timestamp
  end
end
