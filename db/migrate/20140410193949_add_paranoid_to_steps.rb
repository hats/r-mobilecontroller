class AddParanoidToSteps < ActiveRecord::Migration
  def change
    add_column :steps, :deleted_at, :timestamp
  end
end
