class ChangeLoginToEmailInUsers < ActiveRecord::Migration
  def change
    remove_column :users, :login
    add_column :users, :email, :string, null: false
  end
end
