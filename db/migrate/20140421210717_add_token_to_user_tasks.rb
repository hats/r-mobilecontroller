class AddTokenToUserTasks < ActiveRecord::Migration
  def change
    add_column :user_tasks, :token, :string
  end
end
